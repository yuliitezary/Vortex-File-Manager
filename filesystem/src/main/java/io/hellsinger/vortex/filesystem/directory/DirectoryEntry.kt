package io.hellsinger.vortex.filesystem.directory

import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.path.PathOwner

interface DirectoryEntry<P : Path> : PathOwner<P> {
    override val path: P
}