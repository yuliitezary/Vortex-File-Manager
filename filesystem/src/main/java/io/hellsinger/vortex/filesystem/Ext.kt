package io.hellsinger.vortex.filesystem

import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.path.PathOwner

fun <P : Path> P.checkScheme(scheme: Int) {
    check(this.scheme == scheme) { "Invalid scheme ${this.scheme}" }
}

fun <P : Path> P.checkScheme(scheme: Int, lazyMessage: () -> Any) {
    check(
        value = this.scheme == scheme,
        lazyMessage = lazyMessage
    )
}

fun <P : Path> PathOwner<P>.checkScheme(scheme: Int) {
    path.checkScheme(scheme)
}

fun <P : Path> PathOwner<P>.checkScheme(scheme: Int, lazyMessage: () -> Any) {
    path.checkScheme(scheme, lazyMessage)
}