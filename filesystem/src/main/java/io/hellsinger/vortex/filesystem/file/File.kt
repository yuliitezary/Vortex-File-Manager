package io.hellsinger.vortex.filesystem.file

import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.path.PathOwner
import io.hellsinger.vortex.filesystem.size.Size
import io.hellsinger.vortex.filesystem.size.SizeOwner

interface File<P : Path> : PathOwner<P>, SizeOwner {

    override val path: P

    val name: String
        get() = path.getNameAt().toString()

    override val size: Size

}