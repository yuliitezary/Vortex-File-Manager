package io.hellsinger.vortex.filesystem.directory

import io.hellsinger.vortex.filesystem.path.Path
import kotlinx.coroutines.flow.Flow

abstract class DirectoryWrapper<P : Path>(
    override val path: P,
) : Directory<P> {

    abstract override fun walk(walker: DirectoryWalker): Flow<DirectoryEntry<P>>

}