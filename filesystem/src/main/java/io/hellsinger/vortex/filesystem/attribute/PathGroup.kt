package io.hellsinger.vortex.filesystem.attribute

interface PathGroup {
    val id: Int
    val name: ByteArray
}