package io.hellsinger.vortex.filesystem.attribute.mimetype

import io.hellsinger.vortex.filesystem.path.Path

/**
 * Should scan file content to define mimetype
 */
interface MimeTypeDescriptor {

    /**
     *  @return: path extension
     * */
    fun describe(path: Path): MimeType

}