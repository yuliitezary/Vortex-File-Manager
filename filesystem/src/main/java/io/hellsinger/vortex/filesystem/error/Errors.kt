package io.hellsinger.vortex.filesystem.error

import io.hellsinger.vortex.filesystem.path.Path

fun PathDoesNotExist(path: Path): Nothing = throw Throwable("$path does not exist")

fun FileOpenError(path: Path): Nothing = throw Throwable("Couldn't open $path")