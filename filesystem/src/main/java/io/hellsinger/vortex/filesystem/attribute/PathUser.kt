package io.hellsinger.vortex.filesystem.attribute

interface PathUser {
    val id: Int
    val name: ByteArray
}