package io.hellsinger.vortex.filesystem.size

import io.hellsinger.vortex.filesystem.size.Size.Companion.Bit
import io.hellsinger.vortex.filesystem.size.Size.Companion.EiB
import io.hellsinger.vortex.filesystem.size.Size.Companion.GiB
import io.hellsinger.vortex.filesystem.size.Size.Companion.KiB
import io.hellsinger.vortex.filesystem.size.Size.Companion.MiB
import io.hellsinger.vortex.filesystem.size.Size.Companion.PiB
import io.hellsinger.vortex.filesystem.size.Size.Companion.TiB
import io.hellsinger.vortex.filesystem.size.Size.Companion.YiB
import io.hellsinger.vortex.filesystem.size.Size.Companion.ZiB

@Suppress("FunctionName")
fun SiSize(
    original: Long,
): Size = object : SizeWrapper(original) {
    override fun onCreateSize(original: Long): Size = SiSize(original)
    override val delimiter: Int = 1000
    override fun onCreateTypeTitle(type: Int) = when (type) {
        Bit -> "B"
        KiB -> "KiB"
        MiB -> "MiB"
        GiB -> "GiB"
        TiB -> "TiB"
        PiB -> "PiB"
        EiB -> "EiB"
        ZiB -> "ZiB"
        YiB -> "YiB"
        else -> throw UnsupportedOperationException("Unsupported type of size")
    }
}