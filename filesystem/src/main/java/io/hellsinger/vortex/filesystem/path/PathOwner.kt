package io.hellsinger.vortex.filesystem.path

interface PathOwner<P : Path> {
    val path: P
}