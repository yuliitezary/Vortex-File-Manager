package io.hellsinger.vortex.filesystem.directory

import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.path.PathOwner
import kotlinx.coroutines.flow.Flow

interface Directory<P : Path> : PathOwner<P> {

    override val path: P

    fun walk(walker: DirectoryWalker): Flow<DirectoryEntry<P>>

}