package io.hellsinger.vortex.filesystem.attribute

import io.hellsinger.vortex.filesystem.attribute.time.PathTime

const val PATH_ATTR_TYPE_FILE = 0
const val PATH_ATTR_TYPE_DIRECTORY = 1
const val PATH_ATTR_TYPE_LINK = 2
const val PATH_ATTR_TYPE_PIPE = 3
const val PATH_ATTR_TYPE_SOCKET = 4
const val PATH_ATTR_TYPE_BLOCK_DEVICE = 5
const val PATH_ATTR_TYPE_CHAR_DEVICE = 6

interface PathAttribute {

    object EmptyPathAttribute : PathAttribute {
        override val id: Long = -1
        override val type: Int = -1
        override val size: Long = -1
        override val creationTime: PathTime = PathTime(-1, -1)
        override val lastAccessTime: PathTime = PathTime(-1, -1)
        override val lastModifiedTime: PathTime = PathTime(-1, -1)
        override val permission: String = ""
    }

    val id: Long

    val type: Int

    val size: Long

    val creationTime: PathTime

    val lastAccessTime: PathTime

    val lastModifiedTime: PathTime

    val permission: String
}

inline val PathAttribute.isFile
    get() = type == PATH_ATTR_TYPE_FILE

inline val PathAttribute.isDirectory
    get() = type == PATH_ATTR_TYPE_DIRECTORY

inline val PathAttribute.isLink
    get() = type == PATH_ATTR_TYPE_LINK

inline val PathAttribute.isPipe
    get() = type == PATH_ATTR_TYPE_PIPE

inline val PathAttribute.isSocket
    get() = type == PATH_ATTR_TYPE_SOCKET

inline val PathAttribute.isBlockDevice
    get() = type == PATH_ATTR_TYPE_BLOCK_DEVICE

inline val PathAttribute.isCharDevice
    get() = type == PATH_ATTR_TYPE_CHAR_DEVICE