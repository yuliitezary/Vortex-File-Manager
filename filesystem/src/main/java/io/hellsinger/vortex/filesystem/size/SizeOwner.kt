package io.hellsinger.vortex.filesystem.size

interface SizeOwner {
    val size: Size
}