package io.hellsinger.theme.vortex

class VortexThemeColors internal constructor(private val theme: VortexTheme) {
    // Bar
    val barSurfaceColorKey = createKey("barSurfaceColor")
    val barNavigationIconTintColorKey = createKey("barNavigationIconTintColor")
    val barActionIconTintColorKey = createKey("barActionIconTintColor")
    val barActionIconRippleColorKey = createKey("barActionIconRippleColor")
    val barTitleTextColorKey = createKey("barTitleTextColor")
    val barSubtitleTextColorKey = createKey("barSubtitleTextColor")

    // Bar (contextual)
    val barSurfaceContextualColorKey = createKey("barSurfaceContextualColor")
    val barNavigationIconContextualTintColorKey = createKey("barNavigationIconContextualTintColor")
    val barActionIconContextualTintColorKey = createKey("barActionIconContextualTintColor")
    val barTitleContextualTextColorKey = createKey("barTitleContextualTextColor")

    // Drawer
    val drawerTitleColorKey = createKey("drawerTitleColor")
    val drawerBackgroundColorKey = createKey("drawerBackgroundColor")
    val drawerSurfaceColorKey = createKey("drawerSurfaceColor")
    val drawerDividerColorKey = createKey("drawerDividerColor")

    val drawerItemBackgroundColorKey = createKey("drawerItemBackgroundColor")
    val drawerItemBackgroundRippleColorKey = createKey("drawerItemBackgroundRippleColor")
    val drawerItemIconTintColorKey = createKey("drawerItemIconTintColor")
    val drawerItemTitleTextColorKey = createKey("drawerItemTitleTextColor")

    // Drawer (selected)
    val drawerItemSelectedBackgroundColorKey = createKey("drawerItemSelectedBackgroundColor")
    val drawerItemIconSelectedTintColorKey = createKey("drawerItemIconSelectedTintColor")
    val drawerItemTitleSelectedTextColorKey = createKey("drawerItemTitleSelectedTextColor")

    // Storage List Screen
    val storageListBackgroundColorKey = createKey("storageListBackgroundColor")

    // Storage List Screen : Trail
    val trailSurfaceColorKey = createKey("trailSurfaceColor")
    val trailItemTitleTextColorKey = createKey("trailItemTitleTextColor")
    val trailItemArrowTintColorKey = createKey("trailItemArrowTintColor")
    val trailItemRippleTintColorKey = createKey("trailItemRippleTintColor")

    // Storage List Screen : Trail (selected)
    val trailItemTitleSelectedTextColorKey = createKey("trailItemTitleSelectedTextColor")
    val trailItemArrowSelectedTintColorKey = createKey("trailItemArrowSelectedTintColor")
    val trailItemRippleSelectedTintColorKey = createKey("trailItemRippleSelectedTintColor")

    val storageCreateCheckBoxTintColorKey = createKey("storageCreateCheckBoxTintColor")

    // Storage List Screen : storageList Item
    val storageListItemSurfaceColorKey = createKey("storageListItemSurfaceColor")
    val storageListItemSurfaceRippleColorKey = createKey("storageListItemSurfaceRippleColor")
    val storageListItemTitleTextColorKey = createKey("storageListItemTitleTextColor")
    val storageListItemSecondaryTextColorKey = createKey("storageListItemSecondaryTextColor")
    val storageListItemIndexTextColorKey = createKey("storageListItemIndexTextColor")
    val storageListItemIconTintColorKey = createKey("storageListItemIconTintColor")
    val storageListItemIconBackgroundColorKey = createKey("storageListItemIconBackgroundColor")

    // Storage List Screen : StorageList Item (selected)
    val storageListItemIconSelectedTintColorKey = createKey("storageListItemIconSelectedTintColor")

    // Storage List Screen : SnackBar
    val storageSnackBarSurfaceColorKey = createKey("storageSnackBarSurfaceColor")
    val storageSnackBarUnitTextColorKey = createKey("storageSnackUnitTextColor")
    val storageSnackBarTitleTextColorKey = createKey("storageSnackTitleTextColor")
    val storageSnackBarIndicatorColorKey = createKey("storageSnackIndicatorColor")
    val storageSnackBarTrackColorKey = createKey("storageSnackTrackColor")

    // Layouts : Info
    val layoutInfoBackgroundColorKey = createKey("layoutInfoBackgroundColor")
    val layoutInfoIconTintColorKey = createKey("layoutInfoWarningIconTintColor")
    val layoutInfoTitleTextColorKey = createKey("layoutInfoTitleTextColor")
    val layoutInfoActionContentColorKey = createKey("layoutInfoActionContentColor")

    // Layouts : Progress
    val layoutProgressBarBackgroundColorKey = createKey("layoutProgressBarBackgroundColorKey")
    val layoutProgressBarTintColorKey = createKey("layoutProgressBarTintColor")
    val layoutProgressTitleTextColorKey = createKey("layoutProgressTitleTextColor")
    val layoutProgressActionTintColorKey = createKey("layoutProgressActionTintColor")

    fun createKey(key: String) = VortexTheme.createKey<Int>(key)
}