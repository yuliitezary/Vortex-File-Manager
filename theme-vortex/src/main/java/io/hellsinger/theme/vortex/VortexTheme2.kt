package io.hellsinger.theme.vortex

// Memory optimized version
interface VortexTheme2 {
    // Only colors, dimens predetermined, texts as well
    val primary: Int
    val onPrimary: Int
    val secondary: Int
    val onSecondary: Int
    val variant: Int
    val onVariant: Int
    val surface: Int
    val onSurface: Int
    val error: Int
    val onError: Int
    val background: Int
    val onBackground: Int
}