package io.hellsinger.theme.vortex

class VortexThemeTexts internal constructor(private val theme: VortexTheme) {

    val navigationVortexFileManagerActionTitleKey = createKey(
        "navigationVortexFileManagerActionTitle"
    )
    val navigationVortexBookmarksActionTitleKey = createKey("navigationVortexBookmarksActionTitle")
    val navigationPluginGroupTitleKey = createKey("navigationPluginGroupTitleKey")
    val navigationPluginManagerActionTitleKey = createKey("navigationPluginManagerActionTitle")

    val storageListItemMimeTypeApplicationKey = createKey("storageListItemMimeTypeApplication")
    val storageListItemMimeTypeImageKey = createKey("storageListItemMimeTypeImage")
    val storageListItemMimeTypeVideoKey = createKey("storageListItemMimeTypeVideo")
    val storageListItemMimeTypeAudioKey = createKey("storageListItemMimeTypeAudio")
    val storageListItemMimeTypeTextKey = createKey("storageListItemMimeTypeText")

    // Item : Size
    val storageListItemSizeBKey = createKey("storageListItemSizeB")
    val storageListItemSizeKiBKey = createKey("storageListItemSizeKiB")
    val storageListItemSizeMiBKey = createKey("storageListItemSizeMiB")
    val storageListItemSizeGiBKey = createKey("storageListItemSizeGiB")
    val storageListItemSizeTiBKey = createKey("storageListItemSizeTiB")
    val storageListItemSizePiBKey = createKey("storageListItemSizePiB")
    val storageListItemSizeEiBKey = createKey("storageListItemSizeEiB")
    val storageListItemSizeZiBKey = createKey("storageListItemSizeZiB")
    val storageListItemSizeYiBKey = createKey("storageListItemSizeYiB")

    // Item
    val storageListItemNameKey = createKey("storageListItemName")
    val storageListItemSizeKey = createKey("storageListItemSize")
    val storageListItemInfoSeparatorKey = createKey("storageListItemInfoSeparator")

    // Item : Directory Content
    val storageListItemsCountKey = createKey("storageListItemsCount")
    val storageListItemCountKey = createKey("storageListItemCount")
    val storageListItemEmptyKey = createKey("storageListItemEmpty")


    val storageListLoadingInitiatingTitleKey = createKey("storageListLoadingInitiatingTitle")
    val storageListLoadingNavigatingTitleKey = createKey("storageListLoadingNavigatingTitle")

    // Warning
    val storageListWarningEmptyTitleKey = createKey("storageListWarningEmptyTitle")
    val storageListWarningRestrictedTitleKey = createKey("storageListWarningRestrictedTitle")

    // Warning : Permission
    val storageListWarningFullStorageAccessTitleKey =
        createKey("fileListWarningFullStorageAccessTitle")
    val storageListWarningStorageAccessTitleKey = createKey("storageListWarningStorageAccessTitle")
    val storageListWarningNotificationAccessTitleKey =
        createKey("storageListWarningNotificationAccessTitle")

    // Warning : Action
    val storageListWarningFullStorageAccessActionTitleKey =
        createKey("storageListWarningFullStorageAccessActionTitle")
    val storageListWarningStorageAccessActionTitleKey =
        createKey("storageListWarningStorageAccessActionTitle")
    val storageListWarningNotificationAccessActionTitleKey =
        createKey("storageListWarningNotificationAccessActionTitle")

    // Bar
    val navigationSearchActionTitleKey = createKey("navigationSearchActionTitle")
    val navigationMoreActionTitleKey = createKey("navigationMoreActionTitle")

    // Storage list : Create dialog
    val storageListCreateDialogNameHintTitleKey = createKey("storageListCreateDialogNameHintTitle")
    val storageListCreateDialogPathHintTitleKey = createKey("storageListCreateDialogPathHintTitle")

    // Storage list : Drawer Groups
    val storageListGroupViewActionTitleKey = createKey("storageListGroupViewActionTitle")
    val storageListGroupOrderActionTitleKey = createKey("storageListGroupOrderActionTitleKey")
    val storageListGroupSortActionTitleKey = createKey("storageListGroupSortActionTitle")
    val storageListGroupFilterActionTitleKey = createKey("storageListGroupFilterActionTitle")

    // Storage list : Drawer Groups : View
    val storageListViewListActionTitleKey = createKey("storageListViewListActionTitle")
    val storageListViewGridActionTitleKey = createKey("storageListViewGridActionTitle")

    // Storage list : Drawer Groups : Order
    val storageListOrderAscendingActionTitleKey = createKey("storageListOrderAscendingActionTitle")
    val storageListOrderDescendingActionTitleKey =
        createKey("storageListOrderDescendingActionTitles")

    // Storage list : Drawer Groups : Sort
    val storageListSortNameActionTitleKey = createKey("storageListSortNameActionTitle")
    val storageListSortPathActionTitleKey = createKey("storageListSortPathActionTitle")
    val storageListSortSizeActionTitleKey = createKey("storageListSortSizeActionTitle")
    val storageListSortLastModifiedTimeActionTitleKey =
        createKey("storageListSortLastModifiedTimeActionTitle")
    val storageListSortLastAccessTimeActionTitleKey =
        createKey("storageListSortLastAccessTimeActionTitle")
    val storageListSortCreationTimeActionTitleKey =
        createKey("storageListSortCreationTimeActionTitle")

    // Storage list : Drawer Groups : Filter
    val storageListFilterOnlyFilesActionTitleKey =
        createKey("storageListFilterOnlyFilesActionTitle")
    val storageListFilterOnlyFoldersActionTitleKey =
        createKey("storageListFilterOnlyFoldersActionTitle")
    val storageListFilterOnlyApplicationFileActionTitleKey =
        createKey("storageListFilterOnlyApplicationFileActionTitle")
    val storageListFilterOnlyAudioFileActionTitleKey =
        createKey("storageListFilterOnlyAudioFileActionTitle")
    val fileListFilterOnlyImageFileActionTitleKey =
        createKey("storageListFilterOnlyImageFileActionTitle")
    val fileListFilterOnlyVideoFileActionTitleKey =
        createKey("storageListFilterOnlyVideoFileActionTitle")
    val fileListFilterOnlyTextFileActionTitleKey =
        createKey("storageListFilterOnlyTextFileActionTitle")

    // Storage list : Drawer Groups : More
    val storageListGroupMoreActionTitleKey = createKey("storageListGroupMoreActionTitle")

    val storageListMoreSelectAllActionTitleKey = createKey("storageListMoreSelectAllActionTitle")
    val storageListMoreDeselectAllActionTitleKey =
        createKey("storageListMoreDeselectAllActionTitle")
    val storageListMoreInfoActionTitleKey = createKey("storageListMoreInfoActionTitle")
    val storageListMoreNavigateLeftActionTitleKey =
        createKey("storageListMoreNavigateLeftActionTitle")
    val storageListMoreNavigateRightActionTitleKey =
        createKey("storageListMoreNavigateRightActionTitle")

    // List
    val storageListFilesCountTitleKey = createKey("storageListFilesCountTitle")
    val storageListDirectoriesCountTitleKey = createKey("storageListDirectoriesCountTitle")
    val storageListSelectionTitleKey = createKey("storageListSelectionTitle")

    val storageListFilesCountSectionKey = createKey("storageListFilesCountSection")
    val storageListLinksCountSectionKey = createKey("storageListLinksCountSection")

    val storageListDirectoriesCountSectionKey = createKey("storageListDirectoriesCountSection")

    // Operation
    val storageListOperationDeleteItemTitleKey = createKey("storageListOperationDeleteItemTitleKey")
    val storageListOperationDeleteItemPerformedTitleKey =
        createKey("storageListOperationDeleteItemPerformedTitle")

    val storageListGroupOperationDefaultActionTitleKey =
        createKey("fileListGroupOperationDefaultActionTitle")

    val storageListOperationAddBookmarkTitleKey = createKey("storageListOperationAddBookmarkTitle")
    val storageListOperationRemoveBookmarkTitleKey =
        createKey("storageListOperationRemoveBookmarkTitle")

    val storageListOperationOpenTitleKey = createKey("storageListOperationOpenTitle")
    val storageListOperationCopyPathTitleKey = createKey("storageListOperationCopyPathTitle")
    val storageListOperationSwapNamesActionTitleKey =
        createKey("storageListOperationSwapNamesActionTitle")
    val storageListOperationDeleteActionTitleKey =
        createKey("storageListOperationDeleteActionTitle")
    val storageListOperationRenameActionTitleKey =
        createKey("storageListOperationRenameActionTitle")
    val storageListOperationCopyActionTitleKey = createKey("storageListOperationCopyActionTitle")
    val storageListOperationMoveActionTitleKey = createKey("storageListOperationMoveActionTitle")
    val storageListOperationAddActionNewTitleKey =
        createKey("storageListOperationAddActionNewTitle")

    // Vortex Service
    val vortexServiceConnectedKey = createKey("vortexServiceConnected")
    val vortexServiceDisconnectedKey = createKey("vortexServiceDisconnected")

    val vortexServiceDeleteTransactionPathDeleteTitleKey =
        createKey("vortexServiceDeleteTransactionPathDeleteTitle")
    val vortexServiceCreateTransactionPathCreateTitleKey =
        createKey("vortexServiceCreateTransactionPathCreateTitle")

    fun createKey(key: String) = VortexTheme.createKey<String>(key)
}