package io.hellsinger.theme.vortex

import io.hellsinger.theme.Theme
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

object VortexTheme : Theme() {
    val texts = VortexThemeTexts(theme = this)
    val colors = VortexThemeColors(theme = this)
    val dimens = VortexThemeDimens(theme = this)
    val behaviors = VortexThemeBehaviors(theme = this)

    private val scope = CoroutineScope(CoroutineName("VortexThemeUpdateScope") + Main.immediate)

    private val listeners = mutableListOf<Listener>()

    private val values = mutableMapOf<Key<*>, Any?>()

    operator fun <T> set(key: Key<T>, value: T) {
        values[key] = value
    }

    fun <T> update(key: Key<T>, value: T) {
        val current = values[key]
        if (current == value) return

        set(key, value)
        onUpdate(key, value)
    }

    operator fun <T> get(key: Key<T>): T? = values[key] as T?

    fun addThemeListener(listener: Listener) {
        listeners.add(listener)
    }

    fun removeThemeListener(listener: Listener) {
        listeners.remove(listener)
    }

    private fun onUpdate(key: Key<*>, value: Any?) {
        scope.launch {
            listeners.forEach { listener ->
                listener.onKeyUpdate(key, value)
            }
        }
    }

    fun <T> get(key: Key<T>, default: T): T {
        val current = values[key] as T?
        if (current == null) {
            set(key, default)
            return default
        }
        return current
    }

    override fun <T> createKey(alias: String): Key<T> = object : Key<T> {
        override fun set(value: T) {
            this@VortexTheme[this] = value
        }

        override fun get(): T =
            this@VortexTheme[this] ?: throw IllegalArgumentException("Value is not present")

        override fun update(value: T) = update(this, value)

        override fun toString(): String = alias
    }

}

operator fun VortexTheme.set(key: Theme.Key<Int>, value: Long) = set(key, value.toInt())