package io.hellsinger.theme.vortex

import io.hellsinger.theme.Colors

const val integerSpecifier = "%d"
const val stringSpecifier = "%s"

fun InitVortexTheme(isDarkMode: Boolean, theme: VortexTheme = VortexTheme) {
    InitBehaviors(theme)
    InitDimens(theme)
    InitServiceTexts(theme)
    InitTexts(theme)
    if (isDarkMode) InitDarkColors(theme) else InitLightColors(theme)
}

private fun InitBehaviors(theme: VortexTheme = VortexTheme) {
    val behaviors = VortexTheme.behaviors
    theme[behaviors.maxPathLengthKey] = 4096
}

private fun InitDimens(theme: VortexTheme = VortexTheme) {
    val dimens = VortexTheme.dimens

    theme[dimens.navigationBarWidthKey] = -1
    theme[dimens.navigationBarHeightKey] = 56

    theme[dimens.trailWidthKey] = -1
    theme[dimens.trailHeightKey] = 40

    theme[dimens.trailItemHeightKey] = 40
    theme[dimens.trailItemLeftPaddingKey] = 8
    theme[dimens.trailItemRightPaddingKey] = 8

    theme[dimens.trailElevationKey] = 4F

    theme[dimens.drawerItemWidthKey] = -1
    theme[dimens.drawerItemHeightKey] = 48

    theme[dimens.storageListLoadingWidthKey] = -1
    theme[dimens.storageListLoadingHeightKey] = 48

    theme[dimens.storageListItemLinearWidthDimenKey] = -1
    theme[dimens.storageListItemLinearHeightDimenKey] = 56
    theme[dimens.storageListItemHorizontalTitlePaddingKey] = 24
    theme[dimens.storageListItemHorizontalSubtitlePaddingKey] = 24

    theme[dimens.storageListSnackBarElevationKey] = 4F

    theme[dimens.storageListItemSurfaceSelectedElevationKey] = 8
    theme[dimens.storageListItemSurfaceElevationKey] = 2
}

private fun InitTexts(theme: VortexTheme = VortexTheme) {
    val texts = VortexTheme.texts

    theme[texts.navigationVortexFileManagerActionTitleKey] = "File manager"
    theme[texts.navigationVortexBookmarksActionTitleKey] = "Bookmarks"

    theme[texts.navigationPluginGroupTitleKey] = "Plugin"
    theme[texts.navigationPluginManagerActionTitleKey] = "Plugin Manager"

    theme[texts.storageListItemMimeTypeApplicationKey] = "Application"
    theme[texts.storageListItemMimeTypeImageKey] = "Image"
    theme[texts.storageListItemMimeTypeVideoKey] = "Video"
    theme[texts.storageListItemMimeTypeAudioKey] = "Audio"
    theme[texts.storageListItemMimeTypeTextKey] = "Text"

    // Item : Size
    theme[texts.storageListItemSizeBKey] = "B"
    theme[texts.storageListItemSizeKiBKey] = "KB"
    theme[texts.storageListItemSizeMiBKey] = "MB"
    theme[texts.storageListItemSizeGiBKey] = "GB"
    theme[texts.storageListItemSizeTiBKey] = "TB"
    theme[texts.storageListItemSizePiBKey] = "PB"
    theme[texts.storageListItemSizeEiBKey] = "EB"
    theme[texts.storageListItemSizeZiBKey] = "ZB"
    theme[texts.storageListItemSizeYiBKey] = "YB"

    // Item
    theme[texts.storageListItemNameKey] = stringSpecifier
    theme[texts.storageListItemSizeKey] = stringSpecifier
    theme[texts.storageListItemInfoSeparatorKey] = " | "

    // Item : Directory Content
    theme[texts.storageListItemsCountKey] = "$integerSpecifier items"
    theme[texts.storageListItemCountKey] = "One item"
    theme[texts.storageListItemEmptyKey] = "Empty"

    // Storage list : Create dialog
    theme[texts.storageListCreateDialogNameHintTitleKey] = "Enter name"
    theme[texts.storageListCreateDialogPathHintTitleKey] = "Enter path"

    // Storage list : Drawer Groups
    theme[texts.storageListGroupViewActionTitleKey] = "View"
    theme[texts.storageListGroupOrderActionTitleKey] = "Order"
    theme[texts.storageListGroupSortActionTitleKey] = "Sort"
    theme[texts.storageListGroupFilterActionTitleKey] = "Filter"

    // Storage list : Drawer Groups : View
    theme[texts.storageListViewListActionTitleKey] = "Column"
    theme[texts.storageListViewGridActionTitleKey] = "Grid"

    // Storage list : Drawer Groups : View
    theme[texts.storageListOrderAscendingActionTitleKey] = "Ascending"
    theme[texts.storageListOrderDescendingActionTitleKey] = "Descending"

    // Storage list : Drawer Groups : Sort
    theme[texts.storageListSortNameActionTitleKey] = "Name"
    theme[texts.storageListSortPathActionTitleKey] = "Path"
    theme[texts.storageListSortSizeActionTitleKey] = "Size"
    theme[texts.storageListSortLastModifiedTimeActionTitleKey] = "Last modified time"
    theme[texts.storageListSortLastAccessTimeActionTitleKey] = "Last access time"
    theme[texts.storageListSortCreationTimeActionTitleKey] = "Creation time"

    theme[texts.storageListOperationOpenTitleKey] = "Open"
    theme[texts.storageListOperationCopyPathTitleKey] = "Copy path"
    theme[texts.storageListOperationSwapNamesActionTitleKey] = "Swap names"
    theme[texts.storageListOperationDeleteActionTitleKey] = "Delete"
    theme[texts.storageListOperationRenameActionTitleKey] = "Rename"
    theme[texts.storageListOperationCopyActionTitleKey] = "Copy"
    theme[texts.storageListOperationMoveActionTitleKey] = "Move"
    theme[texts.storageListOperationAddActionNewTitleKey] = "Add new"

    theme[texts.storageListFilterOnlyFoldersActionTitleKey] = "Only folders"
    theme[texts.storageListFilterOnlyFilesActionTitleKey] = "Only files"
    theme[texts.storageListFilterOnlyApplicationFileActionTitleKey] = "Only application files"
    theme[texts.storageListFilterOnlyAudioFileActionTitleKey] = "Only audio files"
    theme[texts.fileListFilterOnlyImageFileActionTitleKey] = "Only image files"
    theme[texts.fileListFilterOnlyVideoFileActionTitleKey] = "Only video files"
    theme[texts.fileListFilterOnlyTextFileActionTitleKey] = "Only text \uD83D\uDC7F files"

    theme[texts.storageListLoadingInitiatingTitleKey] = "Initiating..."
    theme[texts.storageListLoadingNavigatingTitleKey] = "Navigating to $stringSpecifier"

    // Warning
    theme[texts.storageListWarningEmptyTitleKey] = "$stringSpecifier is empty"
    theme[texts.storageListWarningRestrictedTitleKey] = "Restricted directory"

    // Warning : Permission
    theme[texts.storageListWarningFullStorageAccessTitleKey] =
        "App requires full storage access.\n" +
                "Full storage access gives app full control of your file-system storage.\n" +
                "App guaranties your files can't be damaged.\n" +
                "Remember: Plugins can disallow this rule."
    theme[texts.storageListWarningStorageAccessTitleKey] = "App requires storage access.\n" +
            "Storage access gives app full control of your file-system storage.\n" +
            "App guaranties your files can't be damaged.\n" +
            "Remember: Plugins can disallow this rule.\n"

    theme[texts.storageListWarningNotificationAccessTitleKey] = "App requires"

    // Warning : Action
    theme[texts.storageListWarningFullStorageAccessActionTitleKey] = "Provide full storage access"
    theme[texts.storageListWarningStorageAccessActionTitleKey] = "Provide storage access"
    theme[texts.storageListWarningNotificationAccessActionTitleKey] = "Provide notifications access"

    // Bar
    theme[texts.navigationSearchActionTitleKey] = "Search"
    theme[texts.navigationMoreActionTitleKey] = "More"

    // More
    theme[texts.storageListGroupMoreActionTitleKey] = "Default ($stringSpecifier)"

    theme[texts.storageListMoreSelectAllActionTitleKey] = "Select all"
    theme[texts.storageListMoreDeselectAllActionTitleKey] = "Deselect all"
    theme[texts.storageListMoreInfoActionTitleKey] = "Info"
    theme[texts.storageListMoreNavigateLeftActionTitleKey] = "Navigate left"
    theme[texts.storageListMoreNavigateRightActionTitleKey] = "Navigate right"

    theme[texts.storageListFilesCountTitleKey] = "Files: $integerSpecifier"
    theme[texts.storageListDirectoriesCountTitleKey] = "Folders: $integerSpecifier"
    theme[texts.storageListSelectionTitleKey] = "Selected $integerSpecifier"

    theme[texts.storageListFilesCountSectionKey] = "Files ($integerSpecifier)"
    theme[texts.storageListLinksCountSectionKey] = "Links ($integerSpecifier)"
    theme[texts.storageListDirectoriesCountSectionKey] = "Folders ($integerSpecifier)"

    // Operation
    theme[texts.storageListOperationDeleteItemTitleKey] = "Deleting $stringSpecifier"
    theme[texts.storageListOperationDeleteItemPerformedTitleKey] = "Deletion performed"

    theme[texts.storageListGroupOperationDefaultActionTitleKey] = "Default"

    theme[texts.storageListOperationAddBookmarkTitleKey] = "Add to bookmarks"
    theme[texts.storageListOperationRemoveBookmarkTitleKey] = "Remove from bookmarks"
}

private fun InitServiceTexts(theme: VortexTheme = VortexTheme) {
    val texts = VortexTheme.texts

    theme[texts.vortexServiceConnectedKey] = "Vortex Service is connected!"
    theme[texts.vortexServiceDisconnectedKey] = "Vortex Service is disconnected!"

    theme[texts.vortexServiceDeleteTransactionPathDeleteTitleKey] = "$stringSpecifier deleted"
    theme[texts.vortexServiceCreateTransactionPathCreateTitleKey] =
        "$stringSpecifier created at $stringSpecifier"
}

private fun InitLightColors(theme: VortexTheme = VortexTheme) {
    val colors = VortexTheme.colors

    theme[colors.barSurfaceColorKey] = Colors.White
    theme[colors.barNavigationIconTintColorKey] = Colors.Black
    theme[colors.barActionIconTintColorKey] = Colors.Black
    theme[colors.barActionIconRippleColorKey] = 0x32000000
    theme[colors.barTitleTextColorKey] = Colors.Black
    theme[colors.barSubtitleTextColorKey] = Colors.Gray

    theme[colors.barSurfaceContextualColorKey] = 0xFF212121
    theme[colors.barActionIconContextualTintColorKey] = Colors.White
    theme[colors.barNavigationIconContextualTintColorKey] = Colors.White
    theme[colors.barTitleContextualTextColorKey] = Colors.White

    theme[colors.drawerTitleColorKey] = Colors.Gray
    theme[colors.drawerBackgroundColorKey] = Colors.White
    theme[colors.drawerSurfaceColorKey] = Colors.White
    theme[colors.drawerDividerColorKey] = Colors.DarkGray

    theme[colors.drawerItemBackgroundColorKey] = Colors.White
    theme[colors.drawerItemBackgroundRippleColorKey] = 0x32FFFFFF
    theme[colors.drawerItemIconTintColorKey] = Colors.Black
    theme[colors.drawerItemTitleTextColorKey] = Colors.Black

    theme[colors.drawerItemSelectedBackgroundColorKey] = 0x4D3062FF
    theme[colors.drawerItemIconSelectedTintColorKey] = 0xFF3062FF
    theme[colors.drawerItemTitleSelectedTextColorKey] = 0xFF3062FF

    theme[colors.storageListBackgroundColorKey] = 0xFFEEEEEE

    theme[colors.trailSurfaceColorKey] = Colors.White
    theme[colors.trailItemTitleTextColorKey] = Colors.Black
    theme[colors.trailItemArrowTintColorKey] = Colors.Black
    theme[colors.trailItemRippleTintColorKey] = 0x52000000

    theme[colors.trailItemTitleSelectedTextColorKey] = 0xFF3062FF
    theme[colors.trailItemArrowSelectedTintColorKey] = 0xFF3062FF
    theme[colors.trailItemRippleSelectedTintColorKey] = 0x523062FF

    theme[colors.storageCreateCheckBoxTintColorKey] = 0xFF3062FF

    theme[colors.layoutInfoBackgroundColorKey] = Colors.White
    theme[colors.layoutInfoIconTintColorKey] = Colors.Black
    theme[colors.layoutInfoTitleTextColorKey] = Colors.Black
    theme[colors.layoutInfoActionContentColorKey] = 0xFF3062FF

    theme[colors.storageListItemSurfaceColorKey] = Colors.White
    theme[colors.storageListItemSurfaceRippleColorKey] = 0x52000000
    theme[colors.storageListItemTitleTextColorKey] = Colors.Black
    theme[colors.storageListItemSecondaryTextColorKey] = Colors.DarkGray
    theme[colors.storageListItemIndexTextColorKey] = Colors.Gray
    theme[colors.storageListItemIconTintColorKey] = Colors.DarkGray
    theme[colors.storageListItemIconBackgroundColorKey] = 0x1F444444

    theme[colors.storageListItemIconSelectedTintColorKey] = 0xFF3062FF

    theme[colors.storageSnackBarSurfaceColorKey] = Colors.White
    theme[colors.storageSnackBarUnitTextColorKey] = Colors.Black
    theme[colors.storageSnackBarTitleTextColorKey] = Colors.Black
    theme[colors.storageSnackBarIndicatorColorKey] = 0xFF3062FF
    theme[colors.storageSnackBarTrackColorKey] = Colors.LightGray

    theme[colors.layoutProgressBarBackgroundColorKey] = Colors.White
    theme[colors.layoutProgressBarTintColorKey] = 0xFF3062FF
    theme[colors.layoutProgressTitleTextColorKey] = Colors.Black
    theme[colors.layoutProgressActionTintColorKey] = Colors.Black
}

private fun InitDarkColors(theme: VortexTheme = VortexTheme) {
    val colors = VortexTheme.colors

    theme[colors.barSurfaceColorKey] = 0xFF212121
    theme[colors.barActionIconTintColorKey] = Colors.White
    theme[colors.barActionIconTintColorKey] = Colors.White
    theme[colors.barActionIconRippleColorKey] = Colors.White
    theme[colors.barNavigationIconTintColorKey] = Colors.White
    theme[colors.barTitleTextColorKey] = Colors.White
    theme[colors.barSubtitleTextColorKey] = Colors.LightGray

    theme[colors.barSurfaceContextualColorKey] = 0xFF212121
    theme[colors.barActionIconContextualTintColorKey] = Colors.White
    theme[colors.barNavigationIconContextualTintColorKey] = Colors.White
    theme[colors.barTitleContextualTextColorKey] = Colors.White

    theme[colors.drawerTitleColorKey] = Colors.LightGray
    theme[colors.drawerBackgroundColorKey] = 0xFF212121
    theme[colors.drawerSurfaceColorKey] = 0xFF212121
    theme[colors.drawerDividerColorKey] = Colors.DarkGray

    theme[colors.drawerItemBackgroundColorKey] = 0xFF212121
    theme[colors.drawerItemBackgroundRippleColorKey] = 0x32FFFFFF
    theme[colors.drawerItemIconTintColorKey] = Colors.White
    theme[colors.drawerItemTitleTextColorKey] = Colors.White

    theme[colors.drawerItemSelectedBackgroundColorKey] = 0x4D3062FF
    theme[colors.drawerItemIconSelectedTintColorKey] = 0xFF3062FF
    theme[colors.drawerItemTitleSelectedTextColorKey] = 0xFF3062FF

    theme[colors.storageListBackgroundColorKey] = 0xFF121212

    theme[colors.trailSurfaceColorKey] = 0xFF212121
    theme[colors.trailItemTitleTextColorKey] = Colors.White
    theme[colors.trailItemArrowTintColorKey] = Colors.White
    theme[colors.trailItemRippleTintColorKey] = 0x32FFFFFF

    theme[colors.trailItemTitleSelectedTextColorKey] = 0xFF3062FF
    theme[colors.trailItemArrowSelectedTintColorKey] = 0xFF3062FF
    theme[colors.trailItemRippleSelectedTintColorKey] = 0x523062FF

    theme[colors.storageCreateCheckBoxTintColorKey] = 0xFF3062FF

    theme[colors.layoutInfoBackgroundColorKey] = 0xFF212121
    theme[colors.layoutInfoIconTintColorKey] = Colors.White
    theme[colors.layoutInfoTitleTextColorKey] = Colors.White
    theme[colors.layoutInfoActionContentColorKey] = 0xFF3062FF

    theme[colors.storageListItemSurfaceColorKey] = 0xFF212121
    theme[colors.storageListItemSurfaceRippleColorKey] = Colors.LightGray
    theme[colors.storageListItemTitleTextColorKey] = Colors.White
    theme[colors.storageListItemSecondaryTextColorKey] = Colors.LightGray
    theme[colors.storageListItemIndexTextColorKey] = Colors.Gray
    theme[colors.storageListItemIconTintColorKey] = Colors.White
    theme[colors.storageListItemIconBackgroundColorKey] = 0x32FFFFFF

    theme[colors.storageListItemIconSelectedTintColorKey] = 0xFF3062FF

    theme[colors.storageSnackBarSurfaceColorKey] = 0xFF212121
    theme[colors.storageSnackBarUnitTextColorKey] = Colors.White
    theme[colors.storageSnackBarTitleTextColorKey] = Colors.White
    theme[colors.storageSnackBarIndicatorColorKey] = 0xFF3062FF
    theme[colors.storageSnackBarTrackColorKey] = Colors.LightGray

    theme[colors.layoutProgressBarBackgroundColorKey] = 0xFF212121
    theme[colors.layoutProgressBarTintColorKey] = 0xFF3062FF
    theme[colors.layoutProgressTitleTextColorKey] = Colors.White
    theme[colors.layoutProgressActionTintColorKey] = Colors.White
}