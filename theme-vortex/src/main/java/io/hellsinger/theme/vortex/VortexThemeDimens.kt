package io.hellsinger.theme.vortex

class VortexThemeDimens internal constructor(private val theme: VortexTheme) {
    val navigationBarWidthKey = createKey("navigationBarWidth")
    val navigationBarHeightKey = createKey("navigationBarHeight")

    val trailWidthKey = createKey("trailWidth")
    val trailHeightKey = createKey("trailHeight")
    val trailElevationKey = createFloatKey("trailElevation")

    val trailItemHeightKey = createKey("trailItemHeight")
    val trailItemLeftPaddingKey = createKey("trailItemLeftPadding")
    val trailItemRightPaddingKey = createKey("trailItemRightPadding")

    val drawerItemWidthKey = createKey("drawerItemWidthKey")
    val drawerItemHeightKey = createKey("drawerItemHeightKey")

    val storageListLoadingWidthKey = createKey("storageListLoadingWidth")
    val storageListLoadingHeightKey = createKey("storageListLoadingHeight")

    val storageListItemLinearWidthDimenKey = createKey("storageListItemLinearWidthDimen")
    val storageListItemLinearHeightDimenKey = createKey("storageListItemLinearHeightDimen")
    val storageListItemHorizontalTitlePaddingKey = createKey(
        "storageListItemHorizontalTitlePadding"
    )
    val storageListItemHorizontalSubtitlePaddingKey = createKey(
        "storageListItemHorizontalSubtitlePadding"
    )

    val storageListSnackBarElevationKey = createFloatKey("storageListSnackBarElevation")

    val storageListItemSurfaceSelectedElevationKey = createKey(
        "storageListItemSurfaceSelectedElevation"
    )
    val storageListItemSurfaceElevationKey = createKey("storageListItemSurfaceElevation")

    fun createKey(key: String) = VortexTheme.createKey<Int>(key)
    fun createFloatKey(key: String) = VortexTheme.createKey<Float>(key)
}