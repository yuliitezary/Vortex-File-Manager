package io.hellsinger.theme.vortex

class VortexThemeBehaviors internal constructor(private val theme: VortexTheme) {
    val maxPathLengthKey = createKey<Int>("maxPathLength")

    fun <T> createKey(key: String) = VortexTheme.createKey<T>(key)
}