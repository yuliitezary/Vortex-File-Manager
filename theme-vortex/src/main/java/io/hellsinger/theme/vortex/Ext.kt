package io.hellsinger.theme.vortex

import io.hellsinger.theme.Theme

fun Formatter(
    key: Theme.Key<String>,
    vararg args: Any?
): String = key.get().format(args = args)