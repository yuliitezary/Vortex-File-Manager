package io.hellsinger.theme

import android.content.Context
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Open
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.filesystem.linux.operation.buffedOpen
import io.hellsinger.filesystem.linux.resolve
import io.hellsinger.filesystem.linux.toLinuxPath
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

// key1,key2,key3,...:value

class ThemeFile(
    private val context: Context,
    private val theme: Theme
) {

    private val root =
        context.filesDir.toLinuxPath().resolve("themes".encodeToByteArray()).asLinuxDirectory()

    suspend fun read(name: String, mode: Int = MODE_SET) = withContext(Dispatchers.IO) {
        val entry = root.path.resolve(name).asLinuxFile()
        val controller =
            entry.buffedOpen(flags = Open.NonBlocking or Open.ReadOnly, capacity = 2048)
        val builder = StringBuilder()
        var setMode = MODE_NONE
        if (mode == MODE_SET) {
            val content = ""

            when (content) {
                "color" -> {
                    setMode = MODE_COLOR
                }

                "text" -> {
                    setMode = MODE_TEXT
                }

                "dimen" -> {
                    setMode = MODE_DIMEN
                }

                else -> {
                    when (setMode) {
                        MODE_COLOR -> {
                            theme.createKey<Int>("").set(0)
                        }

                        MODE_TEXT -> {
                            theme.createKey<String>("").set("")
                        }

                        MODE_DIMEN -> {
                            theme.createKey<Int>("").set(0)
                        }
                    }
                }
            }


        } else {

        }

        controller.close()
    }

    suspend fun write(name: String) = withContext(Dispatchers.IO) {

    }

    companion object {
        private const val MODE_NONE = -1
        private const val MODE_COLOR = 0
        private const val MODE_TEXT = 1
        private const val MODE_DIMEN = 2

        // First read file then create theme
        const val MODE_SET = 0

        // Already theme loaded, we need update components after reading
        const val MODE_UPDATE = 1
    }

}