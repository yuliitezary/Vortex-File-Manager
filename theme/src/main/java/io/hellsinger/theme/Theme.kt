package io.hellsinger.theme

abstract class Theme {

    abstract fun <T> createKey(alias: String): Key<T>

    open fun getIdForName(name: String): Int = throw NotImplementedError()

    open fun getNameForId(id: Int): String = throw NotImplementedError()

    fun interface Listener {
        fun onKeyUpdate(key: Key<*>, value: Any?)
    }

    interface Key<T> {
        fun set(value: T)
        fun get(): T

        fun update(value: T)

        operator fun invoke(value: T) = set(value)
        operator fun invoke() = get()

        override fun toString(): String
    }
}