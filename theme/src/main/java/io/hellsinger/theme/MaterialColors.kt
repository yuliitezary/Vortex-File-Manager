package io.hellsinger.theme

interface AdditionalMaterialColor {
    val colorA100: Int
    val colorA200: Int
    val colorA400: Int
    val colorA700: Int
}

interface MaterialColor {
    val color50: Int
    val color100: Int
    val color200: Int
    val color300: Int
    val color400: Int
    val color500: Int
    val color600: Int
    val color700: Int
    val color800: Int
    val color900: Int
}

object MaterialColors {
    object Red : MaterialColor, AdditionalMaterialColor {
        override val color50: Int = 0xFFFFEBEE.toInt()
        override val color100: Int = 0xFFFFCDD2.toInt()
        override val color200: Int = 0xFFEF9A9A.toInt()
        override val color300: Int = 0xFFE57373.toInt()
        override val color400: Int = 0xFFEF5350.toInt()
        override val color500: Int = 0xFFF44336.toInt()
        override val color600: Int = 0xFFE53935.toInt()
        override val color700: Int = 0xFFD32F2F.toInt()
        override val color800: Int = 0xFFC62828.toInt()
        override val color900: Int = 0xFFB71C1C.toInt()
        override val colorA100: Int = 0xFFFF8A80.toInt()
        override val colorA200: Int = 0xFFFF5252.toInt()
        override val colorA400: Int = 0xFFFF1744.toInt()
        override val colorA700: Int = 0xFFD50000.toInt()
    }

    object Pink : MaterialColor, AdditionalMaterialColor {
        override val color50: Int = 0xFFFCE4EC.toInt()
        override val color100: Int = 0xFFF8BBD0.toInt()
        override val color200: Int = 0xFFF48FB1.toInt()
        override val color300: Int = 0xFFF06292.toInt()
        override val color400: Int = 0xFFEC407A.toInt()
        override val color500: Int = 0xFFE91E63.toInt()
        override val color600: Int = 0xFFD81B60.toInt()
        override val color700: Int = 0xFFC2185B.toInt()
        override val color800: Int = 0xFFAD1457.toInt()
        override val color900: Int = 0xFF880E4F.toInt()
        override val colorA100: Int = 0xFFFF80AB.toInt()
        override val colorA200: Int = 0xFFFF4081.toInt()
        override val colorA400: Int = 0xFFF50057.toInt()
        override val colorA700: Int = 0xFFC51162.toInt()
    }

    object Purple : MaterialColor, AdditionalMaterialColor {
        override val color50: Int = 0xFFF3E5F5.toInt()
        override val color100: Int = 0xFFE1BEE7.toInt()
        override val color200: Int = 0xFFCE93D8.toInt()
        override val color300: Int = 0xFFBA68C8.toInt()
        override val color400: Int = 0xFFAB47BC.toInt()
        override val color500: Int = 0xFF9C27B0.toInt()
        override val color600: Int = 0xFF8E24AA.toInt()
        override val color700: Int = 0xFF7B1FA2.toInt()
        override val color800: Int = 0xFF6A1B9A.toInt()
        override val color900: Int = 0xFF4A148C.toInt()
        override val colorA100: Int = 0xFFEA80FC.toInt()
        override val colorA200: Int = 0xFFE040FB.toInt()
        override val colorA400: Int = 0xFFD500F9.toInt()
        override val colorA700: Int = 0xFFAA00FF.toInt()
    }

    object DeepPurple : MaterialColor, AdditionalMaterialColor {
        override val color50: Int = 0xFFEDE7F6.toInt()
        override val color100: Int = 0xFFD1C4E9.toInt()
        override val color200: Int = 0xFFB39DDB.toInt()
        override val color300: Int = 0xFF9575CD.toInt()
        override val color400: Int = 0xFF7E57C2.toInt()
        override val color500: Int = 0xFF673AB7.toInt()
        override val color600: Int = 0xFF5E35B1.toInt()
        override val color700: Int = 0xFF512DA8.toInt()
        override val color800: Int = 0xFF4527A0.toInt()
        override val color900: Int = 0xFF311B92.toInt()
        override val colorA100: Int = 0xFFB388FF.toInt()
        override val colorA200: Int = 0xFF7C4DFF.toInt()
        override val colorA400: Int = 0xFF651FFF.toInt()
        override val colorA700: Int = 0xFF6200EA.toInt()
    }

    object Indigo : MaterialColor, AdditionalMaterialColor {
        override val color50: Int = 0xFFE8EAF6.toInt()
        override val color100: Int = 0xFFC5CAE9.toInt()
        override val color200: Int = 0xFF9FA8DA.toInt()
        override val color300: Int = 0xFF7986CB.toInt()
        override val color400: Int = 0xFF5C6BC0.toInt()
        override val color500: Int = 0xFF3F51B5.toInt()
        override val color600: Int = 0xFF3949AB.toInt()
        override val color700: Int = 0xFF303F9F.toInt()
        override val color800: Int = 0xFF283593.toInt()
        override val color900: Int = 0xFF1A237E.toInt()
        override val colorA100: Int = 0xFF8C9EFF.toInt()
        override val colorA200: Int = 0xFF536DFE.toInt()
        override val colorA400: Int = 0xFF3D5AFE.toInt()
        override val colorA700: Int = 0xFF304FFE.toInt()
    }
}