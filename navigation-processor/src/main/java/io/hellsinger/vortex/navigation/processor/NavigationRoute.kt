package io.hellsinger.vortex.navigation.processor

@Target(AnnotationTarget.CLASS)
annotation class NavigationRoute(val id: String, val name: String)