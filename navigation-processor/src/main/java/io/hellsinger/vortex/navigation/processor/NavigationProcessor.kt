package io.hellsinger.vortex.navigation.processor

import com.google.devtools.ksp.processing.Resolver
import com.google.devtools.ksp.processing.SymbolProcessor
import com.google.devtools.ksp.processing.SymbolProcessorEnvironment
import com.google.devtools.ksp.processing.SymbolProcessorProvider
import com.google.devtools.ksp.symbol.KSAnnotated

class NavigationProcessor : SymbolProcessor {

    override fun process(resolver: Resolver): List<KSAnnotated> = emptyList()


    class Provider() : SymbolProcessorProvider {
        override fun create(
            environment: SymbolProcessorEnvironment
        ): SymbolProcessor = NavigationProcessor()
    }

}