package manager

import kotlinx.coroutines.flow.Flow
import model.Plugin

interface Repository {

    suspend fun request(id: String): Flow<Plugin>

}