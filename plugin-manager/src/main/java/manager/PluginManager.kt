package manager

interface PluginManager {

    suspend fun install(id: String)

    suspend fun remove(id: String)

}