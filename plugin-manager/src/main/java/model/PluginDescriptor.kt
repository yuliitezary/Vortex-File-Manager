package model

import android.net.Uri

data class PluginDescriptor(
    val name: String,
    val description: String,
    val links: List<String>,
    val images: List<Uri>
)