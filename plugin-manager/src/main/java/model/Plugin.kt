package model

import io.hellsinger.vortex.filesystem.path.Path

data class Plugin(
    val id: String = "",
    val descriptor: PluginDescriptor,
    val dependencies: List<Plugin> = listOf(),
    val optionalDependencies: List<Plugin> = listOf(),
    val packageFiles: List<Path> = listOf(),
    val replaces: List<Plugin> = listOf(),
    val conflicts: List<Plugin> = listOf(),
    val size: Long = -1L,
    val downloadSize: Long = -1L,
)