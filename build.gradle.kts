plugins {
    alias(libs.plugins.android.application) apply false
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.google.ksp) apply false
    alias(libs.plugins.kotlin.android) apply false
    alias(libs.plugins.kotlin.parcelize) apply false
    alias(libs.plugins.kotlin.jvm) apply false
    alias(libs.plugins.android.test) apply false
    alias(libs.plugins.androidx.baseline.profile) apply false
}

tasks.create<Delete>(
    name = "clean",
    configuration = {
        delete(rootProject.layout.buildDirectory)
    }
)

subprojects {
    val args = listOf(
        "-opt-in=kotlin.RequiresOptIn",
        "-Xcontext-receivers",
//        "-Xuse-k2"
    )

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>() {
        kotlinOptions.jvmTarget = libs.versions.jvm.get()
        kotlinOptions.freeCompilerArgs = args
    }
}