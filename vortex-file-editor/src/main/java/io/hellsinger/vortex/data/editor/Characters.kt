package io.hellsinger.vortex.data.editor

object Characters {

    const val Digits = "0 1 2 3 4 5 6 7 8 9"

    object Special {
        const val NewLine = '\n'
        const val Tab = '\t'
        const val Backspace = '\b'
        const val Return = '\r'

        // Line feed
        const val LF = NewLine

        // Carriage return
        const val CR = Return

        const val LF_CR = "$LF$CR"
    }

}