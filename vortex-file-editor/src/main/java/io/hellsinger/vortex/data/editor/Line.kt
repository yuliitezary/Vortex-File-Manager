package io.hellsinger.vortex.data.editor

interface Line : CharSequence {
    override val length: Int

    fun toCharArray(): CharArray

    override fun subSequence(startIndex: Int, endIndex: Int): Line
}

// Memory optimization
object EmptyLine : Line {
    override val length: Int
        get() = 0

    override fun toCharArray(): CharArray = CharArray(0)

    override fun get(index: Int): Char = Char(0)

    override fun subSequence(startIndex: Int, endIndex: Int): Line = this
}

fun Line(content: CharSequence): Line = object : Line {
    private val content = CharArray(content.length)
    override val length: Int = content.length

    override fun get(index: Int): Char {
        return content[index]
    }

    override fun toCharArray(): CharArray = this.content

    override fun subSequence(startIndex: Int, endIndex: Int): Line = Line(content = content)
}