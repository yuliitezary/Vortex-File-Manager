package io.hellsinger.vortex.provider

import io.hellsinger.vortex.data.editor.Line
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

internal class LineManager {

    private val lines = mutableListOf<Line>()

    fun lines(): List<Line> = lines

    private val events = MutableStateFlow<Event>(Event.Idle)

    val scope = CoroutineScope(Dispatchers.Main.immediate + SupervisorJob() + CoroutineName(""))

    fun events(): StateFlow<Event> = events

    fun getLine(index: Int): Line = lines[index]
    fun getLineLength(index: Int) = getLine(index).length
    fun getLineContent(index: Int) = getLine(index).toCharArray()

    fun setLines(lines: List<Line>) {
        this.lines.clear()
        this.lines += lines
    }

    fun addLine(line: String) {
        val wrap = Line(line)
        lines += wrap
        sendEvent(Event.AppendLineEvent(wrap))
    }

    fun addLine(line: Line) {
        lines += line
        sendEvent(Event.AppendLineEvent(line))
    }

    private fun sendEvent(event: Event) = scope.launch { events.emit(event) }

    fun release() {
        lines.clear()
    }

    data class Data(
        val lines: List<Line>,
        val maxLineLength: Int
    )

    interface Event {
        object Idle : Event
        class AppendLineEvent(line: Line) : Event
    }

}