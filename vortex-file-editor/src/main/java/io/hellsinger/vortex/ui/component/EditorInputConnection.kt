package io.hellsinger.vortex.ui.component

import android.view.View
import android.view.inputmethod.BaseInputConnection

internal class EditorInputConnection(
    private val target: View
) : BaseInputConnection(target, true) {

    override fun commitText(text: CharSequence?, newCursorPosition: Int): Boolean {
        if (!target.isEnabled || text == null) return false
        return true
    }

}