package io.hellsinger.vortex.ui.component.gesture

import android.view.ScaleGestureDetector
import android.view.ScaleGestureDetector.OnScaleGestureListener
import io.hellsinger.vortex.ui.component.StorageTextEditor

internal class ViewScale(
    private val target: StorageTextEditor
) : OnScaleGestureListener {

    override fun onScale(detector: ScaleGestureDetector): Boolean {
        return true
    }

    override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
        return true
    }

    override fun onScaleEnd(detector: ScaleGestureDetector) {

    }

}