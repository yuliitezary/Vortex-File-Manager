package io.hellsinger.vortex.ui.component

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.util.TypedValue
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import androidx.annotation.IntDef
import io.hellsinger.vortex.data.editor.Line
import io.hellsinger.vortex.provider.LineManager
import io.hellsinger.vortex.ui.component.gesture.ViewScale
import io.hellsinger.vortex.ui.component.gesture.ViewScroll

class StorageTextEditor(
    context: Context
) : View(context) {
    internal val scroll = ViewScroll(this)
    internal val scale = ViewScale(this)
    internal val manager = LineManager()
    internal val renderer = ViewRenderer(target = this)
    private val detector = GestureDetector(context, scroll)
    private val connection = EditorInputConnection(target = this)

    fun setTextSize(size: Int) = setTextSize(
        TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            size.toFloat(),
            context.resources.displayMetrics
        )
    )

    fun setTextSize(size: Float) {
        renderer.setTextSize(size)
        invalidate()
    }

    fun setText(lines: List<Line>) {
        manager.setLines(lines)
        invalidate()
    }

    fun appendLine(line: String) {
        manager.addLine(line)
        invalidate()
    }

    fun appendLine(line: Line) {
        manager.addLine(line)
    }

    override fun onDraw(canvas: Canvas) {
        val count = canvas.save()

        renderer.onFullDraw(canvas)
        canvas.restoreToCount(count)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) return false

        return detector.onTouchEvent(event)
    }

    override fun onFocusChanged(
        gainFocus: Boolean,
        direction: Int,
        previouslyFocusedRect: Rect?
    ) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
    }

    override fun onCreateInputConnection(
        outAttrs: EditorInfo
    ): InputConnection {
        return connection
    }

    override fun computeScroll() {
        val scroller = scroll.scroller
        val needsInvalidate = true
        if (scroller.computeScrollOffset()) {
            val currentX = scroller.currX
            val currentY = scroller.currY

            scrollX = currentX
            scrollY = currentY
        }

//        if (needsInvalidate) postInvalidateOnAnimation()
    }

    override fun scrollTo(x: Int, y: Int) {
        super.scrollTo(x, y)
    }

    companion object {
        const val DEFAULT_TEXT_SIZE = 18F

        const val LINE_NUMBER_TEXT_LEFT_ALIGN = 0
        const val LINE_NUMBER_TEXT_RIGHT_ALIGN = 1
    }

    @IntDef(value = [LINE_NUMBER_TEXT_LEFT_ALIGN, LINE_NUMBER_TEXT_RIGHT_ALIGN])
    annotation class LineNumberTextAlign
}