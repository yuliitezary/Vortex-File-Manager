package io.hellsinger.vortex.ui.component.layout

interface TextLayout {

    val width: Int
    val height: Int

    fun getLineIndexForRow(row: Int)

}