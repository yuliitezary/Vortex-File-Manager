package io.hellsinger.vortex.ui.component.layout

import io.hellsinger.vortex.data.editor.Line

class DefaultTextLayout : TextLayout {

    private val lines = mutableListOf<Line>()

    override val height: Int
        get() = lines.size

    override val width: Int
        get() = TODO("Not yet implemented")

    override fun getLineIndexForRow(row: Int) {
        TODO("Not yet implemented")
    }
}