package io.hellsinger.vortex.ui.component

import android.graphics.Canvas
import android.graphics.Paint
import android.widget.EdgeEffect
import io.hellsinger.theme.Colors
import io.hellsinger.theme.MaterialColors
import io.hellsinger.vortex.data.editor.Characters
import io.hellsinger.vortex.data.editor.Line
import kotlin.math.max

internal class ViewRenderer(
    private val target: StorageTextEditor
) {

    private val selectedLineIndex = 0

    private val topEdgeEffect = EdgeEffect(target.context)

    private fun sp(size: Int): Int = with(target) { size.sp }
    private fun sp(size: Float): Float = with(target) { size.sp }
    private fun dp(size: Int): Int = with(target) { size.sp }
    private fun dp(size: Float): Float = with(target) { size.sp }

    val lineNumberWidth: Float
        get() = calcLineNumberWidth()

    val lineNumberHeight: Float
        get() = calcLineNumberHeight() * lineCount

    private fun getLineAt(index: Int): Line {
        return target.manager.getLine(index)
    }

    private val lineCount: Int
        get() = target.manager.lines().size

    // Animation things
    private var lineNumberFactor = 1F

    private val textPaint: Paint = Paint(Paint.DITHER_FLAG or Paint.ANTI_ALIAS_FLAG).apply {
        color = MaterialColors.Pink.colorA700
        style = Paint.Style.FILL
        strokeWidth = dp(1F)
        textSize = sp(StorageTextEditor.DEFAULT_TEXT_SIZE)
    }

    private val lineNumberPaint: Paint = Paint(Paint.DITHER_FLAG or Paint.ANTI_ALIAS_FLAG).apply {
        color = MaterialColors.Indigo.colorA700
        style = Paint.Style.FILL
        strokeWidth = dp(2F)
        textSize = sp(StorageTextEditor.DEFAULT_TEXT_SIZE)
    }

    var lineNumberDividerWidth: Float
        get() = lineNumberPaint.strokeWidth
        set(value) {
            lineNumberPaint.strokeWidth = value
        }

    private var lineNumberWidthCache = calcLineNumberWidth()

    private var scale: Float = 1F

    private fun calcLineNumberWidth(): Float {
        var count = 0
        var lineCount = lineCount

        while (lineCount > 0) {
            count++
            lineCount /= 10
        }

        val digitsLength = Characters.Digits.length
        val digitsWidths = FloatArray(digitsLength)
        textPaint.getTextWidths(Characters.Digits, digitsWidths)
        var lineNumberWidth = 0F
        for (i in 0 until digitsLength step 2)
            lineNumberWidth = max(lineNumberWidth, digitsWidths[i])

        return lineNumberWidth * count
    }

    private fun calcLineNumberHeight(): Float {
        return lineNumberPaint.fontMetrics.descent - lineNumberPaint.fontMetrics.ascent
    }

    fun setTextSize(size: Float) {
        textPaint.textSize = size
    }

    fun onFullDraw(canvas: Canvas) {
        if (lineCount <= 0) return
        val lineNumberWidth = calcLineNumberWidth()
        val lineNumberHeight = calcLineNumberHeight()

        lineNumberPaint.setColor(Colors.DarkGray.toInt())

        canvas.drawRect(
            0F,
            0F,
            lineNumberWidth + lineNumberDividerWidth,
            this.lineNumberHeight,
            lineNumberPaint
        )
        lineNumberPaint.setColor(MaterialColors.Indigo.colorA700)

        for (i in 0 until lineCount) {
            val line = getLineAt(i)
            val scaledLineNumberHeight = lineNumberHeight * (i + 1)

            drawLineSegment(
                line = line,
                index = i,
                height = scaledLineNumberHeight,
                width = lineNumberWidth + lineNumberDividerWidth + lineNumberDividerWidth,
                canvas = canvas
            )
        }
        canvas.drawLine(
            lineNumberWidth + lineNumberDividerWidth,
            0F,
            lineNumberWidth + lineNumberDividerWidth,
            this.lineNumberHeight,
            lineNumberPaint
        )
    }

    private fun drawLineSegment(
        line: Line,
        index: Int,
        height: Float,
        width: Float,
        canvas: Canvas
    ) {
        drawText(
            text = (index + 1).toString(),
            offsetX = 0F,
            offsetY = height,
            paint = lineNumberPaint,
            canvas = canvas
        )
        drawText(
            text = line,
            offsetX = width,
            offsetY = height,
            paint = textPaint,
            canvas = canvas
        )
    }

    private fun drawText(
        text: CharSequence,
        offsetX: Float,
        offsetY: Float,
        canvas: Canvas,
        paint: Paint,
        start: Int = 0,
        end: Int = text.length
    ) = canvas.drawText(text, start, end, offsetX, offsetY, paint)
}