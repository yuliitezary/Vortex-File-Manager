package io.hellsinger.vortex.ui.component.gesture

import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.S
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.OverScroller
import io.hellsinger.vortex.ui.component.StorageTextEditor
import kotlin.math.max
import kotlin.math.min

internal class ViewScroll(
    private val target: StorageTextEditor
) : GestureDetector.OnGestureListener {

    val scroller = OverScroller(target.context)

    val offsetX: Int
        get() = scroller.currX

    val offsetY: Int
        get() = scroller.currY

    val maxOffsetX: Int
        get() = max(target.width, target.renderer.lineNumberWidth.toInt())

    val maxOffsetY: Int
        get() = max(target.height, target.renderer.lineNumberHeight.toInt())

    override fun onLongPress(e: MotionEvent) {

    }

    override fun onDown(e: MotionEvent): Boolean {
        releaseEffects()
        scroller.forceFinished(true)
        target.postInvalidateOnAnimation()
        return true
    }

    override fun onShowPress(e: MotionEvent) {

    }

    override fun onSingleTapUp(e: MotionEvent): Boolean {
        return true
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        scroll(distanceX, distanceY)
        target.invalidate()

        return true
    }

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        releaseEffects()

        forceFinish()
        fling((-velocityX).toInt(), (-velocityY).toInt())
        target.postInvalidateOnAnimation()
        return true
    }

    private fun forceFinish(finished: Boolean = true) {
        scroller.forceFinished(finished)
    }

    private fun abortAnimation() {
        scroller.abortAnimation()
    }

    private fun scroll(distanceX: Float, distanceY: Float, duration: Int = 0) {
        val endX = min(max(offsetX + distanceX.toInt(), 0), maxOffsetX)
        val endY = min(max(offsetY + distanceY.toInt(), 0), maxOffsetY)

        scroller.startScroll(
            offsetX,
            offsetY,
            endX - offsetX,
            endY - offsetY,
            duration
        )
    }

    private fun fling(velocityX: Int, velocityY: Int) {
        scroller.fling(
            offsetX,
            offsetY,
            velocityX,
            velocityY,
            0,
            maxOffsetX,
            0,
            maxOffsetY,
            0,
            0
        )
    }

    private fun releaseEffects() {
        if (SDK_INT < S) {
            // TODO: Implement release edge effects
        }
    }

}