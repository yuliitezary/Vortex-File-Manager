package io.hellsinger.theme.vortex

import io.hellsinger.theme.Theme

val VortexThemeTexts.storageTextFieldLineNumberBackgroundKey: Theme.Key<String>
    get() = createKey("storageTextFieldLineNumberBackground")