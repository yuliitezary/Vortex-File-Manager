import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.google.ksp)
    alias(libs.plugins.androidx.baseline.profile)
}

android {
    namespace = "io.hellsinger.vortex"
    compileSdk = AndroidConfigure.targetSdk
//    compileSdkPreview = "UpsideDownCakePrivacySandbox"

    buildFeatures {
        buildConfig = true
    }

    defaultConfig {
        applicationId = AndroidConfigure.applicationId
        minSdk = AndroidConfigure.minSdk
        targetSdk = AndroidConfigure.targetSdk
//        targetSdkPreview = "UpsideDownCakePrivacySandbox"
        versionCode = AndroidConfigure.versionCode
        versionName = AndroidConfigure.versionName
        multiDexEnabled = AndroidConfigure.multiDexEnabled

        javaCompileOptions {
            annotationProcessorOptions {
                arguments += mapOf(
//                    "room.incremental" to "true"
                )
            }
        }
    }

    signingConfigs {
        create(BuildConfig.Type.Release.Name) {
            val props = gradleLocalProperties(rootDir, providers)
            storeFile = file(props["signing.path"].toString())
            storePassword = props["signing.pathPassword"].toString()
            keyAlias = props["signing.alias"].toString()
            keyPassword = props["signing.aliasPassword"].toString()
        }
    }

    buildTypes {
        getByName(BuildConfig.Type.Release.Name) {
            isMinifyEnabled = BuildConfig.Type.Release.isMinifyEnabled
            isShrinkResources = BuildConfig.Type.Release.isShrinkResources
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfig = signingConfigs[BuildConfig.Release.Name]
        }
    }

    compileOptions {
        sourceCompatibility = BuildConfig.JDK.VerEnum
        targetCompatibility = BuildConfig.JDK.VerEnum
    }
}

dependencies {
    implementation(libs.androidx.core)
    implementation(libs.androidx.collection)
    implementation(libs.androidx.recycler)
    implementation(libs.androidx.viewpager)

    implementation(libs.google.dagger)
    ksp(libs.google.dagger.compiler)

    implementation(libs.kotlinx.coroutines.android)

    implementation(projects.filesystem)
    implementation(projects.filesystemLinux)
    implementation(projects.uiComponent)
    implementation(projects.vortexService)
    implementation(projects.vortexFileEditor)
    implementation(projects.navigation)
//    implementation(projects.navigationProcessor)
    implementation(projects.theme)
    implementation(projects.themeVortex)

    implementation(libs.androidx.profile.installer)
    "baselineProfile"(project(":vortex-baseline-profile"))
}