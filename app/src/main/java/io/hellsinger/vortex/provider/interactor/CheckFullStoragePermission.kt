package io.hellsinger.vortex.provider.interactor

import android.os.Build
import android.os.Environment
import androidx.annotation.RequiresApi
import javax.inject.Inject

@RequiresApi(Build.VERSION_CODES.R)
class CheckFullStoragePermission @Inject constructor() {
    operator fun invoke(): Boolean = !Environment.isExternalStorageManager()
}