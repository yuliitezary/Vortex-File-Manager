package io.hellsinger.vortex.provider.interactor

import android.Manifest
import android.content.Context
import io.hellsinger.vortex.utils.isGranted
import javax.inject.Inject

class CheckStoragePermission @Inject constructor(
    private val context: Context
) {
    operator fun invoke(): Boolean {
        val readGranted = context.isGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
        val writeGranted = context.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return !readGranted || !writeGranted
    }
}