package io.hellsinger.vortex.provider.storage

import io.hellsinger.vortex.data.storage.StoragePendingItem
import kotlinx.coroutines.flow.StateFlow

interface StorageSharedState {

    val pending: StateFlow<List<StoragePendingItem>>

    suspend fun registerPending(item: StoragePendingItem)

    suspend fun unregisterPending(item: StoragePendingItem)
}