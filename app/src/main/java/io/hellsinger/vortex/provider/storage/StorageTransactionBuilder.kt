package io.hellsinger.vortex.provider.storage

import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.filesystem.linux.operation.delete
import io.hellsinger.vortex.data.PathItem

// FUTURE API

class StorageTransactionBuilder {

    private val components = mutableListOf<StorageTransactionComponent>()
    private var listener: StorageTransactionListener? = null

    fun add(component: StorageTransactionComponent): StorageTransactionBuilder {
        return this
    }

    fun bind(listener: StorageTransactionListener): StorageTransactionBuilder {
        this.listener = listener
        return this
    }

    fun build() {

    }

    interface StorageTransactionComponent {

        suspend fun onPerform(listener: StorageTransactionListener? = null)
    }

    fun interface StorageTransactionListener {
        suspend fun onEvent(event: Event)

        interface Event
    }
}

fun StorageTransactionBuilder.delete(items: List<PathItem>) = add(
    component = object : StorageTransactionBuilder.StorageTransactionComponent {
        override suspend fun onPerform(
            listener: StorageTransactionBuilder.StorageTransactionListener?
        ) = items.forEach { item ->
            try {
                if (item.isDirectory) {
                    item.asLinuxDirectory().delete { error ->

                    }
                } else {
                    item.asLinuxFile().delete()
                }
            } catch (exception: ErrnoException) {

            }
        }
    }
)