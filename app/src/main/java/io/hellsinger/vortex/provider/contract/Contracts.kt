package io.hellsinger.vortex.provider.contract

import android.content.Intent
import android.net.Uri.fromParts
import android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION
import io.hellsinger.vortex.BuildConfig

object Contracts {
    object Storage {
        const val RequestStorageAccessCode = 0
        const val RequestFullStorageAccessCode = 1

        fun StorageFullAccessIntent(): Intent =
            Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                .setData(fromParts("package", BuildConfig.APPLICATION_ID, null))
                .addCategory("android.intent.category.DEFAULT")
    }
}