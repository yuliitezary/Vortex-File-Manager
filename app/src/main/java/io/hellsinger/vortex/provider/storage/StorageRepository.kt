package io.hellsinger.vortex.provider.storage

import android.os.Build
import androidx.annotation.RequiresApi
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Open
import io.hellsinger.filesystem.linux.attribute.LinuxGroup
import io.hellsinger.filesystem.linux.attribute.LinuxUser
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.filesystem.directory.DirectoryEntry
import io.hellsinger.vortex.filesystem.path.Path
import kotlinx.coroutines.flow.Flow

interface StorageRepository {

    @RequiresApi(Build.VERSION_CODES.R)
    fun checkRestrictedPath(item: PathItem): Boolean

    fun requiresPermissions(): Boolean

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    fun requiresNotificationsAccess(): Boolean

    fun requiresFullStorageAccess(): Boolean

    suspend fun bytes(
        item: PathItem,
        openFlags: Int = Open.ReadOnly or Open.CreateNew or Open.NonBlocking
    ): Flow<Byte>

    suspend fun user(id: Int): LinuxUser

    suspend fun group(id: Int): LinuxGroup

    fun check(item: PathItem, mode: Int): Int

    fun entries(item: PathItem): Flow<DirectoryEntry<Path>>
}