package io.hellsinger.vortex.provider.interactor

import io.hellsinger.filesystem.linux.directory.LinuxDirectoryEntry
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.provider.storage.StorageRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterIsInstance
import javax.inject.Inject
import kotlin.Result.Companion.failure
import kotlin.Result.Companion.success

class GetEntries @Inject constructor(
    private val repository: StorageRepository,
    private val checkExistence: CheckExistence,
    private val checkReadable: CheckReadable
) {
    suspend operator fun invoke(item: PathItem): Result<Flow<LinuxDirectoryEntry<Path>>> {
        if (!checkExistence(item)) return failure(Throwable("${item.path} doesn't exist"))
        if (!checkReadable(item)) return failure(Throwable("${item.path} isn't readable"))

        return success(repository.entries(item).filterIsInstance<LinuxDirectoryEntry<Path>>())
    }
}