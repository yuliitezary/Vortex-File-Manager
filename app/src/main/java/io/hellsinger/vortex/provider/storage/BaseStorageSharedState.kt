package io.hellsinger.vortex.provider.storage

import io.hellsinger.vortex.data.storage.StoragePendingItem
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class BaseStorageSharedState @Inject constructor() : StorageSharedState {

    private val mutable: MutableStateFlow<List<StoragePendingItem>> = MutableStateFlow(listOf())

    override val pending: StateFlow<List<StoragePendingItem>>
        get() = mutable

    override suspend fun registerPending(
        item: StoragePendingItem
    ) = mutable.emit(mutable.value + item)

    override suspend fun unregisterPending(
        item: StoragePendingItem
    ) = mutable.emit(mutable.value - item)

}