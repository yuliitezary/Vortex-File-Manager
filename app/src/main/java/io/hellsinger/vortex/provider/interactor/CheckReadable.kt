package io.hellsinger.vortex.provider.interactor

import io.hellsinger.filesystem.linux.LinuxOperationOptions.Access.Readable
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.provider.storage.StorageRepository
import javax.inject.Inject

class CheckReadable @Inject constructor(
    private val repository: StorageRepository
) {
    suspend operator fun invoke(item: PathItem): Boolean = repository.check(item, Readable) == 0
}