package io.hellsinger.vortex.provider.storage

import android.Manifest.permission.POST_NOTIFICATIONS
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.os.Build
import android.os.Environment
import androidx.annotation.RequiresApi
import io.hellsinger.filesystem.linux.attribute.LinuxGroup
import io.hellsinger.filesystem.linux.attribute.LinuxUser
import io.hellsinger.filesystem.linux.attribute.getLinuxGroup
import io.hellsinger.filesystem.linux.attribute.getLinuxUser
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.directory.defaultWalker
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.filesystem.linux.operation.access
import io.hellsinger.filesystem.linux.operation.read
import io.hellsinger.filesystem.linux.toLinuxPath
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.utils.isGranted
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AndroidStorageRepository @Inject constructor(
    private val dispatcher: DispatcherProvider,
    private val context: Context
) : StorageRepository {

    @RequiresApi(Build.VERSION_CODES.R)
    override fun checkRestrictedPath(item: PathItem): Boolean = when (item.path) {
        RESTRICTED_NAME_OBB -> true
        RESTRICTED_NAME_DATA -> true
        else -> false
    }

    override fun requiresPermissions(): Boolean {
        val readGranted = context.isGranted(READ_EXTERNAL_STORAGE)
        val writeGranted = context.isGranted(WRITE_EXTERNAL_STORAGE)
        return !readGranted || !writeGranted
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun requiresNotificationsAccess(): Boolean = context.isGranted(POST_NOTIFICATIONS)

    @RequiresApi(Build.VERSION_CODES.R)
    override fun requiresFullStorageAccess(): Boolean = !Environment.isExternalStorageManager()

    override suspend fun bytes(item: PathItem, openFlags: Int) = item.asLinuxFile().read()

    override suspend fun user(id: Int): LinuxUser = withContext(dispatcher.io) { getLinuxUser(id) }

    override suspend fun group(id: Int): LinuxGroup =
        withContext(dispatcher.io) { getLinuxGroup(id) }

    override fun check(item: PathItem, mode: Int): Int = item.path.access(mode)

    override fun entries(item: PathItem) = item.path.asLinuxDirectory().defaultWalker()

    companion object {
        val RESTRICTED_NAME_OBB =
            "${Environment.getExternalStorageDirectory().absoluteFile}/Android/obb".toLinuxPath()
        val RESTRICTED_NAME_DATA =
            "${Environment.getExternalStorageDirectory().absoluteFile}/Android/data".toLinuxPath()
    }
}