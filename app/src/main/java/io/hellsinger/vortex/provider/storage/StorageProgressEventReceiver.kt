package io.hellsinger.vortex.provider.storage

import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.provider.storage.StorageProgressEventReceiver.Event
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import javax.inject.Inject

// Future API
interface StorageProgressEventReceiver {
    val event: SharedFlow<Event>

    interface Event {
        object Idle : Event

        interface Failure : Event {
            val error: Throwable
        }

        interface Delete : Event {
            val entry: Path
            val sources: List<Path>
        }

        interface Copy : Event {
            val entry: Path
            val dest: Path
            val sources: List<Path>
        }

        interface Move : Event {
            val entry: Path
            val dest: Path
            val sources: List<Path>
        }

        interface Create : Event {
            val entry: Path
        }

        interface Rename : Event {
            val entry: Path
            val dest: Path
        }

        interface Progress : Event {
            val current: Int
            val max: Int
        }

        interface Permission : Event {
            val source: Path
            val destPermission: Int
        }
    }
}

interface StorageProgressEventSender : StorageProgressEventReceiver {
    override val event: SharedFlow<Event>

    suspend fun notifyEvent(event: Event)
}

class StorageProgressEventSenderImpl @Inject constructor() : StorageProgressEventSender {
    private val _event =
        MutableSharedFlow<Event>(replay = 2, onBufferOverflow = BufferOverflow.DROP_OLDEST)
    override val event: SharedFlow<Event>
        get() = _event

    override suspend fun notifyEvent(event: Event) = _event.emit(event)
}

suspend fun StorageProgressEventSender.notifyIdle() = notifyEvent(Event.Idle)

suspend fun StorageProgressEventSender.notifyFailureEvent(error: Throwable) = notifyEvent(
    event = object : Event.Failure {
        override val error: Throwable = error
    }
)

suspend fun StorageProgressEventSender.notifyCreateEvent(source: Path) = notifyEvent(
    event = object : Event.Create {
        override val entry: Path = source
    }
)

suspend fun StorageProgressEventSender.notifyRenameEvent(source: Path, dest: Path) = notifyEvent(
    event = object : Event.Rename {
        override val entry: Path = source
        override val dest: Path = dest
    }
)

suspend fun StorageProgressEventSender.notifyPermissionEvent(source: Path, destPermission: Int) =
    notifyEvent(
        event = object : Event.Permission {
            override val source: Path = source
            override val destPermission: Int = destPermission
        }
    )

suspend fun StorageProgressEventSender.notifyDeleteEvent(source: Path, sources: List<Path>) =
    notifyEvent(
        event = object : Event.Delete {
            override val entry: Path = source
            override val sources: List<Path> = sources
        }
    )

suspend fun StorageProgressEventSender.notifyCopyEvent(
    entry: Path,
    dest: Path,
    sources: List<Path>
) = notifyEvent(
    event = object : Event.Copy {
        override val entry: Path = entry
        override val dest: Path = dest
        override val sources: List<Path> = sources
    }
)

suspend fun StorageProgressEventSender.notifyMoveEvent(
    entry: Path,
    dest: Path,
    sources: List<Path>
) = notifyEvent(
    event = object : Event.Move {
        override val entry: Path = entry
        override val dest: Path = dest
        override val sources: List<Path> = sources
    }
)

suspend fun StorageProgressEventSender.notifyProgressEvent(
    current: Int,
    max: Int
) = notifyEvent(
    event = object : Event.Progress {
        override val current: Int = current
        override val max: Int = max
    }
)