package io.hellsinger.vortex.provider.interactor

import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.provider.storage.AndroidStorageRepository.Companion.RESTRICTED_NAME_DATA
import io.hellsinger.vortex.provider.storage.AndroidStorageRepository.Companion.RESTRICTED_NAME_OBB
import javax.inject.Inject

class CheckRestrictedItem @Inject constructor() {
    operator fun invoke(item: PathItem): Boolean = when (item.path) {
        RESTRICTED_NAME_OBB -> true
        RESTRICTED_NAME_DATA -> true
        else -> false
    }
}