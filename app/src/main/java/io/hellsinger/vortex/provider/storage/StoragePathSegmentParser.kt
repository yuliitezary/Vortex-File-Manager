package io.hellsinger.vortex.provider.storage

import android.os.Environment
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.storage.VortexStorageController
import io.hellsinger.vortex.service.utils.asPath
import javax.inject.Inject

interface StoragePathSegmentParser {
    fun parse(path: Path): ArrayList<PathItem>
}

class SpeedStoragePathSegmentParser @Inject constructor() : StoragePathSegmentParser {
    override fun parse(path: Path): ArrayList<PathItem> {
        if (path == Environment.getExternalStorageDirectory().asPath()) return arrayListOf(
            PathItem(path)
        )
        val segments = arrayListOf<PathItem>()
        var cPath = path

        // TODO: Replace with for loop, exclude cPath variable and reverse() method
        while (true) {
            if (cPath == VortexStorageController.EXTERNAL_STORAGE.parent) break
            segments += PathItem(cPath)
            cPath = cPath.parent ?: break
        }

        segments.reverse()

        return segments
    }
}

class MemoryStoragePathSegmentParser @Inject constructor() : StoragePathSegmentParser {
    override fun parse(path: Path): ArrayList<PathItem> {
        if (path == Environment.getExternalStorageDirectory().asPath()) return arrayListOf(
            PathItem(path)
        )

        /**
         *  Memory VS Speed
         *  Next lines describes how much we need to allocate memory to put
         *  path segments into list, so basically we just checking
         *  if segments of path don't get away from
         *  Environment.getExternalStorageDirectory() path
         *  **/
        val size = path.nameCount - 1
        var count = 0
        for (i in size downTo 0) {
            val parent = path.getParentAt(i) ?: break
            if (parent != Environment.getExternalStorageDirectory().parentFile?.asPath())
                count++
            else break
        }

        val segments = ArrayList<PathItem>(count + 1)
        while (count >= 0)
            segments.add(PathItem(path.getParentAt(path.nameCount - count--) ?: path))

        return segments
    }
}