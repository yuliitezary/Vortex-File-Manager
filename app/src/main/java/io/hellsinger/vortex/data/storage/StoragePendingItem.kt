package io.hellsinger.vortex.data.storage

import io.hellsinger.vortex.data.PathItem

interface StoragePendingItem {
    val src: List<PathItem>
}

class StorageDeletePendingItem(override val src: List<PathItem>) : StoragePendingItem
class StorageMovePendingItem(override val src: List<PathItem>) : StoragePendingItem
class StorageCopyPendingItem(override val src: List<PathItem>) : StoragePendingItem