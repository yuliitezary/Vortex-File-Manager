package io.hellsinger.vortex.data.tag

import android.os.Parcelable
import androidx.annotation.ColorInt
import io.hellsinger.theme.Theme
import kotlinx.parcelize.Parcelize

@Parcelize
data class Tag(
    val name: String,
    @ColorInt
    val color: Int,
) : Parcelable {
    constructor(name: String, color: Long) : this(name, color.toInt())
    constructor(name: String, key: Theme.Key<Int>) : this(name, key.get())
}