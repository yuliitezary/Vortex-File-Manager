package io.hellsinger.vortex.data

import android.os.Parcelable
import io.hellsinger.filesystem.linux.attribute.attributes
import io.hellsinger.vortex.filesystem.attribute.PathAttribute
import io.hellsinger.vortex.filesystem.attribute.isDirectory
import io.hellsinger.vortex.filesystem.attribute.isFile
import io.hellsinger.vortex.filesystem.attribute.isLink
import io.hellsinger.vortex.filesystem.attribute.mimetype.MimeType
import io.hellsinger.vortex.filesystem.attribute.mimetype.mimeType
import io.hellsinger.vortex.filesystem.attribute.time.PathTime
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.path.PathOwner
import io.hellsinger.vortex.service.storage.VortexStorageController.Companion.EXTERNAL_STORAGE
import io.hellsinger.vortex.service.utils.PathAttributeParceler
import io.hellsinger.vortex.service.utils.PathParceler
import io.hellsinger.vortex.service.utils.parsePath
import io.hellsinger.vortex.ui.component.Items
import io.hellsinger.vortex.ui.component.list.adapter.Item
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.io.File

@Parcelize
class PathItem(
    override val value: @WriteWith<PathParceler> Path,
    val attrs: @WriteWith<PathAttributeParceler> PathAttribute = value.attributes,
) : Item<Path>, PathOwner<Path>, Comparable<PathItem>, Parcelable {

    constructor(path: String) : this(value = parsePath(path = path.encodeToByteArray()))
    constructor(
        path: String,
        attrs: PathAttribute,
    ) : this(
        value = parsePath(path = path.encodeToByteArray()),
        attrs = attrs
    )

    constructor(path: File) : this(path = path.path)
    constructor(
        path: File,
        attrs: PathAttribute,
    ) : this(
        path = path.path,
        attrs = attrs
    )

    constructor(owner: PathOwner<Path>) : this(owner.path)
    constructor(owner: PathOwner<Path>, attrs: PathAttribute) : this(owner.path, attrs)

    override val path: Path
        get() = value

    val permission: String
        get() = attrs.permission

    val scheme: Int
        get() = value.scheme

    val isEmptyAttributes
        get() = attrs is PathAttribute.EmptyPathAttribute

    @IgnoredOnParcel
//    @PrimaryKey
    override val id: Long = attrs.id

    override val viewType: Int
        get() = Items.Storage


    val parent: PathItem?
        get() = value.parent?.let(::PathItem)

    @IgnoredOnParcel
    val name: String = if (value == EXTERNAL_STORAGE) {
        "Internal storage" // TODO: Move to Locale files
    } else {
        value.getNameAt().toString()
    }

    @IgnoredOnParcel
    val mime: MimeType = value.mimeType

    val isHidden: Boolean
        get() = value.isHidden

    val isPathAbsolute: Boolean
        get() = value.isAbsolute

    val isPathEmpty: Boolean
        get() = value.isEmpty

    val type: Int
        get() = attrs.type

    val isDirectory: Boolean
        get() = attrs.isDirectory

    val isFile: Boolean
        get() = attrs.isFile

    val isLink: Boolean
        get() = attrs.isLink

    val lastModifiedTime: PathTime
        get() = attrs.lastModifiedTime

    val lastAccessTime: PathTime
        get() = attrs.lastAccessTime

    val creationTime: PathTime
        get() = attrs.creationTime

    override operator fun compareTo(other: PathItem): Int {
        var result = 0

        result += isDirectory compareTo other.isDirectory
        result += name compareTo other.name

        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other is Path) return other == value
        if (javaClass != other?.javaClass) return false

        other as PathItem

        return value == other.value
    }

    override fun hashCode(): Int {
        val result = value.hashCode()
        return result
    }

}