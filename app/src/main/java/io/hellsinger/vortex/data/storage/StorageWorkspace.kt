package io.hellsinger.vortex.data.storage

import android.os.Parcelable
import io.hellsinger.vortex.data.PathItem
import kotlinx.parcelize.Parcelize

@Parcelize
data class StorageWorkspace(
    val name: String,
    val items: List<PathItem>
) : Parcelable