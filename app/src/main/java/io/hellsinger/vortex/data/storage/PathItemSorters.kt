package io.hellsinger.vortex.data.storage

import io.hellsinger.vortex.data.PathItem

typealias Sorter<T> = Comparator<T>

object PathItemSorters {

    inline val Name: Sorter<PathItem>
        get() = compareByDescending<PathItem> { item ->
            item.isDirectory
        }.thenBy {
            it.name
        }

    inline val Path: Sorter<PathItem>
        get() = compareByDescending<PathItem> { item ->
            item.isDirectory
        }.thenBy {
            it.value
        }

    inline val LastModifiedTime: Sorter<PathItem>
        get() = compareByDescending<PathItem> { item ->
            item.isDirectory
        }.thenBy {
            it.lastModifiedTime.nanos
        }

    inline val LastAccessTime: Sorter<PathItem>
        get() = compareByDescending<PathItem> { item ->
            item.isDirectory
        }.thenBy {
            it.lastAccessTime.nanos
        }

    inline val CreationTime: Sorter<PathItem>
        get() = compareByDescending<PathItem> { item ->
            item.isDirectory
        }.thenBy {
            it.creationTime.nanos
        }

}