package io.hellsinger.vortex.data

import io.hellsinger.filesystem.linux.attribute.attributes
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.directory.size
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_DIRECTORY
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_FILE
import io.hellsinger.vortex.filesystem.directory.DirectoryWalker
import io.hellsinger.vortex.filesystem.directory.DirectoryWalker.Companion.CONTINUE
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.size.SiSize
import io.hellsinger.vortex.utils.DirCount
import io.hellsinger.vortex.utils.parseThemeType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

class PathItemDescriptor {

    companion object {
        const val DIR_COUNT = 0
        const val DIR_SIZE = 1
        const val DIR_LMT = 2
        const val DIR_LAT = 4
        const val DIR_CT = 8
        const val DIR_HIDDEN = 16
        const val DIR_ABSOLUTE = 32

        const val FILE_SIZE = 64
        const val FILE_LMT = 128
        const val FILE_LAT = 256
        const val FILE_CT = 512
        const val FILE_HIDDEN = 1024
        const val FILE_ABSOLUTE = 2048
        const val FILE_MIMETYPE = 4096

        const val DIR_PERM = 8192
        const val FILE_PERM = 16384
    }

    private val builder = StringBuilder()

    fun describeFlow(mask: Int, item: PathItem): Flow<String> = flow {
        when (item.attributes.type) {
            PATH_ATTR_TYPE_DIRECTORY -> {
                val directory = item.asLinuxDirectory()
                // Efficient way
                if (mask and DIR_COUNT == DIR_COUNT && mask and DIR_SIZE == DIR_SIZE) {
                    var count = 0
                    var size = SiSize(0L)
                    directory.walk(object : DirectoryWalker {
                        override suspend fun onPreEnterDirectory(path: Path): Int {
                            if (path.parent == item.path) count++
                            return CONTINUE
                        }

                        override suspend fun onPostEnterDirectory(path: Path) {

                        }

                        override suspend fun onEntry(path: Path): Int {
                            count++
                            size += path.asLinuxFile().size

                            return CONTINUE
                        }
                    }).catch { error ->

                    }.collect()
                    emit(count.toString())
                    emit(size.toString())
                } else if (mask and DIR_COUNT == DIR_COUNT) emit(directory.count().toString())
                else if (mask and DIR_SIZE == DIR_SIZE) emit(directory.size().toString())
                if (mask and DIR_HIDDEN == DIR_HIDDEN) emit("Hidden")
                if (mask and DIR_ABSOLUTE == DIR_ABSOLUTE) emit("Absolute")
                if (mask and DIR_LMT == DIR_LMT) emit(item.lastModifiedTime.toString())
                if (mask and DIR_LAT == DIR_LAT) emit(item.lastAccessTime.toString())
                if (mask and DIR_CT == DIR_CT) emit(item.creationTime.toString())
            }

            PATH_ATTR_TYPE_FILE -> {
                val file = item.asLinuxFile()

                if (mask and FILE_SIZE == FILE_SIZE) emit(file.size.format("#.##").toString())
                if (mask and FILE_MIMETYPE == FILE_MIMETYPE) item.mime.parseThemeType()
                    ?.let { emit(it) }
                if (mask and FILE_HIDDEN == FILE_HIDDEN) emit("Hidden")
                if (mask and FILE_ABSOLUTE == FILE_ABSOLUTE) emit("Absolute")
                if (mask and FILE_LMT == FILE_LMT) emit(item.lastModifiedTime.toString())
                if (mask and FILE_LAT == FILE_LAT) emit(item.lastAccessTime.toString())
                if (mask and FILE_CT == FILE_CT) emit(item.creationTime.toString())
            }
        }
    }

    suspend fun describe(mask: Int, item: PathItem): String = withContext(Dispatchers.IO) {
        builder.clear()

        when (item.attributes.type) {
            PATH_ATTR_TYPE_DIRECTORY -> {
                val directory = item.asLinuxDirectory()
                if (mask and DIR_COUNT == DIR_COUNT) wrap(builder, DirCount(directory.count()))
                if (mask and DIR_SIZE == DIR_SIZE) wrap(builder, directory.size().toString())
                if (mask and DIR_HIDDEN == DIR_HIDDEN) wrap(builder, item.isHidden)
                if (mask and DIR_ABSOLUTE == DIR_ABSOLUTE) wrap(builder, item.isPathAbsolute)
                if (mask and DIR_LMT == DIR_LMT) wrap(builder, item.lastModifiedTime.toString())
                if (mask and DIR_LAT == DIR_LAT) wrap(builder, item.lastAccessTime.toString())
                if (mask and DIR_CT == DIR_CT) wrap(builder, item.creationTime.toString())
                if (mask and DIR_PERM == DIR_PERM) wrap(builder, item.permission)
            }

            PATH_ATTR_TYPE_FILE -> {
                val file = item.asLinuxFile()
                if (mask and FILE_SIZE == FILE_SIZE) wrap(
                    builder,
                    file.size.format("#.##").toString()
                )
                if (mask and FILE_MIMETYPE == FILE_MIMETYPE)
                    item.mime.parseThemeType()?.let { wrap(builder, it) }
                if (mask and FILE_LMT == FILE_LMT) wrap(builder, item.lastModifiedTime.toString())
                if (mask and FILE_LAT == FILE_LAT) wrap(builder, item.lastAccessTime.toString())
                if (mask and FILE_CT == FILE_CT) wrap(builder, item.creationTime.toString())
                if (mask and FILE_HIDDEN == FILE_HIDDEN) wrap(builder, item.isHidden)
                if (mask and FILE_ABSOLUTE == FILE_ABSOLUTE) wrap(builder, item.isPathAbsolute)
                if (mask and FILE_PERM == FILE_PERM) wrap(builder, item.permission)
            }
        }

        builder.toString()
    }

    private fun wrap(builder: StringBuilder, data: String) {
        if (builder.isNotEmpty()) builder.append(VortexTheme.texts.storageListItemInfoSeparatorKey.get())
        builder.append(data)
    }

    private fun wrap(builder: StringBuilder, data: Int) {
        if (builder.isNotEmpty()) builder.append(VortexTheme.texts.storageListItemInfoSeparatorKey.get())
        builder.append(data)
    }

    private fun wrap(builder: StringBuilder, data: Boolean) {
        if (builder.isNotEmpty()) builder.append(VortexTheme.texts.storageListItemInfoSeparatorKey.get())
        builder.append(data)
    }
}

fun PathItem.describe(
    mask: Int,
    descriptor: PathItemDescriptor = PathItemDescriptor(),
) = flow {
    descriptor.describeFlow(mask, this@describe).collect(::emit)
}

fun Flow<PathItem>.describe(
    mask: Int,
    descriptor: PathItemDescriptor = PathItemDescriptor(),
) = flow {
    collect { item ->
        emit(descriptor.describe(mask, item))
    }
}