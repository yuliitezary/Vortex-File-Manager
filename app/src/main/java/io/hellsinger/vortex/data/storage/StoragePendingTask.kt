package io.hellsinger.vortex.data.storage

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StoragePendingTask(
    val id: Long,
    val title: String,
    val description: String,
    val type: Int
) : Parcelable {
    object Type {
        const val Delete = 0
        const val Copy = 4
        const val Move = 8

        const val Pending = 16
    }
}