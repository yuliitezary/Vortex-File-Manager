package io.hellsinger.vortex.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import io.hellsinger.vortex.service.di.BindModule
import javax.inject.Scope

@Scope
annotation class StorageScope

@StorageScope
@Component(modules = [BindModule::class])
interface StorageComponent {

    // Hate dagger injecting process (lateinit, public property, etc.)
//    fun inject(controller: StorageScreenController)
//    fun inject(controller: BottomStorageOperationScreenController)
//    fun inject(controller: StorageCreateItemScreenController)
//    fun inject(controller: StorageInfoScreenController)
//    fun inject(controller: StorageItemInfoScreenController)
//    fun inject(controller: SettingsScreenController)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bindContext(context: Context): Builder

        fun build(): StorageComponent
    }
}