package io.hellsinger.vortex.di

import dagger.Binds
import dagger.Module
import io.hellsinger.vortex.provider.storage.AndroidStorageRepository
import io.hellsinger.vortex.provider.storage.BaseStorageSharedState
import io.hellsinger.vortex.provider.storage.SpeedStoragePathSegmentParser
import io.hellsinger.vortex.provider.storage.StoragePathSegmentParser
import io.hellsinger.vortex.provider.storage.StorageProgressEventReceiver
import io.hellsinger.vortex.provider.storage.StorageProgressEventSender
import io.hellsinger.vortex.provider.storage.StorageProgressEventSenderImpl
import io.hellsinger.vortex.provider.storage.StorageRepository
import io.hellsinger.vortex.provider.storage.StorageSharedState
import javax.inject.Singleton

@Module
abstract class BindModule {

    @Binds
    @Singleton
    abstract fun bindStorageSender(impl: StorageProgressEventSenderImpl): StorageProgressEventSender

    @Binds
    @Singleton
    abstract fun bindStorageReceiver(impl: StorageProgressEventSenderImpl): StorageProgressEventReceiver

    @Binds
    @Singleton
    abstract fun bindStorageSharedState(base: BaseStorageSharedState): StorageSharedState

    @Binds
    abstract fun bindStorageSegmentParser(impl: SpeedStoragePathSegmentParser): StoragePathSegmentParser

    @Binds
    abstract fun bindStorageRepository(impl: AndroidStorageRepository): StorageRepository

}