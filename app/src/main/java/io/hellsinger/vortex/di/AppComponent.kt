package io.hellsinger.vortex.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import io.hellsinger.vortex.ui.screen.settings.SettingsScreenController
import io.hellsinger.vortex.ui.screen.storage.StorageScreenController
import io.hellsinger.vortex.ui.screen.storage.editor.StorageFileEditorScreenController
import io.hellsinger.vortex.ui.screen.storage.info.StorageInfoScreenController
import io.hellsinger.vortex.ui.screen.storage.info.StorageItemInfoScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.create.StorageCreateItemScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.create.StorageCreateLinkItemScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.permission.StorageItemPermissionScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.rename.StorageRenameItemScreenController
import javax.inject.Singleton

@Component(modules = [BindModule::class, io.hellsinger.vortex.service.di.BindModule::class])
@Singleton
interface AppComponent {

    // Hate dagger injecting process (lateinit, public property, etc.)
    fun inject(controller: StorageScreenController)
    fun inject(controller: BottomStorageOperationScreenController)
    fun inject(controller: StorageCreateItemScreenController)
    fun inject(controller: StorageInfoScreenController)
    fun inject(controller: StorageItemInfoScreenController)
    fun inject(controller: SettingsScreenController)
    fun inject(controller: StorageRenameItemScreenController)
    fun inject(controller: StorageItemPermissionScreenController)
    fun inject(controller: StorageCreateLinkItemScreenController)
    fun inject(controller: StorageFileEditorScreenController)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bindContext(context: Context): Builder

        fun build(): AppComponent
    }

}