package io.hellsinger.vortex.utils

import android.view.View
import android.view.ViewGroup
import io.hellsinger.theme.Theme
import io.hellsinger.vortex.ui.component.udp
import kotlin.math.min

context (View)
@Suppress("FunctionName")
inline fun ViewMeasure(
    widthSpec: Int,
    heightSpec: Int,
    widthKey: Theme.Key<Int>,
    heightKey: Theme.Key<Int>,
    onResult: (width: Int, height: Int) -> Unit,
) {
    val widthSize = View.MeasureSpec.getSize(widthSpec)
    val widthMode = View.MeasureSpec.getMode(widthSpec)
    val heightSize = View.MeasureSpec.getSize(heightSpec)
    val heightMode = View.MeasureSpec.getMode(heightSpec)

    val desireWidth = widthKey.get().udp
    val desireHeight = heightKey.get().udp

    val width =
        if (desireWidth == ViewGroup.LayoutParams.MATCH_PARENT) widthSize else when (widthMode) {
            View.MeasureSpec.EXACTLY -> widthSize
            View.MeasureSpec.AT_MOST -> min(desireWidth, widthSize)
            else -> desireWidth
        } + paddingRight + paddingLeft

    val height =
        if (desireHeight == ViewGroup.LayoutParams.MATCH_PARENT) heightSize else when (heightMode) {
            View.MeasureSpec.EXACTLY -> heightSize
            View.MeasureSpec.AT_MOST -> min(desireHeight, heightSize)
            else -> desireHeight
        } + paddingTop + paddingBottom

    onResult(width, height)
}