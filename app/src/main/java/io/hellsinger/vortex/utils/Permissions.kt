package io.hellsinger.vortex.utils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Process

fun Context.isGranted(permission: String): Boolean {
    return checkPermission(
        permission,
        Process.myPid(),
        Process.myUid()
    ) == PackageManager.PERMISSION_GRANTED
}