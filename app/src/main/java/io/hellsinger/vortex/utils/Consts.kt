package io.hellsinger.vortex.utils

const val vortexPackageName = "io.hellsinger.vortex"
const val vortexServiceActionName = "io.hellsinger.VORTEX_SERVICE"

// Coroutines Name
const val StorageStateUpdateScope = "StorageStateUpdateScope"
const val SettingsStateUpdateScope = "SettingsStateUpdateScope"