package io.hellsinger.vortex.utils

import io.hellsinger.vortex.service.DispatcherProvider
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.SupervisorJob

inline val DispatcherProvider.storage
    get() = CoroutineName(StorageStateUpdateScope) + SupervisorJob() + immediate

inline val DispatcherProvider.settings
    get() = CoroutineName(SettingsStateUpdateScope) + SupervisorJob() + immediate
