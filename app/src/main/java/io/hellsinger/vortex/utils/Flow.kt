package io.hellsinger.vortex.utils

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.flow

fun <T> Flow<T>.throttle(periodMillis: Long = 1000 /* 1 sec */) = flow {
    conflate().collect { value ->
        emit(value)
        delay(periodMillis)
    }
}