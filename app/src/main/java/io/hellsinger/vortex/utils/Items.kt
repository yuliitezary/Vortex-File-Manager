package io.hellsinger.vortex.utils

import android.graphics.drawable.Drawable
import io.hellsinger.theme.Theme
import io.hellsinger.vortex.ui.component.ItemFactory
import io.hellsinger.vortex.ui.component.Items.toBehaviorSettingsItem
import io.hellsinger.vortex.ui.component.Items.toColorSettingsItem
import io.hellsinger.vortex.ui.component.Items.toDimenSettingsItem
import io.hellsinger.vortex.ui.component.Items.toTextSettingsItem
import io.hellsinger.vortex.ui.component.list.adapter.Item

fun MutableList<Item<*>>.title(content: String, index: Int = size) =
    add(index, ItemFactory.Title(content))

fun MutableList<Item<*>>.title(key: Theme.Key<String>, index: Int = size) =
    add(index, ItemFactory.Title(key))

fun MutableList<Item<*>>.drawer(
    id: Int,
    title: String,
    icon: Drawable?,
    index: Int = size
) = add(index, ItemFactory.Drawer(id, title, icon))

fun MutableList<Item<*>>.drawer(
    id: Int,
    key: Theme.Key<String>,
    icon: Drawable?,
    index: Int = size,
) = add(index, ItemFactory.Drawer(id, key, icon))

fun MutableList<Item<*>>.divider() = add(ItemFactory.Divider)

fun MutableList<Item<*>>.colorSettingsOption(key: Theme.Key<*>) = add(key.toColorSettingsItem())
fun MutableList<Item<*>>.textSettingsOption(key: Theme.Key<*>) = add(key.toTextSettingsItem())
fun MutableList<Item<*>>.dimenSettingsOption(key: Theme.Key<*>) = add(key.toDimenSettingsItem())
fun MutableList<Item<*>>.behaviorSettingsOption(key: Theme.Key<*>) =
    add(key.toBehaviorSettingsItem())

fun MutableList<Item<*>>.twoLine(
    title: String,
    desc: String,
    index: Int = size,
) = add(index, ItemFactory.TwoLine(title, desc))
