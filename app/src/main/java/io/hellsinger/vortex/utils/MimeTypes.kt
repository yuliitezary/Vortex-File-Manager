package io.hellsinger.vortex.utils

import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_APPLICATION
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_AUDIO
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_IMAGE
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_TEXT
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_VIDEO
import io.hellsinger.vortex.filesystem.attribute.mimetype.MimeType

fun MimeType.parseThemeType(): String? {
    return when (type) {
        MIME_TYPE_APPLICATION -> VortexTheme.texts.storageListItemMimeTypeApplicationKey.get()
        MIME_TYPE_VIDEO -> VortexTheme.texts.storageListItemMimeTypeVideoKey.get()
        MIME_TYPE_IMAGE -> VortexTheme.texts.storageListItemMimeTypeImageKey.get()
        MIME_TYPE_TEXT -> VortexTheme.texts.storageListItemMimeTypeTextKey.get()
        MIME_TYPE_AUDIO -> VortexTheme.texts.storageListItemMimeTypeAudioKey.get()
        else -> null
    }
}