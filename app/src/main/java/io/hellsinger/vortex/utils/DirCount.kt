package io.hellsinger.vortex.utils

import io.hellsinger.theme.vortex.Formatter
import io.hellsinger.theme.vortex.VortexTheme

fun DirCount(count: Int) = when (count) {
    -1 -> VortexTheme.texts.storageListItemEmptyKey.get()
    0 -> VortexTheme.texts.storageListItemEmptyKey.get()
    1 -> VortexTheme.texts.storageListItemCountKey.get()
    else -> Formatter(VortexTheme.texts.storageListItemsCountKey, count)
}