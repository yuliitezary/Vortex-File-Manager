package io.hellsinger.vortex.utils

import android.graphics.drawable.Drawable
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_BLOCK_DEVICE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_CHAR_DEVICE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_DIRECTORY
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_FILE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_LINK
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_PIPE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_SOCKET
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_AUDIO
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_IMAGE
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_TEXT
import io.hellsinger.vortex.filesystem.attribute.mimetype.MIME_TYPE_VIDEO
import io.hellsinger.vortex.ui.icon.Icons

val PathItem.icon: Drawable?
    get() = when (type) {
        PATH_ATTR_TYPE_FILE -> when (mime.type) {
            MIME_TYPE_VIDEO -> Icons.Rounded.Video
            MIME_TYPE_IMAGE -> Icons.Rounded.Image
            MIME_TYPE_AUDIO -> Icons.Rounded.Audio
            MIME_TYPE_TEXT -> Icons.Rounded.Text
            else -> Icons.Rounded.File
        }

        PATH_ATTR_TYPE_DIRECTORY -> Icons.Rounded.Directory
        PATH_ATTR_TYPE_LINK -> Icons.Rounded.Link
        PATH_ATTR_TYPE_PIPE -> Icons.Rounded.Pipe
        PATH_ATTR_TYPE_SOCKET -> Icons.Rounded.Merge
        PATH_ATTR_TYPE_CHAR_DEVICE -> Icons.Rounded.Merge
        PATH_ATTR_TYPE_BLOCK_DEVICE -> Icons.Rounded.Publish
        else -> null
    }