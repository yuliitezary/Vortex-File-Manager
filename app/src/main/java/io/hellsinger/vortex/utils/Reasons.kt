package io.hellsinger.vortex.utils

import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController

fun StorageTransactionTextReason(reason: Int) = when (reason) {
    StorageTransactionController.REASON_EMPTY_PATH -> "Empty path"
    StorageTransactionController.REASON_NOT_WRITEABLE -> "Not writable"
    StorageTransactionController.REASON_NOT_EXISTED_PATH -> "Path doesn't exist"
    StorageTransactionController.REASON_ALREADY_EXISTED_PATH -> "Path already exists"
    StorageTransactionController.REASON_NOT_ABSOLUTE_PATH -> "Path is not absolute"
    else -> throw Throwable("Unsupported reason ($reason)")
}