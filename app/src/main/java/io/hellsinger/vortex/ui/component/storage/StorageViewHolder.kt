package io.hellsinger.vortex.ui.component.storage

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class StorageViewHolder(root: View) : RecyclerView.ViewHolder(root)