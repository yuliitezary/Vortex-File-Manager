package io.hellsinger.vortex.ui.component.storage

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.hellsinger.vortex.Bits.Companion.addFlag
import io.hellsinger.vortex.Bits.Companion.hasFlag
import io.hellsinger.vortex.Bits.Companion.removeFlag
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.data.PathItemDescriptor
import io.hellsinger.vortex.data.PathItemDescriptor.Companion.DIR_COUNT
import io.hellsinger.vortex.data.PathItemDescriptor.Companion.FILE_LMT
import io.hellsinger.vortex.data.PathItemDescriptor.Companion.FILE_MIMETYPE
import io.hellsinger.vortex.data.PathItemDescriptor.Companion.FILE_SIZE
import io.hellsinger.vortex.provider.storage.AndroidStorageRepository
import io.hellsinger.vortex.ui.component.ItemListener
import io.hellsinger.vortex.ui.component.list.adapter.SingleAdapter
import io.hellsinger.vortex.ui.component.storage.standard.linear.StandardStorageLinearCell
import io.hellsinger.vortex.utils.icon
import io.hellsinger.vortex.utils.isAndroidR
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class StorageAdapter(
    private val isSelected: (PathItem) -> Boolean = { false },
    private val isDisabled: (PathItem) -> Boolean = { false },
    private val listener: ItemListener,
    private val viewProvider: (ViewGroup) -> View = { parent -> StandardStorageLinearCell(parent.context) }
) : SingleAdapter<PathItem, StorageViewHolder>() {

    override fun createDiffer(new: List<PathItem>): DiffUtil.Callback = Differ(list, new)

    override fun getItemId(position: Int): Long = list[position].id

    override fun createViewHolder(parent: ViewGroup): StorageViewHolder {
        val view = viewProvider(parent)
        if (view is StandardStorageLinearCell) {
            view.onBindListener { v ->
                if (!(view.flag hasFlag UPDATE_ITEM_DISABLE))
                    listener.onItemClick(v, view.tag as Int)
            }
            view.onBindLongListener { v ->
                if (!(view.flag hasFlag UPDATE_ITEM_DISABLE))
                    return@onBindLongListener listener.onLongItemClick(v, view.tag as Int)
                false
            }
        }
        return StorageViewHolder(view)
    }

    fun update(position: Int, type: Int) {
        notifyItemChanged(position, type)
    }

    fun applyDisable(
        position: Int
    ) = update(position, ADD_DISABLEMENT)

    fun applyEnable(
        position: Int
    ) = update(position, REMOVE_DISABLEMENT)

    fun applySelect(
        position: Int
    ) = update(position, ADD_SELECTION)

    fun applyDeselect(
        position: Int
    ) = update(position, REMOVE_SELECTION)

    fun clearSelection() {
        for (i in list.indices) update(i, REMOVE_SELECTION)
    }

    override fun onBindViewHolder(holder: StorageViewHolder, position: Int) {
        val item = get(position)

        holder.itemView.tag = holder.bindingAdapterPosition
        with(holder.itemView as StandardStorageLinearCell) {
            title = item.name
            icon = item.icon
            if (isSelected(item)) {
                flag = flag addFlag UPDATE_ITEM_SELECTION addFlag UPDATE_WITHOUT_ANIMATION
            }
            if (isDisabled(item)) {
                flag = flag addFlag UPDATE_ITEM_DISABLE addFlag UPDATE_WITHOUT_ANIMATION
            }
            if (info.isNullOrEmpty()) {
                if (isAndroidR) {
                    val isObb = item.path == AndroidStorageRepository.RESTRICTED_NAME_OBB
                    val isData = item.path == AndroidStorageRepository.RESTRICTED_NAME_DATA
                    if (isObb || isData) {
                        info = "Restricted"
                        return
                    }
                }

                if (!item.isEmptyAttributes) {
                    info = "Getting info.."
                    CoroutineScope(Dispatchers.Main.immediate).launch {
                        info = PathItemDescriptor().describe(
                            DIR_COUNT or FILE_MIMETYPE or FILE_SIZE or FILE_LMT,
                            item,
                        )
                    }
                }
            }
        }
    }

    override fun onViewRecycled(holder: StorageViewHolder) {
        with(holder.itemView as StandardStorageLinearCell) {
            title = null
            icon = null
            info = null
            flag = 0
        }
    }

    override fun onBindViewHolder(
        holder: StorageViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) return onBindViewHolder(holder, position)

        payloads.onEach { payload ->
            if (payload !is Int) return
            with(holder.itemView as StandardStorageLinearCell) {
                when (payload) {
                    ADD_SELECTION -> flag = flag addFlag UPDATE_ITEM_SELECTION
                    REMOVE_SELECTION -> flag = flag removeFlag UPDATE_ITEM_SELECTION
                    ADD_DISABLEMENT -> flag = flag addFlag UPDATE_ITEM_DISABLE
                    REMOVE_DISABLEMENT -> flag = flag removeFlag UPDATE_ITEM_DISABLE
                }
            }
        }
    }

    class Differ(
        private val old: List<PathItem>,
        private val new: List<PathItem>,
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = old.size

        override fun getNewListSize(): Int = new.size

        override fun areItemsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ) = old[oldItemPosition].id == new[newItemPosition].id

        override fun areContentsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ): Boolean {
            val old = old[oldItemPosition]
            val new = new[newItemPosition]

            return old == new
        }
    }

    companion object {
        const val ADD_SELECTION = 1
        const val REMOVE_SELECTION = 2
        const val ADD_DISABLEMENT = 4
        const val REMOVE_DISABLEMENT = 8

        const val UPDATE_ITEM_SELECTION = 1
        const val UPDATE_ITEM_DISABLE = 2
        const val UPDATE_WITHOUT_ANIMATION = 4
    }
}