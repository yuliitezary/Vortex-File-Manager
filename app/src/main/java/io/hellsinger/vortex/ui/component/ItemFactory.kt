package io.hellsinger.vortex.ui.component

import android.graphics.drawable.Drawable
import io.hellsinger.theme.Theme
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.data.storage.StoragePendingTask
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.ui.component.item.button.ButtonItem
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItem
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.component.menu.MenuAction
import io.hellsinger.vortex.ui.component.settings.SettingsItem
import java.io.File

object ItemFactory {

    val Divider = object : Item<Unit> {

        override val id: Long
            get() = Long.MIN_VALUE

        override val value: Unit
            get() = Unit

        override val viewType: Int
            get() = Items.Divider
    }

    fun Title(value: String) = object : Item<String> {
        override val id: Long
            get() = value.hashCode().toLong()
        override val viewType: Int
            get() = Items.Title
        override val value: String
            get() = value
    }

    fun Title(key: Theme.Key<String>) = Title(key.get())

    fun Drawer(id: Int, title: String, icon: Drawable? = null) = object : Item<MenuAction> {
        override val id: Long
            get() = id.toLong()
        override val viewType: Int
            get() = Items.Drawer
        override val value: MenuAction = MenuAction(id, title, icon)
    }

    fun Drawer(
        id: Int,
        key: Theme.Key<String>,
        icon: Drawable? = null
    ) = Drawer(
        id = id,
        title = key.get(),
        icon = icon
    )

    fun TwoLine(title: String, description: String) = object : Item<TwoLineItem> {
        override val id: Long
            get() = title.hashCode().toLong()
        override val viewType: Int
            get() = Items.TwoLine
        override val value: TwoLineItem = TwoLineItem(title, description)
    }

    fun Button(id: Int, text: String) = object : Item<ButtonItem> {
        override val id: Long
            get() = id.toLong()

        override val viewType: Int
            get() = Items.Button

        override val value: ButtonItem = ButtonItem(id, text)
    }

    fun Settings(key: String, value: String) = object : Item<SettingsItem> {
        override val id: Long
            get() = value.hashCode().toLong()
        override val viewType: Int
            get() = Items.Settings
        override val value: SettingsItem = SettingsItem(key, value)
    }

    fun Path(path: Path) = PathItem(path)
    fun Path(path: String) = PathItem(path)
    fun Path(path: File) = PathItem(path)

    fun StoragePendingItem(
        id: Long,
        title: String,
        description: String,
        type: Int
    ) = object : Item<StoragePendingTask> {
        override val id: Long
            get() = id
        override val viewType: Int
            get() = Items.Task
        override val value: StoragePendingTask = StoragePendingTask(id, title, description, type)
    }

}