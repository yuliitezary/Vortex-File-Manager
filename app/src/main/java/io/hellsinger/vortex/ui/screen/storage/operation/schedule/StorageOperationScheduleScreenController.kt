package io.hellsinger.vortex.ui.screen.storage.operation.schedule

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout.LayoutParams
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout.LayoutParams.WRAP_CONTENT
import io.hellsinger.vortex.data.storage.StoragePendingItem
import io.hellsinger.vortex.ui.component.dsl.field
import io.hellsinger.vortex.ui.component.dsl.linear
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity

class StorageOperationScheduleScreenController(
    private val context: NavigationActivity
) : VortexScreenController() {

    private var root: LinearLayout? = null

    private var args: Args? = null

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onCreateViewImpl(context: Context): View {
        root = linear(context) {
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
            gravity = Gravity.BOTTOM
            field {
                hint = "Time"
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            }
            field {
                hint = "Date"
            }

        }

        return root!!
    }

    override fun onPrepareImpl() {

    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {

    }

    override fun onDestroyImpl() {
        root?.removeAllViews()
        root = null
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    fun setArgs(args: Args) {
        this.args = args
    }

    interface Args {
        class Item(val pending: StoragePendingItem) : Args
    }
}
