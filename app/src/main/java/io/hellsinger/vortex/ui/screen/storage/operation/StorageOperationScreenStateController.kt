package io.hellsinger.vortex.ui.screen.storage.operation

import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.data.storage.StorageCopyPendingItem
import io.hellsinger.vortex.data.storage.StorageDeletePendingItem
import io.hellsinger.vortex.data.storage.StorageMovePendingItem
import io.hellsinger.vortex.data.storage.StoragePendingItem
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.provider.storage.StorageProgressEventSender
import io.hellsinger.vortex.provider.storage.StorageSharedState
import io.hellsinger.vortex.provider.storage.notifyCreateEvent
import io.hellsinger.vortex.provider.storage.notifyDeleteEvent
import io.hellsinger.vortex.provider.storage.notifyFailureEvent
import io.hellsinger.vortex.provider.storage.notifyIdle
import io.hellsinger.vortex.provider.storage.notifyPermissionEvent
import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.service.VortexServiceBinder
import io.hellsinger.vortex.service.storage.VortexStorageController
import io.hellsinger.vortex.service.storage.transaction.CopyTransaction
import io.hellsinger.vortex.service.storage.transaction.CreateTransaction
import io.hellsinger.vortex.service.storage.transaction.DeleteTransaction
import io.hellsinger.vortex.service.storage.transaction.LinkTransaction
import io.hellsinger.vortex.service.storage.transaction.MoveTransaction
import io.hellsinger.vortex.service.storage.transaction.PermissionTransaction
import io.hellsinger.vortex.service.storage.transaction.RenameTransaction
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.CANCEL
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.PROCEED
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.REPLACE
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.SKIP
import io.hellsinger.vortex.ui.component.AdapterDataNavigation
import io.hellsinger.vortex.ui.component.ItemFactory
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.intent
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class StorageOperationScreenStateController @Inject constructor(
    private val dispatchers: DispatcherProvider,
    private val sender: StorageProgressEventSender,
    private val shared: StorageSharedState
) : BitStateController() {

    private val navigator = AdapterDataNavigation<Item<*>>()

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    private var lastTransaction: StorageTransactionController? = null

    private var serviceCollectingJob: Job? = null

    private var listener: Listener? = null
    private var controller: VortexStorageController? = null

    private var _data = MutableData()
    val data: Data
        get() = _data

    val pending: List<StoragePendingItem>
        get() = shared.pending.value

    fun bindListener(listener: Listener) {
        this.listener = listener
    }

    fun startListenService(service: StateFlow<VortexServiceBinder?>) = intent {
        coroutineScope {
            serviceCollectingJob = launch {
                service.collect { service ->
                    bindService(service?.storage)
                }
            }
        }
    }

    fun stopListenService() {
        serviceCollectingJob?.cancel()
        serviceCollectingJob = null
    }

    fun registerPending(pending: StoragePendingItem) = intent {
        shared.registerPending(pending)
    }

    fun unregisterPending(pending: StoragePendingItem) = intent {
        shared.unregisterPending(pending)
    }

    fun update(items: List<Item<*>>) {
        _data.content.clear()
        _data.content.addAll(items)
        updateEvent(UPDATE_OPTIONS)
    }

    fun navigate(items: List<Item<*>>) {
        update(navigator.navigate(items))
    }

    fun navigateBack(): Boolean {
        if (navigator.current <= 0) return false
        _data.content.clear()
        _data.content.addAll(navigator.navigateBack())
        updateEvent(UPDATE_OPTIONS)
        return true
    }

    fun bindService(service: VortexStorageController?) {
        this.controller = service
    }

    private suspend fun checkListenerConflictOrCancel(path: Path, reason: Int): Int {
        return listener?.onConflictPrepare(path, reason) ?: CANCEL
    }

    fun delete(pending: StorageDeletePendingItem) {
        val items = pending.src.map(PathItem::path)
        val transaction = DeleteTransaction(
            paths = items,
            onComplete = {
                unregisterPending(pending)
                sender.notifyIdle()
            },
            onConflictPrepare = ::checkListenerConflictOrCancel,
            onError = sender::notifyFailureEvent,
            onEvent = { event ->
                event as DeleteTransaction.DeleteEvent
                sender.notifyDeleteEvent(source = event.path, sources = items)
            },
        )

        prepareTransaction(transaction)
    }

    fun move(
        pending: StorageMovePendingItem,
        dest: Path
    ) {
        val items = pending.src.map(PathItem::path)
        val transaction = MoveTransaction(
            src = items,
            dest = dest,
            onConflictPrepare = ::checkListenerConflictOrCancel,
            onComplete = {
                unregisterPending(pending)
                sender.notifyIdle()
            },
            onError = sender::notifyFailureEvent,
            onEvent = { event ->
//                event as MoveTransaction.
            }
        )

        prepareTransaction(transaction)
    }

    fun copy(
        pending: StorageCopyPendingItem,
        dest: Path
    ) {
        val items = pending.src.map(PathItem::path)
        val transaction = CopyTransaction(
            src = items,
            dest = dest,
            onConflictPrepare = ::checkListenerConflictOrCancel,
            onComplete = {
                unregisterPending(pending)
                sender.notifyIdle()
            },
            onError = sender::notifyFailureEvent,
            onEvent = { event ->
                event as CopyTransaction.CopyEvent
            }
        )

        prepareTransaction(transaction)
    }

    fun create(
        source: Path,
        isDirectory: Boolean,
        mode: Int = 777
    ) {
        val transaction = CreateTransaction(
            path = source,
            isDirectory = isDirectory,
            mode = mode,
            onConflictPrepare = ::checkListenerConflictOrCancel,
            onError = sender::notifyFailureEvent,
            onComplete = sender::notifyIdle,
            onEvent = { event ->
                event as CreateTransaction.CreateEvent
                sender.notifyCreateEvent(source = event.path)
            }
        )

        prepareTransaction(transaction)
    }

    fun rename(source: Path, dest: Path) {
        val transaction = RenameTransaction(
            src = source,
            dest = dest,
            onComplete = sender::notifyIdle,
            onError = sender::notifyFailureEvent,
            onConflictPrepare = ::checkListenerConflictOrCancel,
            onEvent = { event ->
//                sender.notifyRenameEvent(source, dest)
            }
        )

        prepareTransaction(transaction)
    }

    fun permission(source: Path, destPermission: Int) {
        val transaction = PermissionTransaction(
            src = source,
            mode = destPermission,
            onComplete = sender::notifyIdle,
            onError = sender::notifyFailureEvent,
            onConflictPrepare = ::checkListenerConflictOrCancel,
            onEvent = { event ->
                sender.notifyPermissionEvent(source, destPermission)
            }
        )

        prepareTransaction(transaction)
    }

    fun link(source: Path, path: Path, isHard: Boolean = false) {
        val transaction = LinkTransaction(
            path = path,
            source = source,
            isHard = isHard,
            onConflictPrepare = ::checkListenerConflictOrCancel,
            onError = sender::notifyFailureEvent,
            onComplete = sender::notifyIdle,
            onEvent = { event ->

            },
        )

        prepareTransaction(transaction)
    }

    fun prepareTransaction(transaction: StorageTransactionController) = intent {
        val loading = launch {
            delay(1000)
            updateEvent(UPDATE_LOADING_TITLE addFlag REASON_PREPARING)
        }

        val states = withContext(dispatchers.io) {
            transaction.prepare()
        }

        val content = mutableListOf<Item<*>>()
        var skipped = 0
        var replace = 0
        var proceed = 0

        for (i in states.indices) {
            val state = states[i]

            if (state.mask hasFlag SKIP) skipped++
            if (state.mask hasFlag REPLACE) replace++
            if (state.mask hasFlag PROCEED) proceed++
        }

        when (transaction) {
            is CreateTransaction -> "Summary (Source (${states.size}) :Create)"
            is DeleteTransaction -> "Summary (Source (${states.size}) : Delete)"
            is CopyTransaction -> "Summary (Source (${states.size - 1}): Dest (1): Copy)"
            is MoveTransaction -> "Summary (Source (${states.size - 1}): Dest (1): Move)"
            is PermissionTransaction -> "Summary (Source (1): Permission)"
            is RenameTransaction -> "Summary (Source (1): Dest (1): Rename)"
            else -> null
        }?.let { summary ->
            content.add(ItemFactory.Title(summary))
        }

        if (proceed > 0) {
            content.add(
                ItemFactory.TwoLine(
                    title = "Items ($proceed)",
                    description = "$proceed will be proceed."
                )
            )
        }

        if (replace > 0) {
            content.add(
                ItemFactory.TwoLine(
                    title = "Replace ($replace)",
                    description = if (transaction is CreateTransaction) {
                        "$proceed will be replaced as empty"
                    } else {
                        "$proceed will be replaced."
                    }
                )
            )
        }

        if (skipped > 0) {
            content.add(
                ItemFactory.TwoLine(
                    title = "Skipped ($skipped)",
                    description = "$skipped marked as skipped."
                )
            )
        }

        content.add(ItemFactory.Button(ViewIds.Storage.Snack.ConfirmButtonId, "Confirm"))

        loading.cancel()

        lastTransaction = transaction

        navigate(content)
    }

    fun runLastRememberedTransaction() {
        val controller = controller ?: return
        val transaction = lastTransaction ?: return
        controller.registerTransaction(transaction).runTransaction(transaction)
        lastTransaction = null
    }

    override fun onEvent() {
        listener?.onEvent(this)
    }

    data class MutableData(
        override val content: MutableList<Item<*>> = mutableListOf()
    ) : Data

    interface Data {
        val content: List<Item<*>>
    }

    interface Listener {
        fun onEvent(controller: StorageOperationScreenStateController)

        suspend fun onConflictPrepare(path: Path, reason: Int): Int
    }

    companion object {
        const val UPDATE_OPTIONS = 0

        const val UPDATE_LOADING_TITLE = 1

        const val REASON_PREPARING = 3 shl 26

        const val UPDATE_NAVIGATE_BACK = 16
    }

}