package io.hellsinger.vortex.ui.screen.settings.dimen

import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.screen.settings.SettingsOptionsRenderer
import io.hellsinger.vortex.utils.dimenSettingsOption
import io.hellsinger.vortex.utils.title

class DimenSettingsOptionsRenderer : SettingsOptionsRenderer {

    override fun render(): List<Item<*>> = buildList {
        title("Navigation Bar")
        dimenSettingsOption(VortexTheme.dimens.navigationBarWidthKey)
        dimenSettingsOption(VortexTheme.dimens.navigationBarHeightKey)
        title("Storage (Trail)")
        dimenSettingsOption(VortexTheme.dimens.trailWidthKey)
        dimenSettingsOption(VortexTheme.dimens.trailHeightKey)
        dimenSettingsOption(VortexTheme.dimens.trailElevationKey)
        title("Storage (Trail:Item)")
        dimenSettingsOption(VortexTheme.dimens.trailItemHeightKey)
        dimenSettingsOption(VortexTheme.dimens.trailItemLeftPaddingKey)
        dimenSettingsOption(VortexTheme.dimens.trailItemRightPaddingKey)
        title("Storage (Item)")
        dimenSettingsOption(VortexTheme.dimens.storageListItemLinearWidthDimenKey)
        dimenSettingsOption(VortexTheme.dimens.storageListItemLinearHeightDimenKey)
        dimenSettingsOption(VortexTheme.dimens.storageListItemHorizontalTitlePaddingKey)
        dimenSettingsOption(VortexTheme.dimens.storageListItemHorizontalSubtitlePaddingKey)
        dimenSettingsOption(VortexTheme.dimens.storageListItemSurfaceElevationKey)
        dimenSettingsOption(VortexTheme.dimens.storageListItemSurfaceSelectedElevationKey)
        title("Storage (Loading)")
        dimenSettingsOption(VortexTheme.dimens.storageListLoadingWidthKey)
        dimenSettingsOption(VortexTheme.dimens.storageListLoadingHeightKey)
        title("Storage (SnackBar)")
        dimenSettingsOption(VortexTheme.dimens.storageListSnackBarElevationKey)
    }

}