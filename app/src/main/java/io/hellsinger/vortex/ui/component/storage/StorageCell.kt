package io.hellsinger.vortex.ui.component.storage

import android.graphics.drawable.Drawable
import io.hellsinger.theme.vortex.Formatter
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.data.PathItemDescriptor
import io.hellsinger.vortex.data.PathItemDescriptor.Companion.DIR_COUNT
import io.hellsinger.vortex.data.PathItemDescriptor.Companion.FILE_LMT
import io.hellsinger.vortex.data.PathItemDescriptor.Companion.FILE_MIMETYPE
import io.hellsinger.vortex.data.PathItemDescriptor.Companion.FILE_SIZE
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView
import io.hellsinger.vortex.utils.icon
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

interface StorageCell {

    var title: String?

    var info: String?

    var icon: Drawable?

    var flag: Int

    var isBookmarked: Boolean

}

interface RecyclableStorageCell : StorageCell, RecyclableView<PathItem> {

    override fun onBind(item: PathItem) {
        title = Formatter(
            VortexTheme.texts.storageListItemNameKey,
            item.name
        )
        icon = item.icon
        if (info.isNullOrEmpty()) {
            if (!item.isEmptyAttributes) {
                CoroutineScope(Dispatchers.Main.immediate).launch {
                    info = PathItemDescriptor().describe(
                        DIR_COUNT or FILE_MIMETYPE or FILE_SIZE or FILE_LMT,
                        item,
                    )
                }
            }
        }
    }

    override fun onUnbind() {
        title = null
        icon = null
        info = null
    }

}