package io.hellsinger.vortex.ui.component.settings

import androidx.recyclerview.widget.RecyclerView.ViewHolder

class SettingsViewHolder(val root: SettingsCellView) : ViewHolder(root)