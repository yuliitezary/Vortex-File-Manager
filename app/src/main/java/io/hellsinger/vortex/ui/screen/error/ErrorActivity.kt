package io.hellsinger.vortex.ui.screen.error

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.widget.TextView
import androidx.core.content.getSystemService
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.vortex.ui.component.dsl.button
import io.hellsinger.vortex.ui.component.dsl.frame
import io.hellsinger.vortex.ui.component.dsl.onClick
import io.hellsinger.vortex.ui.component.dsl.text
import io.hellsinger.vortex.utils.serializable

class ErrorActivity : Activity(), View.OnClickListener {

    private var root: FrameLayout? = null
    private var copy: Button? = null
    private var message: TextView? = null
    private val error by lazy(LazyThreadSafetyMode.NONE) {
        intent.extras?.serializable<Throwable>("error")
    }

    override fun onCreate(inState: Bundle?) {
        super.onCreate(inState)

        root = frame(this) {
            fitsSystemWindows = true
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)

            message = text {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
                isNestedScrollingEnabled = true
                isVerticalScrollBarEnabled = true
                text = this@ErrorActivity.error.toString()
            }

            copy = button {
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    gravity = Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL
                }
                text = "Copy stack trace"

                onClick(this@ErrorActivity)
            }
        }

        setContentView(root)
    }

    override fun onClick(v: View) {
        val error = error ?: return
        if (error is ErrnoException) return copyErrno(error)

        copyThrowable(error)
    }

    private fun copyErrno(error: ErrnoException) {
        Log.e("Loggable", error.message!!)
        val clipboard = getSystemService<ClipboardManager>()

        val clip = ClipData.newPlainText("vortex_stack_trace", error.message)

        clipboard?.setPrimaryClip(clip)
    }

    private fun copyThrowable(error: Throwable) {
        val trace = error.fillInStackTrace().stackTraceToString()
        Log.e("Loggable", trace)
        val clipboard = getSystemService<ClipboardManager>()

        val clip = ClipData.newPlainText("vortex_stack_trace", trace)

        clipboard?.setPrimaryClip(clip)
    }

}