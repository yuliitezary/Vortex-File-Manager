package io.hellsinger.vortex.ui.component.settings

interface SettingsCell {
    var key: String
    var value: String
}