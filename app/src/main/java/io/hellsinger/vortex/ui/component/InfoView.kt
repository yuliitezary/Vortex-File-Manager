package io.hellsinger.vortex.ui.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme

class InfoView(context: Context) : LinearLayout(context), Theme.Listener {

    private val iconView = ImageView(context).apply {
        minimumHeight = 56.dp
        minimumWidth = 56.dp
        setColorFilter(VortexTheme.colors.layoutInfoIconTintColorKey.get())
    }

    private val messageView = TextView(context).apply {
        textSize = 20F
        textAlignment = TEXT_ALIGNMENT_CENTER
        setTextColor(VortexTheme.colors.layoutInfoTitleTextColorKey.get())
    }

    var icon: Drawable?
        get() = iconView.drawable
        set(value) {
            iconView.setImageDrawable(value)
        }

    var message: CharSequence?
        get() = messageView.text
        set(value) {
            if (value != messageView.text) messageView.text = value
        }

    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER
        addView(iconView, LayoutParams(WRAP_CONTENT, WRAP_CONTENT))
        addView(messageView, LayoutParams(MATCH_PARENT, WRAP_CONTENT))
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        VortexTheme.addThemeListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        VortexTheme.removeThemeListener(this)
    }

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {
        when (key) {
            VortexTheme.colors.layoutInfoIconTintColorKey -> {
                iconView.setColorFilter(value as Int)
            }

            VortexTheme.colors.layoutInfoTitleTextColorKey -> {
                messageView.setTextColor(value as Int)
            }
        }
    }

}