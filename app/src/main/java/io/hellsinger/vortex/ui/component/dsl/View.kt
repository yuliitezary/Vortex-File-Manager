package io.hellsinger.vortex.ui.component.dsl

import android.view.View

fun View.onClick(listener: View.OnClickListener): View.OnClickListener {
    setOnClickListener(listener)
    return listener
}