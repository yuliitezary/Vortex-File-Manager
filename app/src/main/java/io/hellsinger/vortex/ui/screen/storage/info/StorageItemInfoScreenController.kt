package io.hellsinger.vortex.ui.screen.storage.info

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.ItemListener
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItemView
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.screen.BitStateController.Companion.IDLE
import io.hellsinger.vortex.ui.screen.ListController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.storage.info.StorageItemInfoStateController.Companion.UPDATE_ITEMS
import javax.inject.Inject

class StorageItemInfoScreenController(
    private val context: NavigationActivity
) : ListController(), ItemListener, StorageItemInfoStateController.Listener {

    @Inject
    lateinit var controller: StorageItemInfoStateController

    private var args: PathItem? = null

    fun setArgs(item: PathItem) {
        args = item
    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onCreateViewImpl(context: Context): View {
        super.onCreateViewImpl(context)
        list!!.background = RoundedCornerDrawable(VortexTheme.colors.storageListBackgroundColorKey)
        return list!!
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        val statusBar = compat.getInsets(WindowInsetsCompat.Type.statusBars())
        val navigationBar = compat.getInsets(WindowInsetsCompat.Type.navigationBars())
        val bottomPadding = navigator?.bar?.height ?: navigationBar.bottom

        v.updatePadding(
            top = statusBar.top,
            bottom = bottomPadding
        )
        return insets
    }

    override fun onEvent(controller: StorageItemInfoStateController) = with(controller) {
        val event = controller.extractEvent()
        if (event == IDLE) return@with

        when (event) {
            UPDATE_ITEMS -> {
                navigator?.bar?.title = args?.name
                adapter.replace(data().content)
            }
        }
    }

    override fun onPrepareImpl() {
        context.component.inject(this)
        controller.bindListener(this)

        navigator?.bar?.replaceItems(emptyList())
        controller.load(args ?: return)
    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {

    }

    override fun onItemClick(view: View, position: Int) {
        when (view) {
            is TwoLineItemView -> {
                val item = args ?: return
                when (view.title) {
//                    "Permissions" -> {
//                        val screen = StorageItemPermissionScreenController(context)
//                        screen.setArgs(StorageItemPermissionScreenController.Args.Source(item))
//                        navigateForward(screen)
//                    }

//                    "Name" -> {
//                        val screen = StorageRenameItemScreenController(context)
//                        screen.setArgs(StorageRenameItemScreenController.Args.Source(item))
//                        navigateForward(screen)
//                    }
                }
            }
        }
    }

    override fun onLongItemClick(view: View, position: Int): Boolean {
        return false
    }

    override val id: Int = Navigation.StorageItemInfo

    override fun onDestroyImpl() {
        controller.onDestroy()
        list?.removeAllViews()
        list = null
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }
}