package io.hellsinger.vortex.ui.screen.storage

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import io.hellsinger.navigation.screen.HostScreenController
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.vortex.ui.component.dsl.frame
import io.hellsinger.vortex.ui.component.dsl.pager
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity

class StorageContainerScreenController(
    private val context: NavigationActivity
) : HostScreenController() {

    private val controllers = mutableListOf(
        StorageScreenController(context),
        StorageScreenController(context)
    )
    private var root: FrameLayout? = null

    override fun getControllerCount(): Int = controllers.size

    override fun getController(position: Int): ScreenController = controllers[position]

    override fun onPrepareController(position: Int, controller: ScreenController) {

    }

    override fun onHideController(position: Int, controller: ScreenController) {

    }

    override fun onCreateViewImpl(context: Context): View {
        root = frame(context) {
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)

            pager = pager {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
                adapter = this@StorageContainerScreenController.adapter
                addOnPageChangeListener(this@StorageContainerScreenController)
            }
        }
        return root!!
    }

    override fun onPrepareImpl() {

    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {

    }

    override fun onDestroyImpl() {
        adapter.destroy()
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }
}