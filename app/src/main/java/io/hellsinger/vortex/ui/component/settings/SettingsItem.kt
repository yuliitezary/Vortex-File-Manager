package io.hellsinger.vortex.ui.component.settings

data class SettingsItem(
    val key: String,
    val value: String
)