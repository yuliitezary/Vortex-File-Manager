package io.hellsinger.vortex.ui.screen

import io.hellsinger.vortex.Bits
import kotlinx.coroutines.cancel

abstract class BitStateController : StateController, Bits {

    protected open var event = -1

    fun extractEvent(): Int {
        val current = event
        event = IDLE
        return current
    }

    protected fun updateEventSilent(mask: Int): BitStateController {
        event = mask
        return this
    }

    protected fun updateEvent(mask: Int): BitStateController {
        event = mask
        onEvent()
        return this
    }

    protected abstract fun onEvent()

    override fun onDestroy() {
        scope.cancel()
    }

    companion object {
        const val IDLE = -1
    }

}