package io.hellsinger.vortex.ui.screen

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

interface StateController {

    val scope: CoroutineScope

    fun onDestroy()

}

fun StateController.intent(
    context: CoroutineContext = EmptyCoroutineContext,
    block: suspend CoroutineScope.() -> Unit,
) {
    scope.launch(
        context = context,
        block = block
    )
}