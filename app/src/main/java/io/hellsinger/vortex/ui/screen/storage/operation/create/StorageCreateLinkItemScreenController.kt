package io.hellsinger.vortex.ui.screen.storage.operation.create

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.view.Gravity
import android.view.View
import android.view.WindowInsets
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.LinearLayout.VERTICAL
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import io.hellsinger.filesystem.linux.resolve
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds.Storage
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.dsl.field
import io.hellsinger.vortex.ui.component.dsl.linear
import io.hellsinger.vortex.ui.component.item.field.TextFieldItemView
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.component.storage.StorageActions
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController.Operation.Link

class StorageCreateLinkItemScreenController(
    private val context: NavigationActivity
) : VortexScreenController(), TextFieldItemView.TextWatcher {

    lateinit var controller: StorageCreateLinkItemStateController

    private var args: Args? = null

    private var root: LinearLayout? = null
    private var name: TextFieldItemView? = null
    private var source: TextFieldItemView? = null

    override val id: Int
        get() = Navigation.StorageCreateLink

    fun setArgs(args: Args) {
        this.args = args
    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onCallIdImpl(identifier: Int): Boolean {
        return when (identifier) {
            Storage.Menu.CheckId -> {
                if (args !is Args.Source) return true
                val source = (args as Args.Source).item
                val name = name?.text ?: return true
                val resolved = source.path.parent?.resolve(name.toString()) ?: return true
                val screen = BottomStorageOperationScreenController(context)
                screen.setOperation(
                    operation = Link(
                        path = resolved,
                        source = source
                    )
                )
                navigateForward(screen)
                true
            }

            else -> false
        }
    }

    override fun onCreateViewImpl(context: Context): View {
        root = linear(context) {
            orientation = VERTICAL
            layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            updatePadding(left = 8.dp, right = 8.dp)
            gravity = Gravity.BOTTOM
            setBackgroundColor(VortexTheme.colors.storageListBackgroundColorKey.get())

            name = field {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                hint = "Link name"
                addTextWatcher(this@StorageCreateLinkItemScreenController)
//                observeInsets(this@StorageCreateLinkItemScreenController)
            }
            source = field {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                hint = "Source to link"
                observeInsets(this@StorageCreateLinkItemScreenController)
                addTextWatcher(this@StorageCreateLinkItemScreenController)
            }
        }
        return root!!
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        val ime = compat.getInsets(WindowInsetsCompat.Type.ime())
        val barHeight = navigator?.bar?.height ?: 0
        v.updatePadding(bottom = if (ime.bottom > barHeight) ime.bottom else barHeight)
        return insets
    }

    override fun afterTextChanged(view: TextFieldItemView, sequence: Editable) {

    }

    override fun beforeTextChanged(
        view: TextFieldItemView,
        sequence: CharSequence,
        start: Int,
        count: Int,
        after: Int
    ) {

    }

    override fun onTextChanged(
        view: TextFieldItemView,
        sequence: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {

    }

    override fun onPrepareImpl() {
        context.component.inject(this)

        navigator?.bar?.replaceItems(listOf(StorageActions.Validate))
        navigator?.bar?.title = "Create Link"
        if (args is Args.Source) source?.text = (args as Args.Source).item.path.toString()
    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {
        controller.unbindListener()
    }

    override fun onDestroyImpl() {
        controller.onDestroy()
        root?.removeAllViews()
        root = null
        name = null
        source = null
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    interface Args {
        class Source(val item: PathItem) : Args
    }
}