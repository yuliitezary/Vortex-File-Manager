package io.hellsinger.vortex.ui.component.item.drawer

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.component.menu.MenuAction
import io.hellsinger.vortex.utils.ViewMeasure

class DrawerItemView(
    context: Context,
) : FrameLayout(context), RecyclableView<MenuAction>, Theme.Listener {

    private val iconHorizontalPadding = 16.dp
    private val titleHorizontalPadding = 32.dp
    private val iconSize = 24.dp

    private val surface = RoundedCornerDrawable(
        key = VortexTheme.colors.drawerItemBackgroundColorKey,
        radius = 8F.dp,
        paddingLeft = 8.dp,
        paddingRight = 8.dp,
        paddingBottom = 4.dp,
        paddingTop = 4.dp
    )

    private val iconView = ImageView(context).apply {
        imageTintList = createIconColorStateList()
        layoutParams = LayoutParams(24.dp, 24.dp)
    }

    private val titleView = TextView(context).apply {
        textSize = 16F
        setTextColor(createTitleColorStateList())
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
    }

    var icon: Drawable?
        get() = iconView.drawable
        set(value) {
            iconView.setImageDrawable(value)
        }

    private val isEmptyIcon: Boolean
        get() = icon == null

    var title: CharSequence?
        get() = titleView.text
        set(value) {
            titleView.text = value
        }

    init {
        isClickable = true
        isFocusable = true

        addView(iconView)
        addView(titleView)

        background = surface
        foreground = createForegroundDrawable()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        VortexTheme.addThemeListener(listener = this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        VortexTheme.removeThemeListener(listener = this)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        ViewMeasure(
            widthMeasureSpec,
            heightMeasureSpec,
            VortexTheme.dimens.drawerItemWidthKey,
            VortexTheme.dimens.drawerItemHeightKey,
            ::setMeasuredDimension
        )

        measureChildren(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var widthLeft = iconHorizontalPadding
        if (!isEmptyIcon) {
            iconView.layout(
                widthLeft,
                (measuredHeight - iconView.measuredHeight) shr 1,
                widthLeft + iconView.measuredWidth,
                ((measuredHeight - iconView.measuredHeight) shr 1) + iconView.measuredHeight
            )
            widthLeft += iconSize
        }

        if (!isEmptyIcon) {
            widthLeft += titleHorizontalPadding
        }

        titleView.layout(
            widthLeft,
            (height - titleView.lineHeight) shr 1,
            widthLeft + titleView.measuredWidth,
            ((height - titleView.lineHeight) shr 1) + titleView.measuredHeight
        )
    }

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {
        when (key) {
            VortexTheme.colors.drawerItemIconTintColorKey -> iconView.setColorFilter(value as Int)
            VortexTheme.colors.drawerItemBackgroundColorKey -> surface.setColor(value as Int)
            VortexTheme.colors.drawerItemTitleTextColorKey -> titleView.setTextColor(value as Int)
        }
    }

    override fun onBind(item: MenuAction) {
        id = item.id
        icon = item.icon
        title = item.title
    }

    override fun onUnbind() {
        title = null
        icon = null
    }

    override fun onBindListener(listener: OnClickListener) {
        setOnClickListener(listener)
    }

    override fun onBindSelection(isSelected: Boolean) {
        setSelected(isSelected)
    }

    override fun onUnbindListeners() {
        setOnClickListener(null)
    }

    private fun createForegroundDrawable(): RippleDrawable {
        return RippleDrawable(
            createShapeColorStateList(),
            null,
            surface
        )
    }

    private fun createIconColorStateList(): ColorStateList {
        return ColorStateList(
            arrayOf(
                intArrayOf(android.R.attr.state_selected),
                intArrayOf()
            ),
            intArrayOf(
                VortexTheme.colors.drawerItemIconSelectedTintColorKey.get(),
                VortexTheme.colors.drawerItemIconTintColorKey.get()
            )
        )
    }

    private fun createShapeColorStateList(): ColorStateList {
        return ColorStateList(
            arrayOf(
                intArrayOf(android.R.attr.state_selected),
                intArrayOf()
            ),
            intArrayOf(
                VortexTheme.colors.drawerItemSelectedBackgroundColorKey.get(),
                VortexTheme.colors.drawerItemBackgroundRippleColorKey.get()
            )
        )
    }

    private fun createTitleColorStateList(): ColorStateList {
        return ColorStateList(
            arrayOf(
                intArrayOf(android.R.attr.state_selected),
                intArrayOf()
            ),
            intArrayOf(
                VortexTheme.colors.drawerItemTitleSelectedTextColorKey.get(),
                VortexTheme.colors.drawerItemTitleTextColorKey.get()
            )
        )
    }

}