package io.hellsinger.vortex.ui.component

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.hellsinger.vortex.data.storage.StoragePendingTask
import io.hellsinger.vortex.ui.component.item.button.ButtonItem
import io.hellsinger.vortex.ui.component.item.button.ButtonItemView
import io.hellsinger.vortex.ui.component.item.drawer.DrawerItemView
import io.hellsinger.vortex.ui.component.item.title.TitleItemView
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItem
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItemView
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.component.list.adapter.MultipleAdapter
import io.hellsinger.vortex.ui.component.list.adapter.diff.ItemDiffer
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.component.menu.MenuAction
import io.hellsinger.vortex.ui.component.settings.SettingsCellView
import io.hellsinger.vortex.ui.component.settings.SettingsItem
import io.hellsinger.vortex.ui.component.storage.task.StorageTaskItemView

open class ItemAdapter(
    private val listener: ItemListener
) : MultipleAdapter() {

    override fun createDiffer(new: List<Item<*>>): DiffUtil.Callback = ItemDiffer(list, new)

    override fun createView(
        parent: ViewGroup,
        viewType: Int
    ) = Items.resolveViewType(
        parent = parent,
        listener = listener,
        viewType = viewType
    )

    override fun onBindViewHolder(holder: MultipleViewHolder, position: Int) {
        val item = list[position]
        when (item.viewType) {
            Items.Drawer -> bind<DrawerItemView, MenuAction>(holder, item)
            Items.TwoLine -> bind<TwoLineItemView, TwoLineItem>(holder, item)
            Items.Title -> bind<TitleItemView, String>(holder, item)
            Items.Button -> bind<ButtonItemView, ButtonItem>(holder, item)
            Items.Settings -> bind<SettingsCellView, SettingsItem>(holder, item)
            Items.Task -> bind<StorageTaskItemView, StoragePendingTask>(holder, item)
        }
    }

    override fun onViewRecycled(holder: MultipleViewHolder) {
        when (holder.itemViewType) {
            Items.Drawer -> unbind<DrawerItemView, MenuAction>(holder)
            Items.TwoLine -> unbind<TwoLineItemView, TwoLineItem>(holder)
            Items.Title -> unbind<TitleItemView, String>(holder)
            Items.Button -> unbind<ButtonItemView, ButtonItem>(holder)
            Items.Settings -> unbind<SettingsCellView, SettingsItem>(holder)
            Items.Task -> unbind<StorageTaskItemView, StoragePendingTask>(holder)
        }
    }

    private inline fun <reified V, T> bind(
        holder: MultipleViewHolder,
        item: Item<*>
    ) where V : View, V : RecyclableView<T> {
        val root = holder.view as V
        root.tag = holder.bindingAdapterPosition

        root.onBind(item.value as T)
    }

    private inline fun <reified V : RecyclableView<T>, T> unbind(
        holder: MultipleViewHolder
    ) {
        val root = holder.view as V

        root.onUnbind()
    }


}