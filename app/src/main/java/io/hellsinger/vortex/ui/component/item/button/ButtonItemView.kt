package io.hellsinger.vortex.ui.component.item.button

import android.content.Context
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Button
import android.widget.FrameLayout
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView

class ButtonItemView(context: Context) : Button(context), RecyclableView<ButtonItem> {

    init {
        layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
            leftMargin = 8.dp
            rightMargin = 8.dp
            bottomMargin = 8.dp
        }
    }

    override fun onBind(item: ButtonItem) {
        id = item.id
        text = item.text
    }

    override fun onBindListener(listener: OnClickListener) {
        setOnClickListener(listener)
    }

    override fun onBindLongListener(listener: OnLongClickListener) {
        setOnLongClickListener(listener)
    }

}