package io.hellsinger.vortex.ui.screen.storage.operation.rename

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.view.Gravity
import android.view.View
import android.view.WindowInsets
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams
import android.widget.LinearLayout.LayoutParams.WRAP_CONTENT
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import io.hellsinger.filesystem.linux.resolve
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds.Storage
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.dsl.field
import io.hellsinger.vortex.ui.component.dsl.linear
import io.hellsinger.vortex.ui.component.item.field.TextFieldItemView
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.component.storage.StorageActions
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController.Operation.Rename

class StorageRenameItemScreenController(
    private val context: NavigationActivity
) : VortexScreenController(), TextFieldItemView.TextWatcher {

    lateinit var controller: StorageRenameItemStateController

    private var root: LinearLayout? = null
    private var source: TextFieldItemView? = null

    private var args: Args? = null

    override val id: Int
        get() = Navigation.StorageRename

    fun setArgs(args: Args) {
        this.args = args
    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onCallIdImpl(identifier: Int): Boolean {
        return when (identifier) {
            Storage.Menu.CheckId -> {
                if (args !is Args.Source) return true
                val source = (args as Args.Source).item
                val name = this.source?.text?.toString() ?: return true
                val dest = source.parent?.path?.resolve(name) ?: return true

                val screen = BottomStorageOperationScreenController(context)
                screen.setOperation(
                    operation = Rename(
                        source = source,
                        dest = PathItem(dest)
                    )
                )
                navigateForward(screen)
                true
            }

            else -> false
        }
    }

    override fun onCreateViewImpl(context: Context): View {
        root = linear(context) {
            layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.BOTTOM
            setBackgroundColor(VortexTheme.colors.storageListBackgroundColorKey.get())
            updatePadding(left = 8.dp, right = 8.dp)

            source = field {
                hint = "Destination"
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                setHintTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setFieldTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                observeInsets(this@StorageRenameItemScreenController)
            }
        }

        return root!!
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        val ime = compat.getInsets(WindowInsetsCompat.Type.ime())
        val barHeight = navigator?.bar?.height ?: 0
        root?.updatePadding(bottom = if (ime.bottom > barHeight) ime.bottom else barHeight)
        return insets
    }

    override fun onPrepareImpl() {
        if (args is Args.Source) {
            context.component.inject(this)
            val item = (args as Args.Source).item
            source?.text = item.name
            navigator?.bar?.title = "Rename ${item.name}"
            navigator?.bar?.replaceItems(listOf(StorageActions.Validate))
        }
    }

    override fun afterTextChanged(
        view: TextFieldItemView,
        sequence: Editable
    ) {

    }

    override fun onTextChanged(
        view: TextFieldItemView,
        sequence: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {

    }

    override fun beforeTextChanged(
        view: TextFieldItemView,
        sequence: CharSequence,
        start: Int,
        count: Int,
        after: Int
    ) {

    }

    override fun onFocusImpl() {
        source?.requestFocus()
    }

    override fun onHideImpl() {
        controller.unbindListener()
    }

    override fun onDestroyImpl() {
        controller.onDestroy()
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    interface Args {
        class Source(val item: PathItem) : Args
    }
}