package io.hellsinger.vortex.ui.screen

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import io.hellsinger.vortex.ui.component.ItemAdapter
import io.hellsinger.vortex.ui.component.ItemListener
import io.hellsinger.vortex.ui.component.dsl.recycler
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.navigation.VortexScreenController

abstract class ListController : VortexScreenController(), ItemListener {

    protected open val adapter = ItemAdapter(this)
    protected var list: RecyclerView? = null
    override fun onCreateViewImpl(context: Context): View {
        list = recycler(context) {
            adapter = this@ListController.adapter
            clipToPadding = false
            layoutManager = onCreateLayoutManager(context)
            observeInsets(this@ListController)
        }

        return list!!
    }

    protected open fun onCreateLayoutManager(context: Context): LayoutManager {
        return LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

}