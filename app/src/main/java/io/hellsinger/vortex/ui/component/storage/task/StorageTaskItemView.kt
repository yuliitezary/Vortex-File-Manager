package io.hellsinger.vortex.ui.component.storage.task

import android.content.Context
import android.content.res.ColorStateList.valueOf
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.text.TextUtils
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.TextView
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.Bits.Companion.removeFlag
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.data.storage.StoragePendingTask
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.icon.Icons
import io.hellsinger.vortex.utils.ViewMeasure

class StorageTaskItemView(
    context: Context
) : FrameLayout(context), Theme.Listener, RecyclableView<StoragePendingTask> {

    private val _icon: ImageView = ImageView(context).apply {
        layoutParams = LayoutParams(40.dp, 40.dp).apply {
            gravity = Gravity.START or Gravity.CENTER_VERTICAL
        }
        background = RoundedCornerDrawable(
            VortexTheme.colors.storageListItemIconBackgroundColorKey,
            50F.dp
        )
        scaleType = ImageView.ScaleType.CENTER_INSIDE
        imageTintList = valueOf(VortexTheme.colors.storageListItemIconTintColorKey.get())
    }

    private val _title: TextView = TextView(context).apply {
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {

        }
        textSize = 16F
        isSingleLine = true
        maxLines = 1
        ellipsize = TextUtils.TruncateAt.MARQUEE
        marqueeRepeatLimit = -1
        setTextColor(VortexTheme.colors.storageListItemTitleTextColorKey.get())
    }
    private val _desc: TextView = TextView(context).apply {
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {

        }
    }

    var title: String?
        get() = _title.text?.toString()
        set(value) {
            _title.text = value
        }

    var description: String?
        get() = _desc.text?.toString()
        set(value) {
            _desc.text = value
        }

    var icon: Drawable?
        get() = _icon.drawable
        set(value) {
            _icon.setImageDrawable(value)
        }

    private val surface = RoundedCornerDrawable(VortexTheme.colors.storageListItemSurfaceColorKey)

    init {
        id = ViewIds.Storage.Task.RootId
        isClickable = true
        isFocusable = true
        clipToOutline = true

        background = surface
        foreground = createRippleForeground()

        addView(_icon, 0)
        addView(_title, 1)
        addView(_desc, 2)
    }

    private fun createRippleForeground(): RippleDrawable {
        return RippleDrawable(
            valueOf(VortexTheme.colors.storageListItemSurfaceRippleColorKey.get()),
            null,
            surface
        )
    }

    override fun onBind(item: StoragePendingTask) {
        icon = when (item.type removeFlag StoragePendingTask.Type.Pending) {
            StoragePendingTask.Type.Delete -> Icons.Rounded.Delete
            StoragePendingTask.Type.Copy -> Icons.Rounded.Copy
            StoragePendingTask.Type.Move -> Icons.Rounded.FileMove
            else -> null
        }
        title = item.title
        description = item.description
    }

    override fun onBindListener(listener: OnClickListener) {
        setOnClickListener(listener)
    }

    override fun onBindLongListener(listener: OnLongClickListener) {
        setOnLongClickListener(listener)
    }

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        ViewMeasure(
            widthMeasureSpec,
            heightMeasureSpec,
            VortexTheme.dimens.storageListItemLinearWidthDimenKey,
            VortexTheme.dimens.storageListItemLinearHeightDimenKey,
            ::setMeasuredDimension
        )

        measureChildren(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var widthLeft = 16.dp
        _icon.layout(
            widthLeft,
            (measuredHeight - _icon.measuredHeight) shr 1,
            widthLeft + _icon.measuredWidth,
            ((measuredHeight - _icon.measuredHeight) shr 1) + _icon.measuredHeight
        )
        widthLeft += _icon.measuredWidth
        var titleY = 8.dp
        _title.layout(
            widthLeft + 24.dp,
            titleY,
            widthLeft + 24.dp + _title.measuredWidth,
            titleY + _title.measuredHeight
        )
        titleY += _title.measuredHeight
        _desc.layout(
            widthLeft + 24.dp,
            titleY,
            widthLeft + 24.dp + _desc.measuredWidth,
            titleY + _desc.measuredHeight
        )
    }
}