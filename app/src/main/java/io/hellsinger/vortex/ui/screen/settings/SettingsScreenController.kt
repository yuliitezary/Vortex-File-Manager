package io.hellsinger.vortex.ui.screen.settings

import android.os.Bundle
import io.hellsinger.navigation.screen.HostScreenController
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.vortex.component
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.settings.behavior.BehaviorSettingsOptionsRenderer
import io.hellsinger.vortex.ui.screen.settings.color.ColorSettingsOptionsRenderer
import io.hellsinger.vortex.ui.screen.settings.dimen.DimenSettingsOptionsRenderer
import io.hellsinger.vortex.ui.screen.settings.text.TextSettingsOptionsRenderer
import javax.inject.Inject

class SettingsScreenController(
    private val context: NavigationActivity,
) : HostScreenController() {

    @Inject
    lateinit var controller: SettingsStateController

    override val id: Int = Navigation.Settings

    override fun getControllerCount(): Int = SCREEN_COUNT

    override fun getController(position: Int): ScreenController {
        return when (position) {
            SCREEN_COLOR -> SettingsEntryScreenController(context, ColorSettingsOptionsRenderer())
            SCREEN_TEXT -> SettingsEntryScreenController(context, TextSettingsOptionsRenderer())
            SCREEN_DIMEN -> SettingsEntryScreenController(context, DimenSettingsOptionsRenderer())
            SCREEN_BEHAVIOR -> SettingsEntryScreenController(
                context,
                BehaviorSettingsOptionsRenderer()
            )

            else -> throw IllegalArgumentException("Illegal position for SettingsScreenController")
        }
    }

    override fun onPrepareImpl() {
        context.component.inject(this)
        context.navigation?.bar?.replaceItems(emptyList())
        context.navigation?.bar?.title = when (pager?.currentItem ?: return) {
            SCREEN_COLOR -> "Colors"
            SCREEN_TEXT -> "Texts"
            SCREEN_DIMEN -> "Dimens"
            SCREEN_BEHAVIOR -> "Behaviors"
            else -> throw IllegalArgumentException("Illegal position for SettingsScreenController")
        }
    }

    override fun onPrepareController(position: Int, controller: ScreenController) {

    }

    override fun onHideController(position: Int, controller: ScreenController) {

    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {

    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        val title = when (position) {
            SCREEN_COLOR -> "Colors"
            SCREEN_TEXT -> "Texts"
            SCREEN_DIMEN -> "Dimens"
            SCREEN_BEHAVIOR -> "Behaviors"
            else -> throw IllegalArgumentException("Illegal position for SettingsScreenController")
        }
        context.navigation?.bar?.setTitleWithAnimation(title)
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onDestroyImpl() {
        controller.onDestroy()
        adapter.destroy()
        pager?.removeAllViews()
        pager = null
    }

    companion object {
        const val SCREEN_COUNT = 4

        const val SCREEN_COLOR = 0
        const val SCREEN_TEXT = 1
        const val SCREEN_DIMEN = 2
        const val SCREEN_BEHAVIOR = 3
    }
}