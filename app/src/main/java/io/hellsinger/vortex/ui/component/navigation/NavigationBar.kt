package io.hellsinger.vortex.ui.component.navigation

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.res.ColorStateList.valueOf
import android.graphics.drawable.RippleDrawable
import android.text.TextUtils
import android.view.ViewGroup
import android.view.ViewPropertyAnimator
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.children
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.MaterialShapeDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.menu.MenuAction
import io.hellsinger.vortex.ui.component.menu.MenuActionListener
import io.hellsinger.vortex.ui.component.menu.MenuLayout
import io.hellsinger.vortex.utils.ViewMeasure

// TODO: Animate sliding title
class NavigationBar(context: Context) : FrameLayout(context), Theme.Listener {

    private val icon = NavigationIcon().apply {
        type = NavigationIcon.Type.CLOSE
    }

    private var currentAnimator: ViewPropertyAnimator? = null
    var state = 0
        private set

    private val listeners = mutableListOf<StateListener>()

    private val horizontalPadding = 16.dp
    private val verticalPadding = 16.dp
    private val titleHorizontalPadding = 32.dp
//    private var navBottomInsetPadding = 0

    private val background = MaterialShapeDrawable(
        VortexTheme.colors.barSurfaceColorKey.get()
    ).setElevation(4F.dp)

    private val navigationIconView = ImageView(context).apply {
        isClickable = true
        isFocusable = true
        layoutParams = LayoutParams(24.dp, 24.dp)
        background = RippleDrawable(
            valueOf(VortexTheme.colors.barNavigationIconTintColorKey.get()),
            null,
            null
        )
        setColorFilter(VortexTheme.colors.barNavigationIconTintColorKey.get())
        setImageDrawable(icon)
    }

    private val titleView = arrayOfNulls<TextView>(size = 2)

    private val subtitleView = TextView(context).apply {
        textSize = 14F
        setTextColor(VortexTheme.colors.barSubtitleTextColorKey.get())
    }

    private val menuView = MenuLayout(context).apply {

    }

    private val searchField = EditText(context).apply {
        hint = "Enter what you want to search"
    }

    val navigationIcon: NavigationIcon
        get() = icon

    var animatesTitleChanges: Boolean = true
    var animatesSubtitleChanges: Boolean = true

    private var textWidth = 0

    var title: CharSequence?
        get() = titleView.getOrNull(0)?.text
        set(value) {
            ensureContainingTitle(0)
            titleView[0]?.text = value
        }

    var subtitle: CharSequence?
        get() = subtitleView.text
        set(value) {
            if (value == null) {
                removeView(subtitleView)
                return
            }
            if (subtitle == value) return
            ensureContainingSubtitle()
            if (animatesSubtitleChanges) {
                subtitleView.animate().alpha(0F).setDuration(150L).withEndAction {
                    subtitleView.text = value
                    subtitleView.animate().alpha(1F).setDuration(150L).start()
                }.start()
            } else {
                subtitleView.text = value
            }
        }

    val containsTitle: Boolean
        get() = (titleView[0] ?: false) in children

    val containsSubtitle: Boolean
        get() = subtitleView in children

    val containsNavigationIcon: Boolean
        get() = navigationIconView in children

    val containsMenu: Boolean
        get() = menuView in children

    init {
        setBackground(background)
        elevation = 4F.dp
        ensureContainingNavigationIcon()
    }

    private fun createTitle(index: Int) {
        titleView[index] = TextView(context).apply {
            textSize = 18F
            ellipsize = TextUtils.TruncateAt.END
            setLines(1)
            setTextColor(VortexTheme.colors.barTitleTextColorKey.get())
            layoutParams = LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }

    private fun ensureContainingTitle(index: Int) {
        if (!containsTitle) {
            createTitle(index)
            addView(titleView[index])
        }
    }

    private fun ensureContainingSubtitle() {
        if (!containsSubtitle) addView(subtitleView)
    }


    private fun ensureContainingNavigationIcon() {
        if (!containsNavigationIcon) addView(navigationIconView)
    }

    private fun ensureContainingMenu() {
        if (!containsMenu) addView(menuView)
    }

    fun setNavigationClickListener(listener: OnClickListener?) {
        navigationIconView.setOnClickListener(listener)
    }

    fun addStateListener(listener: StateListener) {
        listeners.add(listener)
    }

    fun removeStateListener(listener: StateListener) {
        listeners.remove(listener)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        if (parent is ViewGroup) {
            (parent as ViewGroup).clipChildren = false
        }
    }

    fun show(animate: Boolean = true) {
        slideUp(animate)
    }

    fun hide(animate: Boolean = true) {
        slideDown(animate)
    }

    private fun slideUp(
        animate: Boolean = true,
    ) {
        if (state == STATE_SCROLLED_UP) return
        if (currentAnimator != null) {
            currentAnimator!!.cancel()
            clearAnimation()
        }
        state = STATE_SCROLLED_UP
        val targetTranslationY = 0
        if (animate) {
            animate()
                .setDuration(100L)
                .translationY(targetTranslationY.toFloat())
                .setUpdateListener { animator ->
                    listeners.forEach { listener -> listener.onSlide(animator.animatedFraction) }
                }
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        currentAnimator = null
                    }
                }).start()
        } else {
            translationY = targetTranslationY.toFloat()
        }
    }

    private fun slideDown(animate: Boolean = true) {
        if (state == STATE_SCROLLED_DOWN) return
        if (currentAnimator != null) {
            currentAnimator!!.cancel()
            clearAnimation()
        }

        val targetTranslationY = height
        state = STATE_SCROLLED_DOWN
        listeners.forEach { listener -> listener.onStateChanged(state) }
        if (animate) {
            animate()
                .setDuration(100L)
                .translationY(targetTranslationY.toFloat())
                .setUpdateListener { animator ->
                    listeners.forEach { listener -> listener.onSlide(animator.animatedFraction) }
                }
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        currentAnimator = null
                    }
                })
        } else {
            translationY = targetTranslationY.toFloat()
        }
    }

    fun addItem(action: MenuAction) {
        ensureContainingMenu()
        menuView.addItem(action)
    }

    fun replaceItems(actions: List<MenuAction>) {
        ensureContainingMenu()
        menuView.replaceItems(actions)
    }

    fun removeItem(action: MenuAction) {
        ensureContainingMenu()
        menuView.removeItem(action)
    }

    fun registerListener(listener: MenuActionListener) {
        menuView.addListener(listener)
    }

    fun unregisterListener(listener: MenuActionListener) {
        menuView.removeListener(listener)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        ViewMeasure(
            widthMeasureSpec,
            heightMeasureSpec,
            VortexTheme.dimens.navigationBarWidthKey,
            VortexTheme.dimens.navigationBarHeightKey
        ) { width, height ->
            setMeasuredDimension(
                width,
                height
            )

            if (containsNavigationIcon) measureChild(
                navigationIconView,
                widthMeasureSpec,
                heightMeasureSpec
            )

            if (containsTitle) titleView.forEach { view ->
                view?.let {
                    if (it.visibility != GONE) {
                        measureChild(
                            it,
                            widthMeasureSpec,
                            heightMeasureSpec
                        )
                        it.pivotX = 0F
                        it.pivotY = 1F
                    }
                }
            }

            if (containsSubtitle) measureChild(
                subtitleView,
                widthMeasureSpec,
                heightMeasureSpec
            )

            if (containsMenu) menuView.measure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST),
                56.dp
            )

        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        var widthPosition = horizontalPadding
        var textHeightPosition = 8.dp

        if (containsNavigationIcon) {
            navigationIconView.layout(
                widthPosition,
                verticalPadding,
                widthPosition + navigationIconView.measuredWidth,
                verticalPadding + navigationIconView.measuredHeight
            )
            widthPosition += navigationIconView.measuredWidth
        }

        if (containsNavigationIcon) widthPosition += titleHorizontalPadding
        if (!containsSubtitle) textHeightPosition *= 2

        if (containsTitle) {
            titleView.forEach { view ->
                view?.layout(
                    widthPosition,
                    textHeightPosition,
                    widthPosition + view.measuredWidth,
                    textHeightPosition + view.measuredHeight
                )
            }

            textHeightPosition += titleView[0]?.measuredHeight ?: 0
        }

        if (containsSubtitle) {
            subtitleView.layout(
                widthPosition,
                textHeightPosition,
                widthPosition + subtitleView.measuredWidth,
                textHeightPosition + subtitleView.measuredHeight
            )
        }

        widthPosition += textWidth

        if (containsMenu) {
            menuView.layout(
                0,
                0,
                menuView.measuredWidth,
                measuredHeight - paddingBottom
            )
        }
    }

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {
        when (key) {
            VortexTheme.colors.barSurfaceColorKey -> background.setTint(
                value as Int
            )

            VortexTheme.colors.barTitleTextColorKey -> {
                titleView[0]?.setTextColor(value as Int)
                titleView[1]?.setTextColor(value as Int)
            }

            VortexTheme.colors.barSubtitleTextColorKey -> subtitleView.setTextColor(
                value as Int
            )

            VortexTheme.colors.barNavigationIconTintColorKey -> navigationIconView.setColorFilter(
                value as Int
            )

        }
    }

    fun setTitleWithAnimation(
        title: CharSequence?,
        isVertical: Boolean = true,
        isReverse: Boolean = false,
        duration: Long = 150L,
    ) {
        if (title == this.title) return
        val isCrossfade = title.isNullOrEmpty()

        if (isCrossfade) {

        }

        if (titleView[1]?.parent != null) {
            (titleView[1]?.parent as ViewGroup).removeView(titleView[1])
            titleView[1] = null
        }
        titleView[1] = titleView[0]
        titleView[0] = null
        this.title = title
        titleView[0]?.alpha = 0F


        val a: ViewPropertyAnimator? = titleView[0]?.animate()?.alpha(1f)?.setDuration(
            duration
        )

        if (isVertical) a?.translationY(0F)
        else a?.translationX(0F)


        a?.start()

        val animator = titleView[1]?.animate()?.alpha(0F)
        if (!isCrossfade) {
            if (isVertical) {
                animator?.translationY(
                    if (isReverse) {
                        (-20F).dp
                    } else {
                        20F.dp
                    }
                )
            } else {
                animator?.translationX(
                    if (isReverse) {
                        (-20F).dp
                    } else {
                        20F.dp
                    }
                )
            }
        }
        animator?.setDuration(duration)?.setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                if (titleView[1]?.parent != null) {
                    (titleView[1]?.parent as ViewGroup).removeView(titleView[1])
                    titleView[1] = null
                }

                requestLayout()
            }
        })
        animator?.start()
        requestLayout()
    }

    interface StateListener {

        fun onSlide(factor: Float)

        fun onStateChanged(state: Int)

    }

    companion object {
        const val STATE_SCROLLED_DOWN = 1
        const val STATE_SCROLLED_UP = 2
    }
}