package io.hellsinger.vortex.ui.component

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable

class MaterialShapeDrawable(
    surfaceColor: Int = DEFAULT_SURFACE_COLOR,
    private val shadowColor: Int = DEFAULT_SHADOW_COLOR,
) : Drawable() {

    private val surface: Paint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG).apply {
        color = surfaceColor
    }

    private var elevation: Float = 0F

    var surfaceColor = surface.color
        set(value) {
            if (field != value) {
                field = value
                invalidateSelf()
            }
        }

    fun setElevation(elevation: Float): MaterialShapeDrawable {
        if (this.elevation != elevation) {
            surface.setShadowLayer(elevation, 0F, 0F, shadowColor)
            invalidateSelf()
        }
        return this
    }

    override fun draw(canvas: Canvas) {
        canvas.drawRect(bounds, surface)
    }

    override fun setAlpha(alpha: Int) {
        surface.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        if (surface.colorFilter != colorFilter) {
            surface.colorFilter = colorFilter
            invalidateSelf()
        }
    }

    @Deprecated(
        "Deprecated in Java",
        ReplaceWith("PixelFormat.UNKNOWN", "android.graphics.PixelFormat")
    )
    override fun getOpacity(): Int = PixelFormat.UNKNOWN

    companion object {
        const val DEFAULT_SURFACE_COLOR = 0xFFFFFFFF.toInt()
        const val DEFAULT_SHADOW_COLOR = 0x18000000
    }
}