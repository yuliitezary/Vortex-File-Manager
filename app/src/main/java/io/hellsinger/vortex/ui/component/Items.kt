package io.hellsinger.vortex.ui.component

import android.view.View
import android.view.ViewGroup
import io.hellsinger.theme.Theme
import io.hellsinger.vortex.ui.component.item.button.ButtonItemView
import io.hellsinger.vortex.ui.component.item.divider.DividerItemView
import io.hellsinger.vortex.ui.component.item.drawer.DrawerItemView
import io.hellsinger.vortex.ui.component.item.title.TitleItemView
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItem
import io.hellsinger.vortex.ui.component.item.twoline.TwoLineItemView
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.component.list.adapter.UnsupportedViewTypeException
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.component.settings.SettingsCellView
import io.hellsinger.vortex.ui.component.storage.standard.linear.StandardStorageLinearCell
import io.hellsinger.vortex.ui.component.storage.task.StorageTaskItemView
import io.hellsinger.vortex.ui.component.trail.TrailItemView

object Items {

    const val Title = 0
    const val Divider = 1
    const val Drawer = 2
    const val Switch = 3
    const val TwoLine = 4
    const val Button = 5

    const val Storage = 10
    const val Trail = 11
    const val Task = 12

    const val Settings = 20

    fun resolveViewType(
        parent: ViewGroup,
        listener: ItemListener,
        viewType: Int
    ): View {
        return when (viewType) {
            Title -> TitleItemView(parent.context)
            Divider -> DividerItemView(parent.context)

            Drawer -> wrapViewWithListener(DrawerItemView(parent.context), listener)
            TwoLine -> wrapViewWithListener(TwoLineItemView(parent.context), listener)
            Button -> wrapViewWithListener(ButtonItemView(parent.context), listener)

            Storage -> wrapViewWithListener(StandardStorageLinearCell(parent.context), listener)
            Trail -> wrapViewWithListener(TrailItemView(parent.context), listener)
            Task -> wrapViewWithListener(StorageTaskItemView(parent.context), listener)

            Settings -> wrapViewWithListener(SettingsCellView(parent.context), listener)

            else -> throw UnsupportedViewTypeException(viewType)
        }
    }

    private fun <V> wrapViewWithListener(
        root: V,
        listener: ItemListener
    ): V where V : View, V : RecyclableView<*> {

        root.onBindListener { view ->
            listener.onItemClick(view, root.tag as Int)
        }

        root.onBindLongListener { view ->
            listener.onLongItemClick(view, root.tag as Int)
        }

        return root
    }

    @OptIn(ExperimentalStdlibApi::class)
    fun Theme.Key<*>.toColorSettingsItem(): Item<TwoLineItem> {
        return ItemFactory.TwoLine(
            toString(),
            "#${(get() as Int).toHexString(HexFormat.UpperCase)}"
        )
    }

    fun Theme.Key<*>.toTextSettingsItem(): Item<TwoLineItem> {
        val value = get() as String
        val data = if (value.length > 30) "Tap to edit" else value
        return ItemFactory.TwoLine(toString(), data)
    }

    fun Theme.Key<*>.toDimenSettingsItem(): Item<TwoLineItem> {
        val value = get() as Number
        val data = if (value == -1 /* MATCH_PARENT */) "Match layout size" else value.toString()
        return ItemFactory.TwoLine(toString(), data)
    }

    fun Theme.Key<*>.toBehaviorSettingsItem(): Item<TwoLineItem> {
        val value = get() as Number
        val data = value.toString()
        return ItemFactory.TwoLine(toString(), data)
    }

}