package io.hellsinger.vortex.ui.screen.settings

import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.ui.screen.StateController
import io.hellsinger.vortex.utils.settings
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import javax.inject.Inject

class SettingsStateController @Inject constructor(
    val dispatchers: DispatcherProvider
) : StateController {

    override val scope: CoroutineScope = CoroutineScope(dispatchers.settings)

    override fun onDestroy() {
        scope.cancel()
    }

    companion object {
        const val IDLE = 1
        const val UPDATE_SCREEN = 0
        const val UPDATE_LIST = 1 shl 0
    }
}