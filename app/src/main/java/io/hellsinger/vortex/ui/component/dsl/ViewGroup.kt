package io.hellsinger.vortex.ui.component.dsl

import android.content.Context
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import io.hellsinger.vortex.ui.component.InfoView
import io.hellsinger.vortex.ui.component.LoadingView
import io.hellsinger.vortex.ui.component.StorageTextEditor
import io.hellsinger.vortex.ui.component.item.field.TextFieldItemView
import io.hellsinger.vortex.ui.component.trail.TrailListView

inline fun frame(context: Context, block: FrameLayout.() -> Unit): FrameLayout {
    return FrameLayout(context).apply(block)
}

inline fun linear(context: Context, block: LinearLayout.() -> Unit): LinearLayout {
    return LinearLayout(context).apply(block)
}

inline fun recycler(context: Context, block: RecyclerView.() -> Unit): RecyclerView {
    return RecyclerView(context).apply(block)
}

inline fun ViewGroup.linear(
    index: Int = -1,
    block: LinearLayout.() -> Unit
): LinearLayout {
    val linear = LinearLayout(context).apply(block)
    addView(linear, index)
    return linear
}

inline fun ViewGroup.pager(
    index: Int = -1,
    block: ViewPager.() -> Unit,
): ViewPager {
    val pager = ViewPager(context).apply(block)
    addView(pager, index)
    return pager
}

inline fun ViewGroup.trail(
    index: Int = -1,
    block: TrailListView.() -> Unit
): TrailListView {
    val trail = TrailListView(context).apply(block)
    addView(trail, index)
    return trail
}

inline fun ViewGroup.recycler(
    index: Int = -1,
    block: RecyclerView.() -> Unit,
): RecyclerView {
    val recycler = RecyclerView(context).apply(block)
    addView(recycler, index)
    return recycler
}

inline fun ViewGroup.text(
    index: Int = -1,
    block: TextView.() -> Unit,
): TextView {
    val text = TextView(context).apply(block)
    addView(text, index)
    return text
}

inline fun ViewGroup.button(
    index: Int = -1,
    block: Button.() -> Unit,
): Button {
    val button = Button(context).apply(block)
    addView(button, index)
    return button
}

inline fun ViewGroup.image(
    index: Int = -1,
    block: ImageView.() -> Unit
): ImageView {
    val image = ImageView(context).apply(block)
    addView(image, index)
    return image
}

inline fun ViewGroup.field(
    index: Int = -1,
    block: TextFieldItemView.() -> Unit
): TextFieldItemView {
    val field = TextFieldItemView(context).apply(block)
    addView(field, index)
    return field
}

inline fun ViewGroup.loading(
    index: Int = -1,
    block: LoadingView.() -> Unit
): LoadingView {
    val loading = LoadingView(context).apply(block)
    addView(loading, index)
    return loading
}

inline fun ViewGroup.info(
    index: Int = -1,
    block: InfoView.() -> Unit
): InfoView {
    val info = InfoView(context).apply(block)
    addView(info, index)
    return info
}

inline fun ViewGroup.check(
    index: Int = -1,
    block: CheckBox.() -> Unit
): CheckBox {
    val check = CheckBox(context).apply(block)
    addView(check, index)
    return check
}

inline fun ViewGroup.editor(
    index: Int = -1,
    block: StorageTextEditor.() -> Unit
): StorageTextEditor {
    val editor = StorageTextEditor(context).apply(block)
    addView(editor, index)
    return editor
}