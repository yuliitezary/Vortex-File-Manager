package io.hellsinger.vortex.ui.component.trail

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.ItemListener
import io.hellsinger.vortex.ui.component.list.adapter.SingleAdapter

class TrailAdapter(
    private val listener: ItemListener
) : SingleAdapter<PathItem, TrailViewHolder>() {

    var selected: Int = -1
        private set

    fun updateSelected(index: Int) {
        notifyItemChanged(index)
        val old = selected
        selected = index
        notifyItemChanged(old)
    }

    override fun getItemId(position: Int): Long = list[position].id

    override fun createDiffer(new: List<PathItem>): DiffUtil.Callback = Differ(list, new)

    override fun createViewHolder(
        parent: ViewGroup
    ): TrailViewHolder {
        val root = TrailItemView(parent.context)

        root.onBindListener { view ->
            listener.onItemClick(
                view,
                view.tag as Int
            )
        }

        root.onBindLongListener { view ->
            listener.onLongItemClick(
                view,
                view.tag as Int
            )
            false
        }

        return TrailViewHolder(root)
    }

    override fun onBindViewHolder(holder: TrailViewHolder, position: Int) {
        val item = list[position]
        with(holder.itemView as TrailItemView) {
            title = item.name
            tag = holder.bindingAdapterPosition

            onBindSelection(selected == position)
            isArrowVisible = position != list.lastIndex
        }
    }

    override fun getItemCount(): Int = list.size

    class Differ(
        private val old: List<PathItem>,
        private val new: List<PathItem>,
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = old.size

        override fun getNewListSize(): Int = new.size

        override fun areItemsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ) = old[oldItemPosition].id == new[newItemPosition].id

        override fun areContentsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ): Boolean {
            val old = old[oldItemPosition]
            val new = new[newItemPosition]
            return old == new
        }
    }
}