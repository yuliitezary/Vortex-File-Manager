package io.hellsinger.vortex.ui.screen.storage.editor

import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.data.editor.Line
import io.hellsinger.vortex.filesystem.size.SizeUnit
import io.hellsinger.vortex.provider.storage.StorageRepository
import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.intent
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

class StorageFileEditorStateController @Inject constructor(
    private val dispatchers: DispatcherProvider,
    private val repository: StorageRepository,
) : BitStateController() {

    private val actions: MutableStateFlow<Action> = MutableStateFlow(Action.Idle)

    override val scope: CoroutineScope =
        CoroutineScope(CoroutineName("File Editor Scope") + dispatchers.storage)

    private var listener: Listener? = null

    private var collectingActions: Job? = null

    fun actions(): StateFlow<Action> = actions

    private val data = MutableData()

    fun data(): Data = data

    fun bindListener(listener: Listener) {
        this.listener = listener
    }

    fun unbindListener() {
        this.listener = null
    }

    fun subscribeActions(collector: FlowCollector<Action>) {
        collectingActions = CoroutineScope(dispatchers.immediate + SupervisorJob()).launch {
            actions().collect(collector)
        }
    }

    fun unsubscribeActions() {
        collectingActions?.cancel()
        collectingActions = null
    }

    override fun onEvent() {
        listener?.handleEvent(this)
    }

    fun load(item: PathItem) = intent {
        val lines = data.lines
        lines.clear()
        val builder = StringBuilder()
        val sourceSize = item.attrs.size

        if (sourceSize > SizeUnit.create(5, SizeUnit.MiB)) {
            return@intent actions.emit(Action.Message("${item.name} is too long"))
        }

        val loading = launch {
            delay(1000L)
            actions.emit(Action.Loading)
        }

        repository.bytes(item)
            .onEach { byte ->
                val sym = byte.toInt().toChar()
                if (sym == '\n') {
                    builder.append(sym)
                    lines += Line(builder.toString())
                    builder.clear()
                    return@onEach
                }
                builder.append(sym)

            }.flowOn(dispatchers.io).collect()

        if (builder.isNotEmpty()) {
            lines += Line(builder.toString())
            builder.clear()
        }

        loading.cancel()
        actions.emit(Action.SetLines(lines))
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindListener()
        unsubscribeActions()
    }

    interface Listener {
        fun handleEvent(controller: StorageFileEditorStateController)
    }

    sealed interface Action {
        object Idle : Action

        object Loading : Action

        class Message(val text: String) : Action

        class AppendLine(val line: Line) : Action
        class SetLines(val lines: List<Line>) : Action
    }

    interface Data {
        val item: PathItem?
        val lines: List<Line>
        val encoding: String
    }

    private class MutableData(
        override val item: PathItem? = null,
        override var lines: MutableList<Line> = mutableListOf(),
        override val encoding: String = "UTF-8"
    ) : Data

}