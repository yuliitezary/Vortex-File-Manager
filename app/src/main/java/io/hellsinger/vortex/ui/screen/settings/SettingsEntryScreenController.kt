package io.hellsinger.vortex.ui.screen.settings

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.screen.ListController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity

class SettingsEntryScreenController(
    private val context: NavigationActivity,
    private val renderer: SettingsOptionsRenderer
) : ListController() {

    override fun onItemClick(view: View, position: Int) {

    }

    override fun onLongItemClick(view: View, position: Int): Boolean {
        return false
    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onCreateViewImpl(context: Context): View {
        super.onCreateViewImpl(context)

        list!!.background = RoundedCornerDrawable(VortexTheme.colors.storageListBackgroundColorKey)
        list!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) this@SettingsEntryScreenController.context.navigation?.bar?.hide()
                else this@SettingsEntryScreenController.context.navigation?.bar?.show()
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

            }
        })

        return list!!
    }

    override fun onPrepareImpl() {
        adapter.replace(renderer.render())
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        val statusBar = compat.getInsets(WindowInsetsCompat.Type.statusBars())
        val navigationBar = compat.getInsets(WindowInsetsCompat.Type.navigationBars())

        v.updatePadding(
            top = statusBar.top,
            bottom = navigationBar.bottom
        )
        return insets
    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {

    }

    override fun onDestroyImpl() {
        list?.removeAllViews()
        list = null
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }
}