package io.hellsinger.vortex.ui.screen.storage.operation

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.LayoutTransition
import android.animation.ValueAnimator
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.Bits.Companion.hasFlag
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.data.storage.StorageCopyPendingItem
import io.hellsinger.vortex.data.storage.StorageDeletePendingItem
import io.hellsinger.vortex.data.storage.StorageMovePendingItem
import io.hellsinger.vortex.data.storage.StoragePendingTask
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController
import io.hellsinger.vortex.ui.component.InfoView
import io.hellsinger.vortex.ui.component.ItemAdapter
import io.hellsinger.vortex.ui.component.ItemListener
import io.hellsinger.vortex.ui.component.LoadingView
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.dsl.frame
import io.hellsinger.vortex.ui.component.dsl.info
import io.hellsinger.vortex.ui.component.dsl.loading
import io.hellsinger.vortex.ui.component.dsl.recycler
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.ui.screen.BitStateController.Companion.IDLE
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.storage.StorageActionsRenderer
import io.hellsinger.vortex.ui.screen.storage.StorageScreenController
import io.hellsinger.vortex.ui.screen.storage.StorageScreenController.Args.NavDestination
import io.hellsinger.vortex.ui.screen.storage.info.StorageInfoScreenController
import io.hellsinger.vortex.ui.screen.storage.info.StorageItemInfoScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.StorageOperationScreenStateController.Companion.REASON_PREPARING
import io.hellsinger.vortex.ui.screen.storage.operation.StorageOperationScreenStateController.Companion.UPDATE_LOADING_TITLE
import io.hellsinger.vortex.ui.screen.storage.operation.StorageOperationScreenStateController.Companion.UPDATE_OPTIONS
import io.hellsinger.vortex.ui.screen.storage.operation.create.StorageCreateItemScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.create.StorageCreateLinkItemScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.permission.StorageItemPermissionScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.rename.StorageRenameItemScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.schedule.StorageOperationScheduleScreenController
import io.hellsinger.vortex.utils.StorageTransactionTextReason
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.coroutines.resume

class BottomStorageOperationScreenController(
    private val context: NavigationActivity,
) : VortexScreenController(), ItemListener, StorageOperationScreenStateController.Listener {

    override val usePopupMode: Boolean = true

    @Inject
    lateinit var controller: StorageOperationScreenStateController

    private var args: Operation? = null

    private var items: ArrayList<PathItem> = arrayListOf()

    private val adapter = ItemAdapter(this)
    private var animator: ValueAnimator? = null

    private var container: FrameLayout? = null
    private var options: RecyclerView? = null
    private var loading: LoadingView? = null
    private var info: InfoView? = null
    private var communication: Communication? = null

    private fun onOpCalled(identifier: Int) {
        when (identifier) {
            ViewIds.Storage.Menu.StorageInfoId -> {
                val item = items.singleOrNull() ?: return
                val screen = StorageInfoScreenController(context)
                screen.setArgs(StorageInfoScreenController.Args.Single(item))
                navigateForward(screen)
            }

            ViewIds.Storage.Menu.ChangePermissionsId -> {
                val item = items.singleOrNull() ?: return
                val screen = StorageItemPermissionScreenController(context)
                screen.setArgs(StorageItemPermissionScreenController.Args.Source(item))
                navigateForward(screen)
            }

            ViewIds.Storage.Snack.ConfirmButtonId -> {
                controller.runLastRememberedTransaction()
                navigateBackward()
            }

            ViewIds.Storage.Menu.DeleteId -> {
                controller.registerPending(StorageDeletePendingItem(items))
                communication?.onDeselectItems(items)
                navigateBackward()
            }

            ViewIds.Storage.Menu.OpenId -> {
                val item = items.singleOrNull() ?: return
                (navigator?.previousScreen as StorageScreenController).setArgs(NavDestination(item))
                communication?.onDeselectItems(items)
                navigateBackward()
            }

            ViewIds.Storage.Menu.CopyId -> {
                controller.registerPending(StorageCopyPendingItem(items))
                communication?.onDeselectItems(items)
                navigateBackward()
            }

            ViewIds.Storage.Menu.MoveId -> {
                controller.registerPending(StorageMovePendingItem(items))
                communication?.onDeselectItems(items)
                navigateBackward()
            }

            ViewIds.Storage.Menu.CopyPathId -> {
                context.service.value?.storage?.copyPathToClipboard(items.map(PathItem::path))
                communication?.onDeselectItems(items)
                navigateBackward()
            }

            ViewIds.Storage.Menu.AddNewId -> {
                val current = items.singleOrNull() ?: return
                val screen = StorageCreateItemScreenController(context)
                screen.setArgs(StorageCreateItemScreenController.Args.Parent(current))
                navigateForward(screen)
            }

            ViewIds.Storage.Menu.RenameId -> {
                val current = items.singleOrNull() ?: return
                val screen = StorageRenameItemScreenController(context)
                screen.setArgs(StorageRenameItemScreenController.Args.Source(current))
                navigateForward(screen)
            }

            ViewIds.Storage.Menu.AddSymbolicLinkId -> {
                val current = items.singleOrNull() ?: return
                val screen = StorageCreateLinkItemScreenController(context)
                screen.setArgs(StorageCreateLinkItemScreenController.Args.Source(current))
                navigateForward(screen)
            }

            ViewIds.Storage.Menu.InfoId -> {
                val current = items.singleOrNull() ?: return
                val screen = StorageItemInfoScreenController(context)
                screen.setArgs(current)
                navigateForward(screen)
            }
        }
    }

    private fun createSwitchAnimator(
        duration: Long = 150L
    ): ValueAnimator {
        if (animator != null) return animator!!

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = duration
        this.animator = animator
        return animator
    }

    private fun navigateLoading() {
        if (loading?.visibility == VISIBLE) return
        animator?.cancel()
        createSwitchAnimator()

        animator?.addUpdateListener { anim ->
            if (options?.visibility == VISIBLE) options?.alpha = 1F - anim.animatedFraction
            if (info?.visibility == VISIBLE) info?.alpha = 1F - anim.animatedFraction
            loading?.alpha = anim.animatedFraction
        }

        animator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationCancel(animation: Animator) {
                loading?.alpha = 1F
                loading?.visibility = VISIBLE
                options?.visibility = GONE
                info?.visibility = GONE
                animator = null
            }

            override fun onAnimationStart(animation: Animator) {
                loading?.visibility = GONE
            }

            override fun onAnimationEnd(animation: Animator) {
                loading?.alpha = 1F
                loading?.visibility = VISIBLE
                options?.visibility = GONE
                info?.visibility = GONE
                animator = null
            }
        })

        animator?.start()
    }

    private fun navigateInfo() {
        if (info?.visibility == VISIBLE) return
        animator?.cancel()
        createSwitchAnimator()

        animator?.addUpdateListener { anim ->
            if (options?.visibility == VISIBLE) options?.alpha = 1F - anim.animatedFraction
            if (loading?.visibility == VISIBLE) loading?.alpha = 1F - anim.animatedFraction
            info?.alpha = anim.animatedFraction
        }

        animator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationCancel(animation: Animator) {
                info?.alpha = 1F
                info?.visibility = VISIBLE
                options?.visibility = GONE
                loading?.visibility = GONE
                animator = null
            }

            override fun onAnimationStart(animation: Animator) {
                info?.visibility = GONE
            }

            override fun onAnimationEnd(animation: Animator) {
                info?.alpha = 1F
                info?.visibility = VISIBLE
                options?.visibility = GONE
                loading?.visibility = GONE
                animator = null
            }
        })

        animator?.start()
    }

    private fun navigateOptions() {
        if (options?.visibility == VISIBLE) return
        animator?.cancel()
        createSwitchAnimator()

        animator?.addUpdateListener { anim ->
            if (info?.visibility == VISIBLE) options?.alpha = 1F - anim.animatedFraction
            if (loading?.visibility == VISIBLE) loading?.alpha = 1F - anim.animatedFraction
            options?.alpha = anim.animatedFraction
        }

        animator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                options?.visibility = GONE
            }

            override fun onAnimationCancel(animation: Animator) {
                options?.alpha = 1F
                options?.visibility = VISIBLE
                info?.visibility = GONE
                loading?.visibility = GONE
                animator = null
            }

            override fun onAnimationEnd(animation: Animator) {
                options?.alpha = 1F
                options?.visibility = VISIBLE
                info?.visibility = GONE
                loading?.visibility = GONE
                animator = null
            }
        })

        animator?.start()
    }

    fun setOperation(operation: Operation) {
        this.args = operation
    }

    fun attachCommunication(communication: Communication) {
        check(this.communication == null)

        this.communication = communication
    }

    override fun onBackActivatedImpl(): Boolean {
        return controller.navigateBack()
    }

    override fun onCreateViewImpl(context: Context): View {
        container = frame(context) {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                gravity = Gravity.BOTTOM
            }
            layoutTransition = LayoutTransition()
            background = RoundedCornerDrawable(
                key = VortexTheme.colors.barSurfaceColorKey,
                topLeft = 16F.dp,
                topRight = 16F.dp
            )

            options = recycler(index = 0) {
                adapter = this@BottomStorageOperationScreenController.adapter
                layoutManager = LinearLayoutManager(context)
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            }

            info = info(1) {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT).apply {
                    gravity = Gravity.CENTER
                }
                visibility = GONE
            }

            loading = loading(index = 2) {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
                visibility = GONE
            }
        }

        return container!!
    }

    override fun onLongItemClick(view: View, position: Int): Boolean = when (view.id) {
        ViewIds.Storage.Task.RootId -> {
            val task = controller.data.content[position].value as StoragePendingTask
            val screen = StorageOperationScheduleScreenController(context)
            screen.setArgs(StorageOperationScheduleScreenController.Args.Item(controller.pending[task.id.toInt()]))
            navigateForward(screen)
            true
        }

        else -> false
    }

    override fun onItemClick(view: View, position: Int) {
        when (view.id) {
            ViewIds.Storage.Task.RootId -> {
                val task = controller.data.content[position].value as StoragePendingTask
                when (val pending = controller.pending[task.id.toInt()]) {
                    is StorageDeletePendingItem -> controller.delete(pending)
                    is StorageMovePendingItem -> controller.move(
                        pending,
                        dest = items.singleOrNull()?.path ?: return
                    )

                    is StorageCopyPendingItem -> controller.copy(
                        pending,
                        dest = items.singleOrNull()?.path ?: return
                    )
                }
            }

            else -> onOpCalled(view.id)
        }
    }

    override fun onPrepareImpl() {
        context.component.inject(this)
        controller.bindListener(this)
        when (args) {
            is Operation.ChooseOption -> {
                val chooseOption = args as Operation.ChooseOption
                items.addAll(chooseOption.items)
                controller.navigate(chooseOption.renderer.render())
            }

            is Operation.Create -> {
                val create = args as Operation.Create

                controller.create(create.path, create.isDirectory, create.mode)
            }

            is Operation.Copy -> {
                val copy = args as Operation.Copy

                controller.copy(StorageCopyPendingItem(copy.sources), copy.dest.path)
            }

            is Operation.Move -> {
                val move = args as Operation.Move

                controller.move(StorageMovePendingItem(move.sources), move.dest.path)
            }

            is Operation.Rename -> {
                val rename = args as Operation.Rename

                controller.rename(rename.source.path, rename.dest.path)
            }

            is Operation.Permission -> {
                val permission = args as Operation.Permission

                controller.permission(permission.source.path, permission.destPermission)
            }

            is Operation.Link -> {
                val link = args as Operation.Link

                controller.link(
                    source = link.source.path,
                    path = link.path,
                    isHard = link.isHard
                )
            }
        }
        controller.startListenService(context.service)
    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {
        controller.stopListenService()
    }

    override fun onDestroyImpl() {
        controller.onDestroy()
        container?.removeAllViews()
        container = null
        options = null
        loading = null
        info = null
    }

    override fun onEvent(controller: StorageOperationScreenStateController) {
        val event = controller.extractEvent()
        if (event == IDLE) return

        if (event hasFlag UPDATE_OPTIONS) {
            adapter.replace(controller.data.content)
            navigateOptions()
        }

        if (event hasFlag UPDATE_LOADING_TITLE) {
            if (event hasFlag REASON_PREPARING) {
                loading?.title = "Preparing..."
            } else {
                loading?.title = null
            }
            navigateLoading()
        }
    }

    override suspend fun onConflictPrepare(
        path: Path, reason: Int
    ): Int = withContext(Dispatchers.Main.immediate) {
        suspendCancellableCoroutine { continuation ->
            info?.message =
                "Detected problem with $path (${StorageTransactionTextReason(reason)})." +
                        "\nClick button below to skip"
            navigateInfo()
            continuation.resume(StorageTransactionController.ItemOperationState.SKIP)
        }
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onTransitionFactorImpl(factor: Float) {
        val container = container ?: return
        container.translationY = container.height * (1F - factor)
    }

    interface Communication {
        fun onDeselectItems(items: List<PathItem>)
    }

    interface Operation {
        class ChooseOption(
            val items: List<PathItem>,
            val renderer: StorageActionsRenderer
        ) : Operation

        class Create(
            val path: Path,
            val isDirectory: Boolean,
            val mode: Int = 777
        ) : Operation

        class Copy(
            val sources: List<PathItem>,
            val dest: PathItem
        ) : Operation

        class Move(
            val sources: List<PathItem>,
            val dest: PathItem
        ) : Operation

        class Rename(
            val source: PathItem,
            val dest: PathItem,
        ) : Operation

        class Link(
            val path: Path,
            val source: PathItem,
            val isHard: Boolean = false
        ) : Operation

        class Permission(
            val source: PathItem,
            val destPermission: Int,
        ) : Operation
    }
}