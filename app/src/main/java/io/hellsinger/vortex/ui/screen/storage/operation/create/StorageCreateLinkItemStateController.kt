package io.hellsinger.vortex.ui.screen.storage.operation.create

import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.intent
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class StorageCreateLinkItemStateController @Inject constructor(
    private val dispatchers: DispatcherProvider
) : BitStateController() {

    private var listener: Listener? = null

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    fun validateName(name: String) = intent {

    }

    fun valiadateSoruce(source: String) = intent {

    }

    fun bindListener(listener: Listener) {
        this.listener = listener
    }

    fun unbindListener() {
        this.listener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindListener()
    }

    override fun onEvent() {

    }

    interface Listener {
        fun handleEvent(controller: StorageCreateLinkItemScreenController)
    }
}