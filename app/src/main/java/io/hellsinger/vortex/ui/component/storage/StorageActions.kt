package io.hellsinger.vortex.ui.component.storage

import android.os.Build
import androidx.annotation.RequiresApi
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.ui.component.menu.MenuAction
import io.hellsinger.vortex.ui.icon.Icons

object StorageActions {

    inline val Validate
        get() = MenuAction(
            ViewIds.Storage.Menu.CheckId,
            "Validate",
            Icons.Rounded.Check
        )

    inline val Tasks
        get() = MenuAction(
            id = ViewIds.Storage.Menu.TasksId,
            title = "Show tasks",
            icon = Icons.Rounded.Tasks
        )

    inline val ProvideFullStorageAccess
        @RequiresApi(Build.VERSION_CODES.R)
        get() = MenuAction(
            id = ViewIds.Storage.Menu.ProvideFullStorageAccessId,
            title = VortexTheme.texts.storageListWarningFullStorageAccessActionTitleKey.get(),
            icon = Icons.Rounded.Check
        )

    inline val ProvideStorageAccess
        get() = MenuAction(
            id = ViewIds.Storage.Menu.ProvideStorageAccessId,
            title = VortexTheme.texts.storageListWarningStorageAccessActionTitleKey.get(),
            icon = Icons.Rounded.Check
        )

}