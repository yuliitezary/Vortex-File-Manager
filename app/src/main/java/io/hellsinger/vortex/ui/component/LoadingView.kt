package io.hellsinger.vortex.ui.component

import android.content.Context
import android.content.res.ColorStateList.valueOf
import android.view.Gravity.CENTER
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.children
import androidx.core.view.updatePadding
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme

class LoadingView(context: Context) : LinearLayout(context), Theme.Listener {

    private val progressView = ProgressBar(context).apply {
        isIndeterminate = true
        progressTintList = valueOf(VortexTheme.colors.layoutProgressBarTintColorKey.get())
    }
    private val titleView = TextView(context).apply {
        textSize = 18F
        setTextColor(VortexTheme.colors.layoutProgressTitleTextColorKey.get())
        textAlignment = TEXT_ALIGNMENT_CENTER
        layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        updatePadding(
            0,
            16.dp,
            0,
            0
        )
    }

    var title: CharSequence?
        get() = titleView.text
        set(value) {
            ensureContainingTitle()
            titleView.text = value
        }

    private val containsProgressView
        get() = progressView in children

    private val containsTitleView
        get() = titleView in children

    init {
        orientation = VERTICAL
        gravity = CENTER

        addView(progressView)
    }

    private fun ensureContainingTitle() {
        if (!containsTitleView) {
            addView(titleView)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        VortexTheme.addThemeListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        VortexTheme.removeThemeListener(this)
    }

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {
        when (key) {
//            VortexTheme.colors.layoutProgressBarTintColorKey -> progressView.setIndicatorColor(value as Int)
            VortexTheme.colors.layoutProgressTitleTextColorKey -> titleView.setTextColor(value as Int)
            VortexTheme.colors.layoutProgressBarBackgroundColorKey -> background.setTint(value as Int)
        }
    }

}