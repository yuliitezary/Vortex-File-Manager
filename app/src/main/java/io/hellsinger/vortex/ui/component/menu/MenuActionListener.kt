package io.hellsinger.vortex.ui.component.menu

fun interface MenuActionListener {
    fun onMenuActionCall(id: Int): Boolean
}