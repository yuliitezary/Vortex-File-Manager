package io.hellsinger.vortex.ui.screen.storage.info

import io.hellsinger.filesystem.linux.attribute.LinuxPathAttribute
import io.hellsinger.filesystem.linux.directory.LinuxDirectory
import io.hellsinger.filesystem.linux.directory.LinuxDirectoryEntry
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.directory.treeWalker
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_DIRECTORY
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_FILE
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.size.SiSize
import io.hellsinger.vortex.provider.storage.AndroidStorageRepository.Companion.RESTRICTED_NAME_DATA
import io.hellsinger.vortex.provider.storage.AndroidStorageRepository.Companion.RESTRICTED_NAME_OBB
import io.hellsinger.vortex.provider.storage.StorageRepository
import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.service.utils.restrictedTreeWalker
import io.hellsinger.vortex.ui.component.ItemFactory
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.component.list.adapter.SuperItem
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.intent
import io.hellsinger.vortex.utils.isAndroidR
import io.hellsinger.vortex.utils.parseThemeType
import io.hellsinger.vortex.utils.storage
import io.hellsinger.vortex.utils.throttle
import io.hellsinger.vortex.utils.title
import io.hellsinger.vortex.utils.twoLine
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.util.Locale
import javax.inject.Inject

class StorageItemInfoStateController @Inject constructor(
    private val dispatchers: DispatcherProvider,
    private val repository: StorageRepository
) : BitStateController() {

    private var listener: Listener? = null

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    private val data = MutableData()

    fun data(): Data = data

    private var jobs = mutableListOf<Job>()

    fun bindListener(listener: Listener?) {
        this.listener = listener
    }

    fun load(item: PathItem) = intent {
        with(data.content) {
            clear()

            title("General")
            twoLine("Index node", item.id.toString())
            twoLine("Name", item.name)
            twoLine("Path", item.path.toString())
            twoLine("Scheme", "unix")

            title("Attributes")
            twoLine("Permissions", item.permission)

            if (item.isDirectory) {
                val index = size

                jobs += launch {
                    val directory = item.asLinuxDirectory()

                    var size = 0L
                    var dirsCount = 0
                    var filesCount = 0

                    twoLine(
                        "Count",
                        "File: 0. Directory: 0",
                        index,
                    )
                    twoLine(
                        "Size",
                        "0B (0B)",
                        index + 1
                    )

                    getTreeWalker(directory)
                        .filterIsInstance<LinuxDirectoryEntry<*>>()
                        .onEach { entry ->
                            when (entry.attributes.type) {
                                PATH_ATTR_TYPE_DIRECTORY -> dirsCount++

                                PATH_ATTR_TYPE_FILE -> {
                                    filesCount++
                                    size += entry.asLinuxFile().size.original
                                }
                            }
                        }
                        .catch { error ->

                        }
                        .flowOn(dispatchers.io)
                        .throttle()
                        .onEach { _ ->
                            updateElement(
                                index,
                                ItemFactory.TwoLine(
                                    "Count",
                                    "File: $filesCount. Directory: $dirsCount"
                                )
                            )
                            updateElement(
                                index + 1,
                                ItemFactory.TwoLine(
                                    "Size",
                                    "${SiSize(size).format("#.##")} ($size bytes)"
                                )
                            )
                            updateEvent(UPDATE_ITEMS)
                        }.collect()

                    updateEvent(UPDATE_ITEMS)
                }
            }

            if (item.isFile) {
                val size = SiSize(item.attrs.size)

                twoLine(
                    "Size",
                    "${size.format("#.##")} (${size.original} bytes)",
                )

                title("Content")
                twoLine(
                    "Mimetype",
                    "${item.mime.extension}(${item.mime.parseThemeType()})"
                )
            }

            twoLine(
                "Last modified time",
                item.lastModifiedTime.toStringWithLocale(Locale.getDefault())
            )
            twoLine(
                "Last access time",
                item.lastAccessTime.toStringWithLocale(Locale.getDefault())
            )
            twoLine(
                "Creation time",
                item.creationTime.toStringWithLocale(Locale.getDefault())
            )

            if (item.attrs is LinuxPathAttribute) {
                jobs += launch {
                    val user = repository.user(item.attrs.userId)
                    title("User")

                    twoLine(
                        "Id",
                        user.id.toString()
                    )
                    twoLine(
                        "Name",
                        user.name.decodeToString()
                    )
                    twoLine(
                        "Home directory",
                        user.home.decodeToString()
                    )

                    val shell = user.shell.decodeToString()
                    if (shell.isNotEmpty()) {
                        twoLine(
                            "Shell",
                            shell
                        )
                    }
                }

                jobs += launch {
                    val group = repository.group(item.attrs.groupId)
                    title("Group")
                    twoLine(
                        "Id",
                        group.id.toString()
                    )
                    twoLine(
                        "Name",
                        group.name.decodeToString()
                    )
                }
                updateEvent(UPDATE_ITEMS)
            }
        }

        updateEvent(UPDATE_ITEMS)
    }

    private fun updateElement(index: Int, item: SuperItem) {
        data.content[index] = item
    }

    private fun getTreeWalker(directory: LinuxDirectory<Path>) = if (isAndroidR) {
        directory.restrictedTreeWalker(
            restrictedChecker = { path ->
                path == RESTRICTED_NAME_OBB || path == RESTRICTED_NAME_DATA
            }
        )
    } else {
        directory.treeWalker()
    }

    override fun onEvent() {
        listener?.onEvent(this)
    }

    data class MutableData(
        override val content: MutableList<Item<*>> = mutableListOf()
    ) : Data

    interface Data {
        val content: List<Item<*>>
    }

    interface Listener {
        fun onEvent(controller: StorageItemInfoStateController)
    }

    override fun onDestroy() {
        super.onDestroy()
        listener = null
    }

    companion object {
        const val UPDATE_ITEMS = 0
    }
}