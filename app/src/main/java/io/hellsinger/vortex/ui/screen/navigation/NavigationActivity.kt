package io.hellsinger.vortex.ui.screen.navigation

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.window.OnBackInvokedDispatcher.PRIORITY_DEFAULT
import androidx.core.graphics.ColorUtils
import androidx.core.view.WindowCompat
import androidx.core.view.WindowCompat.getInsetsController
import io.hellsinger.navigation.screen.NavScreenRestorer
import io.hellsinger.navigation.screen.ScreenController
import io.hellsinger.theme.Colors
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.service.ServiceActions
import io.hellsinger.vortex.service.VortexService
import io.hellsinger.vortex.service.VortexServiceBinder
import io.hellsinger.vortex.ui.component.menu.MenuActionListener
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.navigation.Navigation.Settings
import io.hellsinger.vortex.ui.navigation.Navigation.Storage
import io.hellsinger.vortex.ui.navigation.VortexNavigationController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class NavigationActivity : Activity(), Theme.Listener, MenuActionListener,
    ServiceConnection, NavScreenRestorer {

    var navigation: VortexNavigationController? = null
        private set

    private val _service = MutableStateFlow<VortexServiceBinder?>(null)
    val service: StateFlow<VortexServiceBinder?>
        get() = _service

    override fun onCreate(inState: Bundle?) {
        super.onCreate(inState)

        bindService(
            Intent(this, VortexService::class.java),
            this,
            BIND_AUTO_CREATE
        )

        WindowCompat.setDecorFitsSystemWindows(window, false)

        if (ColorUtils.calculateLuminance((VortexTheme.colors.barSurfaceColorKey.get())) > 0.5) {
            with(getInsetsController(window, window.decorView)) {
                isAppearanceLightNavigationBars = true
                isAppearanceLightStatusBars = true
            }
        } else {
            with(getInsetsController(window, window.decorView)) {
                isAppearanceLightNavigationBars = false
                isAppearanceLightStatusBars = false
            }
        }

        window.statusBarColor = Colors.Transparent
        window.navigationBarColor = Colors.Transparent

        navigation = VortexNavigationController(context = this, restorer = this)
        navigation?.createView()
        navigation?.bar?.setNavigationClickListener { view ->
            onMenuActionCall(ViewIds.Navigation.Menu.NavigationId)
        }
        navigation?.bar?.registerListener(this)

        setContentView(navigation!!.createView())

        if (inState != null) navigation?.restore(inState)
        else navigation?.init(Storage(context = this))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            onBackInvokedDispatcher.registerOnBackInvokedCallback(PRIORITY_DEFAULT) { onBackPressed() }
        }

        val intent = Intent(applicationContext, VortexService::class.java).apply {
            action = ServiceActions.Start
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else {
            startService(intent)
        }
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        CoroutineScope(Dispatchers.Main.immediate).launch {
            _service.emit(service as VortexServiceBinder?)
        }
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        CoroutineScope(Dispatchers.Main.immediate).launch {
            _service.emit(null)
        }
    }

    override fun onSaveInstanceState(buffer: Bundle) {
        navigation?.save(buffer)
    }

    override fun onBackPressed() {
        onBackInvoked()
    }

    private fun onBackInvoked() {
        if (navigation?.onBackActivated() == false) super.onBackPressed()
    }

    override fun onMenuActionCall(id: Int): Boolean {
        if ((navigation?.onCallId(id)) == true) return true
        if ((navigation?.currentScreen as? MenuActionListener)?.onMenuActionCall(id) == true) return true

        return when (id) {
            ViewIds.Navigation.Menu.NavigationId -> {
                if (navigation?.currentIndex == 0) navigation?.navigateForward(
                    Navigation.Navigator(
                        context = this
                    )
                )
                else navigation?.navigateBackward()
                true
            }

            else -> false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (navigation?.onActivityResult(requestCode, resultCode, data) == true) return
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onScreenRestore(context: Context, id: Int): ScreenController? {
        return when (id) {
            Settings -> Settings(this)
//            Host -> Host(this)
            Storage -> Storage(this)
            else -> null
        }
    }

    override fun onDestroy() {
        VortexTheme.removeThemeListener(this)
        unbindService(this)
        stopService(Intent(applicationContext, VortexService::class.java).apply {
            action = ServiceActions.Stop
        })
        navigation?.clear()
        navigation?.onDestroyView()
        navigation = null
        super.onDestroy()
    }

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {

    }
}