package io.hellsinger.vortex.ui.screen.storage.info

import io.hellsinger.filesystem.linux.attribute.fileSystemInfo
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.filesystem.size.SiSize
import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.ui.component.ItemFactory
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.intent
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.DecimalFormat
import javax.inject.Inject

class StorageInfoStateController @Inject constructor(
    private val dispatchers: DispatcherProvider
) : BitStateController() {

    private var listener: Listener? = null

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    private val _data = MutableData()
    val data: Data
        get() = _data

    fun bindListener(listener: Listener?) {
        this.listener = listener
    }

    fun update(item: PathItem) = intent {
        val content = _data.content

        val loading = launch {
            delay(1000L)
            updateEvent(UPDATE_LOADING)
        }

        content.clear()

        val info = withContext(dispatchers.io) { item.path.fileSystemInfo }

        content.add(ItemFactory.Title("Attributes"))

        content.add(
            ItemFactory.TwoLine(
                "Id",
                "${info.id}"
            )
        )

        content.add(
            ItemFactory.TwoLine(
                "Max path length",
                "${info.maxNameLength}"
            )
        )

        content.add(ItemFactory.Title("Attributes"))

        content.add(
            ItemFactory.TwoLine(
                "Read only",
                if (info.isReadOnly) "Yes" else "No"
            )
        )

        content.add(
            ItemFactory.TwoLine(
                "Access to device files",
                if (info.hasAccessToDevices) "Granted" else "Denied"
            )
        )

        content.add(
            ItemFactory.TwoLine(
                "Access to execute",
                if (info.hasAccessToExecute) "Granted" else "Denied"
            )
        )

        content.add(ItemFactory.Title("Spaces"))

        val formatter = DecimalFormat("#.##")

        val totalSize = withContext(dispatchers.default) {
            SiSize(info.totalSpace).format("#.##").toString()
        }

        content.add(
            ItemFactory.TwoLine(
                "Total space",
                "$totalSize (${info.totalSpace})"
            )
        )

        val usedSize = withContext(dispatchers.default) {
            SiSize(info.usedSpace).format("#.##").toString()
        }
        val usedSizePercent = info.usedSpace * 100.0 / info.totalSpace

        content.add(
            ItemFactory.TwoLine(
                "Used space",
                "$usedSize (${info.usedSpace}, ${formatter.format(usedSizePercent)}%)"
            )
        )

        val availableSize = withContext(dispatchers.default) {
            SiSize(info.availableSpace).format("#.##").toString()
        }
        val availableSizePercent = info.availableSpace * 100.0 / info.totalSpace

        content.add(
            ItemFactory.TwoLine(
                "Available space",
                "$availableSize (${info.availableSpace}, ${formatter.format(availableSizePercent)}%)"
            )
        )

        val privilegedSize = withContext(dispatchers.default) {
            SiSize(info.freeSpace - info.availableSpace).format("#.##").toString()
        }
        val privilegedSizePercent = (info.freeSpace - info.availableSpace) * 100.0 / info.totalSpace

        content.add(
            ItemFactory.TwoLine(
                "Privileged space",
                "$privilegedSize (${info.freeSpace - info.availableSpace}, ${
                    formatter.format(
                        privilegedSizePercent
                    )
                }%)"
            )
        )

        loading.cancel()

        updateEvent(UPDATE_INFO)
    }

    override fun onEvent() {
        listener?.onEvent(this)
    }

    interface Data {
        val content: List<Item<*>>
    }

    class MutableData(
        override val content: MutableList<Item<*>> = mutableListOf()
    ) : Data

    interface Listener {
        fun onEvent(controller: StorageInfoStateController)
    }

    companion object {
        const val UPDATE_INFO = 0
        const val UPDATE_LOADING = 1
    }

}