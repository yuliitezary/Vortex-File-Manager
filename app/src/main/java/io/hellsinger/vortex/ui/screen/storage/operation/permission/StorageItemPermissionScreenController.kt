package io.hellsinger.vortex.ui.screen.storage.operation.permission

import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.WindowInsets
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.updatePadding
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds.Storage
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.dsl.check
import io.hellsinger.vortex.ui.component.dsl.linear
import io.hellsinger.vortex.ui.component.dsl.text
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.component.storage.StorageActions
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.ui.screen.BitStateController.Companion.IDLE
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController.Operation.Permission
import io.hellsinger.vortex.ui.screen.storage.operation.permission.StorageItemPermissionStateController.Companion.UPDATE_PERMISSIONS
import io.hellsinger.vortex.ui.screen.storage.operation.permission.StorageItemPermissionStateController.Companion.UPDATE_VALIDATE_PERMISSIONS
import io.hellsinger.vortex.utils.parcelable
import javax.inject.Inject

class StorageItemPermissionScreenController(
    private val context: NavigationActivity
) : VortexScreenController(), StorageItemPermissionStateController.Listener,
    OnCheckedChangeListener {

    @Inject
    lateinit var controller: StorageItemPermissionStateController

    private var root: LinearLayout? = null

    override val id: Int = Navigation.StoragePermission

    private var args: Args? = null

    private var userPermTitle: TextView? = null
    private var userRead: CheckBox? = null
    private var userWrite: CheckBox? = null
    private var userExecute: CheckBox? = null

    private var groupPermTitle: TextView? = null
    private var groupRead: CheckBox? = null
    private var groupWrite: CheckBox? = null
    private var groupExecute: CheckBox? = null

    private var otherPermTitle: TextView? = null
    private var otherRead: CheckBox? = null
    private var otherWrite: CheckBox? = null
    private var otherExecute: CheckBox? = null

    fun setArgs(args: Args) {
        this.args = args
    }

    override fun onCallIdImpl(identifier: Int): Boolean = when (identifier) {
        Storage.Menu.CheckId -> {
            if (args is Args.Source) {
                val source = (args as Args.Source).item
                val destPermission = controller.data().mode

                val screen = BottomStorageOperationScreenController(context)
                screen.setOperation(
                    operation = Permission(
                        source = source,
                        destPermission = destPermission
                    )
                )
                navigateForward(screen)
            }

            true
        }

        else -> false
    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onCreateViewImpl(context: Context): View {
        root = linear(context) {
            layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.BOTTOM
            setBackgroundColor(VortexTheme.colors.drawerBackgroundColorKey.get())
            observeInsets(this@StorageItemPermissionScreenController)
            updatePadding(left = 8.dp, right = 8.dp)

            text {
                text = "Permission"
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 18F)
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            // Permission (mode)
            // User permission
            userPermTitle = text {
                text = "User permissions (0)"
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            linear {
                orientation = LinearLayout.HORIZONTAL

                userRead = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.UserReadId
                    text = "Read"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                userWrite = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.UserWriteId
                    text = "Write"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                userExecute = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.UserExecuteId
                    text = "Execute"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
            }

            // Group permission
            groupPermTitle = text {
                text = "Group permissions (0)"
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            linear {
                orientation = LinearLayout.HORIZONTAL

                groupRead = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.GroupReadId
                    text = "Read"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                groupWrite = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.GroupWriteId
                    text = "Write"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                groupExecute = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.GroupExecuteId
                    text = "Execute"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
            }

            // Other permission
            otherPermTitle = text {
                text = "Other permissions (0)"
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            linear {
                orientation = LinearLayout.HORIZONTAL

                otherRead = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.OtherReadId
                    text = "Read"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                otherWrite = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.OtherWriteId
                    text = "Write"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                otherExecute = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.OtherExecuteId
                    text = "Execute"
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    ).apply {
                        weight = 1F
                    }
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
            }
        }

        return root!!
    }

    override fun onCheckedChanged(view: CompoundButton, isChecked: Boolean) {
        when (view.id) {
            Storage.Create.UserReadId -> controller.updateUserReadPermission(isChecked)
            Storage.Create.UserWriteId -> controller.updateUserWritePermission(isChecked)
            Storage.Create.UserExecuteId -> controller.updateUserExecutePermission(isChecked)

            Storage.Create.GroupReadId -> controller.updateGroupReadPermission(isChecked)
            Storage.Create.GroupWriteId -> controller.updateGroupWritePermission(isChecked)
            Storage.Create.GroupExecuteId -> controller.updateGroupExecutePermission(isChecked)

            Storage.Create.OtherReadId -> controller.updateOtherReadPermission(isChecked)
            Storage.Create.OtherWriteId -> controller.updateOtherWritePermission(isChecked)
            Storage.Create.OtherExecuteId -> controller.updateOtherExecutePermission(isChecked)
        }
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets): WindowInsets {
        val barHeight = navigator?.bar?.height ?: 0

        root?.updatePadding(
            bottom = barHeight + context.dp(4)
        )
        return insets
    }

    override fun onPrepareImpl() {
        context.component.inject(this)
        controller.bindListener(this)
        if (args is Args.Source) {
            val source = (args as Args.Source).item
            navigator?.bar?.replaceItems(listOf(StorageActions.Validate))
            navigator?.bar?.title = "${source.name} (${source.permission})"
            controller.validateItem(source)

            userRead?.setOnCheckedChangeListener(this)
            userWrite?.setOnCheckedChangeListener(this)
            userExecute?.setOnCheckedChangeListener(this)

            groupRead?.setOnCheckedChangeListener(this)
            groupWrite?.setOnCheckedChangeListener(this)
            groupExecute?.setOnCheckedChangeListener(this)

            otherRead?.setOnCheckedChangeListener(this)
            otherWrite?.setOnCheckedChangeListener(this)
            otherExecute?.setOnCheckedChangeListener(this)
        }
    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {
        userRead?.setOnCheckedChangeListener(null)
        userWrite?.setOnCheckedChangeListener(null)
        userExecute?.setOnCheckedChangeListener(null)

        groupRead?.setOnCheckedChangeListener(null)
        groupWrite?.setOnCheckedChangeListener(null)
        groupExecute?.setOnCheckedChangeListener(null)

        otherRead?.setOnCheckedChangeListener(null)
        otherWrite?.setOnCheckedChangeListener(null)
        otherExecute?.setOnCheckedChangeListener(null)
    }

    override fun handleEvent(controller: StorageItemPermissionStateController) = with(controller) {
        val event = extractEvent()
        if (event == IDLE) return@with

        val mode = data().mode
        // First digit
        val userPerm = mode / 100
        // Middle digit
        val groupPerm = (mode - (userPerm * 100)) / 10
        // Last digit
        val otherPerm = (mode - (userPerm * 100) - (groupPerm * 10))

        when (event) {
            UPDATE_VALIDATE_PERMISSIONS -> {
                updateTextMode(userPerm, groupPerm, otherPerm)

                userRead?.isChecked = userPerm >= 1
                userWrite?.isChecked = userPerm >= 4
                userExecute?.isChecked = userPerm >= 7

                groupRead?.isChecked = groupPerm >= 1
                groupWrite?.isChecked = groupPerm >= 4
                groupExecute?.isChecked = groupPerm >= 7

                otherRead?.isChecked = otherPerm >= 1
                otherWrite?.isChecked = otherPerm >= 4
                otherExecute?.isChecked = otherPerm >= 7
            }

            UPDATE_PERMISSIONS -> updateTextMode(userPerm, groupPerm, otherPerm)
        }
    }

    private fun updateTextMode(userPerm: Int, groupPerm: Int, otherPerm: Int) {
        userPermTitle?.text = "User permissions (${userPerm})"
        groupPermTitle?.text = "Group permissions ($groupPerm)"
        otherPermTitle?.text = "Other permissions ($otherPerm)"
    }

    override fun onDestroyImpl() {
        controller.onDestroy()
        root?.removeAllViews()
        onHideImpl()

        userPermTitle = null
        groupPermTitle = null
        otherPermTitle = null

        userRead = null
        userWrite = null
        userExecute = null

        groupRead = null
        groupWrite = null
        groupExecute = null

        otherRead = null
        otherWrite = null
        otherExecute = null

        root = null
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        if (args is Args.Source) {
            buffer.putParcelable(wrapPrefixBundle(prefix), (args as Args.Source).item)
            return true
        }
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        args = Args.Source(buffer.parcelable<PathItem>(wrapPrefixBundle(prefix)) ?: return false)
        return true
    }

    private fun wrapPrefixBundle(prefix: String): String {
        return prefix + CURRENT_OPERATION_ITEM
    }

    interface Args {
        class Source(val item: PathItem) : Args
    }

    companion object {
        private const val CURRENT_OPERATION_ITEM = "current_permission_op_item"
    }
}