package io.hellsinger.vortex.ui.component.trail

import androidx.recyclerview.widget.RecyclerView

class TrailViewHolder(private val root: TrailItemView) : RecyclerView.ViewHolder(root) {

    var isArrowVisible: Boolean
        get() = root.isArrowVisible
        set(value) {
            root.isArrowVisible = value
        }

}