package io.hellsinger.vortex.ui.screen.storage.operation.create

import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.intent
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class StorageCreateItemStateController @Inject constructor(
    private val dispatchers: DispatcherProvider
) : BitStateController() {

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    private val data = MutableData()
    fun data(): Data = data

    private var listener: Listener? = null

    fun bindListener(listener: Listener?) {
        this.listener = listener
    }

    fun unbindListener() {
        this.listener = null
    }

    fun updateName(name: String) = intent {
        data.name = name
    }

    fun validateName(): Int {
        data.isNameValid = false

        if (data.name.isEmpty()) return PART_IS_EMPTY
        if (data.name.contains('/')) return INVALID_SYMBOL
        if (data.name.contains('\u0000')) return INVALID_SYMBOL

        data.isNameValid = true

        return PART_IS_OKAY
    }

    fun updatePath(path: String) = intent {
        data.path = path
    }

    private fun validatePath(): Int {
        data.isPathValid = false
        if (data.path.isEmpty()) return PART_IS_EMPTY
        if (data.path[0] != '/') return PATH_MUST_STARTS_FROM_SLASH
        data.isPathValid = true
        return PART_IS_OKAY
    }

    private fun updateMode(isChecked: Boolean, value: Int) {
        data.mode += if (isChecked) value else -value
        updateEvent(0)
    }

    fun updateUserReadPermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 100)
    }

    fun updateUserWritePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 200)
    }

    fun updateUserExecutePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 400)
    }

    fun updateGroupReadPermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 10)
    }

    fun updateGroupWritePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 20)
    }

    fun updateGroupExecutePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 40)
    }

    fun updateOtherReadPermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 1)
    }

    fun updateOtherWritePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 2)
    }

    fun updateOtherExecutePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 4)
    }

    private fun validateMode(): Boolean {


        return true
    }


    override fun onEvent() {
        listener?.onEvent(this)
    }

    interface Listener {
        fun onEvent(controller: StorageCreateItemStateController)
    }

    interface Data {
        val name: String
        val isNameValid: Boolean
        val path: String
        val isPathValid: Boolean
        val mode: Int
    }

    class MutableData(
        override var name: String = "",
        override var path: String = "",
        override var isNameValid: Boolean = false,
        override var isPathValid: Boolean = true,
        override var mode: Int = 777
    ) : Data

    companion object {
        const val PART_IS_OKAY = -1
        const val PART_IS_EMPTY = 0
        const val INVALID_SYMBOL = 1
        const val PATH_MUST_STARTS_FROM_SLASH = 2
    }

}