package io.hellsinger.vortex.ui.navigation

import android.content.Context
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import io.hellsinger.navigation.screen.BaseNavScreenController
import io.hellsinger.navigation.screen.NavScreenRestorer
import io.hellsinger.vortex.ui.component.navigation.NavigationBar
import io.hellsinger.vortex.ui.component.observeInsets

class VortexNavigationController(
    private val context: Context,
    restorer: NavScreenRestorer,
) : BaseNavScreenController(context, restorer) {

    var bar: NavigationBar? = null
        private set

    override fun onCreateView(root: FrameLayout) {
        bar = NavigationBar(context).apply {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                gravity = Gravity.BOTTOM
            }
            observeInsets { v, insets ->
                val i = WindowInsetsCompat.toWindowInsetsCompat(insets)
                val pb = i.getInsets(WindowInsetsCompat.Type.navigationBars()).bottom
                updatePadding(bottom = pb)
                insets
            }
        }
        root.addView(bar)
    }

    fun onDestroyView() {
        root?.removeAllViews()
        host = null
        bar = null
        root = null
    }
}