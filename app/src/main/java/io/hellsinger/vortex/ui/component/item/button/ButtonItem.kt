package io.hellsinger.vortex.ui.component.item.button

data class ButtonItem(
    val id: Int,
    val text: String,
)