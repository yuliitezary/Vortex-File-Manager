package io.hellsinger.vortex.ui.screen.settings.behavior

import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.screen.settings.SettingsOptionsRenderer
import io.hellsinger.vortex.utils.behaviorSettingsOption
import io.hellsinger.vortex.utils.title

class BehaviorSettingsOptionsRenderer : SettingsOptionsRenderer {
    override fun render(): List<Item<*>> = buildList {
        title("Storage")
        behaviorSettingsOption(VortexTheme.behaviors.maxPathLengthKey)
    }
}