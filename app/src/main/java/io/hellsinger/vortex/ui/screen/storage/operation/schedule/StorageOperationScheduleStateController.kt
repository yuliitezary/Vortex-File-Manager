package io.hellsinger.vortex.ui.screen.storage.operation.schedule

import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class StorageOperationScheduleStateController @Inject constructor(
    private val dispatchers: DispatcherProvider
) : BitStateController() {

    private var listener: Listener? = null

    fun bindListener(listener: Listener) {
        this.listener = listener
    }

    fun unbindListener() {
        this.listener = null
    }

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    override fun onEvent() {
        listener?.handleEvent(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindListener()
    }

    interface Listener {
        fun handleEvent(controller: StorageOperationScheduleStateController)
    }
}