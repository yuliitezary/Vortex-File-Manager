package io.hellsinger.vortex.ui.screen.storage

import android.os.Build
import android.os.Parcelable
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Access
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Observe
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Observe.Create
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Observe.Delete
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Observe.DeleteSelf
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Observe.Modify
import io.hellsinger.filesystem.linux.directory.LinuxDirectoryEntry
import io.hellsinger.filesystem.linux.operation.LinuxPathObserver.LinuxObservablePath
import io.hellsinger.vortex.base.utils.logIt
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.data.storage.PathItemSorters
import io.hellsinger.vortex.data.storage.StoragePendingItem
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_DIRECTORY
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_FILE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_LINK
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.hasFlags
import io.hellsinger.vortex.provider.storage.StoragePathSegmentParser
import io.hellsinger.vortex.provider.storage.StorageProgressEventReceiver
import io.hellsinger.vortex.provider.storage.StorageRepository
import io.hellsinger.vortex.provider.storage.StorageSharedState
import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.service.VortexServiceBinder
import io.hellsinger.vortex.service.storage.VortexStorageController
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.intent
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.parcelize.Parcelize
import javax.inject.Inject

// TODO: Remove Singleton scope, instead use custom scope (e.g StorageScope)
class StorageScreenStateController @Inject constructor(
    private val repository: StorageRepository,
    private val receiver: StorageProgressEventReceiver,
    private val segment: StoragePathSegmentParser,
    private val dispatchers: DispatcherProvider,
    private val shared: StorageSharedState,
) : BitStateController() {

    private var listener: Listener? = null

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    private var controller: VortexStorageController? = null

    private var filter: Int = 0

    private var serviceCollectingJob: Job? = null
    private var eventCollectingJob: Job? = null

    private val data = MutableData()

    val pending: List<StoragePendingItem>
        get() = shared.pending.value

    fun data(): Data = data

    fun startListenService(service: StateFlow<VortexServiceBinder?>) = intent {
        serviceCollectingJob = launch {
            service.collect { service ->
                controller = service?.storage
                boundServiceCollecting()
            }
        }
    }

    fun startListenEvent() = intent {
        eventCollectingJob = launch {
            receiver.event.collect { event ->
                listener?.handleProgress(event)
            }
        }
    }

    fun stopListenService() {
        serviceCollectingJob?.cancel()
        serviceCollectingJob = null
    }

    fun stopListenEvent() {
        eventCollectingJob?.cancel()
        eventCollectingJob = null
    }

    fun registerPending(pending: StoragePendingItem) = intent {
        shared.registerPending(pending)
    }

    fun unregisterPending(pending: StoragePendingItem) = intent {
        shared.unregisterPending(pending)
    }

    fun boundServiceCollecting() = intent {
        val service = controller
        if (service != null) {
            data.selectedTrailItem?.let(::observe)

            service.observables.filterIsInstance<LinuxObservablePath<Path>>()
                .onEach { (event, path, cookie) ->
                    Observe.maskToString(event).logIt()
                    val isDirectory = event hasFlag Observe.Attributes.Dir
                    if (event hasFlag Create) return@onEach onPathAdded(
                        path,
                        cookie,
                        isDirectory
                    )
                    if (event hasFlag Modify) return@onEach onPathModified(
                        path,
                        cookie,
                        isDirectory
                    )
                    if (event.hasFlags(Delete, DeleteSelf)) return@onEach onPathRemoved(
                        path,
                        cookie,
                        isDirectory
                    )
                }.launchIn(this)
            shared.pending.onEach { pending ->
                updateEvent(UPDATE_PENDING)
            }.launchIn(this)
        }
    }

    fun needPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (repository.requiresFullStorageAccess()) {
                updateEvent(UPDATE_MESSAGE addFlag REASON_STORAGE_FULL_PERMISSION)
                return true
            }
        } else {
            if (repository.requiresPermissions()) {
                updateEvent(UPDATE_MESSAGE addFlag REASON_STORAGE_PERMISSION)
                return true
            }
        }
        return false
    }

    fun navigateFromTrail(position: Int) = navigate(data().trail[position])

    fun navigate(
        item: PathItem,
    ) = intent {
        if (!item.isPathAbsolute) return@intent
        if (item == data().selectedTrailItem) return@intent // updateCurrent()

        val segments = segment.parse(item.path)
        val selectedIndex = segments.lastIndex

        var hasPrefix = true
        for (index in segments.indices) {
            if (hasPrefix && index < data.trail.size) {
                if (data.trail[index] != segments[index]) {
                    hasPrefix = false
                }
            }
        }
        if (hasPrefix) {
            for (index in segments.size until data.trail.size) {
                segments.add(data.trail[index])
            }
        }

        data.trail.clear()
        data.trail.addAll(segments)
        data.selectedTrailItemPosition = selectedIndex

        updateEvent(UPDATE_TRAIL_LIST)

        val progress = launch {
            delay(1000L)
            updateEvent(UPDATE_LOADING_PROGRESS)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (repository.checkRestrictedPath(item)) {
                progress.cancel()
                // Since Android 13 fixed feature with SAF, we don't want to support restricted dirs
                // Besides, Thanks Google (sarcastic)
                updateEvent(UPDATE_MESSAGE addFlag REASON_RESTRICTED_ITEM)
                return@intent
            }
        }

        data.directories = 0
        data.files = 0
        data.links = 0

        val checkResult = repository.check(item, Access.Readable or Access.Existance)

        if (checkResult != 0) {
            progress.cancel()
            updateEvent(UPDATE_MESSAGE addFlag REASON_ITEM_DOES_NOT_EXIST)
            return@intent
        }

        observe(item)

        var items = prepareItems(item)

        progress.cancel()

        if (items.isEmpty()) {
            updateEvent(UPDATE_TRAIL_LIST addFlag UPDATE_MESSAGE addFlag REASON_EMPTY_ITEMS)
            return@intent
        }

        items = items.sortedWith(PathItemSorters.Name)
        data.replace(items)

        updateEvent(UPDATE_LIST)
    }

    fun updateCurrent() = intent {
        val current = data().selectedTrailItem ?: return@intent

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (repository.checkRestrictedPath(current)) {
                // Since Android 13 fixed feature with SAF, we don't want to support restricted dirs
                // Besides, Thanks Google (sarcastic)
                updateEvent(UPDATE_MESSAGE addFlag REASON_RESTRICTED_ITEM)
                return@intent
            }
        }

        data.directories = 0
        data.files = 0
        data.links = 0

        val items = prepareItems(current)

        if (items.isEmpty()) {
            updateEvent(UPDATE_TRAIL_LIST addFlag UPDATE_MESSAGE addFlag REASON_EMPTY_ITEMS)
            return@intent
        }

        if (!items.containsAll(data.items)) {
            data.replace(items.sortedWith(PathItemSorters.Name))
            updateEvent(UPDATE_LIST)
        }
    }

    fun navigateBack() = intent {
        data.selectedTrailItemPosition--

        data.directories = 0
        data.files = 0
        data.links = 0

        val progress = launch {
            delay(1000L)
            updateEvent(UPDATE_LOADING_PROGRESS)
        }

        val item = data.trail[data.selectedTrailItemPosition]

        var items = prepareItems(item)

        progress.cancel()

        if (items.isEmpty()) {
            updateEvent(UPDATE_TRAIL_SELECTED_CHANGED addFlag UPDATE_MESSAGE addFlag REASON_EMPTY_ITEMS)
            return@intent
        }

        items = items.sortedWith(PathItemSorters.Name)

        data.replace(items)

        updateEvent(UPDATE_TRAIL_SELECTED_CHANGED addFlag UPDATE_LIST)
    }

    private suspend fun prepareItems(directory: PathItem): List<PathItem> {
        var flow = repository.entries(directory)
            .filterIsInstance<LinuxDirectoryEntry<Path>>()
            .map { entry ->
                when (entry.attributes.type) {
                    PATH_ATTR_TYPE_DIRECTORY -> data.directories++
                    PATH_ATTR_TYPE_FILE -> data.files++
                    PATH_ATTR_TYPE_LINK -> data.links++
                }
                PathItem(entry.path, entry.attributes)
            }
        when (filter) {
            ONLY_DIRECTORIES_FILTER -> flow = flow.filter { item -> item.isDirectory }
            ONLY_FILES_FILTER -> flow = flow.filter { item -> item.isFile }
        }

        return flow.flowOn(dispatchers.io).toList()
    }

    fun bindListener(listener: Listener?) {
        this.listener = listener
    }

    fun isSelected(position: Int): Boolean = isSelected(data().items[position])

    fun isSelected(item: PathItem): Boolean = data().selected.contains(item)

    fun toggleSelection(position: Int) {
        val item = data().items[position]

        if (data().selected.contains(item)) {
            data.selected.remove(item)
            listener?.handleDeselection(item, position)
        } else {
            data.selected.add(item)
            listener?.handleSelection(item, position)
        }

        updateEvent(UPDATE_SELECTION_COUNT)
    }

    fun select(item: PathItem): PathItem {
        if (data().selected.contains(item)) return item

        data.selected.add(item)
        listener?.handleSelection(item, data.items.indexOf(item))
        return item
    }

    fun select(position: Int) {
        val item = data.items[position]
        if (data().selected.contains(item)) return

        data.selected.add(item)
        listener?.handleSelection(item, position)
        updateEvent(UPDATE_SELECTION_COUNT)
    }

    fun select(items: List<PathItem> = data().items) {
        if (data().selected.isEmpty()) {
            data.selected.addAll(items)
            updateEvent(UPDATE_SELECTION_COUNT)
            return
        }

        if (data().selected.containsAll(items)) return
    }

    fun deselect(item: PathItem) {
        if (item !in data.selected) return

        data.selected.remove(item)
        updateEvent(UPDATE_SELECTION_COUNT)
    }

    fun deselect(position: Int) {
        val item = data.items[position]
        if (!data().selected.contains(item)) return

        data.selected.remove(item)
        listener?.handleDeselection(item, position)
        updateEvent(UPDATE_SELECTION_COUNT)
    }

    fun deselect(items: List<PathItem>) {
        if (items.isEmpty()) return

        data.selected.removeAll(items)
        updateEvent(UPDATE_SELECTION_COUNT)
    }

    fun applySelection() {
        for (i in data.items.indices) {
            val item = data.items[i]
            if (item !in data.selected) {
                data.selected.add(item)
                listener?.handleSelection(item, i)
            }
        }
    }

    fun clearSelection() {
        if (data().selected.isEmpty()) return

        data.selected.clear()
        updateEvent(UPDATE_SELECTION_COUNT)
    }

    override fun onEvent() {
        listener?.handleEvent(this)
    }

    private fun onPathRemoved(path: Path, cookie: Int, isDirectory: Boolean) {
        if (path == data().selectedTrailItem?.path) {
            for (i in data.selectedTrailItemPosition until data.trail.size) {
                data.trail.removeAt(i)
            }
            data.selectedTrailItemPosition--
            updateEvent(UPDATE_TRAIL_LIST)
            return excludedTrailNavigation()
        }

        val ti = data().trail.indexOfFirst { item -> item.path == path }

        // Got it: Monitored path present in trail
        // Remove parent and children with selection checking
        if (ti >= 0) {
            if (ti <= data.selectedTrailItemPosition) {
                data.selectedTrailItemPosition = ti - 1
                for (i in data.selectedTrailItemPosition until data.trail.size) {
                    data.trail.removeAt(i)
                }
                data.selectedTrailItemPosition--
                updateEvent(UPDATE_TRAIL_LIST)
                return excludedTrailNavigation()
            }

            for (i in ti until data.trail.size) {
                data.trail.removeAt(i)
            }
            updateEvent(UPDATE_TRAIL_LIST)
        }

        val li = data().items.indexOfFirst { item -> item.path == path }

        // Got it: Monitored path present in list
        // Remove parent
        if (li >= 0) {
            val item = data.items.removeAt(li)
            deselect(item)
            if (isDirectory) data.directories--
            else data.files--

            if (data.items.isEmpty()) {
                updateEvent(UPDATE_TRAIL_SELECTED_CHANGED addFlag UPDATE_MESSAGE addFlag REASON_EMPTY_ITEMS)
                return
            }

            updateEvent(UPDATE_LIST)
        }
    }

    private fun excludedTrailNavigation() = intent {
        data.directories = 0
        data.files = 0
        data.links = 0

        val progress = launch {
            delay(1000L)
            updateEvent(UPDATE_LOADING_PROGRESS)
        }

        val item = data.trail[data.selectedTrailItemPosition]
        var items = prepareItems(item)

        progress.cancel()

        if (items.isEmpty()) {
            updateEvent(UPDATE_TRAIL_SELECTED_CHANGED addFlag UPDATE_MESSAGE addFlag REASON_EMPTY_ITEMS)
            return@intent
        }

        items = items.sortedWith(PathItemSorters.Name)

        data.replace(items)

        updateEvent(UPDATE_TRAIL_SELECTED_CHANGED addFlag UPDATE_LIST)
    }

    private fun onPathAdded(path: Path, cookie: Int, isDirectory: Boolean) = intent {
        if (path.parent != data().selectedTrailItem?.path) return@intent

        val item = PathItem(path)

        if (item in data.items) return@intent

        if (isDirectory) data.directories++
        else data.files++

        data.items.add(item)
        data.replace(data.items.sortedWith(PathItemSorters.Name))
        updateEvent(UPDATE_LIST)
    }

    private fun onPathModified(path: Path, cookie: Int, isDirectory: Boolean) = intent {
        if (path == data().selectedTrailItem?.path) {
            data.selectedTrailItem = PathItem(path)
            updateEvent(UPDATE_TRAIL_LIST)
        }

        val ti = data().trail.indexOfFirst { item -> item.path == path }

        if (ti >= 0) {
            data.trail[ti] = PathItem(path)
            updateEvent(UPDATE_TRAIL_LIST)
        }

        val li = data().items.indexOfFirst { item -> item.path == path }

        if (li >= 0) {
            data.items[li] = PathItem(path)
            updateEvent(UPDATE_LIST)
        }
    }

    private fun observe(item: PathItem) {
        controller?.addObservablePath(item.path, Observe.Default)
    }

    override fun onDestroy() {
        super.onDestroy()
        listener = null
        controller?.removeAllObservablePath()
        data.clear()
    }

    interface Data {
        val items: List<PathItem>
        val selected: List<PathItem>
        val directories: Int
        val files: Int
        val links: Int
        val trail: List<PathItem>
        val selectedTrailItemPosition: Int

        val selectedTrailItem: PathItem?
            get() = trail.getOrNull(selectedTrailItemPosition)
    }

    fun applyFilter(filter: Int) {
        this.filter = filter
        updateCurrent()
    }

    @Parcelize
    data class MutableData(
        override val items: MutableList<PathItem> = mutableListOf(),
        override val selected: MutableList<PathItem> = mutableListOf(),
        override var directories: Int = 0,
        override var files: Int = 0,
        override var links: Int = 0,
        override val trail: MutableList<PathItem> = mutableListOf(),
        override var selectedTrailItemPosition: Int = -1,
    ) : Parcelable, Data {

        override var selectedTrailItem: PathItem?
            get() = super.selectedTrailItem
            set(value) {
                trail[selectedTrailItemPosition] = value ?: return
            }

        fun replace(list: List<PathItem>) {
            items.clear()
            items.addAll(list)
        }

        fun clear() {
            items.clear()
            selected.clear()
            directories = 0
            files = 0
            links = 0
            trail.clear()
            selectedTrailItemPosition = -1
        }

        fun toImmutable(): Data = object : Data {
            override val items: List<PathItem> = this@MutableData.items
            override val selected: List<PathItem> = this@MutableData.selected
            override val directories: Int = this@MutableData.directories
            override val files: Int = this@MutableData.files
            override val links: Int = this@MutableData.links
            override val trail: List<PathItem> = this@MutableData.trail
            override val selectedTrailItemPosition: Int = this@MutableData.selectedTrailItemPosition
        }
    }

    interface Listener {
        fun handleEvent(controller: StorageScreenStateController)

        fun handleProgress(event: StorageProgressEventReceiver.Event)

        fun handleSelection(item: PathItem, index: Int)

        fun handleDeselection(item: PathItem, index: Int)
    }

    companion object {
        const val UPDATE_SCREEN = 0

        const val UPDATE_LIST = 1
        const val UPDATE_LIST_REMOVAL = 1 shl 1
        const val UPDATE_LIST_ADDITION = 1 shl 2
        const val UPDATE_LIST_CHANGE = 1 shl 3

        const val UPDATE_LOADING_PROGRESS = 1 shl 4

        const val UPDATE_MESSAGE = 1 shl 5

        const val REASON_ITEM_IS_NOT_READABLE = 3 shl 23
        const val REASON_ITEM_DOES_NOT_EXIST = 3 shl 24

        // Flags for UPDATE_MESSAGE, describes what kind
        // Empty list (e.g. directory empty)
        const val REASON_EMPTY_ITEMS = 3 shl 25

        // Restricted directory (Android 11)
        const val REASON_RESTRICTED_ITEM = 3 shl 26
        const val REASON_STORAGE_PERMISSION = 3 shl 27
        const val REASON_STORAGE_FULL_PERMISSION = 3 shl 28

        const val UPDATE_TRAIL_LIST = 1 shl 7
        const val UPDATE_TRAIL_REMOVAL = 1 shl 8
        const val UPDATE_TRAIL_ADDITION = 1 shl 9
        const val UPDATE_TRAIL_CHANGE = 1 shl 10
        const val UPDATE_TRAIL_SELECTED_CHANGED = 1 shl 11

        const val UPDATE_SELECTION_COUNT = 1 shl 12

        const val UPDATE_PENDING = 1 shl 13

        const val UPDATE_LIST_ITEM_SELECTION_TOGGLE = 1
        const val UPDATE_LIST_ITEM_AVAILABILITY_TOGGLE = 1 shl 1

        // No filter
        const val DEFAULT_FILTER = 0
        const val ONLY_FILES_FILTER = 1
        const val ONLY_DIRECTORIES_FILTER = 2
    }
}