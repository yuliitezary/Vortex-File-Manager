package io.hellsinger.vortex.ui.component.item.twoline

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.RippleDrawable
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.contains
import androidx.core.view.updatePadding
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView

class TwoLineItemView(
    context: Context,
) : LinearLayout(
    context
), RecyclableView<TwoLineItem>, Theme.Listener {

    private val shape = RoundedCornerDrawable(VortexTheme.colors.drawerBackgroundColorKey)

    private val background = RippleDrawable(
        ColorStateList.valueOf(VortexTheme.colors.drawerItemBackgroundRippleColorKey.get()),
        shape,
        null
    )

    private val titleView = TextView(context).apply {
        setTextColor(VortexTheme.colors.barTitleTextColorKey.get())
        textSize = 16F
    }

    private val descriptionView = TextView(context).apply {
        setTextColor(VortexTheme.colors.barSubtitleTextColorKey.get())
        textSize = 14F
    }

    private fun ensureContainingTitle() {
        if (!contains(titleView)) {
            addView(titleView)
        }
    }

    private fun ensureContainingDescription() {
        if (!contains(descriptionView)) {
            addView(descriptionView)
        }
    }

    var title: String?
        get() = titleView.text.toString()
        set(value) {
            ensureContainingTitle()
            titleView.text = value
        }

    var description: String?
        get() = descriptionView.text.toString()
        set(value) {
            ensureContainingDescription()
            descriptionView.text = value
        }

    init {
        orientation = VERTICAL
        layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        updatePadding(
            left = 16.dp,
            right = 16.dp,
            top = 8.dp,
            bottom = 8.dp
        )
        isClickable = true
        isFocusable = true
        setBackground(background)
    }

    override fun onBind(item: TwoLineItem) {
        title = item.title
        description = item.description
    }

    override fun onUnbind() {
        title = null
        description = null
    }

    override fun onBindListener(listener: OnClickListener) {
        setOnClickListener(listener)
    }

    override fun onUnbindListeners() {
        setOnClickListener(null)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        VortexTheme.addThemeListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        VortexTheme.removeThemeListener(this)
    }

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {
        when (key) {
            VortexTheme.colors.barTitleTextColorKey -> titleView.setTextColor(value as Int)
            VortexTheme.colors.barSubtitleTextColorKey -> descriptionView.setTextColor(value as Int)
            VortexTheme.colors.drawerBackgroundColorKey -> shape.setTint(value as Int)
        }
    }

}