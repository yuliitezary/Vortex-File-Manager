package io.hellsinger.vortex.ui.screen.navigation

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout.LayoutParams
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.BuildConfig
import io.hellsinger.vortex.ViewIds.Navigation.Menu
import io.hellsinger.vortex.ui.component.ItemAdapter
import io.hellsinger.vortex.ui.component.ItemListener
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.dsl.linear
import io.hellsinger.vortex.ui.component.dsl.recycler
import io.hellsinger.vortex.ui.component.dsl.text
import io.hellsinger.vortex.ui.icon.Icons
import io.hellsinger.vortex.ui.navigation.Navigation.Settings
import io.hellsinger.vortex.ui.navigation.Navigation.Storage
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.utils.divider
import io.hellsinger.vortex.utils.drawer
import io.hellsinger.vortex.utils.title

class BottomNavigatorScreenController(
    private val context: NavigationActivity,
) : VortexScreenController(), ItemListener {

    private var root: LinearLayout? = null
    private var recycler: RecyclerView? = null
    private var adapter = ItemAdapter(this)

    override val usePopupMode: Boolean = true

    override fun onCreateViewImpl(context: Context): View {
        root = linear(context) {
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                gravity = Gravity.BOTTOM

            }
            background = RoundedCornerDrawable(
                key = VortexTheme.colors.drawerSurfaceColorKey,
                topLeft = 16F.dp,
                topRight = 16F.dp
            )
            orientation = LinearLayout.VERTICAL

            text {
                text = "Vortex"
                setTextColor(VortexTheme.colors.barTitleTextColorKey.get())
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 20F)
                layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                    gravity = Gravity.START
                    leftMargin = 16.dp
                    topMargin = 16.dp
                }
            }

            text {
                text = BuildConfig.VERSION_NAME
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
                layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                    gravity = Gravity.START
                    leftMargin = 16.dp
                    bottomMargin = 18.dp
                }
            }

            recycler = recycler {
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    gravity = Gravity.BOTTOM
                }
                layoutManager = LinearLayoutManager(context)
                adapter = this@BottomNavigatorScreenController.adapter
            }


        }
        adapter.replace(buildList {
            drawer(
                Menu.FileManagerId,
                "File Manager",
                Icons.Rounded.Directory
            )
            drawer(
                Menu.MediaId,
                "Media",
                Icons.Rounded.Image
            )
            divider()
            title("Additional")
            drawer(
                Menu.SettingsId,
                "Settings",
                Icons.Rounded.Settings
            )
        })

        return root!!
    }

    override fun onDestroyImpl() {
        root?.removeAllViews()
        root = null
    }

    override fun onItemClick(view: View, position: Int) {
        when (view.id) {
            Menu.FileManagerId -> navigateForward(Storage(context))
            Menu.SettingsId -> navigateForward(Settings(context))
        }
    }

    override fun onLongItemClick(view: View, position: Int): Boolean {
        return when (view.id) {
            Menu.NavigationId -> true
            Menu.SettingsId -> true
            else -> false
        }
    }

    override fun onTransitionFactorImpl(factor: Float) {
        val root = root ?: return
        root.translationY = root.height * (1F - factor)
    }

    override fun onBackActivatedImpl(): Boolean = false

    override fun onPrepareImpl() {

    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {

    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }
}