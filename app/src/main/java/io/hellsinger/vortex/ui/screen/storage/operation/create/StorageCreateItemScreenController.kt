package io.hellsinger.vortex.ui.screen.storage.operation.create

import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.Editable
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.WindowInsets
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams
import android.widget.LinearLayout.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import io.hellsinger.filesystem.linux.resolve
import io.hellsinger.filesystem.linux.toLinuxPath
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds.Storage
import io.hellsinger.vortex.base.utils.logIt
import io.hellsinger.vortex.base.utils.toastIt
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.dsl.check
import io.hellsinger.vortex.ui.component.dsl.field
import io.hellsinger.vortex.ui.component.dsl.linear
import io.hellsinger.vortex.ui.component.dsl.text
import io.hellsinger.vortex.ui.component.item.field.TextFieldItemView
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.component.storage.StorageActions
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController.Operation.Create
import io.hellsinger.vortex.ui.screen.storage.operation.create.StorageCreateItemStateController.Companion.PART_IS_OKAY
import javax.inject.Inject

class StorageCreateItemScreenController(
    private val context: NavigationActivity,
) : VortexScreenController(), OnCheckedChangeListener, StorageCreateItemStateController.Listener,
    TextFieldItemView.TextWatcher {

    @Inject
    lateinit var controller: StorageCreateItemStateController

    private var root: LinearLayout? = null

    private var generalTitle: TextView? = null
    private var name: TextFieldItemView? = null
    private var type: TextFieldItemView? = null
    private var path: TextFieldItemView? = null

    private var userPermTitle: TextView? = null
    private var userRead: CheckBox? = null
    private var userWrite: CheckBox? = null
    private var userExecute: CheckBox? = null

    private var groupPermTitle: TextView? = null
    private var groupRead: CheckBox? = null
    private var groupWrite: CheckBox? = null
    private var groupExecute: CheckBox? = null

    private var otherPermTitle: TextView? = null
    private var otherRead: CheckBox? = null
    private var otherWrite: CheckBox? = null
    private var otherExecute: CheckBox? = null

    private var args: Args? = null

    override val id: Int = Navigation.StorageCreate

    fun setArgs(args: Args) {
        this.args = args
    }

    interface Args {
        class Parent(val parent: PathItem) : Args
    }

    override fun onCallIdImpl(identifier: Int): Boolean = when (identifier) {
        Storage.Menu.CheckId -> {
            if (controller.data().isNameValid && controller.data().isPathValid) {
                val name = controller.data().name
                val path = controller.data().path.logIt().toLinuxPath()
                val resolved = path.resolve(name)
                val isDirectory = type?.text == "Directory"
                val screen = BottomStorageOperationScreenController(context)
                screen.setOperation(
                    operation = Create(
                        path = resolved,
                        isDirectory = isDirectory,
                        mode = controller.data().mode
                    )
                )

                navigateForward(screen)
            }

            true
        }

        else -> false
    }

    override fun onCreateViewImpl(context: Context): View {
        root = linear(context) {
            layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.BOTTOM
            setBackgroundColor(VortexTheme.colors.drawerBackgroundColorKey.get())
            observeInsets(this@StorageCreateItemScreenController)
            updatePadding(left = 8.dp, right = 8.dp)

            generalTitle = text {
                text = "General"
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 18F)
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            name = field {
                id = Storage.Create.NameFieldId
                hint = "Name"
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                setHintTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setFieldTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
            }
            path = field {
                id = Storage.Create.PathFieldId
                hint = "Path"
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                setHintTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setFieldTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
            }
            type = field {
                id = Storage.Create.TypeFieldId
                hint = "Type (e.g. Directory, Link, File by default)"
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                setHintTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setFieldTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
            }

            text {
                text = "Permission"
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 18F)
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            // Permission (mode)
            // User permission
            userPermTitle = text {
                text = "User permissions (7)"
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            linear {
                orientation = LinearLayout.HORIZONTAL

                userRead = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.UserReadId
                    text = "Read"
                    isChecked = true
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                userWrite = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.UserWriteId
                    text = "Write"
                    isChecked = true
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                userExecute = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.UserExecuteId
                    text = "Execute"
                    isChecked = true
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
            }

            // Group permission
            groupPermTitle = text {
                text = "Group permissions (7)"
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            linear {
                orientation = LinearLayout.HORIZONTAL

                groupRead = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.GroupReadId
                    text = "Read"
                    isChecked = true
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                groupWrite = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.GroupWriteId
                    text = "Write"
                    isChecked = true
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                groupExecute = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.GroupExecuteId
                    text = "Execute"
                    isChecked = true
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
            }

            // Other permission
            otherPermTitle = text {
                text = "Other permissions (7)"
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
                setTextColor(VortexTheme.colors.drawerTitleColorKey.get())
                updatePadding(
                    top = 8.dp,
                    bottom = 8.dp
                )
            }

            linear {
                orientation = LinearLayout.HORIZONTAL

                otherRead = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.OtherReadId
                    isChecked = true
                    text = "Read"
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                otherWrite = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.OtherWriteId
                    text = "Write"
                    isChecked = true
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
                otherExecute = check {
                    buttonTintList = ColorStateList.valueOf(
                        VortexTheme.colors.storageCreateCheckBoxTintColorKey.get()
                    )
                    id = Storage.Create.OtherExecuteId
                    text = "Execute"
                    isChecked = true
                    layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
                        weight = 1F
                    }
                    setOnCheckedChangeListener(this@StorageCreateItemScreenController)
                    setTextColor(VortexTheme.colors.drawerItemTitleTextColorKey.get())
                }
            }
        }

        return root!!
    }

    override fun onCheckedChanged(view: CompoundButton, isChecked: Boolean) {
        when (view.id) {
            Storage.Create.UserReadId -> controller.updateUserReadPermission(isChecked)
            Storage.Create.UserWriteId -> controller.updateUserWritePermission(isChecked)
            Storage.Create.UserExecuteId -> controller.updateUserExecutePermission(isChecked)

            Storage.Create.GroupReadId -> controller.updateGroupReadPermission(isChecked)
            Storage.Create.GroupWriteId -> controller.updateGroupWritePermission(isChecked)
            Storage.Create.GroupExecuteId -> controller.updateGroupExecutePermission(isChecked)

            Storage.Create.OtherReadId -> controller.updateOtherReadPermission(isChecked)
            Storage.Create.OtherWriteId -> controller.updateOtherWritePermission(isChecked)
            Storage.Create.OtherExecuteId -> controller.updateOtherExecutePermission(isChecked)
        }
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        val statusBar = compat.getInsets(WindowInsetsCompat.Type.statusBars())
        val navigationBar = compat.getInsets(WindowInsetsCompat.Type.navigationBars())
        val ime = compat.getInsets(WindowInsetsCompat.Type.ime())

        val barHeight = navigator?.bar?.height ?: 0
        val bottomPadding = if (ime.bottom > 0) ime.bottom else barHeight

        root?.updatePadding(
            bottom = bottomPadding + context.dp(4)
        )
        return insets
    }

    override fun afterTextChanged(view: TextFieldItemView, sequence: Editable) {
        when (view.id) {
            Storage.Create.NameFieldId -> {
                val result = controller.validateName()
                if (result != PART_IS_OKAY) "Invalid name".toastIt(context)
            }

            Storage.Create.PathFieldId -> {
                val result = controller.validateName()
                if (result != PART_IS_OKAY) "Invalid path".toastIt(context)
            }

            Storage.Create.TypeFieldId -> {

            }
        }
    }

    override fun beforeTextChanged(
        view: TextFieldItemView,
        sequence: CharSequence,
        start: Int,
        count: Int,
        after: Int
    ) {
        when (view.id) {

        }
    }

    override fun onTextChanged(
        view: TextFieldItemView,
        sequence: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {
        when (view.id) {
            Storage.Create.NameFieldId -> controller.updateName(sequence.toString())
            Storage.Create.PathFieldId -> controller.updatePath(sequence.toString())
            Storage.Create.TypeFieldId -> {}
        }
    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onPrepareImpl() {
        context.component.inject(this)

        when (args) {
            is Args.Parent -> {
                val parent = (args as Args.Parent).parent.path.toString()
                path?.text = parent
                controller.updatePath(parent)
            }
        }

        navigator?.bar?.title = "Create new storage item"
        navigator?.bar?.replaceItems(listOf(StorageActions.Validate))
        controller.bindListener(this)

        name?.addTextWatcher(this@StorageCreateItemScreenController)
        path?.addTextWatcher(this@StorageCreateItemScreenController)
        type?.addTextWatcher(this@StorageCreateItemScreenController)
    }

    override fun onEvent(controller: StorageCreateItemStateController) = with(controller.data()) {
        // First digit
        val userPerm = mode / 100
        // Middle digit
        val groupPerm = (mode - (userPerm * 100)) / 10
        // Last digit
        val otherPerm = (mode - (userPerm * 100) - (groupPerm * 10))

        userPermTitle?.text = "User permissions ($userPerm)"
        groupPermTitle?.text = "Group permissions ($groupPerm)"
        otherPermTitle?.text = "Other permissions ($otherPerm)"
    }

    override fun onFocusImpl() {
        name?.requestFocus()
    }

    override fun onHideImpl() {
        controller.unbindListener()
        userRead?.setOnCheckedChangeListener(null)
        userWrite?.setOnCheckedChangeListener(null)
        userExecute?.setOnCheckedChangeListener(null)

        groupRead?.setOnCheckedChangeListener(null)
        groupWrite?.setOnCheckedChangeListener(null)
        groupExecute?.setOnCheckedChangeListener(null)

        otherRead?.setOnCheckedChangeListener(null)
        otherWrite?.setOnCheckedChangeListener(null)
        otherExecute?.setOnCheckedChangeListener(null)
    }

    override fun onDestroyImpl() {
        controller.onDestroy()
        root?.removeAllViews()
        root = null
        userPermTitle = null
        groupPermTitle = null
        otherPermTitle = null
        userRead = null
        groupRead = null
        otherRead = null
        userWrite = null
        groupWrite = null
        otherWrite = null
        userExecute = null
        groupExecute = null
        otherExecute = null
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }
}