package io.hellsinger.vortex.ui.screen.storage.operation.permission

import io.hellsinger.filesystem.linux.attribute.LinuxPathAttribute
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.intent
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class StorageItemPermissionStateController @Inject constructor(
    private val dispatchers: DispatcherProvider
) : BitStateController() {

    private var listener: Listener? = null

    private val data = MutableData()

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    fun data() = data

    override fun onEvent() {
        listener?.handleEvent(this)
    }

    fun bindListener(listener: Listener) {
        this.listener = listener
    }

    fun unbindListener() {
        listener = null
    }

    private fun updateMode(isChecked: Boolean, value: Int) {
        data.mode += if (isChecked) value else -value
        updateEvent(UPDATE_PERMISSIONS)
    }

    fun updateUserReadPermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 100)
    }

    fun updateUserWritePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 200)
    }

    fun updateUserExecutePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 400)
    }

    fun updateGroupReadPermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 10)
    }

    fun updateGroupWritePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 20)
    }

    fun updateGroupExecutePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 40)
    }

    fun updateOtherReadPermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 1)
    }

    fun updateOtherWritePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 2)
    }

    fun updateOtherExecutePermission(isChecked: Boolean) = intent {
        updateMode(isChecked, 4)
    }

    private fun hasReadPermission(symbol: Char) = symbol == 'r'
    private fun hasWritePermission(symbol: Char) = symbol == 'w'
    private fun hasExecutePermission(symbol: Char) = symbol == 'x'

    fun validateItem(item: PathItem) = intent {
        data.mode = 0

        if (item.attrs is LinuxPathAttribute) {
            data.mode = item.attrs.mode
            updateEvent(UPDATE_VALIDATE_PERMISSIONS)
            return@intent
        }

        val permission = item.permission
        // User
        if (hasReadPermission(permission[0])) data.mode += 100
        if (hasWritePermission(permission[1])) data.mode += 200
        if (hasExecutePermission(permission[2])) data.mode += 400
        // Group
        if (hasReadPermission(permission[3])) data.mode += 10
        if (hasWritePermission(permission[4])) data.mode += 20
        if (hasExecutePermission(permission[5])) data.mode += 40
        // Other
        if (hasReadPermission(permission[6])) data.mode += 1
        if (hasWritePermission(permission[7])) data.mode += 2
        if (hasExecutePermission(permission[8])) data.mode += 4

        updateEvent(UPDATE_VALIDATE_PERMISSIONS)
    }

    override fun onDestroy() {
        super.onDestroy()
        listener = null
    }

    interface Data {
        val mode: Int
    }

    class MutableData(override var mode: Int = 0) : Data

    interface Listener {
        fun handleEvent(controller: StorageItemPermissionStateController)
    }

    companion object {
        const val UPDATE_VALIDATE_PERMISSIONS = 0
        const val UPDATE_PERMISSIONS = 1
    }
}