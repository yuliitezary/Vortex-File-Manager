package io.hellsinger.vortex.ui.component

class AdapterDataNavigation<I> {

    private val _data = mutableListOf<List<I>>()

    var current = -1
        private set

    fun navigate(data: List<I>): List<I> {
        _data.add(data)
        current++
        return data
    }

    fun poll(): List<I> {
        return _data.removeAt(current--)
    }

    fun navigateBack(): List<I> {
        return _data.removeAt(--current)
    }
}