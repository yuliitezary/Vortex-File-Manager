package io.hellsinger.vortex.ui.screen.storage.editor

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.InfoView
import io.hellsinger.vortex.ui.component.LoadingView
import io.hellsinger.vortex.ui.component.StorageTextEditor
import io.hellsinger.vortex.ui.component.dsl.editor
import io.hellsinger.vortex.ui.component.dsl.frame
import io.hellsinger.vortex.ui.component.dsl.info
import io.hellsinger.vortex.ui.component.dsl.loading
import io.hellsinger.vortex.ui.icon.Icons
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.ui.screen.BitStateController.Companion.IDLE
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.utils.parcelable
import javax.inject.Inject

class StorageFileEditorScreenController(
    private val context: NavigationActivity,
) : VortexScreenController(), StorageFileEditorStateController.Listener {

    override val id: Int
        get() = Navigation.StorageFileEditor

    @Inject
    lateinit var controller: StorageFileEditorStateController

    private var root: FrameLayout? = null

    // TODO: Implement custom optimized edit text
    private var editor: StorageTextEditor? = null
    private var info: InfoView? = null
    private var loading: LoadingView? = null

    private var animator: ValueAnimator? = null

    private var args: Args? = null

    fun setArgs(args: Args) {
        this.args = args
    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onCreateViewImpl(context: Context): View {
        root = frame(context) {
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
            setBackgroundColor(VortexTheme.colors.storageListBackgroundColorKey.get())

            editor = editor(index = 0) {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
            }

            info = info(index = 1) {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
                visibility = GONE
            }

            loading = loading(index = 2) {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
                visibility = GONE
            }
        }

        return root!!
    }

    override fun onPrepareImpl() {
        context.component.inject(this)

        controller.subscribeActions(::handleAction)

        (args as? Args.Source)?.item?.apply {
            navigator?.bar?.title = name
            navigator?.bar?.replaceItems(emptyList())
            controller.load(this)
        }

        navigator?.bar?.hide()
    }

    fun handleAction(action: StorageFileEditorStateController.Action) = when (action) {
        is StorageFileEditorStateController.Action.SetLines -> {

            editor?.setText(action.lines)
            navigateField()
        }

        is StorageFileEditorStateController.Action.AppendLine -> {
            editor?.appendLine(action.line)
            navigateField()
        }

        is StorageFileEditorStateController.Action.Loading -> navigateLoading()
        is StorageFileEditorStateController.Action.Idle -> navigateField()
        is StorageFileEditorStateController.Action.Message -> {
            info?.icon = Icons.Rounded.File
            info?.message = action.text
            navigateInfo()
        }

    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {
        controller.unbindListener()
        controller.unsubscribeActions()
    }

    override fun onDestroyImpl() {
        controller.onDestroy()
        root?.removeAllViews()
        root = null
        editor = null
    }

    override fun handleEvent(controller: StorageFileEditorStateController) = with(controller) {
        val event = extractEvent()
        if (event == IDLE) return@with

    }

    private fun createSwitchAnimator(
        duration: Long = 150L
    ): ValueAnimator {
        if (animator != null) return animator!!

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = duration
        this.animator = animator
        return animator
    }

    private fun navigateField() {
        if (editor?.visibility == VISIBLE) return
        animator?.cancel()
        createSwitchAnimator()

        animator?.addUpdateListener { anim ->
            if (loading?.visibility == VISIBLE) loading?.alpha = 1F - anim.animatedFraction
            if (info?.visibility == VISIBLE) info?.alpha = 1F - anim.animatedFraction
            editor?.alpha = anim.animatedFraction
        }

        animator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationCancel(animation: Animator) {
                editor?.alpha = 1F
                editor?.visibility = VISIBLE
                loading?.visibility = GONE
                info?.visibility = GONE
                animator = null
            }

            override fun onAnimationStart(animation: Animator) {
                editor?.visibility = GONE
            }

            override fun onAnimationEnd(animation: Animator) {
                editor?.alpha = 1F
                editor?.visibility = VISIBLE
                loading?.visibility = GONE
                info?.visibility = GONE
                animator = null
            }
        })
    }

    private fun navigateLoading() {
        if (loading?.visibility == VISIBLE) return
        animator?.cancel()
        createSwitchAnimator()

        animator?.addUpdateListener { anim ->
            if (editor?.visibility == VISIBLE) editor?.alpha = 1F - anim.animatedFraction
            if (info?.visibility == VISIBLE) info?.alpha = 1F - anim.animatedFraction
            loading?.alpha = anim.animatedFraction
        }

        animator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationCancel(animation: Animator) {
                loading?.alpha = 1F
                loading?.visibility = VISIBLE
                info?.visibility = GONE
                editor?.visibility = GONE
                animator = null
            }

            override fun onAnimationStart(animation: Animator) {
                loading?.visibility = GONE
            }

            override fun onAnimationEnd(animation: Animator) {
                loading?.alpha = 1F
                loading?.visibility = VISIBLE
                editor?.visibility = GONE
                info?.visibility = GONE
                animator = null
            }
        })

        animator?.start()
    }

    private fun navigateInfo() {
        if (loading?.visibility == VISIBLE) return
        animator?.cancel()
        createSwitchAnimator()

        animator?.addUpdateListener { anim ->
            if (editor?.visibility == VISIBLE) editor?.alpha = 1F - anim.animatedFraction
            if (loading?.visibility == VISIBLE) info?.alpha = 1F - anim.animatedFraction
            info?.alpha = anim.animatedFraction
        }

        animator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationCancel(animation: Animator) {
                info?.alpha = 1F
                info?.visibility = VISIBLE
                loading?.visibility = GONE
                editor?.visibility = GONE
                animator = null
            }

            override fun onAnimationStart(animation: Animator) {
                info?.visibility = GONE
            }

            override fun onAnimationEnd(animation: Animator) {
                info?.alpha = 1F
                info?.visibility = VISIBLE
                editor?.visibility = GONE
                loading?.visibility = GONE
                animator = null
            }
        })

        animator?.start()
    }

    private fun createPrefixBundle(prefix: String): String {
        return prefix + "storage_file_editor_item"
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        val current = controller.data().item ?: return false
        buffer.putParcelable(createPrefixBundle(prefix), current)
        return true
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        val item = buffer.parcelable<PathItem>(createPrefixBundle(prefix)) ?: return false
        setArgs(Args.Source(item))
        return true
    }

    interface Args {
        class Source(val item: PathItem) : Args
    }
}