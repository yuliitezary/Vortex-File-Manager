package io.hellsinger.vortex.ui.component.settings

import android.content.Context
import android.content.res.ColorStateList.valueOf
import android.graphics.drawable.RippleDrawable
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView

class SettingsCellView(context: Context) : FrameLayout(context),
    RecyclableView<SettingsItem>, SettingsCell {

    private val titleView = TextView(context).apply {
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            gravity = Gravity.START or Gravity.TOP
        }
    }
    private val subtitleView = TextView(context).apply {
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            gravity = Gravity.START or Gravity.BOTTOM
        }
    }

    override var key: String
        get() = titleView.text.toString()
        set(value) {
            titleView.text = value
        }
    override var value: String
        get() = subtitleView.text.toString()
        set(value) {
            subtitleView.text = value
        }

    init {
        addView(titleView, 0)
        addView(subtitleView, 1)
        layoutParams = LayoutParams(MATCH_PARENT, 48.dp)
        background = RoundedCornerDrawable(VortexTheme.colors.storageListItemSurfaceColorKey)
        foreground = RippleDrawable(
            valueOf(VortexTheme.colors.storageListItemSurfaceRippleColorKey.get()),
            null,
            background
        )
    }

    override fun onUnbind() {

    }

    override fun onBind(item: SettingsItem) {
        key = item.key
        value = item.value
    }
}