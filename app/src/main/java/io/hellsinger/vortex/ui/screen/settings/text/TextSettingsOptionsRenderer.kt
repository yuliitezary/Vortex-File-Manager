package io.hellsinger.vortex.ui.screen.settings.text

import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.screen.settings.SettingsOptionsRenderer
import io.hellsinger.vortex.utils.textSettingsOption
import io.hellsinger.vortex.utils.title

class TextSettingsOptionsRenderer : SettingsOptionsRenderer {
    override fun render(): List<Item<*>> = buildList {
        title("Navigation")
        textSettingsOption(VortexTheme.texts.navigationVortexFileManagerActionTitleKey)
        title("Storage (Item : Mimetype)")
        textSettingsOption(VortexTheme.texts.storageListItemMimeTypeApplicationKey)
        textSettingsOption(VortexTheme.texts.storageListItemMimeTypeImageKey)
        textSettingsOption(VortexTheme.texts.storageListItemMimeTypeVideoKey)
        textSettingsOption(VortexTheme.texts.storageListItemMimeTypeAudioKey)
        textSettingsOption(VortexTheme.texts.storageListItemMimeTypeTextKey)
        title("Storage (Item : Size)")
        textSettingsOption(VortexTheme.texts.storageListItemSizeBKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizeKiBKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizeMiBKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizeGiBKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizeTiBKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizePiBKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizeEiBKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizeZiBKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizeYiBKey)
        title("Storage (Item)")
        textSettingsOption(VortexTheme.texts.storageListItemNameKey)
        textSettingsOption(VortexTheme.texts.storageListItemSizeKey)
        textSettingsOption(VortexTheme.texts.storageListItemInfoSeparatorKey)
        textSettingsOption(VortexTheme.texts.storageListItemsCountKey)
        textSettingsOption(VortexTheme.texts.storageListItemCountKey)
        textSettingsOption(VortexTheme.texts.storageListItemEmptyKey)
        title("Storage (Loading)")
    }
}