package io.hellsinger.vortex.ui.component

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Path
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import io.hellsinger.theme.Theme

class RoundedCornerDrawable(
    private var color: Int,
    private var topLeft: Float = 0F,
    private var topRight: Float = 0F,
    private var bottomLeft: Float = 0F,
    private var bottomRight: Float = 0F,
    private val paddingLeft: Int = 0,
    private val paddingTop: Int = 0,
    private val paddingRight: Int = 0,
    private val paddingBottom: Int = 0
) : Drawable() {

    private val path = Path()

    constructor(
        key: Theme.Key<Int>,
        topLeft: Float = 0F,
        topRight: Float = 0F,
        bottomLeft: Float = 0F,
        bottomRight: Float = 0F,
        paddingLeft: Int = 0,
        paddingTop: Int = 0,
        paddingRight: Int = 0,
        paddingBottom: Int = 0
    ) : this(
        key.get(),
        topLeft,
        topRight,
        bottomLeft,
        bottomRight,
        paddingLeft,
        paddingTop,
        paddingRight,
        paddingBottom
    )

    constructor(
        color: Long,
        topLeft: Float = 0F,
        topRight: Float = 0F,
        bottomLeft: Float = 0F,
        bottomRight: Float = 0F,
        paddingLeft: Int = 0,
        paddingTop: Int = 0,
        paddingRight: Int = 0,
        paddingBottom: Int = 0
    ) : this(
        color.toInt(),
        topLeft,
        topRight,
        bottomLeft,
        bottomRight,
        paddingLeft,
        paddingTop,
        paddingRight,
        paddingBottom
    )

    constructor(
        color: Int,
        radius: Float = 0F,
        paddingLeft: Int = 0,
        paddingTop: Int = 0,
        paddingRight: Int = 0,
        paddingBottom: Int = 0
    ) : this(
        color,
        radius,
        radius,
        radius,
        radius,
        paddingLeft,
        paddingTop,
        paddingRight,
        paddingBottom
    )

    constructor(
        key: Theme.Key<Int>,
        radius: Float = 0F,
        paddingLeft: Int = 0,
        paddingTop: Int = 0,
        paddingRight: Int = 0,
        paddingBottom: Int = 0
    ) : this(key.get(), radius, paddingLeft, paddingTop, paddingRight, paddingBottom)

    constructor(
        color: Long,
        radius: Float = 0F,
        paddingLeft: Int = 0,
        paddingTop: Int = 0,
        paddingRight: Int = 0,
        paddingBottom: Int = 0
    ) : this(color.toInt(), radius, paddingLeft, paddingTop, paddingRight, paddingBottom)

    override fun draw(canvas: Canvas) {
        val rounded = Paints.rounded()
        rounded.set(
            (bounds.left + paddingLeft).toFloat(),
            (bounds.top + paddingTop).toFloat(),
            (bounds.right - paddingRight).toFloat(),
            (bounds.bottom - paddingBottom).toFloat()
        )

        path.addRoundRect(
            rounded,
            floatArrayOf(
                topLeft, topLeft,
                topRight, topRight,
                bottomLeft, bottomLeft,
                bottomRight, bottomRight
            ),
            Path.Direction.CW
        )

        canvas.drawPath(
            path,
            Paints.filling(color)
        )
    }

    fun setColor(color: Int) {
        this.color = color
        invalidateSelf()
    }

    fun setColor(key: Theme.Key<Int>) {
        this.color = key.get()
        invalidateSelf()
    }

    override fun setAlpha(alpha: Int) {

    }

    override fun setColorFilter(filter: ColorFilter?) {

    }

    @Deprecated(
        "Deprecated in Java",
        ReplaceWith("PixelFormat.UNKNOWN", "android.graphics.PixelFormat")
    )
    override fun getOpacity(): Int = PixelFormat.UNKNOWN
}