package io.hellsinger.vortex.ui.component.trail

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.RippleDrawable
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import androidx.core.view.contains
import androidx.core.view.isVisible
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.component.udp
import io.hellsinger.vortex.ui.icon.Icons

class TrailItemView(
    context: Context,
) : FrameLayout(context), RecyclableView<PathItem>, Theme.Listener {

    private val leftPadding = VortexTheme.dimens.trailItemLeftPaddingKey.get().udp
    private val rightPadding = VortexTheme.dimens.trailItemRightPaddingKey.get().udp

    private val surface = RoundedCornerDrawable(
        VortexTheme.colors.trailSurfaceColorKey,
        16F.dp
    )

    var title: CharSequence?
        get() = titleView.text
        set(value) {
            if (value == titleView.text) return
            ensureContainingTitle()
            titleView.text = value
        }

    var isArrowVisible: Boolean
        get() = arrowView.isVisible
        set(value) {
            if (value == arrowView.isVisible) return
            ensureContainingArrow()
            arrowView.isVisible = value
        }

    init {
        id = ViewIds.Storage.Trail.ItemRootId
        isClickable = true
        isFocusable = true
        background = surface
        foreground = createRippleForeground()
    }

    private val titleView = TextView(context).apply {
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        textSize = 16F
    }

    private val arrowView = ImageView(context).apply {
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        setImageDrawable(Icons.Rounded.ArrowRight)
    }

    private fun ensureContainingTitle() {
        if (!contains(titleView)) {
            addView(titleView)
        }
    }

    private fun ensureContainingArrow() {
        if (!contains(arrowView)) {
            addView(arrowView)
        }
    }

    override fun onBind(item: PathItem) {
        title = item.name
    }

    override fun onBindSelection(isSelected: Boolean) {
        updateStateLists()
        setSelected(isSelected)
    }

    override fun onBindPayload(payload: Any?) {
        if (payload is Boolean) {
            isArrowVisible = payload
        }
    }

    override fun onUnbind() {
        title = null
        isArrowVisible = false
    }

    override fun onBindListener(listener: OnClickListener) {
        setOnClickListener(listener)
    }

    override fun onBindLongListener(listener: OnLongClickListener) {
        setOnLongClickListener(listener)
    }

    override fun onUnbindListeners() {
        setOnClickListener(null)
        setOnLongClickListener(null)
    }

    private fun updateStateLists() {
        titleView.setTextColor(createTitleStateList())
        arrowView.imageTintList = createArrowStateList()
        foreground = createRippleForeground()
        surface.setTint(VortexTheme.colors.trailSurfaceColorKey.get())
    }

    private fun createRippleForeground() = RippleDrawable(
        createRippleStateList(),
        null,
        background,
    )

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {

    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        VortexTheme.addThemeListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        VortexTheme.removeThemeListener(this)
    }

    private fun createRippleStateList(): ColorStateList {
        return ColorStateList(
            arrayOf(
                intArrayOf(android.R.attr.state_selected),
                intArrayOf(),
            ),
            intArrayOf(
                VortexTheme.colors.trailItemRippleSelectedTintColorKey.get(),
                VortexTheme.colors.trailItemRippleTintColorKey.get(),
            )
        )
    }

    private fun createArrowStateList(): ColorStateList {
        return ColorStateList(
            arrayOf(
                intArrayOf(android.R.attr.state_selected),
                intArrayOf(),
            ),
            intArrayOf(
                VortexTheme.colors.trailItemArrowSelectedTintColorKey.get(),
                VortexTheme.colors.trailItemArrowTintColorKey.get(),
            )
        )
    }

    private fun createTitleStateList(): ColorStateList {
        return ColorStateList(
            arrayOf(
                intArrayOf(android.R.attr.state_selected),
                intArrayOf(),
            ),
            intArrayOf(
                VortexTheme.colors.trailItemTitleSelectedTextColorKey.get(),
                VortexTheme.colors.trailItemTitleTextColorKey.get(),
            )
        )
    }

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        var width = leftPadding + rightPadding
        val height = VortexTheme.dimens.trailItemHeightKey.get().udp

        if (!title.isNullOrEmpty()) {
            measureChild(
                titleView,
                widthSpec,
                heightSpec
            )
            width += titleView.measuredWidth
        }

        if (arrowView.isVisible) {
            measureChild(
                arrowView,
                widthSpec,
                heightSpec
            )
            width += arrowView.measuredWidth
        }

        setMeasuredDimension(width, height)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var width = 0

        if (!title.isNullOrEmpty()) {
            width += leftPadding
            val padding = getVerticalPadding(titleView)
            titleView.layout(
                width,
                padding,
                width + titleView.measuredWidth,
                padding + titleView.measuredHeight
            )
            width += titleView.measuredWidth
        }

        if (isArrowVisible) {
            width += leftPadding
            val padding = getVerticalPadding(arrowView)
            arrowView.layout(
                width,
                padding,
                width + arrowView.measuredWidth,
                padding + arrowView.measuredHeight
            )
        }
    }

    private fun getVerticalPadding(
        view: View,
    ): Int {
        return (height - view.measuredHeight) / 2
    }

    private fun getHorizontalPadding(
        view: View,
    ): Int {
        return (width - view.measuredWidth) / 2
    }

}