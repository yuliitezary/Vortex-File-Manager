package io.hellsinger.vortex.ui.navigation

import io.hellsinger.navigation.screen.ScreenController

abstract class VortexScreenController : ScreenController() {

    override val navigator: VortexNavigationController?
        get() = super.navigator as VortexNavigationController?
}