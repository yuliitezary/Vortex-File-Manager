package io.hellsinger.vortex.ui.component.trail

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.theme.Theme
import io.hellsinger.theme.Theme.Listener
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.MaterialShapeDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.foundtation.MarginItemDecoration
import io.hellsinger.vortex.utils.ViewMeasure


class TrailListView(
    context: Context
) : RecyclerView(context), Listener {

    private val shape = MaterialShapeDrawable(
        VortexTheme.colors.trailSurfaceColorKey.get()
    ).setElevation(4F.dp)

    init {
        background = shape
        layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        addItemDecoration(MarginItemDecoration(horizontal = 4.dp))
    }

    override fun setElevation(elevation: Float) {
        shape.setElevation(elevation)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        VortexTheme.addThemeListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        VortexTheme.removeThemeListener(this)
    }

    override fun onMeasure(widthSpec: Int, heightSpec: Int) = ViewMeasure(
        widthSpec,
        heightSpec,
        VortexTheme.dimens.trailWidthKey,
        VortexTheme.dimens.trailHeightKey,
        ::setMeasuredDimension
    )

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {
        when (key) {
            VortexTheme.colors.trailSurfaceColorKey -> {
                shape.setTint(value as Int)
            }
        }
    }

}