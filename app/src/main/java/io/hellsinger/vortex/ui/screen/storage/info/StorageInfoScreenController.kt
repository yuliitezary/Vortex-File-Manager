package io.hellsinger.vortex.ui.screen.storage.info

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.view.WindowInsets
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.ItemListener
import io.hellsinger.vortex.ui.component.LoadingView
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.navigation.Navigation
import io.hellsinger.vortex.ui.screen.BitStateController.Companion.IDLE
import io.hellsinger.vortex.ui.screen.ListController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.storage.info.StorageInfoStateController.Companion.UPDATE_INFO
import io.hellsinger.vortex.ui.screen.storage.info.StorageInfoStateController.Companion.UPDATE_LOADING
import javax.inject.Inject

class StorageInfoScreenController(
    private val context: NavigationActivity
) : ListController(), ItemListener, StorageInfoStateController.Listener {

    @Inject
    lateinit var controller: StorageInfoStateController

    private var root: FrameLayout? = null
    private var loading: LoadingView? = null
    private var animator: ValueAnimator? = null

    private var args: Args? = null

    override val id: Int = Navigation.StorageInfo

    fun setArgs(args: Args) {
        this.args = args
    }

    override fun onBackActivatedImpl(): Boolean {
        return false
    }

    override fun onCreateViewImpl(context: Context): View {
        root = FrameLayout(context).apply {
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            setBackgroundColor(VortexTheme.colors.storageListBackgroundColorKey.get())
            observeInsets(this@StorageInfoScreenController)
        }

        super.onCreateViewImpl(context)

        loading = LoadingView(context).apply {
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            visibility = GONE
        }

        root!!.addView(list)
        root!!.addView(loading)

        return root!!
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        when (v) {
            is FrameLayout -> {
                val statusBarInsets = compat.getInsets(WindowInsetsCompat.Type.statusBars())
                v.updatePadding(top = statusBarInsets.top)
            }

            is RecyclerView -> {
                val navigationBarInsets = compat.getInsets(WindowInsetsCompat.Type.navigationBars())
                v.updatePadding(bottom = navigationBarInsets.bottom)
            }
        }
        return insets
    }

    override fun onLongItemClick(view: View, position: Int): Boolean {
        return false
    }

    override fun onItemClick(view: View, position: Int) {

    }

    override fun onPrepareImpl() {
        context.component.inject(this)

        controller.bindListener(this)
        if (args is Args.Single) controller.update((args as Args.Single).item)
        navigator?.bar?.replaceItems(emptyList())
    }

    override fun onEvent(controller: StorageInfoStateController) {
        val event = controller.extractEvent()
        if (event == IDLE) return
        val data = controller.data

        when (event) {
            UPDATE_INFO -> {
                adapter.replace(data.content)
                navigateOptions()
            }

            UPDATE_LOADING -> navigateLoading()
        }
    }

    private fun createSwitchAnimator(
        duration: Long = 150L
    ): ValueAnimator {
        if (animator != null) return animator!!

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = duration
        this.animator = animator
        return animator
    }

    private fun navigateLoading() {
        if (loading?.visibility == View.VISIBLE) return
        animator?.cancel()
        createSwitchAnimator()

        animator?.addUpdateListener { anim ->
            if (list?.isVisible == true) list?.alpha = 1F - anim.animatedFraction
            loading?.alpha = anim.animatedFraction
        }

        animator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationCancel(animation: Animator) {
                loading?.alpha = 1F
                loading?.visibility = View.VISIBLE
                list?.visibility = GONE
                animator = null
            }

            override fun onAnimationStart(animation: Animator) {
                loading?.visibility = GONE
            }

            override fun onAnimationEnd(animation: Animator) {
                loading?.alpha = 1F
                loading?.visibility = View.VISIBLE
                list?.visibility = GONE
                animator = null
            }
        })

        animator?.start()
    }

    private fun navigateOptions() {
        if (list?.visibility == View.VISIBLE) return
        animator?.cancel()
        createSwitchAnimator()

        animator?.addUpdateListener { anim ->
            if (loading?.isVisible == true) loading?.alpha = 1F - anim.animatedFraction
            list?.alpha = anim.animatedFraction
        }

        animator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                list?.visibility = GONE
            }

            override fun onAnimationCancel(animation: Animator) {
                list?.alpha = 1F
                list?.visibility = View.VISIBLE
                loading?.visibility = GONE
                animator = null
            }

            override fun onAnimationEnd(animation: Animator) {
                list?.alpha = 1F
                list?.visibility = View.VISIBLE
                loading?.visibility = GONE
                animator = null
            }
        })

        animator?.start()
    }

    override fun onFocusImpl() {

    }

    override fun onHideImpl() {
        controller.bindListener(null)
    }

    override fun onDestroyImpl() {
        controller.onDestroy()
        root?.removeAllViews()
        root = null
        list = null
        loading = null
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        return false
    }

    interface Args {
        class Single(val item: PathItem) : Args
    }
}