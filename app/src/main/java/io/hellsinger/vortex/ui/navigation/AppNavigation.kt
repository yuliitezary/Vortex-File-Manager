package io.hellsinger.vortex.ui.navigation

import io.hellsinger.vortex.ui.screen.navigation.BottomNavigatorScreenController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.settings.SettingsScreenController
import io.hellsinger.vortex.ui.screen.storage.StorageScreenController

object Navigation {
    fun Navigator(context: NavigationActivity) = BottomNavigatorScreenController(context)

    fun Storage(context: NavigationActivity) = StorageScreenController(context)

    fun Settings(context: NavigationActivity) = SettingsScreenController(context)

    const val Settings = 0
    const val Storage = 1
    const val StorageInfo = 2
    const val StorageItemInfo = 3
    const val StorageCreate = 4
    const val StorageCreateLink = 5
    const val StorageRename = 6
    const val StoragePermission = 7
    const val StorageFileEditor = 8
}