package io.hellsinger.vortex.ui.component.navigation

import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.ui.component.menu.MenuAction
import io.hellsinger.vortex.ui.icon.Icons

object NavigationActions {

    inline val More
        get() = MenuAction(
            id = ViewIds.Navigation.Menu.MoreId,
            title = VortexTheme.texts.navigationMoreActionTitleKey.get(),
            icon = Icons.Rounded.More
        )

    inline val Search
        get() = MenuAction(
            id = ViewIds.Navigation.Menu.SearchId,
            title = VortexTheme.texts.navigationSearchActionTitleKey.get(),
            icon = Icons.Rounded.Search
        )

}