package io.hellsinger.vortex.ui.screen.storage

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Environment.getExternalStorageDirectory
import android.view.Gravity.BOTTOM
import android.view.Gravity.CENTER
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup.GONE
import android.view.ViewGroup.MarginLayoutParams
import android.view.WindowInsets
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsCompat.Type
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.Formatter
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.component
import io.hellsinger.vortex.data.Counter
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.hasFlags
import io.hellsinger.vortex.provider.contract.Contracts
import io.hellsinger.vortex.provider.storage.StorageProgressEventReceiver
import io.hellsinger.vortex.service.utils.asPath
import io.hellsinger.vortex.ui.component.InfoView
import io.hellsinger.vortex.ui.component.ItemListener
import io.hellsinger.vortex.ui.component.LoadingView
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.dsl.info
import io.hellsinger.vortex.ui.component.dsl.loading
import io.hellsinger.vortex.ui.component.dsl.recycler
import io.hellsinger.vortex.ui.component.dsl.trail
import io.hellsinger.vortex.ui.component.linear
import io.hellsinger.vortex.ui.component.navigation.NavigationActions
import io.hellsinger.vortex.ui.component.navigation.NavigationBar
import io.hellsinger.vortex.ui.component.observeInsets
import io.hellsinger.vortex.ui.component.storage.StorageActions
import io.hellsinger.vortex.ui.component.storage.StorageAdapter
import io.hellsinger.vortex.ui.component.storage.StorageSnackBar
import io.hellsinger.vortex.ui.component.trail.TrailAdapter
import io.hellsinger.vortex.ui.component.trail.TrailListView
import io.hellsinger.vortex.ui.icon.Icons
import io.hellsinger.vortex.ui.navigation.Navigation.Storage
import io.hellsinger.vortex.ui.navigation.VortexScreenController
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.ui.screen.navigation.NavigationActivity
import io.hellsinger.vortex.ui.screen.storage.StorageScreenStateController.Companion.REASON_STORAGE_FULL_PERMISSION
import io.hellsinger.vortex.ui.screen.storage.StorageScreenStateController.Companion.REASON_STORAGE_PERMISSION
import io.hellsinger.vortex.ui.screen.storage.editor.StorageFileEditorScreenController
import io.hellsinger.vortex.ui.screen.storage.operation.BottomStorageOperationScreenController
import io.hellsinger.vortex.utils.isAndroidR
import io.hellsinger.vortex.utils.parcelable
import javax.inject.Inject
import kotlin.math.roundToInt

//@NavigationRoute("StorageScreen") // WIP
class StorageScreenController(
    private val context: NavigationActivity,
) : VortexScreenController(), StorageScreenStateController.Listener, ItemListener,
    NavigationBar.StateListener, BottomStorageOperationScreenController.Communication {

    @Inject
    lateinit var controller: StorageScreenStateController

    private var args: Args? = null

    private var container: FrameLayout? = null
    private var recycler: RecyclerView? = null
    private var snack: StorageSnackBar? = null
    private var trail: TrailListView? = null
    private var loading: LoadingView? = null
    private var info: InfoView? = null
    private val counter = Counter(0)
    private var animator: ValueAnimator? = null

    private val trailAdapter: TrailAdapter = TrailAdapter(
        listener = this
    )
    protected val adapter = StorageAdapter(
        isSelected = { item -> controller.isSelected(item) },
        listener = this
    )

    private val selected: List<PathItem>
        get() = controller.data().selected

    override val id: Int = Storage

    fun setArgs(args: Args) {
        this.args = args
    }

    override fun onDeselectItems(items: List<PathItem>) {
        items.forEach { item ->
            val index = controller.data().items.indexOf(item)
            if (index < 0) return@forEach
            controller.deselect(index)
            adapter.applyDeselect(index)
        }
    }

    override fun onCallIdImpl(identifier: Int): Boolean {
        return when (identifier) {
            ViewIds.Storage.Menu.TasksId -> {
                if (controller.pending.isEmpty()) {
                    val snack = createStorageSnackBar(context)
                    snack.title = "There's no pending operations"
                    snack.progress = -1F
                    snack.showAndHideAfter(1000L)
                    return true
                }
                val item = controller.data().selectedTrailItem ?: return true
                navigationForwardStorageOperationScreenController(
                    item,
                    PendingStorageActionsRenderer(controller.pending)
                )
                true
            }

            ViewIds.Navigation.Menu.SearchId -> {
                true
            }

            ViewIds.Navigation.Menu.MoreId -> {
                val item = controller.data().selectedTrailItem ?: return true
                navigationForwardStorageOperationScreenController(
                    item,
                    DefaultStorageActionsRenderer()
                )
                true
            }

            ViewIds.Storage.Menu.ProvideStorageAccessId -> {
                context.requestPermissions(
                    arrayOf(Manifest.permission_group.STORAGE),
                    Contracts.Storage.RequestStorageAccessCode
                )
                true
            }

            ViewIds.Storage.Menu.ProvideFullStorageAccessId -> {
                context.startActivityForResult(
                    Contracts.Storage.StorageFullAccessIntent(),
                    Contracts.Storage.RequestFullStorageAccessCode
                )
                true
            }

            else -> false
        }
    }

    private inline fun checkParent(
        context: Context,
        block: FrameLayout.() -> Unit,
    ): FrameLayout {
        if (container == null) {
            container = object : FrameLayout(context), Theme.Listener {
                override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {
                    when (key) {
                        VortexTheme.colors.storageListBackgroundColorKey -> setBackgroundColor(value as Int)
                    }
                }

                init {
                    setBackgroundColor(VortexTheme.colors.storageListBackgroundColorKey.get())
                    layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
                    clipChildren = false
                }
            }
        }

        return container!!.apply(block)
    }

    override fun onCreateViewImpl(context: Context): View = checkParent(context) {
        recycler = recycler(0) {
            id = ViewIds.Storage.List.RootId
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
            adapter = this@StorageScreenController.adapter
            layoutManager = LinearLayoutManager(context)
//            itemAnimator = null
            setHasFixedSize(true)
            isNestedScrollingEnabled = true
            clipToPadding = false
            visibility = GONE
            observeInsets(this@StorageScreenController)

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(parent: RecyclerView, dx: Int, dy: Int) {
                    (parent.layoutManager as? LinearLayoutManager)?.findFirstCompletelyVisibleItemPosition()
                    if (dy > 0) navigator?.bar?.hide()
                    else navigator?.bar?.show()
                    parent.linear {
                        if (findFirstCompletelyVisibleItemPosition() == 0)
                            trail?.elevation = 0F
                        else
                            trail?.elevation = 4F.dp
                    }
                }

                override fun onScrollStateChanged(parent: RecyclerView, newState: Int) {

                }
            })
        }

        info = info(1) {
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
            visibility = GONE
        }

        trail = trail(2) {
            id = ViewIds.Storage.Trail.RootId
            layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            adapter = trailAdapter
            itemAnimator = null
            observeInsets(this@StorageScreenController)
        }

        loading = loading(3) {
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT).apply {
                gravity = CENTER
            }
            visibility = GONE
        }
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets): WindowInsets {
        val compat = WindowInsetsCompat.toWindowInsetsCompat(insets)
        val statusBarInsets = compat.getInsets(Type.statusBars())
        val navigationBarInsets = compat.getInsets(Type.navigationBars())
        when (v.id) {
            ViewIds.Storage.Trail.RootId -> v.updatePadding(top = statusBarInsets.top)

            ViewIds.Storage.List.RootId -> v.updatePadding(
                top = trail!!.measuredHeight,
                bottom = navigationBarInsets.bottom
            )

            ViewIds.Storage.Snack.RootId -> {
                v.updateLayoutParams<MarginLayoutParams> {
                    bottomMargin = navigationBarInsets.bottom + context.dp(4)
                }
            }
        }
        return insets
    }


    override fun onSlide(factor: Float) {
        val view = navigator?.bar ?: return
        val isScrolled = view.state == NavigationBar.STATE_SCROLLED_UP
        val snack = snack ?: return
        if (snack.state == StorageSnackBar.SCROLLED_UP) snack.updateLayoutParams<MarginLayoutParams> {
            val margin = VortexTheme.dimens.navigationBarHeightKey.get() * factor
            bottomMargin += if (isScrolled) margin.roundToInt() else -margin.roundToInt()
        }
    }

    override fun onStateChanged(state: Int) {
        val view = navigator?.bar ?: return
        val isScrolled = view.state == NavigationBar.STATE_SCROLLED_UP

        recycler?.updatePadding(
            bottom = if (isScrolled) view.height else recycler?.paddingBottom!!
        )
    }

    private fun createStorageSnackBar(context: Context): StorageSnackBar {
        if (snack == null) {
            snack = StorageSnackBar(context).apply {
                layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
                    leftMargin = 8.dp
                    rightMargin = 8.dp
                    gravity = CENTER or BOTTOM
                }
                observeInsets(this@StorageScreenController)
                container?.addView(this, 4)
            }
        }

        return snack!!
    }

    private fun wrapBar() {
        if (!counter.isZero) {
            navigator?.bar?.setTitleWithAnimation(
                title = counter.value.toString(),
                isReverse = counter.isBiggerThanLast
            )
        } else {
            navigator?.bar?.setTitleWithAnimation(
                title = controller.data().selectedTrailItem?.name,
                isReverse = trailAdapter.selected < controller.data().selectedTrailItemPosition
            )
        }
    }

    override fun onItemClick(
        view: View,
        position: Int
    ) {
        when (view.id) {
            ViewIds.Storage.List.ItemRootId -> {
                val item = controller.data().items[position]
                if (item.isDirectory) controller.navigate(item)
                else {
                    val screen = StorageFileEditorScreenController(context)
                    screen.setArgs(StorageFileEditorScreenController.Args.Source(item))
                    navigateForward(screen)
                }
            }

            ViewIds.Storage.List.ItemIconId -> controller.toggleSelection(position)
            ViewIds.Storage.Trail.ItemRootId -> controller.navigateFromTrail(position)
        }
    }

    override fun onLongItemClick(view: View, position: Int): Boolean {
        return when (view.id) {
            ViewIds.Storage.List.ItemRootId -> {
                if (controller.isSelected(position)) navigationForwardStorageOperationScreenController()
                else controller.select(position)
                true
            }

            ViewIds.Storage.List.ItemIconId -> {
                controller.select(position)
                navigationForwardStorageOperationScreenController()
                true
            }

            ViewIds.Storage.Trail.ItemRootId -> {
                val item = controller.data().trail[position]
                navigationForwardStorageOperationScreenController(item)
                true
            }

            else -> false
        }
    }

    private fun navigationForwardStorageOperationScreenController(
        items: List<PathItem> = controller.data().selected,
        renderer: StorageActionsRenderer = if (controller.data().selected.size == 1) {
            SingleItemStorageActionsRenderer(controller.data().selected[0])
        } else {
            MultipleItemStorageActionsRenderer(controller.data().selected)
        }
    ) {
        val screen = BottomStorageOperationScreenController(context)
        screen.setOperation(
            BottomStorageOperationScreenController.Operation.ChooseOption(
                items,
                renderer
            )
        )
        screen.attachCommunication(this)
        navigateForward(screen)
    }

    private fun navigationForwardStorageOperationScreenController(
        item: PathItem,
        renderer: StorageActionsRenderer = SingleItemStorageActionsRenderer(item)
    ) = navigationForwardStorageOperationScreenController(listOf(item), renderer)

    override fun handleEvent(controller: StorageScreenStateController): Unit = with(controller) {
        val event = extractEvent()

        if (event == BitStateController.IDLE) return

        if (event hasFlag StorageScreenStateController.UPDATE_SELECTION_COUNT) {
            counter.replace(data().selected.size)
            wrapBar()
        }

        if (event hasFlag StorageScreenStateController.UPDATE_LIST) {
            if (recycler?.isVisible == false) {
                animator?.cancel()
                createEventAnimator()
            }
            animator?.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationCancel(animation: Animator) {
                    recycler?.visibility = VISIBLE
                    recycler?.alpha = 1F
                    loading?.visibility = GONE
                    info?.visibility = GONE
                    animator = null
                }

                override fun onAnimationStart(animation: Animator) {
                    recycler?.visibility = GONE
                }

                override fun onAnimationEnd(animation: Animator) {
                    recycler?.visibility = VISIBLE
                    recycler?.alpha = 1F
                    loading?.visibility = GONE
                    info?.visibility = GONE
                    animator = null
                }
            })
            animator?.addUpdateListener { animator ->
                if (loading?.isVisible == true) loading?.alpha = 1F - animator.animatedFraction
                if (info?.isVisible == true) info?.alpha = 1F - animator.animatedFraction
                recycler?.alpha = animator.animatedFraction
            }

            adapter.replace(data().items)
        }

        if (event hasFlag StorageScreenStateController.UPDATE_TRAIL_LIST) {
            trailAdapter.replace(data().trail)
            wrapBar()
            trailAdapter.updateSelected(data().selectedTrailItemPosition)
        }

        if (event hasFlag StorageScreenStateController.UPDATE_TRAIL_SELECTED_CHANGED) {
            wrapBar()
            trailAdapter.updateSelected(data().selectedTrailItemPosition)
        }

        if (event hasFlag StorageScreenStateController.UPDATE_LOADING_PROGRESS) {
            if (loading?.isVisible == false) {
                animator?.cancel()
                createEventAnimator()
            }
            animator?.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationCancel(animation: Animator) {
                    loading?.visibility = VISIBLE
                    loading?.alpha = 1F
                    info?.visibility = GONE
                    recycler?.visibility = GONE
                    animator = null
                }

                override fun onAnimationStart(animation: Animator) {
                    loading?.visibility = GONE
                }

                override fun onAnimationEnd(animation: Animator) {
                    loading?.visibility = VISIBLE
                    loading?.alpha = 1F
                    info?.visibility = GONE
                    recycler?.visibility = GONE
                    animator = null
                }
            })
            animator!!.addUpdateListener { animator ->
                if (recycler?.isVisible == true) recycler?.alpha = 1F - animator.animatedFraction
                if (info?.isVisible == true) info?.alpha = 1F - animator.animatedFraction
                loading?.alpha = animator.animatedFraction
            }
        }

        if (event hasFlag StorageScreenStateController.UPDATE_MESSAGE) {
            if (info?.isVisible == false) {
                animator?.cancel()
                createEventAnimator()
            }
            animator?.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationCancel(animation: Animator) {
                    info?.visibility = VISIBLE
                    info?.alpha = 1F
                    loading?.visibility = GONE
                    recycler?.visibility = GONE
                    animator = null
                }

                override fun onAnimationStart(animation: Animator) {
                    info?.visibility = GONE
                }

                override fun onAnimationEnd(animation: Animator) {
                    info?.visibility = VISIBLE
                    info?.alpha = 1F
                    loading?.visibility = GONE
                    recycler?.visibility = GONE
                    animator = null
                }
            })
            animator?.addUpdateListener { animator ->
                if (recycler?.isVisible == true) recycler?.alpha = 1F - animator.animatedFraction
                if (loading?.isVisible == true) loading?.alpha = 1F - animator.animatedFraction
                info?.alpha = animator.animatedFraction
            }

            if (event hasFlag StorageScreenStateController.REASON_RESTRICTED_ITEM) {
                info?.icon = Icons.Rounded.Directory
                info?.message = VortexTheme.texts.storageListWarningRestrictedTitleKey.get()
            } else if (event hasFlag StorageScreenStateController.REASON_ITEM_DOES_NOT_EXIST) {
                info?.icon = Icons.Rounded.File
                info?.message = "Entry ${controller.data().selectedTrailItem?.name} doesn't exist"
            } else if (event hasFlag StorageScreenStateController.REASON_ITEM_IS_NOT_READABLE) {
                info?.icon = Icons.Rounded.File
                info?.message = "Entry ${controller.data().selectedTrailItem?.name} is not readable"
            } else if (event hasFlag StorageScreenStateController.REASON_EMPTY_ITEMS) {
                info?.icon = Icons.Rounded.Directory
                info?.message = Formatter(
                    VortexTheme.texts.storageListWarningEmptyTitleKey,
                    controller.data().selectedTrailItem?.name
                )
            } else if (event hasFlag REASON_STORAGE_PERMISSION) {
                info?.icon = Icons.Rounded.Info
                info?.message = VortexTheme.texts.storageListWarningStorageAccessTitleKey.get()
            } else if (event hasFlag REASON_STORAGE_FULL_PERMISSION) {
                info?.icon = Icons.Rounded.Info
                info?.message = VortexTheme.texts.storageListWarningFullStorageAccessTitleKey.get()
            }
        }

        if (event.hasFlags(REASON_STORAGE_PERMISSION, REASON_STORAGE_FULL_PERMISSION)) {
            if (isAndroidR) navigator?.bar?.replaceItems(listOf(StorageActions.ProvideFullStorageAccess))
            else navigator?.bar?.replaceItems(listOf(StorageActions.ProvideStorageAccess))
        }
        if (event.hasFlag(StorageScreenStateController.UPDATE_PENDING)) {
            navigator?.bar?.replaceItems(
                listOf(
                    NavigationActions.More,
                    NavigationActions.Search,
                    StorageActions.Tasks
                )
            )
        }

        animator?.start()
    }

    private fun createEventAnimator(
        duration: Long = DEFAULT_ANIMATOR_DURATION
    ): ValueAnimator {
        if (animator != null) return animator!!

        val animator = ValueAnimator.ofFloat(0F, 1F)

        animator.duration = duration

        this.animator = animator
        return animator
    }

    // TODO: Move logic to classes
    override fun handleProgress(event: StorageProgressEventReceiver.Event) {
        val snack = createStorageSnackBar(context)
        when (event) {
            is StorageProgressEventReceiver.Event.Idle -> {
                snack.hide(1000L)
            }

            is StorageProgressEventReceiver.Event.Failure -> {
                snack.title = event.error.toString()
            }

            is StorageProgressEventReceiver.Event.Delete -> {
                val index = event.sources.indexOf(event.entry)
                val progress = index / event.sources.size.toFloat()

                snack.progress = progress
                snack.title = "${event.entry.getNameAt()} deleted"
            }

            is StorageProgressEventReceiver.Event.Copy -> {
                val item = PathItem(event.entry)
                val index = event.sources.indexOf(event.entry)
                val progress = index / event.sources.size.toFloat()
                snack.progress = progress
                snack.title = "${item.name} copied to ${event.dest.getNameAt()}"
            }

            is StorageProgressEventReceiver.Event.Create -> {
                val item = PathItem(event.entry)
                snack.title = "${item.name} created"
                snack.progress = -1F
                snack.setAction("Navigate") {
                    controller.navigate(item)
                    recycler?.smoothScrollToPosition(controller.data().items.indexOf(item))
                }
                snack.showAndHideAfter(delay = 1000L)
            }

            is StorageProgressEventReceiver.Event.Progress -> {
                val current = event.current + 1
                val max = event.max
                snack.unit = current.toString()
                snack.title = "Progress ($max)"
                snack.updateProgress(current / max.toFloat())
            }
        }
    }


    override fun handleSelection(item: PathItem, index: Int) {
        adapter.applySelect(index)
    }


    override fun handleDeselection(item: PathItem, index: Int) {
        adapter.applyDeselect(index)
    }

    private fun createPrefixBundle(prefix: String): String {
        return prefix + CURRENT_PATH_ITEM
    }

    override fun onRestoreImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean {
        args = Args.NavDestination(
            buffer.parcelable<PathItem>(createPrefixBundle(prefix)) ?: return false
        )
        return true
    }

    override fun onSaveImpl(
        buffer: Bundle,
        prefix: String,
    ): Boolean {
        val current = controller.data().selectedTrailItem ?: return false

        buffer.putParcelable(createPrefixBundle(prefix), current)

        return true
    }

    override fun onActivityResultImpl(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
//        if (result != Activity.RESULT_OK) return false
        return when (requestCode) {
            Contracts.Storage.RequestStorageAccessCode -> {
                if (!controller.needPermission()) {
                    controller.startListenService(context.service)
                    controller.navigate(PathItem(getExternalStorageDirectory().asPath()))
                }
                true
            }

            Contracts.Storage.RequestFullStorageAccessCode -> {
                if (!controller.needPermission()) {
                    controller.startListenService(context.service)
                    controller.navigate(PathItem(getExternalStorageDirectory().asPath()))
                }
                true
            }

            else -> false
        }
    }

    override fun onBackActivatedImpl(): Boolean {
        if (controller.data().selectedTrailItem?.path != getExternalStorageDirectory().asPath()) {
            controller.navigateBack()
            return true
        }

        if (selected.isNotEmpty()) {
            controller.clearSelection()
            adapter.clearSelection()
            return true
        }

        return false
    }

    override fun onDestroyImpl() {
        controller.onDestroy()
        container?.removeAllViews()
        trail = null
        loading = null
        info = null
        container = null
    }

    override fun onFocusImpl() {

    }

    override fun onPrepareImpl() {
        if (!this::controller.isInitialized)
            context.component.inject(this)

        controller.bindListener(this)
        navigator?.bar?.addStateListener(this)

        if (!controller.needPermission()) {
            val item = extractPathFromArgs() ?: controller.data().selectedTrailItem
            ?: PathItem(getExternalStorageDirectory())
            controller.startListenService(context.service)
            controller.navigate(item)
        }

        controller.startListenEvent()

        wrapBar()
    }

    private fun extractPathFromArgs(): PathItem? {
        if (args != null && args is Args.NavDestination) {
            val item = (args as Args.NavDestination).item
            args = null
            return item
        }
        return null
    }

    override fun onHideImpl() {
        controller.bindListener(null)
        controller.stopListenEvent()
        controller.stopListenService()
        navigator?.bar?.removeStateListener(this)
    }

    interface Args {
        class NavDestination(val item: PathItem) : Args
    }

    companion object {
        private const val CURRENT_PATH_ITEM = "current_path_item"
        private const val DEFAULT_ANIMATOR_DURATION = 150L
    }
}