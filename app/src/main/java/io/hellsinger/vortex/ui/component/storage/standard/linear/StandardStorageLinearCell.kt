package io.hellsinger.vortex.ui.component.storage.standard.linear

import android.animation.ValueAnimator
import android.content.Context
import android.content.res.ColorStateList.valueOf
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.text.TextUtils
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.ImageView.ScaleType
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.view.contains
import io.hellsinger.theme.Theme
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.Bits.Companion.hasFlag
import io.hellsinger.vortex.Bits.Companion.removeFlag
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.ui.component.Paints
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView
import io.hellsinger.vortex.ui.component.storage.StorageAdapter.Companion.UPDATE_ITEM_DISABLE
import io.hellsinger.vortex.ui.component.storage.StorageAdapter.Companion.UPDATE_ITEM_SELECTION
import io.hellsinger.vortex.ui.component.storage.StorageAdapter.Companion.UPDATE_WITHOUT_ANIMATION
import io.hellsinger.vortex.ui.component.udp
import io.hellsinger.vortex.utils.ViewMeasure
import java.lang.Math.toRadians
import kotlin.math.cos
import kotlin.math.sin


class StandardStorageLinearCell(context: Context) : FrameLayout(context), RecyclableView<PathItem>,
    Theme.Listener {

    private val largeInnerPadding = 16.dp
    private val middleInnerPadding = 8.dp

    private val titlePadding =
        VortexTheme.dimens.storageListItemHorizontalTitlePaddingKey.get().udp
    private val infoPadding =
        VortexTheme.dimens.storageListItemHorizontalSubtitlePaddingKey.get().udp

    private val iconSize = 40.dp

    private var selectionFactor = 0F
        set(value) {
            if (field == value) return
            field = value
            invalidate()
        }

    private var enablementFactor = 0F
        set(value) {
            field = value
            invalidate()
        }

    private val containsTitle: Boolean
        get() = contains(titleView)

    private val containsInfo: Boolean
        get() = contains(infoView)

    private val containsIcon: Boolean
        get() = contains(iconView)

    private val iconSurface = RoundedCornerDrawable(
        key = VortexTheme.colors.storageListItemIconBackgroundColorKey,
        radius = 50F.dp
    )

    private val surface = RoundedCornerDrawable(
        key = VortexTheme.colors.storageListItemSurfaceColorKey,
        radius = 0F
    )

    private val iconView = ImageView(context).apply {
        layoutParams = LayoutParams(iconSize, iconSize)
        scaleType = ScaleType.CENTER_INSIDE
        id = ViewIds.Storage.List.ItemIconId

        background = createIconRippleBackground()
    }

    private val titleView = TextView(context).apply {
        textSize = 16F
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        isSingleLine = true
        maxLines = 1
        ellipsize = TextUtils.TruncateAt.MARQUEE
        marqueeRepeatLimit = -1
    }

    private val infoView = TextView(context).apply {
        textSize = 14F
        layoutParams = LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        isSingleLine = true
        maxLines = 1
        ellipsize = TextUtils.TruncateAt.MARQUEE
        marqueeRepeatLimit = -1
    }

    var title: String?
        get() = titleView.text.toString()
        set(value) {
            ensureContainingTitle()
            titleView.text = value
        }

    var info: String?
        get() = infoView.text.toString()
        set(value) {
            ensureContainingInfo()
            infoView.text = value
        }

    var icon: Drawable?
        get() = iconView.drawable
        set(value) {
            ensureContainingIcon()
            iconView.setImageDrawable(value)
        }

    var flag: Int = 0
        set(value) {
            if (value == field) return
            field = value removeFlag UPDATE_WITHOUT_ANIMATION
            checkSelectionAnimator(value)
        }

    private fun ensureContainingTitle() {
        if (!containsTitle) {
            addView(titleView)
        }
    }

    private fun ensureContainingInfo() {
        if (!containsInfo) {
            addView(infoView)
        }
    }

    private fun ensureContainingIcon() {
        if (!containsIcon) {
            addView(iconView)
        }
    }

    fun setIcon(@DrawableRes id: Int) {
        ensureContainingIcon()
        iconView.setImageResource(id)
    }

    override fun onKeyUpdate(key: Theme.Key<*>, value: Any?) {
        updateStateLists()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        VortexTheme.addThemeListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        VortexTheme.removeThemeListener(this)
    }

    init {
        id = ViewIds.Storage.List.ItemRootId
        isClickable = true
        isFocusable = true
        clipToOutline = true

        background = surface
        foreground = createRippleForeground()
        updateStateLists()
    }

    private fun createIconRippleBackground(): RippleDrawable {
        return RippleDrawable(
            valueOf(VortexTheme.colors.storageListItemIconTintColorKey.get()),
            iconSurface,
            null
        )
    }

    fun setOnIconClickListener(listener: OnClickListener?) {
        iconView.setOnClickListener(listener)
    }

    fun setOnIconLongClickListener(listener: OnLongClickListener?) {
        iconView.setOnLongClickListener(listener)
    }

    override fun onBindListener(listener: OnClickListener) {
        setOnClickListener(listener)
        setOnIconClickListener(listener)
    }

    override fun onBindLongListener(listener: OnLongClickListener) {
        setOnLongClickListener(listener)
        setOnIconLongClickListener(listener)
    }

    override fun onDrawForeground(canvas: Canvas) {
        val radians = toRadians(45.0)
        val centerX = ((iconView.left + iconView.right) / 2) + (iconView.width / 2 * sin(radians))
        val centerY = ((iconView.top + iconView.bottom) / 2) + (iconView.height / 2 * cos(radians))
        canvas.drawCircle(
            centerX.toFloat(),
            centerY.toFloat(),
            8F.dp * selectionFactor,
            Paints.filling(VortexTheme.colors.storageListItemIconSelectedTintColorKey)
        )
        super.onDrawForeground(canvas)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        ViewMeasure(
            widthMeasureSpec,
            heightMeasureSpec,
            VortexTheme.dimens.storageListItemLinearWidthDimenKey,
            VortexTheme.dimens.storageListItemLinearHeightDimenKey,
            ::setMeasuredDimension
        )

        measureChildren(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var widthLeft = largeInnerPadding
        if (containsIcon) {
            iconView.layout(
                widthLeft,
                (measuredHeight - iconView.measuredHeight) shr 1,
                widthLeft + iconView.measuredWidth,
                ((measuredHeight - iconView.measuredHeight) shr 1) + iconView.measuredHeight
            )
            widthLeft += iconView.measuredWidth
        }
        var titleY = if (!containsInfo) largeInnerPadding else middleInnerPadding
        if (containsTitle) {
            titleView.layout(
                widthLeft + titlePadding,
                titleY,
                widthLeft + titlePadding + titleView.measuredWidth,
                titleY + titleView.measuredHeight
            )
            titleY += titleView.measuredHeight
        }
        if (containsInfo) {
            infoView.layout(
                widthLeft + infoPadding,
                titleY,
                widthLeft + infoPadding + infoView.measuredWidth,
                titleY + infoView.measuredHeight
            )
        }
    }

    private fun checkSelectionAnimator(flag: Int) {
        val isSelected = flag hasFlag UPDATE_ITEM_SELECTION

        if (flag hasFlag UPDATE_WITHOUT_ANIMATION) {
            selectionFactor = if (isSelected) 1F else 0F
            return
        }

        createSelectionAnimator(isSelected).start()
    }

    private fun checkDisablementAnimator(flag: Int) {
        val isDisabled = flag hasFlag UPDATE_ITEM_DISABLE

        if (flag hasFlag UPDATE_WITHOUT_ANIMATION) {
            return
        }

        createDisablementAnimator()
    }

    private fun createSelectionAnimator(
        isSelected: Boolean
    ): ValueAnimator {
        val start: Float
        val end: Float

        if (isSelected) {
            start = 0F
            end = 1F
        } else {
            start = 1F
            end = 0F
        }

        val animator = ValueAnimator.ofFloat(start, end)
        animator.duration = 150L
        animator.addUpdateListener { anim ->
            selectionFactor =
                if (isSelected) anim.animatedFraction else selectionFactor - anim.animatedFraction
        }
        return animator
    }

    private fun createDisablementAnimator(): ValueAnimator {
        val isEnabled = flag hasFlag UPDATE_ITEM_DISABLE

        val start = if (isEnabled) 1F else 0F
        val end = if (isEnabled) 0F else 1F

        val animator = ValueAnimator.ofFloat(start, end)
        animator.duration = 150L
//        animator.interpolator = FastOutSlowInInterpolator()
        animator.addUpdateListener { }
        return animator
    }

    private fun updateStateLists() {
        titleView.setTextColor(VortexTheme.colors.storageListItemTitleTextColorKey.get())
        infoView.setTextColor(VortexTheme.colors.storageListItemSecondaryTextColorKey.get())
        iconView.imageTintList =
            valueOf(VortexTheme.colors.storageListItemIconTintColorKey.get())
        iconSurface.setTintList(valueOf(VortexTheme.colors.storageListItemIconBackgroundColorKey.get()))
    }

    private fun createRippleForeground(): RippleDrawable {
        return RippleDrawable(
            valueOf(VortexTheme.colors.storageListItemSurfaceRippleColorKey.get()),
            null,
            surface
        )
    }

}