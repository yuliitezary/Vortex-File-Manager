package io.hellsinger.vortex.ui.component.settings

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.RippleDrawable
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import android.widget.TextView
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.ui.component.RoundedCornerDrawable
import io.hellsinger.vortex.ui.component.dp
import io.hellsinger.vortex.ui.component.list.adapter.holder.RecyclableView

class SettingsColorCellView(context: Context) : FrameLayout(context), RecyclableView<SettingsItem>,
    SettingsCell {

    private val desireWidth = MATCH_PARENT
    private val desireHeight = 48.dp

    private val titleView = TextView(context).apply {
        layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
            gravity = Gravity.CENTER_VERTICAL or Gravity.START
        }
    }
    private val subtitleView = TextView(context).apply {
        layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
            gravity = Gravity.CENTER_VERTICAL or Gravity.END
        }
    }

    override var key: String
        get() = titleView.text.toString()
        set(value) {
            titleView.text = value
        }
    override var value: String
        get() = subtitleView.text.toString()
        set(value) {
            subtitleView.text = value
        }

    init {
        addView(titleView, 0)
        addView(subtitleView, 1)
        layoutParams = LayoutParams(desireWidth, desireHeight)
        background = RoundedCornerDrawable(VortexTheme.colors.storageListItemSurfaceColorKey)
        foreground = RippleDrawable(
            ColorStateList.valueOf(VortexTheme.colors.storageListItemSurfaceRippleColorKey.get()),
            null,
            background
        )
    }

    override fun onUnbind() {

    }

    override fun onBind(item: SettingsItem) {
        key = item.key
        value = item.value
    }
}