package io.hellsinger.vortex.ui.component.item.twoline

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class TwoLineItem(
    val title: String,
    val description: String
) : Parcelable