package io.hellsinger.vortex.ui.screen.storage.operation.rename

import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.ui.screen.BitStateController
import io.hellsinger.vortex.utils.storage
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

class StorageRenameItemStateController @Inject constructor(
    private val dispatchers: DispatcherProvider
) : BitStateController() {

    private var listener: Listener? = null

    override fun onEvent() {
        listener?.handleEvent(this)
    }

    override val scope: CoroutineScope = CoroutineScope(dispatchers.storage)

    fun bindListener(listener: Listener) {
        this.listener = listener
    }

    fun unbindListener() {
        this.listener = null
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindListener()
    }

    fun interface Listener {
        fun handleEvent(controller: StorageRenameItemStateController)
    }

}