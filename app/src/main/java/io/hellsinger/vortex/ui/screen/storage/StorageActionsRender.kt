package io.hellsinger.vortex.ui.screen.storage

import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.Bits.Companion.addFlag
import io.hellsinger.vortex.Bits.Companion.removeFlag
import io.hellsinger.vortex.ViewIds
import io.hellsinger.vortex.data.PathItem
import io.hellsinger.vortex.data.storage.StorageCopyPendingItem
import io.hellsinger.vortex.data.storage.StorageDeletePendingItem
import io.hellsinger.vortex.data.storage.StorageMovePendingItem
import io.hellsinger.vortex.data.storage.StoragePendingItem
import io.hellsinger.vortex.data.storage.StoragePendingTask
import io.hellsinger.vortex.ui.component.ItemFactory
import io.hellsinger.vortex.ui.component.list.adapter.Item
import io.hellsinger.vortex.ui.icon.Icons
import io.hellsinger.vortex.ui.screen.ListRenderer
import io.hellsinger.vortex.utils.divider
import io.hellsinger.vortex.utils.drawer
import io.hellsinger.vortex.utils.title

// Interface marker
interface StorageActionsRenderer : ListRenderer

class DefaultStorageActionsRenderer : StorageActionsRenderer {
    override fun render(): List<Item<*>> = buildList {
        title("Current directory")
        drawer(
            ViewIds.Storage.Menu.AddNewId,
            VortexTheme.texts.storageListOperationAddActionNewTitleKey,
            Icons.Rounded.FolderAdd
        )
        drawer(
            ViewIds.Storage.Menu.InfoId,
            VortexTheme.texts.storageListMoreInfoActionTitleKey,
            Icons.Rounded.Info
        )
        divider()
        title("Filter")
        drawer(-1, "Only directories", Icons.Rounded.Directory)
        drawer(-1, "Only files", Icons.Rounded.File)
        divider()
        title("Additional")
        drawer(
            ViewIds.Storage.Menu.StorageInfoId,
            "Storage Info",
            Icons.Rounded.Info
        )
    }
}

class SingleItemStorageActionsRenderer(private val item: PathItem) : StorageActionsRenderer {
    override fun render(): List<Item<*>> = buildList {
        title(VortexTheme.texts.storageListGroupOperationDefaultActionTitleKey)
        if (item.isDirectory) {
            drawer(
                ViewIds.Storage.Menu.OpenId,
                VortexTheme.texts.storageListOperationOpenTitleKey,
                Icons.Rounded.FolderOpen
            )
            drawer(
                ViewIds.Storage.Menu.AddNewId,
                VortexTheme.texts.storageListOperationAddActionNewTitleKey,
                Icons.Rounded.FolderAdd
            )
        }
        drawer(
            ViewIds.Storage.Menu.RenameId,
            VortexTheme.texts.storageListOperationRenameActionTitleKey,
            Icons.Rounded.Edit
        )
        drawer(
            ViewIds.Storage.Menu.DeleteId,
            VortexTheme.texts.storageListOperationDeleteActionTitleKey,
            Icons.Rounded.Delete
        )
        drawer(
            ViewIds.Storage.Menu.CopyId,
            VortexTheme.texts.storageListOperationCopyActionTitleKey,
            Icons.Rounded.Copy
        )
        drawer(
            ViewIds.Storage.Menu.MoveId,
            VortexTheme.texts.storageListOperationMoveActionTitleKey,
            Icons.Rounded.FileMove
        )
        divider()
        title("View")
        drawer(
            ViewIds.Storage.Menu.EditTagsId,
            "Edit tags",
            Icons.Rounded.Edit
        )
        drawer(
            ViewIds.Storage.Menu.AddSymbolicLinkId,
            "Add symbolic link",
            Icons.Rounded.Link
        )
        divider()
        title("Additional")
        drawer(
            ViewIds.Storage.Menu.ArchiveId,
            "Archive",
            Icons.Rounded.Archive
        )
        drawer(
            ViewIds.Storage.Menu.ChangePermissionsId,
            "Change permissions",
            Icons.Rounded.Edit
        )
        drawer(
            ViewIds.Storage.Menu.CopyPathId,
            VortexTheme.texts.storageListOperationCopyPathTitleKey,
            Icons.Rounded.Copy
        )
        drawer(
            ViewIds.Storage.Menu.InfoId,
            VortexTheme.texts.storageListMoreInfoActionTitleKey,
            Icons.Rounded.Info
        )
    }
}

class MultipleItemStorageActionsRenderer(
    private val items: List<PathItem>
) : StorageActionsRenderer {
    override fun render(): List<Item<*>> = buildList {
        title(VortexTheme.texts.storageListGroupOperationDefaultActionTitleKey)
        if (items.size == 2) {
            drawer(
                ViewIds.Storage.Menu.SwapId,
                VortexTheme.texts.storageListOperationSwapNamesActionTitleKey,
                Icons.Rounded.Swap
            )
        }
        drawer(
            ViewIds.Storage.Menu.DeleteId,
            VortexTheme.texts.storageListOperationDeleteActionTitleKey,
            Icons.Rounded.Delete
        )
        drawer(
            ViewIds.Storage.Menu.CopyId,
            VortexTheme.texts.storageListOperationCopyActionTitleKey,
            Icons.Rounded.Copy
        )
        drawer(
            ViewIds.Storage.Menu.MoveId,
            VortexTheme.texts.storageListOperationMoveActionTitleKey,
            Icons.Rounded.FileMove
        )
        divider()
        title("Additional")
        drawer(
            ViewIds.Storage.Menu.ArchiveId,
            "Archive",
            Icons.Rounded.Archive
        )
        drawer(
            ViewIds.Storage.Menu.CopyPathId,
            VortexTheme.texts.storageListOperationCopyPathTitleKey,
            Icons.Rounded.Copy
        )
    }
}

class PendingStorageActionsRenderer(
    private val pending: List<StoragePendingItem>
) : StorageActionsRenderer {
    override fun render(): List<Item<*>> = buildList {
        pending.mapIndexed { index, item ->
            var title = ""
            var desc = ""
            var type = -1
            when (item) {
                is StorageDeletePendingItem -> {
                    title = "Delete (${item.src.size})"
                    desc = "Tap to prepare transaction"
                    type = StoragePendingTask.Type.Delete addFlag StoragePendingTask.Type.Pending
                }

                is StorageMovePendingItem -> {
                    title = "Move (${item.src.size})"
                    desc = "Tap to prepare transaction in current directory"
                    type = StoragePendingTask.Type.Move addFlag StoragePendingTask.Type.Pending
                }

                is StorageCopyPendingItem -> {
                    title = "Copy (${item.src.size})"
                    desc = "Tap to prepare transaction in current directory"
                    type = StoragePendingTask.Type.Copy addFlag StoragePendingTask.Type.Pending
                }
            }
            ItemFactory.StoragePendingItem(index.toLong(), title, desc, type)
        }.groupBy { item ->
            when (item.value.type removeFlag StoragePendingTask.Type.Pending) {
                StoragePendingTask.Type.Delete -> "Delete"
                StoragePendingTask.Type.Copy -> "Copy"
                StoragePendingTask.Type.Move -> "Move"
                else -> ""
            }
        }.forEach { (title, tasks) ->
            title(title)
            addAll(tasks)
        }
    }

}