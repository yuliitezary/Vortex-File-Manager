package io.hellsinger.vortex.ui.screen

import io.hellsinger.vortex.ui.component.list.adapter.Item

interface ListRenderer {

    fun render(): List<Item<*>>
}