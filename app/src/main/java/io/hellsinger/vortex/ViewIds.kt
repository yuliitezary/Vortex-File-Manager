package io.hellsinger.vortex

import android.os.Build
import androidx.annotation.RequiresApi

object ViewIds {

    object Navigation {

        object Menu {
            const val NavigationId = R.id.navigation_menu
            const val FileManagerId = R.id.file_manager_dest
            const val MediaId = R.id.storage_media_dest
            const val SettingsId = R.id.settings_dest

            const val SearchId = R.id.file_manager_menu_search
            const val MoreId = R.id.file_manager_menu_more
        }
    }

    object Storage {
        object Create {
            const val NameFieldId = R.id.file_manager_create_name_field
            const val PathFieldId = R.id.file_manager_create_path_field
            const val TypeFieldId = R.id.file_manager_create_type_field

            const val UserReadId = R.id.file_manager_create_user_check_read
            const val UserWriteId = R.id.file_manager_create_user_check_write
            const val UserExecuteId = R.id.file_manager_create_user_check_execute

            const val GroupReadId = R.id.file_manager_create_group_check_read
            const val GroupWriteId = R.id.file_manager_create_group_check_write
            const val GroupExecuteId = R.id.file_manager_create_group_check_execute

            const val OtherReadId = R.id.file_manager_create_other_check_read
            const val OtherWriteId = R.id.file_manager_create_other_check_write
            const val OtherExecuteId = R.id.file_manager_create_other_check_execute
        }

        object Snack {
            const val ConfirmButtonId = R.id.file_manager_operation_confirm_button
            const val RootId = R.id.file_manager_operation_progress
        }

        object Menu {
            const val AddSymbolicLinkId = R.id.file_manager_menu_add_symbolic_link_op
            const val ArchiveId = R.id.file_manager_menu_archive_op
            const val OpenId = R.id.file_manager_menu_open_op
            const val CheckId = R.id.file_manager_menu_check
            const val AddNewId = R.id.file_manager_menu_add_new_op
            const val DeleteId = R.id.file_manager_menu_delete_op
            const val RenameId = R.id.file_manager_menu_rename_op
            const val CopyId = R.id.file_manager_menu_copy_op
            const val CopyPathId = R.id.file_manager_menu_copy_path_op
            const val MoveId = R.id.file_manager_menu_move_op
            const val InfoId = R.id.file_manager_menu_info_op

            const val SwapId = R.id.file_manager_menu_swap_op

            const val ChangePermissionsId = R.id.file_manager_menu_change_permissions

            const val EditTagsId = R.id.file_manager_menu_edit_tags_op

            const val StorageInfoId = R.id.file_manager_menu_storage_info_op

            const val AddWatcherId = R.id.file_manager_menu_add_watcher_op
            const val RemoveWatcherId = R.id.file_manager_menu_remove_watcher_op

            const val TasksId = R.id.file_manager_menu_tasks

            @RequiresApi(Build.VERSION_CODES.R)
            const val ProvideFullStorageAccessId =
                R.id.file_manager_menu_provide_full_storage_access_op
            const val ProvideStorageAccessId = R.id.file_manager_menu_provide_storage_access_op
        }

        object List {
            const val RootId = R.id.file_manager_list
            const val ItemRootId = R.id.file_manager_list_item_root
            const val ItemIconId = R.id.file_manager_list_item_icon
        }

        object Task {
            const val RootId = R.id.file_manager_task_root
        }

        object Trail {
            const val RootId = R.id.file_manager_trail
            const val ItemRootId = R.id.file_manager_trail_item_root
        }
    }

}