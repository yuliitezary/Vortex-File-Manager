package io.hellsinger.vortex

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.res.Configuration.UI_MODE_NIGHT_MASK
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.Process
import android.util.Log
import io.hellsinger.theme.vortex.InitVortexTheme
import io.hellsinger.vortex.di.AppComponent
import io.hellsinger.vortex.ui.icon.IconInitializer
import io.hellsinger.vortex.ui.screen.error.ErrorActivity
import io.hellsinger.vortex.di.DaggerAppComponent.builder as AppComponentBuilder

class App : Application() {

    private var component: AppComponent? = null

    fun component(): AppComponent = checkNotNull(component)

    override fun onCreate() {
        super.onCreate()
        component = AppComponentBuilder().bindContext(this).build()

        val isDarkMode = resources.configuration.uiMode and UI_MODE_NIGHT_MASK == UI_MODE_NIGHT_YES
        InitVortexTheme(isDarkMode)

        IconInitializer.context = this

        Thread.setDefaultUncaughtExceptionHandler { thread, error ->
            val intent = Intent(this, ErrorActivity::class.java)
            intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_NEW_TASK or
                        Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
            )

            Log.e("Loggable", null, error)

            intent.putExtra("error", error)

            startActivity(intent)

            Process.killProcess(Process.myPid())
        }
    }
}

val Context.component: AppComponent
    get() = when (this) {
        is App -> component()
        else -> applicationContext.component
    }