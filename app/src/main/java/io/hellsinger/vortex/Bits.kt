package io.hellsinger.vortex

interface Bits {
    infix fun Int.removeFlag(flag: Int): Int = and(flag.inv())

    infix fun Int.addFlag(flag: Int): Int = or(flag)

    infix fun Int.hasFlag(flag: Int): Boolean = and(flag) == flag

    companion object : Bits
}

inline fun <V> bits(block: Bits.() -> V) = block(Bits)

context (Bits)
fun Int.hasFlags(first: Int, second: Int) = hasFlag(first) || hasFlag(second)

context (Bits)
fun Int.hasFlags(first: Int, second: Int, third: Int): Boolean {
    return hasFlag(first) || hasFlag(second) || hasFlag(third)
}

context (Bits)
fun Int.hasFlags(first: Int, second: Int, third: Int, fourth: Int): Boolean {
    return hasFlag(first) || hasFlag(second) || hasFlag(third) || hasFlag(fourth)
}