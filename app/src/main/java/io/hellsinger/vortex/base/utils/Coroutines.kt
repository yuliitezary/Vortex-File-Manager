package io.hellsinger.vortex.base.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

fun launchIn(
    context: CoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
) = CoroutineScope(context).launch(start = start, block = block)

fun launchIn(
    scope: CoroutineScope,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
) = scope.launch(start = start, block = block)

fun <T> asyncIn(
    context: CoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> T
) = CoroutineScope(context).async(start = start, block = block)

fun <T> asyncIn(
    scope: CoroutineScope,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> T
) = scope.async(start = start, block = block)