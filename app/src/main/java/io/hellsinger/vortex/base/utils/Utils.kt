package io.hellsinger.vortex.base.utils

import android.content.Context
import android.util.Log
import android.widget.Toast

fun <T : Any?> T.logIt(tag: String = "Loggable", message: String = "", type: Int = Log.VERBOSE): T {
    Log.println(type, tag, message + this.toString())
    return this
}

fun <T : Any> T.toastIt(
    context: Context,
    message: String = "",
    duration: Int = Toast.LENGTH_SHORT,
): T {
    Toast.makeText(context, message + this.toString(), duration).show()
    return this
}