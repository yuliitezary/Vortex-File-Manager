cmake_minimum_required(VERSION 3.22.1)

project("filesystem-linux")

add_library(
        ${CMAKE_PROJECT_NAME}
        SHARED
        filesystem-linux.h
        filesystem-linux.cpp
)

target_link_libraries(
        ${CMAKE_PROJECT_NAME}
        android
        log
)
