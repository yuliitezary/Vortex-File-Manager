package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Open
import io.hellsinger.filesystem.linux.file.LinuxFile
import io.hellsinger.vortex.filesystem.path.Path
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

suspend fun <P : Path> LinuxFile<P>.sendFile(
    dest: P,
    count: Int = LinuxFileOperationProvider.getStatus(path.bytes)!!.size.toInt(),
    offset: Long? = null,
    onBytes: suspend (Int) -> Unit
) {
    val fd = LinuxFileOperationProvider.openFileDescriptor(
        path = path.bytes,
        flags = Open.NonBlocking or Open.ReadOnly,
        mode = 777
    )
    val destFD = LinuxFileOperationProvider.openFileDescriptor(
        path = dest.bytes,
        flags = Open.NonBlocking or Open.WriteOny or Open.CreateNew,
        mode = 777
    )

    while (true) {
        val bytes = send(fd, destFD, offset, count)
        if (bytes == 0) break
        onBytes(bytes)
    }

    LinuxFileOperationProvider.closeFileDescriptor(destFD)
    LinuxFileOperationProvider.closeFileDescriptor(fd)
}

private suspend fun send(
    fd: Int,
    destFD: Int,
    offset: Long?,
    count: Int,
): Int = suspendCoroutine { continuation ->
    continuation.resume(
        LinuxFileOperationProvider.sendBytes(
            fd,
            destFD,
            offset,
            count
        )
    )
}