package io.hellsinger.filesystem.linux.attribute

import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.ExecuteGroup
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.ExecuteOther
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.ExecuteUser
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.ReadGroup
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.ReadOther
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.ReadUser
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.WriteGroup
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.WriteOther
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Permission.WriteUser

fun Int.toLinuxEntryModeString() = buildString(capacity = 9) {
    if (and(ReadUser) > 0) append('r') else append('-')
    if (and(WriteUser) > 0) append('w') else append('-')
    if (and(ExecuteUser) > 0) append('x') else append('-')
    if (and(ReadGroup) > 0) append('r') else append('-')
    if (and(WriteGroup) > 0) append('w') else append('-')
    if (and(ExecuteGroup) > 0) append('x') else append('-')
    if (and(ReadOther) > 0) append('r') else append('-')
    if (and(WriteOther) > 0) append('w') else append('-')
    if (and(ExecuteOther) > 0) append('x') else append('-')
}