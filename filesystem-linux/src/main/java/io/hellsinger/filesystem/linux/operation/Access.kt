package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxOperationOptions
import io.hellsinger.vortex.filesystem.path.Path

fun <P : Path> P.access(mode: Int) = LinuxFileOperationProvider.access(bytes, mode)

inline val <P : Path> P.writeable
    get() = access(LinuxOperationOptions.Access.Writable) == 0