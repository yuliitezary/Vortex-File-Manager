package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxOperationOptions
import io.hellsinger.vortex.filesystem.checkScheme
import io.hellsinger.vortex.filesystem.path.Path

fun <P : Path> P.changeMode(mode: Int) {
    checkScheme(LinuxOperationOptions.SCHEME)
    LinuxFileOperationProvider.changeMode(bytes, mode)
}
