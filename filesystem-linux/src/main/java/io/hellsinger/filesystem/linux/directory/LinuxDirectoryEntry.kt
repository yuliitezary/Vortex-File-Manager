package io.hellsinger.filesystem.linux.directory

import io.hellsinger.vortex.filesystem.attribute.PathAttribute
import io.hellsinger.vortex.filesystem.directory.DirectoryEntry
import io.hellsinger.vortex.filesystem.path.Path

interface LinuxDirectoryEntry<P : Path> : DirectoryEntry<P> {
    val attributes: PathAttribute
}