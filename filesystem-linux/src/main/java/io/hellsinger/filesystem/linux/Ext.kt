package io.hellsinger.filesystem.linux

import io.hellsinger.filesystem.linux.path.LinuxPath
import io.hellsinger.vortex.filesystem.checkScheme
import io.hellsinger.vortex.filesystem.path.Path
import java.io.File

fun String.toLinuxPath(): Path = LinuxPath(encodeToByteArray())
fun File.toLinuxPath(): Path = LinuxPath(absolutePath.encodeToByteArray())

fun Path.resolve(other: String): Path {
    checkScheme(LinuxOperationOptions.SCHEME)
    return resolve(other.encodeToByteArray())
}