package io.hellsinger.filesystem.linux.directory

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.attribute.attributes
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.path.LinuxPath
import io.hellsinger.vortex.filesystem.attribute.PathAttribute
import io.hellsinger.vortex.filesystem.attribute.isDirectory
import io.hellsinger.vortex.filesystem.directory.Directory
import io.hellsinger.vortex.filesystem.directory.DirectoryEntry
import io.hellsinger.vortex.filesystem.directory.DirectoryWalker
import io.hellsinger.vortex.filesystem.directory.DirectoryWalker.Companion.CONTINUE
import io.hellsinger.vortex.filesystem.directory.DirectoryWalker.Companion.SKIP
import io.hellsinger.vortex.filesystem.directory.DirectoryWalker.Companion.STOP
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.path.PathOwner
import io.hellsinger.vortex.filesystem.size.SiSize
import io.hellsinger.vortex.filesystem.size.Size
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

interface LinuxDirectory<P : Path> : Directory<P> {
    val name: String
        get() = path.getNameAt().toString()

    suspend fun count(): Int
}

suspend fun <P : Path> LinuxDirectory<P>.isEmpty(): Boolean = count() == 0

private class LinuxDirectoryImpl<P : Path>(
    override val path: P,
) : LinuxDirectory<P> {

    override fun walk(walker: DirectoryWalker): Flow<DirectoryEntry<P>> = flow {
        val pointer = LinuxFileOperationProvider.openDirectoryPointer(path.bytes)
        var entry = LinuxFileOperationProvider.getDirectoryEntry(pointer)
        while (entry != null) {
            if (entry.contentEquals(".".encodeToByteArray()) || entry.contentEquals("..".encodeToByteArray())) {
                entry = LinuxFileOperationProvider.getDirectoryEntry(pointer)
                continue
            }

            val resolvedPath = path.resolve(LinuxPath(entry)) as P
            val attrs = LinuxFileOperationProvider.getStatusAtDir(pointer, resolvedPath.bytes)
                ?: throw Throwable("Path doesn't exist")
            if (attrs.isDirectory) {
                when (walker.onPreEnterDirectory(resolvedPath)) {
                    STOP -> {
                        walker.onPostEnterDirectory(resolvedPath)
                        break
                    }

                    SKIP -> emit(LinuxDirectoryEntryImpl(resolvedPath, attrs))

                    CONTINUE -> {
                        emit(LinuxDirectoryEntryImpl(resolvedPath, attrs))
                        emitAll(LinuxDirectoryImpl(resolvedPath).walk(walker))
                    }
                }
                walker.onPostEnterDirectory(resolvedPath);
            } else {
                when (walker.onEntry(resolvedPath)) {
                    STOP -> {
                        emit(LinuxDirectoryEntryImpl(resolvedPath, attrs))
                        break
                    }

                    // Do nothing
                    SKIP -> {}

                    else -> emit(LinuxDirectoryEntryImpl(resolvedPath, attrs))
                }
            }

            entry = LinuxFileOperationProvider.getDirectoryEntry(pointer)
        }

        LinuxFileOperationProvider.closeDirectoryPointer(pointer)
    }

    override suspend fun count(): Int = LinuxFileOperationProvider.getDirectoryCount(path.bytes)
}

fun <P : Path> P.walk(walker: DirectoryWalker): Flow<DirectoryEntry<P>> {
    return if (attributes.isDirectory) asLinuxDirectory().walk(walker) else emptyFlow()
}

fun <P : Path> P.asLinuxDirectory(): LinuxDirectory<P> = LinuxDirectoryImpl(this)
fun <P : Path> PathOwner<P>.asLinuxDirectory(): LinuxDirectory<P> = LinuxDirectoryImpl(path)

private class LinuxDirectoryEntryImpl<P : Path>(
    override val path: P,
    override val attributes: PathAttribute,
) : LinuxDirectoryEntry<P>

fun <P : Path> LinuxDirectory<P>.defaultWalker(): Flow<DirectoryEntry<P>> = walk(
    object : DirectoryWalker {
        override suspend fun onPreEnterDirectory(path: Path): Int = SKIP
        override suspend fun onPostEnterDirectory(path: Path) {}
        override suspend fun onEntry(path: Path): Int = CONTINUE
    }
)

fun <P : Path> LinuxDirectory<P>.treeWalker(): Flow<DirectoryEntry<P>> = walk(
    object : DirectoryWalker {
        override suspend fun onPreEnterDirectory(path: Path): Int = CONTINUE
        override suspend fun onPostEnterDirectory(path: Path) {}
        override suspend fun onEntry(path: Path): Int = CONTINUE
    }
)

suspend fun <P : Path> Directory<P>.size(): Size {
    return suspendCoroutine { continuation ->
        try {
            continuation.resume(SiSize(LinuxFileOperationProvider.getDirectorySize(path.bytes)))
        } catch (exception: ErrnoException) {
            continuation.resumeWithException(exception) // rethrow
        }
    }
}
