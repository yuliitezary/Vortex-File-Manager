package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxFileOperationProvider.getByte
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Open
import io.hellsinger.filesystem.linux.file.LinuxFile
import io.hellsinger.vortex.filesystem.path.Path
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

fun <P : Path> LinuxFile<P>.read(
    flags: Int = Open.NonBlocking,
    mode: Int = DefaultOpenMode,
    buffer: Long,
    readChunkSize: Int = DEFAULT_BUFFER_SIZE
): Flow<Byte> = flow {
    val fd = LinuxFileOperationProvider.openFileDescriptor(
        path.bytes,
        flags or Open.ReadOnly,
        mode
    )
    var position = 0
    while (true) {
        val count = LinuxFileOperationProvider.read(fd, position, buffer, readChunkSize)
        if (count <= 0) break // Reached end of file (EOF)
        position += count
        for (i in 0..count) emit(getByte(buffer = buffer, index = i))
    }
    LinuxFileOperationProvider.closeFileDescriptor(fd)
}

fun <P : Path> LinuxFile<P>.read(
    flags: Int = Open.NonBlocking,
    mode: Int = DefaultOpenMode,
    bufferSize: Int = DEFAULT_BUFFER_SIZE
): Flow<Byte> = flow {
    val buffer = LinuxFileOperationProvider.allocate(bufferSize)
    read(flags, mode, buffer, bufferSize).collect(::emit)
    LinuxFileOperationProvider.free(buffer)
}