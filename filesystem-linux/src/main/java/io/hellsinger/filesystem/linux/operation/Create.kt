package io.hellsinger.filesystem.linux.operation

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxOperationOptions
import io.hellsinger.vortex.filesystem.checkScheme
import io.hellsinger.vortex.filesystem.directory.Directory
import io.hellsinger.vortex.filesystem.file.File
import io.hellsinger.vortex.filesystem.path.Path

fun <P : Path> Directory<P>.create(mode: Int = 777) {
    checkScheme(LinuxOperationOptions.SCHEME)

    LinuxFileOperationProvider.makeDirectory(path.bytes, mode)
}

fun <P : Path> File<P>.create(mode: Int = 777) {
    checkScheme(LinuxOperationOptions.SCHEME)

    val fd = LinuxFileOperationProvider.openFileDescriptor(
        path.bytes,
        LinuxOperationOptions.Open.CreateNew,
        mode
    )

    LinuxFileOperationProvider.closeFileDescriptor(fd)
}

fun <P : Path> File<P>.truncate(mode: Int = 777) {
    checkScheme(LinuxOperationOptions.SCHEME)

    val fd = LinuxFileOperationProvider.openFileDescriptor(
        path.bytes,
        LinuxOperationOptions.Open.Truncate,
        mode
    )

    LinuxFileOperationProvider.closeFileDescriptor(fd)
}