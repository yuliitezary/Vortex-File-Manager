package io.hellsinger.filesystem.linux.attribute

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxOperationOptions
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Attributes
import io.hellsinger.filesystem.linux.operation.access
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_BLOCK_DEVICE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_CHAR_DEVICE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_DIRECTORY
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_FILE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_LINK
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_PIPE
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_SOCKET
import io.hellsinger.vortex.filesystem.attribute.PathAttribute
import io.hellsinger.vortex.filesystem.attribute.PathAttributeWrapper
import io.hellsinger.vortex.filesystem.checkScheme
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.filesystem.path.PathOwner

interface LinuxPathAttribute : PathAttribute {
    val userId: Int
    val groupId: Int
    val mode: Int
}

internal class LinuxPathAttributeImpl(
    override val userId: Int,
    override val groupId: Int,
    override val mode: Int,
    // inode
    override val id: Long,
    override val size: Long,
    lastAccessTimeSeconds: Long,
    lastAccessTimeNanos: Long,
    lastModifiedTimeSeconds: Long,
    lastModifiedTimeNanos: Long,
    creationTimeSeconds: Long,
    creationTimeNanos: Long,
) : PathAttributeWrapper(
    creationTimeNanos,
    creationTimeSeconds,
    lastAccessTimeNanos,
    lastAccessTimeSeconds,
    lastModifiedTimeNanos,
    lastModifiedTimeSeconds
), LinuxPathAttribute {

    override val type: Int
        get() = when (mode and Attributes.S_IFMT) {
            Attributes.S_IFDIR -> PATH_ATTR_TYPE_DIRECTORY
            Attributes.S_IFLNK -> PATH_ATTR_TYPE_LINK
            Attributes.S_IFREG -> PATH_ATTR_TYPE_FILE
            Attributes.S_IFIFO -> PATH_ATTR_TYPE_PIPE
            Attributes.S_IFSOCK -> PATH_ATTR_TYPE_SOCKET
            Attributes.S_IFBLK -> PATH_ATTR_TYPE_BLOCK_DEVICE
            Attributes.S_IFCHR -> PATH_ATTR_TYPE_CHAR_DEVICE
            else -> -1
        }

    override val permission: String = mode.toLinuxEntryModeString()
}

val Path.attributes: PathAttribute
    get() {
        checkScheme(LinuxOperationOptions.SCHEME)

        return LinuxFileOperationProvider.getStatus(bytes) ?: throw Throwable("Path don't exist")
    }

val Path.attributesOrNull: PathAttribute?
    get() {
        checkScheme(LinuxOperationOptions.SCHEME)

        return LinuxFileOperationProvider.getStatus(bytes)
    }

val Path.exists: Boolean
    get() = access(LinuxOperationOptions.Access.Existance) == 0

val <P : Path> PathOwner<P>.attributes: PathAttribute
    get() = path.attributes
