package io.hellsinger.filesystem.linux.attribute

import io.hellsinger.filesystem.linux.LinuxFileOperationProvider
import io.hellsinger.filesystem.linux.LinuxOperationOptions
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Status.System.NoDeviceFiles
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Status.System.NoExecution
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Status.System.NoUpdateAccessTime
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Status.System.NoUpdateDirectoryAccessTime
import io.hellsinger.filesystem.linux.LinuxOperationOptions.Status.System.ReadOnly
import io.hellsinger.vortex.filesystem.checkScheme
import io.hellsinger.vortex.filesystem.path.Path

interface LinuxFileSystemInfo {

    val id: Long

    val totalSpace: Long

    val usedSpace: Long

    val freeSpace: Long

    val availableSpace: Long

    val maxNameLength: Long

    val hasAccessToDevices: Boolean

    val hasAccessToExecute: Boolean

    val updatesAccessTime: Boolean

    val updatesDirectoryAccessTime: Boolean

    val isReadOnly: Boolean

}

internal class LinuxFileSystemAttributeImpl(
    val blockSize: Long,
    val fragmentSize: Long,
    val blockCount: Long,
    val freeBlockCount: Long,
    val availableBlockCount: Long,
    val fileCount: Long,
    val freeFileCount: Long,
    val availableFileCount: Long,
    override val id: Long,
    val flags: Long,
    override val maxNameLength: Long
) : LinuxFileSystemInfo {
    override val totalSpace: Long
        get() = blockCount * blockSize

    override val usedSpace: Long
        get() = (blockCount - freeBlockCount) * blockSize

    override val freeSpace: Long
        get() = freeBlockCount * blockSize

    override val availableSpace: Long
        get() = availableBlockCount * blockSize

    override val updatesAccessTime: Boolean
        get() = (flags and NoUpdateAccessTime) == NoUpdateAccessTime

    override val isReadOnly: Boolean
        get() = (flags and ReadOnly) == ReadOnly

    override val updatesDirectoryAccessTime: Boolean
        get() = (flags and NoUpdateDirectoryAccessTime) == NoUpdateDirectoryAccessTime

    override val hasAccessToDevices: Boolean
        get() = (flags and NoDeviceFiles) != NoDeviceFiles

    override val hasAccessToExecute: Boolean
        get() = (flags and NoExecution) != NoExecution
}

val Path.fileSystemInfo: LinuxFileSystemInfo
    get() {
        checkScheme(LinuxOperationOptions.SCHEME)
        return LinuxFileOperationProvider.getFileSystemStatus(bytes)
    }