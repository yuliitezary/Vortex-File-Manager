package io.hellsinger.filesystem.linux

object LinuxOperationOptions {

    object Access {
        const val Existance = 0
        const val Executable = 1
        const val Writable = 2
        const val Readable = 4
    }

    internal object Attributes {
        const val S_IFMT = 61440

        const val S_IFIFO = 4096
        const val S_IFCHR = 8192
        const val S_IFDIR = 16384
        const val S_IFBLK = 24576
        const val S_IFREG = 32768
        const val S_IFLNK = 40960
        const val S_IFSOCK = 49152
    }

    object Permission {
        const val ReadUser = 256
        const val WriteUser = 128
        const val ExecuteUser = 64

        const val ReadGroup = 32
        const val WriteGroup = 16
        const val ExecuteGroup = 8

        const val ReadOther = 4
        const val WriteOther = 2
        const val ExecuteOther = 1
    }

    object Observe {

        const val Access = 1

        // Some files were edited
        const val Modify = 2
        const val Attribute = 4
        const val CloseWriting = 8
        const val CLOSE_NOWRITE = 16
        const val Open = 32
        const val MoveFrom = 64
        const val MoveTo = 128

        // Something was created
        const val Create = 256

        // Something was deleted
        const val Delete = 512
        const val DeleteSelf = 1024
        const val MoveSelf = 2048
        const val Unmount = 8192
        const val Overflow = 16_384
        const val Ignored = 32_768
        const val Close = CloseWriting or CLOSE_NOWRITE
        const val Move = MoveFrom or MoveTo

        const val ONLYDIR = 16_777_216
        const val DONT_FOLLOW = 33_554_432
        const val EXCL_UNLINK = 67_108_864
        const val MASK_CREATE = 268_435_456
        const val MASK_ADD = 536_870_912

        object Attributes {
            const val Dir = 107_374_1824
        }

        inline val Default
            get() = Delete or Create

        const val All = Access or
                Modify or
                Attribute or
                CloseWriting or
                CLOSE_NOWRITE or
                Open or
                MoveFrom or
                MoveTo or
                Delete or
                Create or
                DeleteSelf or
                MoveSelf

        fun maskToString(mask: Int): String {
            val builder = StringBuilder("(")
            if (mask and Access == Access) builder.append(" Access ")
            if (mask and Attribute == Attribute) builder.append(" Attribute ")
            if (mask and Open == Open) builder.append(" Open ")
            if (mask and MoveFrom == MoveFrom) builder.append(" Move-from ")
            if (mask and MoveTo == MoveTo) builder.append(" Move-to ")
            if (mask and Delete == Delete) builder.append(" Delete ")
            if (mask and Create == Create) builder.append(" Create ")
            if (mask and DeleteSelf == DeleteSelf) builder.append(" Delete-self ")
            if (mask and MoveSelf == MoveSelf) builder.append(" Move-self ")
            builder.append(")")

            return builder.toString()
        }
    }

    object Open {
        const val ReadOnly = 0
        const val WriteOny = 1
        const val ReadAndWrite = 2
        const val Async = 32
        const val CreateNew = 64
        const val Truncate = 512
        const val Append = 1024
        const val NonBlocking = 2048
        const val CloseOnExit = 524288
        const val Path = 2097152
    }

    object Copy {
        const val NoFollowLinks = 32
        const val ReplaceExists = 64

        const val All = NoFollowLinks or ReplaceExists
    }

    object Delete {
        const val NotifyAll = 128
        const val RemoveDirectory = 512

        const val All = NotifyAll
    }

    object Status {
        object System {
            const val ReadOnly: Long = 0x0001
            const val NoDeviceFiles: Long = 0x0004
            const val NoExecution: Long = 0x0008
            const val NoUpdateAccessTime: Long = 0x0400
            const val NoUpdateDirectoryAccessTime: Long = 0x0800
        }
    }

    const val SCHEME = 0
}