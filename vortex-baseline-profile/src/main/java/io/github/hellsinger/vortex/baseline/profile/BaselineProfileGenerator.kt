package io.github.hellsinger.vortex.baseline.profile

import android.os.Build
import androidx.benchmark.macro.MacrobenchmarkScope
import androidx.benchmark.macro.junit4.BaselineProfileRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.uiautomator.By
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

private const val ALLOW_PASCAL_CASE_TEXT = "Allow"
private const val ALLOW_UPPERCASE_TEXT = "ALLOW"
private const val ALLOW_ONLY_WHILE_USING_THE_APP_TEXT = "Allow only while using the app"
private const val WHILE_USING_THE_APP_TEXT = "While using the app"

@RunWith(AndroidJUnit4::class)
@LargeTest
class BaselineProfileGenerator {

    @get:Rule
    val rule = BaselineProfileRule()

    @Test
    fun generate() = rule.collect("io.github.excu101.vortex") {
        // This block defines the app's critical user journey. Here we are interested in
        // optimizing for app startup. But you can also navigate and scroll
        // through your most important UI.

        // Start default activity for your app
        pressHome()
        startActivityAndWait()

        grantPermission()

        // TODO Write more interactions to optimize advanced journeys of your app.
        // For example:
        // 1. Wait until the content is asynchronously loaded
        // 2. Scroll the feed content
        // 3. Navigate to detail screen

        // Check UiAutomator documentation for more information how to interact with the app.
        // https://d.android.com/training/testing/other-components/ui-automator
    }
}

fun MacrobenchmarkScope.grantPermission() {
    val accept = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        device.findObject(By.res("file_manager_menu_provide_full_storage_access_op"))
    } else {
        device.findObject(By.res("file_manager_menu_provide_storage_access_op"))
    }

    accept.click()
}