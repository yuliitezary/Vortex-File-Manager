# Changelog

## 2024

## 2023

### 09.10
---

### New

- SheetControllerPage - page behaves like Android Jetpack's sheet
  dialog ([Source](https://m2.material.io/components/sheets-bottom))

### Fixed

No fixes
