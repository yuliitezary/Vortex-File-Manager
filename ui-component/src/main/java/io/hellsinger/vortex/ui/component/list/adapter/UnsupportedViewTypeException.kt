package io.hellsinger.vortex.ui.component.list.adapter

class UnsupportedViewTypeException(
    viewType: Int,
) : Throwable("Unsupported view type (viewType: $viewType)")