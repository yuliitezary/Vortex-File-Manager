package io.hellsinger.vortex.ui.component.list.adapter

typealias SuperItem = Item<*>

interface Item<T> {

    val id: Long

    val viewType: Int

    val value: T

}