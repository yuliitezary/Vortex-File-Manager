package io.hellsinger.vortex.ui.component.list.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class MultipleAdapter : RecyclerView.Adapter<MultipleAdapter.MultipleViewHolder>() {

    protected val list = mutableListOf<Item<*>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MultipleViewHolder {
        return MultipleViewHolder(createView(parent, viewType))
    }

    override fun getItemId(position: Int): Long = list[position].id

    fun replace(data: List<Item<*>>) =
        CoroutineScope(Dispatchers.Main.immediate + SupervisorJob()).launch {
        if (data == list) return@launch

        if (data.isEmpty()) {
            val count = itemCount
            list.clear()
            notifyItemRangeRemoved(0, count)
            return@launch
        }

        val differ = createDiffer(data) ?: run {
            list.clear()
            list.addAll(data)
            notifyDataSetChanged()
            return@launch
        }
            val result = withContext(Dispatchers.Default) {
            DiffUtil.calculateDiff(differ)
        }
        list.clear()
        list.addAll(data)

        result.dispatchUpdatesTo(this@MultipleAdapter)
    }

    protected abstract fun createDiffer(new: List<Item<*>>): DiffUtil.Callback?

    protected abstract fun createView(parent: ViewGroup, viewType: Int): View

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int = list[position].viewType

    class MultipleViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}