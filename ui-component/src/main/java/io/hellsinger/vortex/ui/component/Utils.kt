package io.hellsinger.vortex.ui.component

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.ceil
import kotlin.math.roundToInt

context (View)
fun Int.toDp(): Int {
    return dp
}

fun Context.dp(size: Float): Float = resources.displayMetrics.density * size
fun Context.dp(size: Int): Int = (resources.displayMetrics.density * size).roundToInt()

context(View)
val Int.dp: Int
    get() = ceil(context.resources.displayMetrics.density * this).roundToInt()

context (View)
val Int.udp: Int
    get() = if (this == -1) -1 else dp

context(View)
val Float.dp: Float
    get() = ceil(context.resources.displayMetrics.density * this)

context(View)
val Int.sp: Int
    get() = ceil(context.resources.displayMetrics.scaledDensity * this).toInt()

context(View)
val Float.sp: Float
    get() = ceil(context.resources.displayMetrics.scaledDensity * this)

context(Drawable)
val Int.dp: Int
    get() = ceil(Resources.getSystem().displayMetrics.density * this).toInt()

context(Drawable)
val Int.ddp: Int // drawable dp
    get() = dp

context(Drawable)
val Float.dp: Float
    get() = ceil(Resources.getSystem().displayMetrics.density * this)

context(Drawable)
val Float.ddp: Float // drawable dp
    get() = dp


fun View.observeInsets(listener: View.OnApplyWindowInsetsListener) {
    setOnApplyWindowInsetsListener(listener)

    if (isAttachedToWindow) {
        requestApplyInsets()
    } else {
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                v.removeOnAttachStateChangeListener(this)
                v.requestApplyInsets()
            }

            override fun onViewDetachedFromWindow(v: View) = Unit
        })
    }
}

inline fun RecyclerView.linear(block: LinearLayoutManager.() -> Unit) {
    val manager = layoutManager ?: return
    if (manager !is LinearLayoutManager) return

    block(manager)
}