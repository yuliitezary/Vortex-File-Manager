package io.hellsinger.vortex.ui.component

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE

inline fun createFadeAnimator(
    toShow: View?,
    vararg toHide: View?,
    crossinline onCancel: () -> Unit,
    crossinline onEnd: () -> Unit,
) = createFadeAnimator(
    toShow = arrayOf(toShow),
    toHide = toHide,
    onCancel = onCancel,
    onEnd = onEnd
)

inline fun createFadeAnimator(
    toShow: Array<out View?>,
    toHide: Array<out View?>,
    crossinline onCancel: () -> Unit,
    crossinline onEnd: () -> Unit,
): ValueAnimator {
    val animator = ValueAnimator.ofFloat(0F, 1F)

    animator.addUpdateListener { anim ->
        toHide.forEach { view ->
            if (view == null) return@forEach
            if (view.visibility == VISIBLE) view.alpha = 1F - anim.animatedFraction
        }

        toShow.forEach { view ->
            if (view == null) return@forEach
            view.alpha = anim.animatedFraction
        }
    }

    animator.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationCancel(animation: Animator) {
            toShow.forEach { view ->
                if (view == null) return@forEach
                view.alpha = 1F
                view.visibility = VISIBLE
            }
            toHide.forEach { view ->
                if (view == null) return@forEach
                view.visibility = GONE
            }
            onCancel()
        }

        override fun onAnimationStart(animation: Animator) {
            toShow.forEach { view ->
                if (view == null) return@forEach
                view.visibility = GONE
            }
        }

        override fun onAnimationEnd(animation: Animator) {
            toShow.forEach { view ->
                if (view == null) return@forEach
                view.alpha = 1F
                view.visibility = VISIBLE
            }

            toHide.forEach { view ->
                if (view == null) return@forEach
                view.visibility = GONE
            }

            onEnd()
        }
    })

    return animator
}