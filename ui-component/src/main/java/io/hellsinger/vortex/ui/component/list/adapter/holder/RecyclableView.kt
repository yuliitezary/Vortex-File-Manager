package io.hellsinger.vortex.ui.component.list.adapter.holder

import android.view.View

interface RecyclableView<T> {
    fun onBind(item: T) {}

    fun onUnbind() {}

    fun onBindPayload(payload: Any?) {

    }

    fun onBindSelection(isSelected: Boolean) {

    }

    fun onBindListener(listener: View.OnClickListener) {

    }

    fun onBindLongListener(listener: View.OnLongClickListener) {

    }

    fun onUnbindListeners() {

    }
}