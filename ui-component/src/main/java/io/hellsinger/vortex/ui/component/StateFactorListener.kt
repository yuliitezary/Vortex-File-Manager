package io.hellsinger.vortex.ui.component

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.TYPE)
annotation class FactorType

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.TYPE)
annotation class StateType

interface StateFactorListener {

    fun onFactorChange(
        factor: Float,
        @FactorType
        type: Int
    )

    fun onStateChange(
        state: Int,
        @StateType
        type: Int
    )

}