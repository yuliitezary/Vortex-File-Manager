#include <jni.h>
#include <malloc.h>
#include "zlib.h"

#define CHUNCK_SIZE 16384

extern "C"
JNIEXPORT jlong JNICALL
Java_io_hellsinger_filesystem_zip_ZipFileOperationProvider_allocateImpl(
        JNIEnv *env,
        jobject thiz,
        jint capacity
) {
    return (uintptr_t) malloc(capacity);
}

extern "C"
JNIEXPORT void JNICALL
Java_io_hellsinger_filesystem_zip_ZipFileOperationProvider_deflateImpl(
        JNIEnv *env,
        jobject thiz,
        jlong buf,
        jint sourceFD,
        jint destFD
) {

}

extern "C"
JNIEXPORT void JNICALL
Java_io_hellsinger_filesystem_zip_ZipFileOperationProvider_inflateImpl(
        JNIEnv *env, jobject thiz,
        jlong buf,
        jbyteArray source,
        jbyteArray dest
) {

}