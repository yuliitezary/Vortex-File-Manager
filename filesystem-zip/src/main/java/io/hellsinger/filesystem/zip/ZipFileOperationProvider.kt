package io.hellsinger.filesystem.zip

internal object ZipFileOperationProvider {
    init {
        System.loadLibrary("filesystem-zip")
    }

    internal fun allocate(capacity: Int) = allocateImpl(capacity)
    private external fun allocateImpl(capacity: Int): Long

    internal fun deflate(
        buf: Long,
        sourceFD: Int,
        destFD: Int
    ) = deflateImpl(buf, sourceFD, destFD)

    private external fun deflateImpl(buf: Long, sourceFD: Int, destFD: Int)

    internal fun inflate(
        buf: Long,
        source: ByteArray,
        dest: ByteArray
    ) = inflateImpl(buf, source, dest)

    private external fun inflateImpl(buf: Long, source: ByteArray, dest: ByteArray)
}