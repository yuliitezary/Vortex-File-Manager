package io.hellsinger.vortex.service.utils

import android.os.Parcel
import io.hellsinger.filesystem.linux.LinuxOperationOptions
import io.hellsinger.filesystem.linux.path.LinuxPath
import io.hellsinger.vortex.filesystem.path.EmptyPath
import io.hellsinger.vortex.filesystem.path.Path
import kotlinx.parcelize.Parceler
import java.io.File

fun parsePath(scheme: Int = LinuxOperationOptions.SCHEME, path: ByteArray): Path {
    if (path.isEmpty()) return EmptyPath()

    return LinuxPath(path)
}

fun File.asPath(scheme: Int = LinuxOperationOptions.SCHEME): Path = absolutePath.asPath(scheme)

fun String.asPath(scheme: Int = LinuxOperationOptions.SCHEME): Path = parsePath(
    scheme = scheme,
    path = encodeToByteArray()
)

object PathParceler : Parceler<Path> {

    override fun create(parcel: Parcel): Path {
        val scheme = parcel.readInt()
        val path = ByteArray(parcel.readInt())
        parcel.readByteArray(path)

        if (scheme == null) return EmptyPath()
        return parsePath(scheme, path)
    }

    override fun Path.write(parcel: Parcel, flags: Int) {
        parcel.writeInt(scheme)
        parcel.writeInt(bytes.size)
        parcel.writeByteArray(bytes)
    }

}