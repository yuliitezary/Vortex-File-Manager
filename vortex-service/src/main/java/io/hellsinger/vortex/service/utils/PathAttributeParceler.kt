package io.hellsinger.vortex.service.utils

import android.os.Parcel
import io.hellsinger.vortex.filesystem.attribute.PathAttribute
import io.hellsinger.vortex.filesystem.attribute.time.PathTime
import kotlinx.parcelize.Parceler

object PathAttributeParceler : Parceler<PathAttribute> {
    override fun create(parcel: Parcel): PathAttribute {
        val id = parcel.readLong()
        val type = parcel.readInt()
        val createTimeSecond = parcel.readLong()
        val createTimeNano = parcel.readLong()
        val lastAccessSecond = parcel.readLong()
        val lastAccessNano = parcel.readLong()
        val lastModifiedSecond = parcel.readLong()
        val lastModifiedNano = parcel.readLong()
        val size = parcel.readLong()
        val perm = parcel.readString() ?: "------"

        return object : PathAttribute {
            override val id: Long = id
            override val type: Int = type
            override val size: Long = size
            override val creationTime: PathTime = PathTime(createTimeSecond, createTimeNano)
            override val lastAccessTime: PathTime = PathTime(lastAccessSecond, lastAccessNano)
            override val lastModifiedTime: PathTime = PathTime(lastModifiedSecond, lastModifiedNano)
            override val permission: String = perm
        }
    }

    override fun PathAttribute.write(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeInt(type)
        parcel.writeLong(creationTime.seconds)
        parcel.writeLong(creationTime.nanos)
        parcel.writeLong(lastAccessTime.seconds)
        parcel.writeLong(lastAccessTime.nanos)
        parcel.writeLong(lastModifiedTime.seconds)
        parcel.writeLong(lastModifiedTime.nanos)
        parcel.writeLong(size)
        parcel.writeString(permission)
    }
}