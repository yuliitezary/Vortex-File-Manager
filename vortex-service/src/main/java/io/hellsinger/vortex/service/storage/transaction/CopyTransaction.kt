package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.attribute.attributes
import io.hellsinger.filesystem.linux.attribute.attributesOrNull
import io.hellsinger.filesystem.linux.attribute.exists
import io.hellsinger.filesystem.linux.directory.LinuxDirectoryEntry
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.directory.treeWalker
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.filesystem.linux.operation.create
import io.hellsinger.filesystem.linux.operation.sendFile
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.R
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.CANCEL
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.PROCEED
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.REPLACE
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.SKIP
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_DIRECTORY as DirType
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_FILE as FileType
import io.hellsinger.vortex.filesystem.attribute.PATH_ATTR_TYPE_LINK as LinkType

class CopyTransaction(
    src: List<Path>,
    dest: Path,
    private val writeSizeBuffer: Int = DEFAULT_BUFFER_SIZE,
    override val onConflictPrepare: ConflictCallback = { path, conflict -> SKIP },
    override val onError: ErrorCallback = {},
    override val onComplete: CompleteCallback = {},
    override val onEvent: EventCallback = {}
) : BaseStorageTransaction() {

    private val _states = mutableListOf<MutableItemOperationState>()

    init {
        _states.addAll(src.map(StorageTransactionController::MutableItemOperationState))
        _states.add(MutableItemOperationState(dest))
    }

    override suspend fun onPrepare(): List<ItemOperationState> {
        for (i in _states.indices) {
            // Dest
            if (_states.lastIndex == i) {
                val state = _states[i]
                val dest = state.path

                val result = checkDestPath(dest)

                if (result != OK_PATH) {
                    when (onConflictPrepare(dest, result)) {
                        CANCEL -> _states.clear()
                    }
                } else state.addFlag(PROCEED)
            }
            val state = _states[i]
            val source = state.path

            val result = checkSourcePath(state.path)

            if (result != OK_PATH) {
                when (onConflictPrepare(source, result)) {
                    REPLACE -> state.addFlag(REPLACE)
                    SKIP -> state.setFlag(SKIP)
                    CANCEL -> _states.clear()
                }
            } else state.addFlag(PROCEED)
        }

        return _states
    }

    private fun checkSourcePath(path: Path): Int {
        if (path.isEmpty) return REASON_EMPTY_PATH
        if (!path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        if (!path.exists) return REASON_NOT_EXISTED_PATH

        return OK_PATH
    }

    private fun checkDestPath(path: Path): Int {
        if (path.isEmpty) return REASON_EMPTY_PATH
        if (!path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
//        if (path.exists) return REASON_ALREADY_EXISTED_PATH

        return OK_PATH
    }


    override suspend fun onPerform(controller: NotificationController) {
        if (_states.isEmpty()) return
        val destState = _states.last()
        if (!destState.hasFlag(PROCEED)) return
        val dest = destState.path
        for (i in 0 until _states.lastIndex) {
            val state = _states[i]
            val source = state.path
            if (state.hasFlag(REPLACE)) continue
            if (state.hasFlag(PROCEED)) {
                when (source.attributes.type) {
                    FileType -> {
                        val resolved =
                            if (dest.attributesOrNull?.type == DirType) dest resolve source.getNameAt()
                            else dest

                        resolveFileToFileCopy(controller, source, resolved)
                    }

                    DirType -> resolveDirToDirCopy(controller, source, dest)

                    LinkType -> {

                    }
                }
                continue
            }
        }

        MainScope().launch {
            delay(1000L)
            controller.cancel()
        }

        onComplete()
    }

    override suspend fun onRevert() {

    }

    private suspend fun resolveFileToFileCopy(
        controller: NotificationController,
        source: Path,
        dest: Path
    ) {
        source.asLinuxFile().sendFile(dest, writeSizeBuffer) { count ->
            sendEvent(source, dest, count)
        }
        controller.post {
            setContentTitle("Copy operation")
            setSmallIcon(R.drawable.ic_copy_24)
            setContentText("${source.getNameAt()} copied to ${dest.getNameAt()}")
        }
    }

    private suspend fun resolveDirToDirCopy(
        controller: NotificationController,
        source: Path,
        dest: Path,
    ) {
        val resolveSourceDir = dest resolve source.getNameAt()

        // If resolved-source doesn't exist - create it
        // TODO: Implement 'merge'
        if (!resolveSourceDir.exists) resolveSourceDir.asLinuxDirectory().create()

        source.asLinuxDirectory().treeWalker().catch { error -> onError(error) }.collect { entry ->
            entry as LinuxDirectoryEntry<Path>

            val type = entry.attributes.type
            val resolved = resolveSourceDir resolve source.relativize(entry.path).subPath(1)

            when (type) {
                DirType -> resolved.asLinuxDirectory().create()
                FileType -> resolveFileToFileCopy(controller, source, resolved)
            }
        }
    }

    private suspend fun sendEvent(
        source: Path,
        dest: Path,
        bytes: Int
    ) = onEvent(CopyEvent(id, source, dest, bytes))

    class CopyEvent(
        override val transactionId: Int,
        val source: Path,
        val dest: Path,
        val bytes: Int
    ) : Event
}