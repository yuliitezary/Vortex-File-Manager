package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.attribute.exists
import io.hellsinger.filesystem.linux.directory.LinuxDirectoryEntry
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.directory.treeWalker
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.filesystem.linux.operation.create
import io.hellsinger.filesystem.linux.operation.delete
import io.hellsinger.filesystem.linux.operation.truncate
import io.hellsinger.theme.vortex.Formatter
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.filesystem.attribute.isDirectory
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.R
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.CANCEL
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.PROCEED
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.REPLACE
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.SKIP
import kotlinx.coroutines.flow.filterIsInstance

class CreateTransaction(
    private val path: Path,
    private val mode: Int = 777,
    private val isDirectory: Boolean,
    override val onConflictPrepare: ConflictCallback = { _, _ -> SKIP },
    override val onError: ErrorCallback,
    override val onComplete: CompleteCallback,
    override val onEvent: EventCallback
) : BaseStorageTransaction() {

    private val pathState = MutableItemOperationState(path)

    override suspend fun onPerform(controller: NotificationController) {
        try {
            if (pathState.hasFlag(REPLACE)) {
                if (isDirectory) {
                    path.asLinuxDirectory()
                        .treeWalker()
                        .filterIsInstance<LinuxDirectoryEntry<Path>>()
                        .collect { entry ->
                            if (entry.attributes.isDirectory) {
                                entry.asLinuxDirectory().delete(
                                    onDeleteTreeError = { error -> onError(error) }
                                )
                            } else {
                                entry.asLinuxFile().delete()
                            }
                        }
                } else {
                    // Set file content as empty
                    path.asLinuxFile().truncate(mode = mode)
                }
            }
            if (pathState.hasFlag(PROCEED)) {
                if (isDirectory) path.asLinuxDirectory().create(mode = mode)
                else path.asLinuxFile().create(mode = mode)
                controller.post {
                    setContentTitle("Create transaction")
                    setSmallIcon(R.drawable.ic_create_24)
                    setContentText(
                        Formatter(
                            key = VortexTheme.texts.vortexServiceCreateTransactionPathCreateTitleKey,
                            path.getNameAt(),
                            path.parent?.getNameAt()
                        )
                    )
                }
                sendEvent(CreateEvent(id, isDirectory, path))
            }

            controller.cancel()

            onComplete()
        } catch (exception: Throwable) {
            onError(exception)
        }
    }

    override suspend fun onPrepare(): List<ItemOperationState> {
        val source = pathState.path
        val result = checkPath(source)
        if (result != OK_PATH) {
            when (onConflictPrepare(source, result)) {
                SKIP -> pathState.setFlag(SKIP)
                REPLACE -> pathState.addFlag(REPLACE)
                CANCEL -> return emptyList()
            }
        } else pathState.addFlag(PROCEED)
        return listOf(pathState)
    }

    private fun checkPath(path: Path): Int {
        if (path.isEmpty) return REASON_EMPTY_PATH
        if (!path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        if (path.exists) return REASON_ALREADY_EXISTED_PATH

        return OK_PATH
    }

    override suspend fun onRevert() {

    }

    class CreateEvent(
        override val transactionId: Int,
        val isDirectory: Boolean,
        val path: Path
    ) : Event
}