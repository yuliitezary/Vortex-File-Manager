package io.hellsinger.vortex.service

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import javax.inject.Inject

interface DispatcherProvider {
    val default: CoroutineDispatcher
    val io: CoroutineDispatcher
    val main: CoroutineDispatcher
    val immediate: CoroutineDispatcher
}

inline val DispatcherProvider.vortex
    get() = io + CoroutineName("Vortex Storage Component Scope") + SupervisorJob()

class DefaultDispatcherProvider @Inject constructor() : DispatcherProvider {
    override val default = Dispatchers.Default
    override val io = Dispatchers.IO
    override val main = Dispatchers.Main
    override val immediate = Dispatchers.Main.immediate
}