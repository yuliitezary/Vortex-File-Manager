package io.hellsinger.vortex.service.storage

import android.content.Intent
import android.os.Environment
import androidx.core.app.NotificationCompat
import io.hellsinger.vortex.filesystem.path.ObservablePath
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.ResourceOwner
import io.hellsinger.vortex.service.ServiceActions.Operation.Cancel
import io.hellsinger.vortex.service.ServiceActions.Operation.Start
import io.hellsinger.vortex.service.di.ServiceComponent
import io.hellsinger.vortex.service.notification.VortexNotificationController.Companion.PATH_OPERATION_CHANNEL
import io.hellsinger.vortex.service.notification.VortexNotificationController.Companion.SERVICE_NOTIFICATION_ID
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.NotificationController
import io.hellsinger.vortex.service.utils.asPath
import io.hellsinger.vortex.service.vortex
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

class VortexStorageController @Inject constructor(
    private val component: ServiceComponent,
) : ResourceOwner {

    private val observation = component.observation

    private val scope = CoroutineScope(component.dispatchers.vortex)

    private val jobs = mutableMapOf<Int, Job>()

    private val ids = AtomicInteger(1)

    val observables: SharedFlow<ObservablePath<Path>>
        get() = observation.events

    fun copyPathToClipboard(path: List<Path>) {
        val content = path.foldRightIndexed("") { i, current, acc ->
            if (i > 0) "$acc $current" else acc + current
        }
        component.android.directToClipboard("Path", content)
    }

    fun start(intent: Intent) {
        if (intent.action == Start) start(intent.getIntExtra("op_id", -1))
    }

    fun start(id: Int) {
        if (id == -1) return
        jobs[id]?.start()
    }

    fun registerTransaction(transaction: StorageTransactionController): VortexStorageController {
        transaction.id = createIdForTransaction()
        return this
    }

    fun runTransaction(transaction: StorageTransactionController) = scope.launch {
        val controller = createNotificationControllerForTransaction(transaction.id) ?: return@launch
        supervisorScope { transaction.perform(controller) }
    }

    internal fun createIdForTransaction(): Int = ids.getAndIncrement()

    internal fun createNotificationControllerForTransaction(id: Int): NotificationController? {
        if (id == StorageTransactionController.NO_ID) return null
        val controller = object : NotificationController {
            private val builder = component.notifier.getBuilder(PATH_OPERATION_CHANNEL)

            override fun post(block: NotificationCompat.Builder.() -> Unit) {
                component.notifier.post(id, builder.apply(block).build())
            }

            override fun cancel() {
                component.notifier.closeNotification(id)
            }
        }
        return controller
    }

    fun addObservablePath(path: Path, event: Int): VortexStorageController {
        observation.add(path, event)
        component.notifier.post(
            SERVICE_NOTIFICATION_ID,
            component.notifier.getServiceNotificationBuilder()
                .setContentTitle("Monitoring")
                .setContentText("${observation.paths.size} paths are monitoring")
                .build()
        )
        return this
    }

    fun removeObservablePath(path: Path): VortexStorageController {
        observation.remove(path)
        component.notifier.post(
            SERVICE_NOTIFICATION_ID,
            component.notifier.getServiceNotificationBuilder()
                .setContentTitle("Monitoring")
                .setContentText("${observation.paths.size} paths are monitoring")
                .build()
        )
        return this
    }

    fun removeAllObservablePath(): VortexStorageController {
        observation.clear()
        return this
    }

    fun cancel(intent: Intent) {
        if (intent.action == Cancel) cancel(intent.getIntExtra("op_id", -1))
    }

    fun cancel(id: Int) {
        if (id == -1) return
        jobs.remove(id)?.cancel()
    }


    override fun clear() {
        jobs.clear()
        scope.cancel()
        observation.close()
    }

    companion object {
        inline val EXTERNAL_STORAGE
            get() = Environment.getExternalStorageDirectory().asPath()
    }
}