package io.hellsinger.vortex.service

import android.app.PendingIntent
import android.content.Context
import android.content.Intent

object ServiceActions {

    object Music {
        const val Start = "start_vortex_music_service"
        const val Stop = "stop_vortex_music_service"
    }

    object Operation {
        const val Cancel = "cancel_vortex_path_operation"
        const val Start = "start_vortex_path_operation"
        const val CANCEL_OBSERVE = "cancel_vortex_path_observation"

        const val Delete = "vortex"
    }

    const val Start = "start_vortex_service"
    const val Stop = "stop_vortex_service"

    fun StopIntent(): Intent {
        return Intent(Stop)
    }

    fun StopPendingIntent(context: Context): PendingIntent {
        return PendingIntent.getBroadcast(context, 0, StopIntent(), PendingIntent.FLAG_IMMUTABLE)
    }

}