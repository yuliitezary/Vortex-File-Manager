package io.hellsinger.vortex.service.notification

import android.Manifest.permission.POST_NOTIFICATIONS
import android.app.Notification
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build
import androidx.core.R
import androidx.core.app.ActivityCompat.checkSelfPermission
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat.Builder
import androidx.core.app.NotificationManagerCompat
import javax.inject.Inject

class VortexNotificationController @Inject constructor(
    private val context: Context,
) {
    companion object {
        const val PATH_OPERATION_CHANNEL = "path_operation_channel"
        const val VORTEX_SERVICE_CHANNEL = "vortex_service_channel"

        const val SERVICE_NOTIFICATION_ID = 1
    }

    private val manager = NotificationManagerCompat.from(context)

    private val builder: Builder = Builder(context, VORTEX_SERVICE_CHANNEL)

    fun getBuilder(channelId: String): Builder = builder.setChannelId(channelId)

    fun post(id: Int, notification: Notification) {
        if (checkSelfPermission(context, POST_NOTIFICATIONS) != PERMISSION_GRANTED) return
        manager.notify(id, notification)
    }

    fun getServiceNotificationBuilder(): Builder = getBuilder(VORTEX_SERVICE_CHANNEL)

    fun createInitialServiceNotification(): Notification {
        return getServiceNotificationBuilder()
            .setContentTitle("Service have been connected")
            .setSmallIcon(R.drawable.notification_bg_low)
            .build()
    }

    fun closeNotification(id: Int) {
        manager.cancel(id)
    }

    internal fun init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManagerCompat.IMPORTANCE_LOW

            val opChannel = NotificationChannelCompat.Builder(PATH_OPERATION_CHANNEL, importance)
                .setName("Operation channel")
                .setDescription("Operation channel describes what operations happens in foreground Vortex Service")
                .build()

            val vortexChannel =
                NotificationChannelCompat.Builder(VORTEX_SERVICE_CHANNEL, importance)
                    .setName("Vortex Service channel")
                    .setDescription("Vortex Service channel is main notification channel of service. It's about changed inside service work or behavior")
                    .build()

            manager.createNotificationChannel(opChannel)
            manager.createNotificationChannel(vortexChannel)
        }
    }

}