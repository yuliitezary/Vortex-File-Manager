package io.hellsinger.vortex.service.storage.transaction

import androidx.core.app.NotificationCompat
import io.hellsinger.vortex.filesystem.path.Path

abstract class StorageTransactionController {

    var id: Int = NO_ID
        internal set

    var state: Int = IDLE
        private set

    suspend fun prepare(): List<ItemOperationState> {
        val list = onPrepare()
        state = PREPARED
        return list
    }

    protected abstract suspend fun onPrepare(): List<ItemOperationState>

    suspend fun perform(notification: NotificationController) {
        onPerform(notification)
    }

    protected abstract suspend fun onPerform(controller: NotificationController)

    suspend fun revert() {
        onRevert()
    }

    protected abstract suspend fun onRevert()

    protected fun MutableItemOperationState.addFlag(flag: Int) {
        mask = mask or flag
    }

    protected fun MutableItemOperationState.setFlag(flag: Int) {
        mask = flag
    }

    protected fun MutableItemOperationState.hasFlag(flag: Int): Boolean = mask and flag == flag


    interface NotificationController {
        fun post(block: NotificationCompat.Builder.() -> Unit)
        fun cancel()
    }

    interface ItemOperationState {
        val path: Path
        val mask: Int

        companion object {
            const val IDLE = 0
            const val PROCEED = 1
            const val SKIP = 2
            const val REPLACE = 4
            const val CANCEL = Int.MIN_VALUE
        }
    }

    open class MutableItemOperationState(
        override val path: Path,
        override var mask: Int = ItemOperationState.IDLE
    ) : ItemOperationState

    companion object {
        const val NO_ID = -1

        const val IDLE = 0
        const val PREPARED = 1

        const val STATE_IDLE = 0
        const val STATE_PREPARING = 1
        const val STATE_PREPARED = 2
        const val STATE_REVERTING = 4
        const val STATE_REVERTED = 8
        const val STATE_PERFORMING = 16
        const val STATE_PERFORMED = 32

        const val OK_PATH = -1
        const val REASON_NOT_ABSOLUTE_PATH = 0
        const val REASON_EMPTY_PATH = 1
        const val REASON_NOT_EXISTED_PATH = 2
        const val REASON_ALREADY_EXISTED_PATH = 4
        const val REASON_NOT_WRITEABLE = 8
    }
}