package io.hellsinger.vortex.service

import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import io.hellsinger.vortex.service.notification.VortexNotificationController.Companion.SERVICE_NOTIFICATION_ID
import io.hellsinger.vortex.service.di.DaggerServiceComponent.builder as ServiceComponentBuilder

class VortexService : Service() {

    private val binder by lazy(LazyThreadSafetyMode.NONE) {
        VortexServiceBinder(
            component = ServiceComponentBuilder().bindContext(this).build()
        )
    }

    override fun onBind(intent: Intent?): IBinder? {
        if (intent == null) return null

        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        if (intent?.action != null) {
            when (intent.action) {
                ServiceActions.Music.Start -> {

                }

                ServiceActions.Music.Stop -> {

                }

                ServiceActions.Start -> {
                    binder.component.notifier.init()
                    startForeground(
                        SERVICE_NOTIFICATION_ID,
                        binder.component.notifier.createInitialServiceNotification()
                    )
                }

                ServiceActions.Stop -> stopSelf()
            }
        }

        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onUnbind(intent: Intent?): Boolean {
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) stopForeground(STOP_FOREGROUND_REMOVE)
        binder.clear()
        super.onDestroy()
    }

}