package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.attribute.exists
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.operation.rename
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.CANCEL
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.PROCEED
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.SKIP

class RenameTransaction(
    src: Path,
    dest: Path,
    override val onConflictPrepare: ConflictCallback = { _, _ -> CANCEL },
    override val onEvent: EventCallback,
    override val onError: ErrorCallback,
    override val onComplete: CompleteCallback
) : BaseStorageTransaction() {

    private val _states =
        mutableListOf(MutableItemOperationState(src), MutableItemOperationState(dest))

    private val source = _states[0]
    private val dest = _states[1]

    override suspend fun onPrepare(): List<ItemOperationState> {
        var result = checkSrcPath()
        if (result != OK_PATH) {
            when (onConflictPrepare(source.path, result)) {
                SKIP -> source.mask = SKIP
                CANCEL -> _states.clear()
            }
        } else source.addFlag(PROCEED)

        result = checkDestPath()
        if (result != OK_PATH) {
            when (onConflictPrepare(dest.path, result)) {
                SKIP -> source.mask = SKIP
                CANCEL -> _states.clear()
            }
        } else dest.addFlag(PROCEED)

        return _states
    }

    private fun checkSrcPath(): Int {
        if (source.path.isEmpty) return REASON_EMPTY_PATH
        if (!source.path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        if (!source.path.exists) return REASON_NOT_EXISTED_PATH

        return OK_PATH
    }

    private fun checkDestPath(): Int {
        if (dest.path.isEmpty) return REASON_EMPTY_PATH
        if (!dest.path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        if (dest.path.exists) return REASON_ALREADY_EXISTED_PATH

        return OK_PATH
    }

    override suspend fun onPerform(controller: NotificationController) {
        if (_states.isEmpty()) return
        if (source.hasFlag(PROCEED) && dest.hasFlag(PROCEED)) {
            try {
                source.path.rename(dest.path)
            } catch (error: ErrnoException) {
                onError(error)
            }
            onComplete()
        }
    }

    override suspend fun onRevert() {
        dest.path.rename(source.path)
    }

    class RenameEvent(
        override val transactionId: Int,
        val source: Path,
        val dest: Path
    ) : Event
}