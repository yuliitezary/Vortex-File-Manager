package io.hellsinger.vortex.service.storage.transaction

abstract class BaseStorageTransaction : StorageTransactionController() {

    protected abstract val onConflictPrepare: ConflictCallback
    protected abstract val onEvent: EventCallback
    protected abstract val onError: ErrorCallback
    protected abstract val onComplete: CompleteCallback

    protected open suspend fun sendEvent(event: Event) = onEvent(event)
    protected open suspend fun sendError(error: Throwable) = onError(error)

}