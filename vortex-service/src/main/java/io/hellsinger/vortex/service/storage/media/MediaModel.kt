package io.hellsinger.vortex.service.storage.media

data class MediaModel(
    val id: Int,
    val name: String,
    val author: String,
    val duration: Long,
)