package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.attribute.exists
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.operation.changeMode
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.CANCEL
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.PROCEED

class PermissionTransaction(
    src: Path,
    private val mode: Int,
    override val onConflictPrepare: ConflictCallback,
    override val onComplete: CompleteCallback,
    override val onError: ErrorCallback,
    override val onEvent: EventCallback
) : BaseStorageTransaction() {

    private var pathState = MutableItemOperationState(src)

    override suspend fun onPrepare(): List<ItemOperationState> {
        val result = checkPath(pathState.path)
        if (result != OK_PATH) {
            when (onConflictPrepare(pathState.path, result)) {
                CANCEL -> return emptyList()
            }
        } else pathState.addFlag(PROCEED)
        return listOf(pathState)
    }

    private fun checkPath(path: Path): Int {
        if (path.isEmpty) return REASON_EMPTY_PATH
        if (!path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        if (!path.exists) return REASON_NOT_EXISTED_PATH

        return OK_PATH
    }

    override suspend fun onRevert() {

    }

    override suspend fun onPerform(controller: NotificationController) {
        if (pathState.hasFlag(PROCEED)) {
            try {
                pathState.path.changeMode(mode)
                onComplete()
            } catch (errno: ErrnoException) {
                onError(errno)
            }
        }
    }

}