package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.attribute.attributes
import io.hellsinger.filesystem.linux.attribute.exists
import io.hellsinger.filesystem.linux.directory.LinuxDirectory
import io.hellsinger.filesystem.linux.directory.asLinuxDirectory
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.file.LinuxFile
import io.hellsinger.filesystem.linux.file.asLinuxFile
import io.hellsinger.filesystem.linux.operation.delete
import io.hellsinger.filesystem.linux.operation.writeable
import io.hellsinger.theme.vortex.Formatter
import io.hellsinger.theme.vortex.VortexTheme
import io.hellsinger.vortex.filesystem.attribute.isDirectory
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.R
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.CANCEL
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.PROCEED
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.SKIP

class DeleteTransaction(
    paths: List<Path>,
    override val onConflictPrepare: ConflictCallback = { path, conflict -> SKIP },
    override val onError: ErrorCallback,
    override val onEvent: EventCallback,
    override val onComplete: CompleteCallback,
) : BaseStorageTransaction() {

    private val _states = paths.map(::MutableItemOperationState).toMutableList()
    private var progress = 0

    override suspend fun onPrepare(): List<ItemOperationState> {
        for (index in _states.indices) {
            val state = _states[index]
            val result = checkPath(_states[index].path)

            if (result != OK_PATH) {
                when (onConflictPrepare(state.path, result)) {
                    SKIP -> state.setFlag(SKIP)
                    CANCEL -> _states.clear()
                }
            } else state.addFlag(PROCEED)

        }
        return _states
    }

    private fun checkPath(path: Path): Int {
        if (path.isEmpty) return REASON_EMPTY_PATH
        if (!path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        if (!path.exists) return REASON_NOT_EXISTED_PATH
        if (!path.writeable) return REASON_NOT_WRITEABLE

        return OK_PATH
    }

    override suspend fun onRevert() {

    }

    override suspend fun onPerform(controller: NotificationController) {
        if (_states.isEmpty()) return
        progress = 0
        var result = false
        for (i in _states.indices) {
            val state = _states[i]
            if (state.mask and PROCEED == PROCEED) {
                val path = state.path
                result = if (path.attributes.isDirectory) {
                    deleteDirectory(controller, path.asLinuxDirectory())
                } else {
                    deleteFile(controller, path.asLinuxFile())
                }
            }
        }

        controller.cancel()

        if (result) onComplete()
    }

    private suspend fun deleteDirectory(
        controller: NotificationController,
        directory: LinuxDirectory<Path>
    ): Boolean = try {
        directory.delete(onDeleteTreeError = ::sendError)

        controller.post {
            setContentTitle("Delete operation")
            setSmallIcon(R.drawable.ic_delete_24)
            setContentText(
                Formatter(
                    key = VortexTheme.texts.vortexServiceDeleteTransactionPathDeleteTitleKey,
                    directory.name
                )
            )
            setProgress(_states.size, ++progress, false)
        }
        sendEvent(DeleteEvent(id, directory.path))
        true
    } catch (errno: ErrnoException) {
        sendError(errno)
        false
    }

    private suspend fun deleteFile(
        controller: NotificationController,
        file: LinuxFile<Path>
    ): Boolean = try {
        file.delete()

        controller.post {
            setContentTitle("Delete transaction")
            setSmallIcon(R.drawable.ic_delete_24)
            setContentText(
                Formatter(
                    VortexTheme.texts.vortexServiceDeleteTransactionPathDeleteTitleKey,
                    file.name
                )
            )
            setProgress(_states.size, ++progress, false)
        }
        sendEvent(DeleteEvent(id, file.path))
        true
    } catch (errno: ErrnoException) {
        sendError(errno)
        false
    }

    class DeleteEvent(
        override val transactionId: Int,
        val path: Path
    ) : Event
}