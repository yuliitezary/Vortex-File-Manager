package io.hellsinger.vortex.service.di

import dagger.Binds
import dagger.Module
import io.hellsinger.vortex.service.DefaultDispatcherProvider
import io.hellsinger.vortex.service.DispatcherProvider

@Module
abstract class BindModule {

    @Binds
    abstract fun bindDispatcherProvider(default: DefaultDispatcherProvider): DispatcherProvider

}