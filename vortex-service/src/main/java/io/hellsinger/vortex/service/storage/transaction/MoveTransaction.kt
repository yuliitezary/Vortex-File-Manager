package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.attribute.exists
import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.operation.rename
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.CANCEL
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.PROCEED
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.REPLACE
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.SKIP

class MoveTransaction(
    src: List<Path>,
    dest: Path,
    override val onConflictPrepare: ConflictCallback = { _, _ -> SKIP },
    override val onError: ErrorCallback = {},
    override val onEvent: EventCallback = {},
    override val onComplete: CompleteCallback = { },
) : BaseStorageTransaction() {

    private val _states = mutableListOf<MutableItemOperationState>()

    init {
        _states.addAll(src.map(::MutableItemOperationState))
        _states.add(MutableItemOperationState(dest))
    }

    override suspend fun onPrepare(): List<ItemOperationState> {
        for (i in _states.indices) {
            // Dest
            if (_states.lastIndex == i) {
                val state = _states[i]
                val dest = state.path

                val result = checkDestPath(dest)

                if (result != OK_PATH) {
                    when (onConflictPrepare(dest, result)) {
                        CANCEL -> _states.clear()
                    }
                } else state.addFlag(PROCEED)
            }
            val state = _states[i]
            val source = state.path

            val result = checkSourcePath(state.path)

            if (result != OK_PATH) {
                when (onConflictPrepare(source, result)) {
                    REPLACE -> state.addFlag(REPLACE)
                    SKIP -> state.setFlag(SKIP)
                    CANCEL -> _states.clear()
                }
            } else state.addFlag(PROCEED)
        }

        return _states
    }

    private fun checkSourcePath(path: Path): Int {
        if (path.isEmpty) return REASON_EMPTY_PATH
        if (!path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        if (!path.exists) return REASON_NOT_EXISTED_PATH

        return OK_PATH
    }

    private fun checkDestPath(path: Path): Int {
        if (path.isEmpty) return REASON_EMPTY_PATH
        if (!path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        if (path.exists) return REASON_ALREADY_EXISTED_PATH

        return OK_PATH
    }

    override suspend fun onPerform(controller: NotificationController) {
        if (_states.isEmpty()) return
        val destState = _states.last()
        if (!destState.hasFlag(PROCEED)) return
        val dest = destState.path
        for (i in 0 until _states.lastIndex) {
            val state = _states[i]
            val source = state.path
            if (state.hasFlag(PROCEED)) {
                val sourceName = source.getNameAt()
                val destEntry =
                    if (sourceName == dest.getNameAt()) source else dest.resolve(sourceName)

                try {
                    // TODO: Add implementation if source and dest are in different file systems
                    source.rename(destEntry)
                } catch (exception: ErrnoException) {
                    onError(exception)
                }
                continue
            }
            if (state.hasFlag(REPLACE))
            // TODO: Implement replacing in move operation
                continue
        }

        onComplete()
    }

    override suspend fun onRevert() {

    }
}