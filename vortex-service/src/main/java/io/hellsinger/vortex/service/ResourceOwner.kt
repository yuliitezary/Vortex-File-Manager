package io.hellsinger.vortex.service

interface ResourceOwner {

    fun clear()

}