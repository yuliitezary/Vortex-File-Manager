package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.vortex.filesystem.path.Path

internal typealias ConflictCallback = suspend (Path, Int) -> Int
internal typealias ErrorCallback = suspend (Throwable) -> Unit
internal typealias EventCallback = suspend (Event) -> Unit
internal typealias CompleteCallback = suspend () -> Unit

interface Event {
    val transactionId: Int
}