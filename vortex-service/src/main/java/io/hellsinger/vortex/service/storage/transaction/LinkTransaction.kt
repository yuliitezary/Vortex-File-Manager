package io.hellsinger.vortex.service.storage.transaction

import io.hellsinger.filesystem.linux.error.ErrnoException
import io.hellsinger.filesystem.linux.operation.createSymbolicLink
import io.hellsinger.vortex.filesystem.path.Path
import io.hellsinger.vortex.service.storage.transaction.StorageTransactionController.ItemOperationState.Companion.PROCEED

class LinkTransaction(
    private val path: Path,
    private val source: Path,
    private val isHard: Boolean,
    override val onConflictPrepare: ConflictCallback,
    override val onError: ErrorCallback,
    override val onComplete: CompleteCallback,
    override val onEvent: EventCallback
) : BaseStorageTransaction() {

    private val states = mutableListOf(
        MutableItemOperationState(path),
        MutableItemOperationState(source)
    )

    override suspend fun onPrepare(): List<ItemOperationState> {
        // Paths
        val pathState = states[0]
        var result = checkPath(pathState.path)

        if (result != OK_PATH) {
            when (onConflictPrepare(pathState.path, result)) {
                ItemOperationState.CANCEL -> states.clear()
            }
        } else pathState.addFlag(PROCEED)


        // Source
        val sourceState = states[1]
        result = checkPath(sourceState.path)

        if (result != OK_PATH) {
            when (onConflictPrepare(sourceState.path, result)) {
                ItemOperationState.CANCEL -> states.clear()
            }
        } else sourceState.addFlag(PROCEED)

        return states
    }

    private fun checkPath(path: Path): Int {
        if (path.isEmpty) return REASON_EMPTY_PATH
        if (!path.isAbsolute) return REASON_NOT_ABSOLUTE_PATH
        return OK_PATH
    }

    override suspend fun onPerform(controller: NotificationController) {
        if (states.isEmpty()) return

        try {
            if (isHard) {
                // TODO: Add support for hard links
//            source.link()
            } else {
                source.createSymbolicLink(path)
            }
        } catch (exception: ErrnoException) {
            return onError(exception)
        }

        onComplete()
    }

    override suspend fun onRevert() {

    }
}