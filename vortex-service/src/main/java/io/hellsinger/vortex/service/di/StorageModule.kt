package io.hellsinger.vortex.service.di

import dagger.Module
import dagger.Provides
import io.hellsinger.filesystem.linux.operation.LinuxPathObserver
import javax.inject.Singleton

@Module
object StorageModule {

    @Provides
    @Singleton
    fun createLinuxPathObserver(): LinuxPathObserver = LinuxPathObserver()

}