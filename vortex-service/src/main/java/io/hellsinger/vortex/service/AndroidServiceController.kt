package io.hellsinger.vortex.service

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import javax.inject.Inject

class AndroidServiceController @Inject internal constructor(
    private val context: Context,
) {

    private val clipboard by lazy(LazyThreadSafetyMode.NONE) {
        context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    fun directToClipboard(
        label: String,
        content: String
    ) = clipboard.setPrimaryClip(ClipData.newPlainText(label, content))


}