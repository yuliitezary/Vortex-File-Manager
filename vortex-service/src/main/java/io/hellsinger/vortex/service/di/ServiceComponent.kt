package io.hellsinger.vortex.service.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import io.hellsinger.filesystem.linux.operation.LinuxPathObserver
import io.hellsinger.vortex.service.AndroidServiceController
import io.hellsinger.vortex.service.DispatcherProvider
import io.hellsinger.vortex.service.notification.VortexNotificationController
import javax.inject.Singleton

@Component(modules = [NotificationModule::class, StorageModule::class, BindModule::class])
@Singleton
interface ServiceComponent {

    val android: AndroidServiceController

    val dispatchers: DispatcherProvider

    val notifier: VortexNotificationController

    val observation: LinuxPathObserver

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bindContext(context: Context): Builder
        fun build(): ServiceComponent
    }

}