package io.hellsinger.vortex.service.di

import android.content.Context
import dagger.Module
import dagger.Provides
import io.hellsinger.vortex.service.notification.VortexNotificationController
import javax.inject.Singleton

@Module
object NotificationModule {

    @Provides
    @Singleton
    fun createVortexNotificationManager(
        context: Context,
    ): VortexNotificationController = VortexNotificationController(context)

}