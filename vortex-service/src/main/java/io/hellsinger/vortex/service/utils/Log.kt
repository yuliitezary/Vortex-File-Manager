package io.hellsinger.vortex.service.utils

import android.util.Log
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
inline fun throwWithLogging(
    msg: String,
    log: String = msg,
    tag: String = "Loggable",
    builder: (String) -> Throwable = ::Throwable,
): Throwable {
    contract {
        callsInPlace(builder, InvocationKind.AT_LEAST_ONCE)
    }
    Log.e(tag, log)
    throw builder(msg)
}