package io.hellsinger.vortex.service

import android.os.Binder
import io.hellsinger.vortex.service.di.ServiceComponent
import io.hellsinger.vortex.service.storage.VortexStorageController
import javax.inject.Inject


class VortexServiceBinder @Inject constructor(
    internal val component: ServiceComponent,
) : Binder(), ResourceOwner {

    val storage: VortexStorageController = VortexStorageController(component = component)

    override fun clear() {
        storage.clear()
    }

}