plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.google.ksp)
}

android {
    compileSdk = AndroidConfigure.targetSdk
    namespace = "io.hellsinger.vortex.service"

    defaultConfig {
        minSdk = AndroidConfigure.minSdk

        consumerProguardFiles("consumer-rules.pro")
    }

    sourceSets {
//        getByName<com.android.build.api.dsl.AndroidSourceSet>(name = "main") {
//            aidl.srcDirs("src/main/aidl/")
//        }
    }

    buildFeatures {
//        aidl = true
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = BuildConfig.JDK.VerEnum
        targetCompatibility = BuildConfig.JDK.VerEnum
    }
}

dependencies {
    implementation(libs.androidx.core)
    implementation(libs.kotlinx.coroutines.android)

    implementation(projects.filesystem)
    implementation(projects.filesystemLinux)
    implementation(projects.theme)
    implementation(projects.themeVortex)
    implementation(projects.pluginManager)

    implementation(libs.google.dagger)
    ksp(libs.google.dagger.compiler)
}