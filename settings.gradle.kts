pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "Vortex-File-Manager"
include(
    ":app",
    ":filesystem",
    ":filesystem-linux",
    ":filesystem-zip",
    ":plugin-manager",
    ":ui-component",
    ":vortex-service",
    ":theme",
    ":navigation",
    ":navigation-processor",
    ":vortex-baseline-profile",
    ":plugin-manager-vortex",
)

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include(":theme-vortex")
include(":vortex-file-editor")
