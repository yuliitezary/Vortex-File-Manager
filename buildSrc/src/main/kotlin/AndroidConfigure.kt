object AndroidConfigure {
    const val applicationId = "io.github.excu101.vortex"
    const val minSdk = 21 // Android 5
    const val targetSdk = 34 // Android 14
    const val versionCode = 5
    const val versionName = "1.0.0-beta04"
    const val multiDexEnabled = true
}