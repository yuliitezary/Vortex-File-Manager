package io.hellsinger.navigation.screen

import android.content.Context

interface NavScreenRestorer {

    fun onScreenRestore(context: Context, id: Int): ScreenController?

}