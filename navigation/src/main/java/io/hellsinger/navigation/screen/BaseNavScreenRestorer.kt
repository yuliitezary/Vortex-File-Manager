package io.hellsinger.navigation.screen

import android.content.Context

class BaseNavScreenRestorer : NavScreenRestorer {

    override fun onScreenRestore(
        context: Context,
        id: Int
    ): ScreenController? = when (id) {
        else -> null
    }
}