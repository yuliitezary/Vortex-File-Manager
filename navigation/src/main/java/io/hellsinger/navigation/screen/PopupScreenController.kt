package io.hellsinger.navigation.screen

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.view.View
import android.view.animation.DecelerateInterpolator

abstract class PopupScreenController(
    private val context: Context
) : ScreenController() {

    private var popup: HostNavPopupView? = null

    fun create(): PopupScreenController {
        popup = HostNavPopupView(context)
        return this
    }

    fun show(anchor: View) {
        val popup = popup ?: return
        val popupView = onCreateView(context)

        onPrepare()
        popup.addView(popupView)

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = BaseNavScreenController.TRANSITION_DURATION
        animator.interpolator = DecelerateInterpolator(1.78F)
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                onFocus()
            }
        })
        animator.addUpdateListener { anim ->
            val factor = anim.animatedFraction
            popup.scrim.alpha = factor
            onTransitionFactor(factor)
        }
        popup.create()

        val listener = object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                l: Int,
                t: Int,
                r: Int,
                b: Int,
                ol: Int,
                ot: Int,
                or: Int,
                ob: Int
            ) {
                v.removeOnLayoutChangeListener(this)
                animator.start()
            }
        }

        popup.addOnLayoutChangeListener(listener)
        popup.show(anchor)
    }

    fun hide() {
        val popup = popup ?: return

        onHide()
        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = BaseNavScreenController.TRANSITION_DURATION
        animator.interpolator = DecelerateInterpolator(1.78F)
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                popup.hide()
                popup.removeAllViews()
                onDestroy()
            }
        })
        animator.addUpdateListener { anim ->
            val factor = 1F - anim.animatedFraction
            popup.scrim.alpha = factor
            onTransitionFactor(factor)
        }

        animator.start()
    }

}