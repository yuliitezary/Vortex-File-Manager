package io.hellsinger.navigation.screen

import android.widget.Toast

interface ToastController {

    fun toast(text: String, duration: Int = Toast.LENGTH_SHORT)

}