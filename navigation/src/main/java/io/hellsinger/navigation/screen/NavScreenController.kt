package io.hellsinger.navigation.screen

import android.os.Bundle
import io.hellsinger.navigation.NavController

interface NavScreenController : NavController {

    val stack: NavScreenStack

    fun navigateForward(screen: ScreenController)

    fun navigateBackward()

    fun save(buffer: Bundle)

    fun restore(buffer: Bundle)

}