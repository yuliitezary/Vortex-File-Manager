package io.hellsinger.navigation.screen

import android.content.Context

class BaseNavScreenGraph(
    private val providers: Map<String, (Context) -> ScreenController> = mapOf()
) {
    operator fun get(route: String) = providers[route]
}