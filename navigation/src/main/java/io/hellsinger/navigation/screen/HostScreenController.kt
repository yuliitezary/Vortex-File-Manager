package io.hellsinger.navigation.screen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import io.hellsinger.navigation.screen.HostScreenControllerAdapter.ScreenControllerProvider

abstract class HostScreenController : ScreenController(), ScreenControllerProvider,
    OnPageChangeListener {

    protected val adapter = HostScreenControllerAdapter(this)
    protected var pager: ViewPager? = null

    private val current: ScreenController?
        get() {
            val selected = pager?.currentItem ?: return null
            return getController(selected)
        }

    override fun onCreateViewImpl(context: Context): View {
        pager = ViewPager(context).apply {
            layoutParams = FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            adapter = this@HostScreenController.adapter
            addOnPageChangeListener(this@HostScreenController)
        }

        return pager!!
    }

    protected open fun wrapPrefix(prefix: String): String {
        return prefix + "host_screen_controller_"
    }

    override fun onSaveImpl(buffer: Bundle, prefix: String): Boolean {
        var saved = 0
        for (i in 0 until getControllerCount()) {
            if (getController(i).onSave(buffer, prefix)) saved++
        }
        return saved > 0
    }

    override fun onPageSelected(position: Int) {
        focus(position)
    }

    override fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean {
        var restored = 0
        for (i in 0 until getControllerCount()) {
            if (getController(i).onRestore(buffer, wrapPrefix(prefix))) restored++
        }
        return restored > 0
    }

    private fun focus(index: Int) {
        getController(index).onFocus()
    }

    private fun hide(index: Int) {
        getController(index).onHide()
    }

    override fun onCallIdImpl(identifier: Int): Boolean {
        return current?.onCallId(identifier) ?: super.onCallIdImpl(identifier)
    }

    override fun onActivityResultImpl(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ): Boolean = current?.onActivityResult(
        requestCode,
        resultCode,
        data
    ) ?: super.onActivityResultImpl(requestCode, resultCode, data)

    override fun getNavigationController(): BaseNavScreenController? = navigator

    override fun onBackActivatedImpl(): Boolean {
        return current?.onBackActivated() ?: false
    }
}