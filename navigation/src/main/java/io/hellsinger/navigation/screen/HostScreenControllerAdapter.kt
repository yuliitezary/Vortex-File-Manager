package io.hellsinger.navigation.screen

import android.view.View
import android.view.ViewGroup
import androidx.collection.SparseArrayCompat
import androidx.viewpager.widget.PagerAdapter

class HostScreenControllerAdapter(
    private val provider: ScreenControllerProvider,
) : PagerAdapter() {

    interface ScreenControllerProvider {
        fun getControllerCount(): Int
        fun getNavigationController(): BaseNavScreenController?
        fun getController(position: Int): ScreenController
        fun onPrepareController(position: Int, controller: ScreenController)
        fun onHideController(position: Int, controller: ScreenController)
    }

    var isRtl = false

    private val cache = SparseArrayCompat<ScreenController>()

    fun wrapIndex(position: Int): Int {
        return if (isRtl) provider.getControllerCount() - 1 - position else position
    }

    fun getController(index: Int): ScreenController? {
        if (index < 0 || index >= cache.size()) return null
        return cache.valueAt(index)
    }

    override fun getCount(): Int {
        return provider.getControllerCount()
    }

    fun contains(controller: ScreenController): Boolean {
        return cache.containsValue(controller)
    }

    fun notifyControllerInserted(index: Int) {
        for (i in cache.size() - 1 downTo 0) {
            val key = cache.keyAt(i)
            if (key < index) break
            with(cache.valueAt(i)) {
                cache.removeAt(i)
                cache.put(key + 1, this)
            }
        }
    }

    fun notifyControllerRemove(index: Int) {
        var i = cache.indexOfKey(index)
        if (i < 0) return
        val c = cache.valueAt(i)
        cache.removeAt(i)
        c.onDestroy()
        val count = cache.size()

        while (i < count) {
            val key = cache.keyAt(i)
            val item = cache.valueAt(i)
            cache.removeAt(i)
            cache.put(key - 1, item)
            i++
        }
    }

    fun destroy() {
        for (i in 0 until cache.size()) {
            val controller = cache.valueAt(i)

            controller.onDestroy()
        }
        cache.clear()
    }

    override fun getItemPosition(item: Any): Int {
        for (i in 0 until cache.size())
            if (cache.valueAt(i) == item) return cache.keyAt(i)
        return POSITION_NONE
    }

    private fun checkController(position: Int): ScreenController {
        var controller = cache[position]

        if (controller == null) {
            controller = provider.getController(position)
            cache.put(position, controller)
        }

        return controller
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val controller = checkController(position)
        val navigator = provider.getNavigationController()
        val view = controller.onCreateView(container.context)
        provider.onPrepareController(position, controller)
        if (navigator != null) controller.attach(navigator)
        controller.onPrepare()
        container.addView(view)
        return controller
    }

    override fun destroyItem(container: ViewGroup, position: Int, item: Any) {
        item as ScreenController
        val view = item.onCreateView(container.context)

        container.removeView(view)
        provider.onHideController(position, item)
        item.onHide()
        item.onDestroy()
        item.detach()
    }

    override fun isViewFromObject(view: View, item: Any): Boolean {
        return item is ScreenController && item.onCreateView(view.context) == view
    }
}