package io.hellsinger.navigation.screen

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import android.widget.Toast

abstract class ScreenController : View.OnApplyWindowInsetsListener {

    private var _navigator: BaseNavScreenController? = null

    protected open val navigator: BaseNavScreenController?
        get() = _navigator

    private var contentView: View? = null

    protected var state = INITIALIZED
        private set

    // Used to save and restore instance of controller
    open val id: Int = -1

    open val usePopupMode: Boolean = false

    internal fun attach(controller: BaseNavScreenController) {
        this._navigator = controller
        state = state or ATTACHED
        onAttachImpl()
    }

    // Attached to navigation controller (there's instance of it)
    protected open fun onAttachImpl() {

    }

    internal fun detach() {
        onDetachImpl()
        state = state and ATTACHED.inv()
        _navigator = null
    }

    // Detached from navigation controller (null-instance)
    protected open fun onDetachImpl() {

    }

    protected fun navigateForward(screen: ScreenController) {
        _navigator?.navigateForward(screen)
    }

    protected fun navigateBackward() {
        _navigator?.navigateBackward()
    }

    protected fun toast(text: String, duration: Int = Toast.LENGTH_SHORT) {
        _navigator?.toast(text, duration)
    }

    // System-back button activated.
    internal fun onBackActivated(): Boolean {
        return onBackActivatedImpl()
    }

    internal fun onTransitionFactor(factor: Float) {
        onTransitionFactorImpl(factor)
    }

    // Swipe-back, animation, etc.
    protected open fun onTransitionFactorImpl(factor: Float) {}

    protected abstract fun onBackActivatedImpl(): Boolean

    // Check menu items id-calling
    internal fun onCallId(identifier: Int): Boolean {
        return onCallIdImpl(identifier)
    }

    protected open fun onCallIdImpl(identifier: Int): Boolean = false

    // Create view
    internal fun onCreateView(context: Context): View {
        if (contentView == null) {
            contentView = onCreateViewImpl(context)
        }

        return contentView!!
    }

    override fun onApplyWindowInsets(v: View, insets: WindowInsets) = WindowInsets.CONSUMED

    internal fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return onActivityResultImpl(requestCode, resultCode, data)
    }


    protected open fun onActivityResultImpl(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ): Boolean {
        return false
    }

    internal fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean = onRequestPermissionsResultImpl(requestCode, permissions, grantResults)

    protected open fun onRequestPermissionsResultImpl(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean {
        return false
    }

    protected abstract fun onCreateViewImpl(
        context: Context
    ): View

    internal fun onPrepare() {
        onPrepareImpl()
    }

    // Bind listeners, data
    protected abstract fun onPrepareImpl()

    internal fun onFocus() {
        state = state or FOCUSED
        onFocusImpl()
    }

    // Start Animation?
    protected abstract fun onFocusImpl()

    internal fun onHide() {
        state = state and FOCUSED.inv()
        onHideImpl()
    }

    // Unbind listeners
    protected abstract fun onHideImpl()

    internal fun onDestroy() {
        state = state or DESTROYED
        onDestroyImpl()
    }

    // Unbind and destroy listeners, clear data, remove and destroy all views
    protected abstract fun onDestroyImpl()

    internal fun onSave(buffer: Bundle, prefix: String) = onSaveImpl(buffer, prefix)

    protected abstract fun onSaveImpl(buffer: Bundle, prefix: String): Boolean

    internal fun onRestore(buffer: Bundle, prefix: String): Boolean = onRestoreImpl(buffer, prefix)

    protected abstract fun onRestoreImpl(buffer: Bundle, prefix: String): Boolean

    companion object {
        const val NO_ID = -1

        const val INITIALIZED = 0
        const val ATTACHED = 1
        const val FOCUSED = 2
        const val DESTROYED = 4
    }
}