package io.hellsinger.navigation.screen

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.Toast
import androidx.annotation.FloatRange
import io.hellsinger.navigation.screen.ScreenController.Companion.NO_ID

open class BaseNavScreenController(
    private val context: Context,
    private val restorer: NavScreenRestorer = BaseNavScreenRestorer(),
) : NavScreenController, ToastController, SnackController {

    protected var root: FrameLayout? = null
    protected var host: HostNavScreenView? = null
    protected var popup: HostNavPopupView? = null

    private val _stack = BaseNavScreenStack()

    override val stack: NavScreenStack
        get() = _stack

    val currentIndex: Int
        get() = _stack.current

    val currentScreen: ScreenController
        get() = _stack[currentIndex]

    val previousIndex: Int
        get() = _stack.current - 1

    val previousScreen: ScreenController
        get() = _stack[previousIndex]

    open fun createView(): View {
        if (root == null) {
            root = FrameLayout(context).apply {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
            }
            host = HostNavScreenView(context).apply {
                layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
            }
            popup = HostNavPopupView(context)
            root!!.addView(host, 0)
            onCreateView(root!!)
        }

        return root!!
    }

    protected open fun onCreateView(root: FrameLayout) {}

    override fun save(buffer: Bundle) {
        var saved = 0

        for (i in 0 until _stack.size) {
            val prefix = createScreenPrefix(i)
            val screen = _stack[i]

            if (screen.onSave(buffer, prefix)) {
                buffer.putInt(prefix, screen.id)
                saved++
            }
        }
        if (saved > 0) buffer.putInt(PREFIX_SCREEN_ITEM_COUNT, saved)
    }

    override fun restore(buffer: Bundle) {
        val count = buffer.getInt(PREFIX_SCREEN_ITEM_COUNT)
        if (count <= 0) return
        var restored = 0

        for (i in 0 until count) {
            val prefix = createScreenPrefix(i)
            val id = buffer.getInt(prefix, NO_ID)
            if (id == NO_ID) continue
            val screen = restorer.onScreenRestore(context, id) ?: continue

            if (screen.onRestore(buffer, prefix)) {
                if (restored > 0) _stack.insertAt(0, screen)
                else init(screen)
            }
            restored++
        }
    }

    fun init(screen: ScreenController) {
        val view = screen.onCreateView(context)

        _stack.clear()

        _stack.push(screen)
        screen.attach(this)
        prepareScreen(screen)
        host?.addView(view)

        focusScreen(screen)
    }

    override fun navigateForward(screen: ScreenController) {
        if (screen.usePopupMode) return navigateForwardPopup(screen)
        var current = _stack.currentScreen
        while (current.usePopupMode) {
            navigateBackwardPopup(current, _stack.previous)
            _stack.poll()
            current = _stack.currentScreen
        }
        if (current.id == screen.id) return
        val currentView = current.onCreateView(context)
        val nextView = screen.onCreateView(context)

        _stack.push(screen)
        screen.attach(this)
        prepareScreen(screen)
        host?.addView(nextView)

        val next = _stack.currentScreen

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = TRANSITION_DURATION
        animator.interpolator = DecelerateInterpolator(1.78F)

        onPrepareForwardAnimationEffect(currentView, nextView)

        animator.addUpdateListener { anim ->
            val factor = anim.animatedFraction

            onApplyForwardAnimationEffect(currentView, nextView, factor)
        }
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                host?.removeView(currentView)
                hideScreen(current)
                current.detach()
                focusScreen(next)
            }
        })

        animator.start()
    }

    fun bindStackChangeListener(listener: (NavScreenStack.() -> Unit)?) {
        _stack.bindListener(listener)
    }

    private fun navigateForwardPopup(screen: ScreenController) {
        val popup = popup ?: return
        if (screen == _stack.currentScreen) return

        val current = _stack.currentScreen
        val popupView = screen.onCreateView(context)
        popup.scrim.setOnClickListener { navigateBackward() }

        _stack.push(screen)

        screen.attach(this)
        prepareScreen(screen)
        popup.addView(popupView)

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = TRANSITION_DURATION
        animator.interpolator = DecelerateInterpolator(1.78F)
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                hideScreen(current)
                current.detach()
                focusScreen(screen)
            }
        })
        animator.addUpdateListener { anim ->
            val factor = anim.animatedFraction
            popup.scrim.alpha = factor
            screen.onTransitionFactor(factor)
        }
        popup.create()

        val listener = object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                l: Int,
                t: Int,
                r: Int,
                b: Int,
                ol: Int,
                ot: Int,
                or: Int,
                ob: Int
            ) {
                v.removeOnLayoutChangeListener(this)
                animator.start()
            }
        }

        popup.addOnLayoutChangeListener(listener)
        popup.show(root!!)
    }

    open fun onPrepareForwardAnimationEffect(
        currentView: View,
        nextView: View
    ) {
        nextView.alpha = 0F
    }

    open fun onApplyForwardAnimationEffect(
        currentView: View,
        nextView: View,
        @FloatRange(from = 0.0, to = 1.0)
        factor: Float
    ) {
        currentView.alpha = 1F - factor
        nextView.alpha = factor
    }

    override fun navigateBackward() {
        val current = _stack.poll()
        val previous = _stack.currentScreen
        if (current.usePopupMode) return navigateBackwardPopup(current, previous)

        val currentView = current.onCreateView(context)
        val previousView = previous.onCreateView(context)

        previous.attach(this)
        prepareScreen(previous)
        host?.addView(previousView)

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = TRANSITION_DURATION
        animator.interpolator = DecelerateInterpolator(1.78F)

        onPrepareBackwardAnimationEffect(currentView, previousView)

        animator.addUpdateListener { anim ->
            val factor = anim.animatedFraction

            onApplyForwardAnimationEffect(currentView, previousView, factor)
        }

        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                host?.removeView(currentView)
                hideScreen(current)
                destroyScreen(current)
                current.detach()
                focusScreen(previous)
            }
        })

        animator.start()
    }

    private fun navigateBackwardPopup(current: ScreenController, previous: ScreenController) {
        val popup = popup ?: return

        val currentView = current.onCreateView(context)

        current.detach()
        hideScreen(current)

        previous.attach(this)
        prepareScreen(previous)

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = TRANSITION_DURATION
        animator.interpolator = DecelerateInterpolator(1.78F)
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                popup.removeView(currentView)
                popup.hide()
                destroyScreen(current)
                previous.onFocus()
            }
        })
        animator.addUpdateListener { anim ->
            val factor = 1F - anim.animatedFraction
            popup.scrim.alpha = factor
            current.onTransitionFactor(factor)
        }

        animator.start()
    }

    private fun hidePopup(screen: ScreenController) {
        val popup = popup ?: return

        val popupView = screen.onCreateView(context)

        screen.detach()
        hideScreen(screen)

        val animator = ValueAnimator.ofFloat(0F, 1F)
        animator.duration = TRANSITION_DURATION
        animator.interpolator = DecelerateInterpolator(1.78F)
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                popup.hide()
                popup.removeView(popupView)
                destroyScreen(screen)
            }
        })
        animator.addUpdateListener { anim ->
            val factor = 1F - anim.animatedFraction
            popup.scrim.alpha = factor
            screen.onTransitionFactor(factor)
        }

        animator.start()
    }

    open fun onPrepareBackwardAnimationEffect(
        currentView: View,
        previous: View
    ) {
        currentView.alpha = 1F
        previous.alpha = 0F
    }

    open fun onApplyBackwardAnimationEffect(
        currentView: View,
        previousView: View,
        @FloatRange(from = 0.0, to = 1.0)
        factor: Float
    ) {
        currentView.alpha = 1F - factor
        previousView.alpha = factor
    }

    fun onBackActivated(): Boolean {
        if (_stack.currentOrNull?.onBackActivated() == true) return true
        if (stack.current > 0) {
            navigateBackward()
            return true
        }

        return false
    }

    fun onCallId(identifier: Int): Boolean = _stack.currentOrNull?.onCallId(identifier) == true

    fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ): Boolean = _stack.currentOrNull?.onActivityResult(requestCode, resultCode, data) == true

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean = _stack.currentOrNull?.onRequestPermissionsResult(
        requestCode,
        permissions,
        grantResults
    ) == true

    fun clear() {
        _stack.clear()
    }

    protected fun prepareScreen(screen: ScreenController) = screen.onPrepare()
    protected fun focusScreen(screen: ScreenController) = screen.onFocus()
    protected fun hideScreen(screen: ScreenController) = screen.onHide()
    protected fun destroyScreen(screen: ScreenController) = screen.onDestroy()

    private fun createScreenPrefix(index: Int): String = "$PREFIX_SCREEN${index}_"

    override fun toast(text: String, duration: Int) {
        Toast.makeText(context, text, duration).show()
    }

    override fun snack(text: String, duration: Int) = throw NotImplementedError()

    companion object {
        const val PREFIX_SCREEN_ITEM_COUNT = "nav_count_save"
        const val PREFIX_SCREEN = "nav_screen_prefix_item_"

        const val TRANSITION_DURATION = 300L
    }
}