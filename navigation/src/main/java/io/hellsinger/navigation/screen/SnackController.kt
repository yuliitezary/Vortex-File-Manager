package io.hellsinger.navigation.screen

interface SnackController {

    fun snack(text: String, duration: Int)

}