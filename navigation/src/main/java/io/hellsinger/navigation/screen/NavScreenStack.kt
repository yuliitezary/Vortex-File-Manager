package io.hellsinger.navigation.screen

import io.hellsinger.navigation.NavStack

interface NavScreenStack : NavStack<ScreenController> {

    override val size: Int

    override val current: Int

    override operator fun get(index: Int): ScreenController

}