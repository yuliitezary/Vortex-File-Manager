package io.hellsinger.navigation.screen

class BaseNavScreenStack : NavScreenStack {

    private val stack = ArrayList<ScreenController>(5)

    private var listener: (NavScreenStack.() -> Unit)? = null

    override val size: Int
        get() = stack.size

    override var current: Int = -1
        private set

    val currentScreen: ScreenController
        get() = stack[current]

    val previous: ScreenController
        get() = stack[current - 1]

    val next: ScreenController
        get() = stack[current + 1]

    val currentOrNull: ScreenController?
        get() = stack.getOrNull(this.current)

    override fun get(index: Int): ScreenController = stack[index]

    fun next() {
        this.current++
        listener?.invoke(this)
    }

    fun previous() {
        this.current--
        listener?.invoke(this)
    }

    fun bindListener(listener: (NavScreenStack.() -> Unit)?) {
        this.listener = listener
    }

    fun poll(): ScreenController {
        val screen = stack.removeAt(this.current--)
        listener?.invoke(this)
        return screen
    }

    fun insertAt(index: Int, screen: ScreenController) {
        if (index <= this.current) {
            stack.add(index, screen)
            this.current++
            listener?.invoke(this)
        }
    }

    fun push(screen: ScreenController) {
        stack.add(screen)
        next()
    }

    fun clear() {
        stack.forEach { screen ->
            screen.detach()
            screen.onDestroy()
        }
        stack.clear()
        this.current = -1
        listener?.invoke(this)
    }

}