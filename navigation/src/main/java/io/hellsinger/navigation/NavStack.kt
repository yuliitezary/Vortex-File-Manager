package io.hellsinger.navigation

interface NavStack<T> {

    val size: Int

    val current: Int

    operator fun get(index: Int): T

}